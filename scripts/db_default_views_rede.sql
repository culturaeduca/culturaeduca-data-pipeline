BEGIN;
-- agente coletivo
DROP VIEW IF EXISTS plataforma.view_rede_agentes_coletivos CASCADE;
CREATE OR REPLACE VIEW plataforma.view_rede_agentes_coletivos AS (
    SELECT ag.id::text AS pk,
      ag.nome::text AS nome,
      (
        CASE
          WHEN ac.tipo = 'CONSELHO' THEN 'Fórum / Conselho / Comitê'
          WHEN ac.tipo = 'INSTITUICAO' THEN 'Instituição'
          WHEN ac.tipo = 'MOVIMENTO_SOCIAL' THEN 'Movimento Social'
          ELSE 'Grupo / Coletivo'
        END
      )::text AS descricao,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geometry(Point, 4326)
        ELSE NULL
      END AS geom,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geography(Point, 4326)
        ELSE NULL
      END AS geog,
      upper(e.uf_sigla)::text AS uf_sigla,
      e.municipio_codigo::text AS municipio_codigo,
      e.municipio_nome::text AS municipio_nome
    FROM plataforma.agente_coletivo AS ac
      INNER JOIN plataforma.agente AS ag ON ag.id = ac.agente_id
      LEFT JOIN plataforma.endereco AS e ON e.id = ag.endereco_id
    WHERE ag.perfil_publico IS TRUE
  );
-- agente individual
DROP VIEW IF EXISTS plataforma.view_rede_agentes_individuais CASCADE;
CREATE OR REPLACE VIEW plataforma.view_rede_agentes_individuais AS (
    SELECT 'rede_agentes_individuais' AS uri,
      ag.id::text AS pk,
      ag.nome::text AS nome,
      'Pessoa Física'::text AS descricao,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geometry(Point, 4326)
        ELSE NULL
      END AS geom,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geography(Point, 4326)
        ELSE NULL
      END AS geog,
      upper(e.uf_sigla)::text AS uf_sigla,
      e.municipio_codigo::text AS municipio_codigo,
      e.municipio_nome::text AS municipio_nome
    FROM plataforma.agente_individual AS ai
      INNER JOIN plataforma.agente AS ag ON ag.id = ai.agente_id
      LEFT JOIN plataforma.endereco AS e ON e.id = ag.endereco_id
    WHERE ag.perfil_publico IS TRUE
  );
-- atividade
DROP VIEW IF EXISTS plataforma.view_rede_atividades CASCADE;
CREATE OR REPLACE VIEW plataforma.view_rede_atividades AS (
    SELECT 'rede_atividades' AS uri,
      a.id::text AS pk,
      a.titulo::text AS nome,
      (
        CONCAT(
          ag.nome,
          '\n',
          (
            to_char(
              datetime_inicio,
              'DD'
            ) || ' de ' || CASE
              to_char(
                datetime_inicio,
                'MM'
              )
              WHEN '01' THEN 'janeiro'
              WHEN '02' THEN 'fevereiro'
              WHEN '03' THEN 'março'
              WHEN '04' THEN 'abril'
              WHEN '05' THEN 'maio'
              WHEN '06' THEN 'junho'
              WHEN '07' THEN 'julho'
              WHEN '08' THEN 'agosto'
              WHEN '09' THEN 'setembro'
              WHEN '10' THEN 'outubro'
              WHEN '11' THEN 'novembro'
              WHEN '12' THEN 'dezembro'
            END || ' de ' || to_char(
              datetime_inicio,
              'YYYY - HH24:MI'
            )
          )
        )
      )::text AS descricao,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geometry(Point, 4326)
        ELSE NULL
      END AS geom,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geography(Point, 4326)
        ELSE NULL
      END AS geog,
      upper(e.uf_sigla)::text AS uf_sigla,
      e.municipio_codigo::text AS municipio_codigo,
      e.municipio_nome::text AS municipio_nome
    FROM plataforma.atividade AS a
      LEFT JOIN plataforma.agente AS ag ON ag.id = a.agente_id
      LEFT JOIN plataforma.endereco AS e ON e.id = a.endereco_id
    WHERE a.ativo IS TRUE
      AND ag.perfil_publico IS TRUE -- AND now() <= COALESCE(a.datetime_fim, a.datetime_inicio)
  );
-- projeto
DROP VIEW IF EXISTS plataforma.view_rede_projetos CASCADE;
CREATE OR REPLACE VIEW plataforma.view_rede_projetos AS (
    SELECT 'rede_projetos' AS uri,
      p.id::text AS pk,
      p.titulo::text AS nome,
      ag.nome::text AS descricao,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geometry(Point, 4326)
        ELSE NULL
      END AS geom,
      CASE
        WHEN e.latitude IS NOT NULL
        AND e.longitude IS NOT NULL THEN ST_Point(e.longitude, e.latitude, 4326)::geography(Point, 4326)
        ELSE NULL
      END AS geog,
      upper(e.uf_sigla)::text AS uf_sigla,
      e.municipio_codigo::text AS municipio_codigo,
      e.municipio_nome::text AS municipio_nome
    FROM plataforma.projeto AS p
      LEFT JOIN plataforma.agente AS ag ON ag.id = p.agente_id
      LEFT JOIN plataforma.endereco AS e ON e.id = p.endereco_id
    WHERE p.ativo IS TRUE
      AND ag.perfil_publico IS TRUE
  );
END;