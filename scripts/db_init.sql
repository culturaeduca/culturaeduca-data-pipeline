-- schemas
CREATE SCHEMA IF NOT EXISTS metadata;
CREATE SCHEMA IF NOT EXISTS datasets;
CREATE SCHEMA IF NOT EXISTS questions;
CREATE SCHEMA IF NOT EXISTS plataforma;
-- readonly group
CREATE ROLE readonly_group WITH NOLOGIN;
GRANT CONNECT ON DATABASE culturaeduca TO readonly_group;
GRANT USAGE ON SCHEMA public TO readonly_group;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly_group;
GRANT USAGE ON SCHEMA metadata TO readonly_group;
GRANT SELECT ON ALL TABLES IN SCHEMA metadata TO readonly_group;
GRANT USAGE ON SCHEMA datasets TO readonly_group;
GRANT SELECT ON ALL TABLES IN SCHEMA datasets TO readonly_group;
GRANT USAGE ON SCHEMA questions TO readonly_group;
GRANT SELECT ON ALL TABLES IN SCHEMA questions TO readonly_group;
GRANT USAGE ON SCHEMA plataforma TO readonly_group;
GRANT SELECT ON ALL TABLES IN SCHEMA plataforma TO readonly_group;
ALTER DEFAULT PRIVILEGES FOR ROLE culturaeduca
GRANT SELECT ON TABLES TO readonly_group;
-- extensions
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS unaccent;
-- text search config
CREATE TEXT SEARCH CONFIGURATION portuguese_unaccent (COPY = pg_catalog.portuguese);
ALTER TEXT SEARCH CONFIGURATION portuguese_unaccent ALTER MAPPING FOR hword,
hword_part,
word WITH unaccent,
portuguese_stem;
SET default_text_search_config = 'portuguese_unaccent';