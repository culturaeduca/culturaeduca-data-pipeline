# CulturaEduca

## Getting started

If you're on Windows, we highly recommend using [Windows Subsystem for Linux (WSL 2)](https://learn.microsoft.com/en-us/windows/wsl/install) and installing [Ubuntu](https://apps.microsoft.com/detail/9pdxgncfsczv) or other Linux distribution at your choosing. Remember to to use the git repository inside the Linux file system. We recommend using [Windows Terminal](https://apps.microsoft.com/detail/9n0dx20hk701) to more easily use the terminal.

## Technologies

### Typescript

We are using [Typescript](https://www.typescriptlang.org/) as our programming language for the project.

### Node.js

We are using [Node.js](https://nodejs.org/) as the runtime for our backend and for development/build processes on the frontend.

### Nest.js

We are using [Nest.js](https://docs.nestjs.com/) as the backend framework. Nest.js is a framework for building efficient, scalable Node.js server-side applications. It uses Express under the hood and provides a lot of features out of the box, such as a modular architecture, dependency injection, type safety and a powerful CLI for scaffolding and building applications. Nest.js allows us to build a REST API with a simple and intuitive API.

### Vue.js

We are using [Vue.js 3](https://v3.vuejs.org/) as the frontend framework. Vue.js is a progressive and flexible JavaScript framework used for building user interfaces and single-page applications. The [composition API](https://v3.vuejs.org/guide/composition-api-introduction.html) is a new way of writing Vue components and is used in the this project.

### Vuetify

We are using [Vuetify](https://vuetifyjs.com/en/getting-started/installation/) as the UI library. Vuetify is a Material Design component framework for Vue.js that allows us to easily create beautiful and responsive UIs. It provides a lot of pre-built components, such as buttons, cards, forms, tables, and more, and also allows us to customize the look and feel of our application with a simple and intuitive API.

### PostgreSQL with PostGIS

We are using [PostgreSQL](https://www.postgresql.org/docs/) with [PostGIS](https://postgis.net/docs/) as our database. PostGIS is an extension of PostgreSQL that allows us to store and query geospatial data. PostGIS provides us with the functionality to store and query geospatial data in a efficient and scalable way.

### Prisma

We are using [Prisma](https://www.prisma.io/docs) as our Object Relational Mapping (ORM) tool in the backend. Prisma is a powerful tool that simplifies database access and provides a simple and intuitive API for interacting with our database.Prisma allows us to define our database schema and generate a type-safe client that provides a simple and intuitive API for interacting with our database.

### Knex.js

We are using [Knex.js](https://knexjs.org/guide) to dynamically create tables and generate queries for the datasets and attributes that the users create in the data-lake structure of the project. Knex.js is a SQL query builder for Node.js that allows us to create tables and generate queries programmatically. It uses a fluent API to build queries and supports a wide range of databases.

### MongoDB

We are using [MongoDB](https://docs.mongodb.com/) as our NoSQL document-oriented database. MongoDB is a popular choice for storing large amounts of semi-structured data. We use it to store the contents of files we parse in the data ingestion process, for example, the contents of a CSV file, or the contents of a GeoJSON parsed from a Shapefile.

### Geoserver

We are using [Geoserver](https://docs.geoserver.org/) as our web map server. Geoserver is an open source server that provides a lot of features for publishing and editing geospatial data. It is a key component of our application as it allows us to efficiently render maps with a lot of geospatial data. Geoserver provides a [Web Map Service (WMS)](https://docs.geoserver.org/latest/en/user/services/wms/index.html) that allows us to request maps from the server and render them in the browser. This is important as it allows us to optimize the rendering of the map by only requesting the data that is needed for the current view.

### MinIO

We are using [MinIO](https://docs.min.io/docs/minio-quickstart-guide.html) as our object storage. MinIO is a cloud native object storage server that is compatible with Amazon S3. It is a key component of our application as it allows us to store and serve large amounts of data in a efficient and scalable way, while self-hosting it.

### Docker

We are using [Docker](https://docs.docker.com/) as our container runtime for both development and production environments. Docker allows us to package our application and its dependencies into containers that can be run on any machine that supports Docker. We use [Docker Compose](https://docs.docker.com/compose/) to define and run multi-container Docker applications.

## Docker installation

You must install [Docker](https://www.docker.com/) to use all other technology dependencies of the project. You can install the GUI interface [Docker Desktop](https://www.docker.com/products/docker-desktop/) or run this commands to install via cli:

```bash
# 1. download the script
curl -fsSL https://get.docker.com -o install-docker.sh
# 2. (optional) verify the script's content
cat install-docker.sh
# 3. (optional) run the script with --dry-run to verify the steps it executes
sh install-docker.sh --dry-run
# 4. run the script either as root, or using sudo to perform the installation.
sudo sh install-docker.sh
```

## Docker Containers

In the `docker-compose.yml` file we have the description of which images we want to use and how to create the containers. Here is a little overview:

### [NodeJS](https://nodejs.org/en)

Javascript runtime for the Plataforma and Data Pipeline aplications.

- Image: [node/20.13.1-bookworm](https://hub.docker.com/layers/library/node/20.13.1-bookworm/images/sha256-a6a218ea2c972b92a98bad6030c253c3448f19e035cf88b2e543cab2922f5582)
- Containers: culturaeduca_data_pipeline_api, culturaeduca_data_pipeline_app, culturaeduca_plataforma_api, culturaeduca_plataforma_app

### [PostgreSQL + PostGIS](https://postgis.net/)

Main database

- Image: [postgis/postgis:16-3.4-alpine](https://hub.docker.com/r/postgis/postgis)
- Container Name: culturaeduca_db

### [Mongo DB](https://www.mongodb.com/)

NoSQL database to help with data ingestion

- Image: [mongo:7.0](https://hub.docker.com/_/mongo)
- Container Name: culturaeduca_mongo

### [GeoServer](https://geoserver.org/)

Server for sharing geospatial data

- Image: [kartoza/geoserver:2.24.2](https://hub.docker.com/r/kartoza/geoserver)
- Container Name: culturaeduca_geoserver

### [MinIO](https://min.io/)

High performance object storage

- Image: [quay.io/minio/minio:RELEASE.2024-04-18T19-09-19Z](https://quay.io/repository/minio/minio)
- Container Name: culturaeduca_minio

### [Redis](https://redis.io/)

In-memory database to store our sessions

- Image: [redis:7.2-alpine](https://hub.docker.com/_/redis)
- Container Name: culturaeduca_redis

## Setup & Run

As detailed in the next sections, on each folder, we will:

- Configure the environmental variables file duplicating the `default.env`, renaming it as `.env`, and updating the necessary values
- Install dependecies
- Run scripts
- Run development version

### Project Root

Configure the environmental variables:

```bash
cp default.env .env
# then modify it in your text editor of choosing
```

Run Docker Compose:

```bash
# create and start containers (by default it looks for the .env file)
docker compose up minio mongo db geoserver -d
# check if all services are running properly:
docker compose logs -f minio mongo db geoserver
```

In the first time, we need to run `scripts/db_init.sql` on the PostgreSQL database:

```bash
docker exec -i culturaeduca_db psql -U culturaeduca -d culturaeduca < scripts/db_init.sql
# remember to change it to your postgres username if you made changes in the root .env file.
```

### Data Pipeline

#### Backend

```bash
cd data_pipeline/back
```

Configure the environmental variables:

```bash
cp default.env .env
# some default values are placed, you can modify them in your text editor of choosing
```

Update .env with MinIO bucket info:

- With the project running, go to the localhost:9090 (or other port if you changed the .env)
- Create a new bucket with "versioning on"
- Input bucket's name and keys on the .env file

Create the backend service:

```bash
docker compose up data_pipeline_api -d
# By default, the API will run at http://localhost:3001.
# You can update the root .env file based on your needs.
```

You can check the service logs by running:

```bash
docker compose logs -f data_pipeline_api
```

#### Frontend

```bash
cd data_pipeline/front
```

Configure the environmental variables:

```bash
cp default.env .env
# then modify it in your text editor of choosing
```

Create the frontend service:

```bash
docker compose up data_pipeline_app -d
# By default, the App will run at http://localhost:8081.
# You can update the .env file based on your needs.
```

You can check the service logs by running:

```bash
docker compose logs -f data_pipeline_app
```

After creating the Data Pipeline services, check if you can log in using the following credentials:

- URL: http://localhost:8081
- user: admin
- password: 123admin

### Plataforma

#### Backend

```bash
cd plataforma/back
```

Configure the environmental variables:

```bash
cp default.env .env
# some default values are placed, you can modify them in your text editor of choosing
```

Create the backend service:

```bash
docker compose up plataforma_api -d
# By default, the API will run at http://localhost:3000.
# You can update the root .env file based on your needs.
```

You can check the service logs by running:

```bash
docker compose logs -f plataforma_api
```

After the `plataforma_api` service is up, we need to create some views on the database. Run:

```bash
docker exec -i culturaeduca_db psql -U culturaeduca -d culturaeduca < scripts/db_default_views_rede.sql
# remember to change it to your postgres username if you made changes in the root .env file.
```

Validate a user

```bash
curl --request POST \
  --url http://localhost:3000/auth/sign_in \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
	"email": "admin@mail.com",
	"senha": "123admin"
}'
```

```bash
{"id":25,"nome":"admin","email":"admin@mail.com","validacaoEmail":"2024-09-18T20:34:37.161Z","trocaSenha":null,"tokenValidacaoEmail":"739420e2df057aee6505b2720a81c597",...
```

Now you have the "tokenValidacaoEmail", and can make a requset to the validate endpoint

```bash
curl --request POST \
  --url http://localhost:3000/auth/verify_token/739420e2df057aee6505b2720a81c597 \
  --header 'User-Agent: insomnia/8.6.1'
```

#### Frontend

```bash
cd plataforma/front
```

Configure the environmental variables:

```bash
cp default.env .env
# then modify it in your text editor of choosing
```

Create the frontend service:

```bash
docker compose up plataforma_app -d
# By default, the App will run at http://localhost:8084.
# You can update the .env file based on your needs.
```

You can check the service logs by running:

```bash
docker compose logs -f plataforma_app
```

After creating the Plataforma services, check if you can log in using the following credentials:

- URL: http://localhost:8084
- user: admin@mail.com
- password: 123admin

## Next steps

Now that you have the Plataforma and Data Pipeline services up and running in your local environment, check out this video explaining how to configure the project's ETL.
