# Deploy Instructions

## Table of Contents

1. [VM Setup](#vm-setup)
2. [Project Setup](#project-setup)
3. [Build Backend Images](#build-backend-images)
4. [Build Frontend Static Files](#build-frontend-static-files)
5. [First Deploy](#first-deploy)
6. [SSL Certificate for DB](#ssl-certificate-for-db)
7. [Minio Configuration](#minio-configuration)
8. [Geoserver Setup](#geoserver-setup)
9. [Updates](#updates)
10. [Future Improvements](#future-improvements)

## VM Setup

1. **Create VM Instance:**

   - Create a new VM instance in your cloud provider using Ubuntu 24.04 as the base image.

2. **Configure Port Forwarding and Firewall:**

   - Set up security rules to allow external traffic on specific ports:
     - **SSH (Port 22):** To connect via SSH.
     - **HTTP (Port 80):** For web traffic (non-encrypted).
     - **HTTPS (Port 443):** For secure web traffic (SSL).
     - **PostgreSQL (Port 5432):** For database access.
   - Ensure all IP addresses (0.0.0.0/0) can access these ports unless restrictions are needed.

3. **Get RSA Public Key (if not already created):**

   - Use `ssh-keygen -t rsa -b 4096 -C "youremail@example.com"` to create a new key pair if you don't have one.
   - Append the public key (`~/.ssh/id_rsa.pub`) to the `/home/ubuntu/.ssh/authorized_keys` file on the VM.

4. **Login via SSH:**

   - SSH into the server:

     ```bash
     ssh -i /path/to/your/private/key ubuntu@<VM-IP>
     ```

5. **Configure SSH Host (optional):**

   - Open or create the `~/.ssh/config` file on your local machine.
   - Add the following configuration for your server:

     ```
     Host culturaeduca
      HostName <VM-IP>
      Port <SSH-PORT>
      User ubuntu
      IdentityFile ~/.ssh/id_rsa
      IdentitiesOnly yes
     ```

     Replace `<VM-IP>` with the actual IP address of your VM, `<SSH-PORT>` with the port you configured for SSH (normally 22) and `~/.ssh/id_rsa` with the path to your private key file if it's different.

   - Save the file and exit the editor.

   - You can now SSH into the server using the configured alias (in this case, `culturaeduca`):
     ```bash
     ssh culturaeduca
     ```

This allows you to connect to your server using the hostname alias without needing to specify the IP address and key file every time.

5. **Update and Upgrade Packages:**

   - Run the following commands to ensure the system is up to date:
     ```bash
     sudo apt update && sudo apt upgrade -y
     ```

6. **Install Docker:**

   - Install Docker using the `get.docker.com` script:
     ```bash
     curl -fsSL https://get.docker.com -o get-docker.sh
     sh get-docker.sh
     ```
   - Alternatively, install via Snap:
     ```bash
     sudo snap install docker
     ```

7. **Install NGINX:**

   - Install NGINX using the package manager:
     ```bash
     sudo apt install nginx
     ```

8. **Install Certbot for SSL:**
   - Use Snap to install Certbot (for SSL certificate generation):
     ```bash
     sudo snap install certbot --classic
     ```

## Project Setup

1. **Create Project Folder:**

   - Create a folder on the server to host the project files:
     ```bash
     mkdir ~/culturaeduca
     ```

2. **Copy Production Docker Compose File:**

   - Copy `docs/docker-compose-production.yml` to the `culturaeduca` directory as `docker-compose.yml`.

3. **Copy Production .env File:**

   - Copy `docs/production.env` to the `culturaeduca` directory as `.env`.

   - Populate it with the necessary environment variables (usernames, passwords, API keys). You can generate secure passwords using [random.org](https://www.random.org/passwords/?num=16&len=32&format=html&rnd=new).

4. **Copy Backend Env Files:**

   - Create a config folder on the server: `~/culturaeduca/config`

   - Copy `data_pipeline/back/default.env` to the config directory as `data_pipeline_api.env`

   - Copy `plataforma/back/default.env` to the config directory as `plataforma_api.env`

   - Populate these env files with the required environment variables.

5. **Copy Project Scripts:**
   - Copy the sql scripts from the `scripts` folder into `~/culturaeduca/scripts/db` on the server.

## Build Backend Images

### data_pipeline back

1. **Modify Build Script:**

   - Duplicate the `data_pipeline/back/default-build-push` script and modify it with production-specific values.
   - Save it as `build-push.sh`.

2. **Build and Push Image:**
   - Run the new script to build and push the Docker image to your Docker registry:
     ```bash
     ./build-push.sh
     ```

### plataforma back

1. **Modify Build Script:**

   - Duplicate the `plataforma/back/default-build-push` script and set production values.
   - Save it as `build-push.sh`.

2. **Build and Push Image:**
   - Run the script to build and push the Docker image:
     ```bash
     ./build-push.sh
     ```

## Build Frontend Static Files

### data_pipeline front

1. **Set Production Environment Variables:**

   - Duplicate the `data_pipeline/front/default.env` file, update with production values, and save as `.env.production`.

2. **Modify Build Script:**

   - Duplicate the `data_pipeline/front/default-build-rsync` script, set production values, and save it as `build-rsync.sh`.

3. **Run the Rsync Script:**
   - Run the rsync script to deploy the frontend files:
     ```bash
     ./build-rsync.sh
     ```

### plataforma front

1. **Set Production Environment Variables:**

   - Duplicate the `plataforma/front/default.env` file, update it with production values, and save as `.env.production`.

2. **Modify Build Script:**

   - Duplicate the `default-build-rsync` script, set production values, and save it as `build-rsync.sh`.

3. **Run the Rsync Script:**
   - Run the rsync script to deploy the frontend files:
     ```bash
     ./build-rsync.sh
     ```

## First Deploy

1. **Using tmux for Persistent Docker Services:**

   - **Start a new tmux session:**

   ```bash
   tmux new -s culturaeduca
   ```

   - **Reattach to the session later:**

   ```bash
   tmux attach-session -t culturaeduca
   ```

   - **List all existing tmux sessions:**

   ```bash
   tmux list-sessions
   ```

   - **In tmux:**
     - Navigate to the project folder and start Docker Compose:
       ```bash
       cd ~/culturaeduca && docker compose up
       ```
   - **Detach from tmux without stopping the process:**
     - Press `Ctrl + b`, then `d`.

- **Note:** Running `docker compose up -d` without tmux will cause the services to stop when you exit the SSH session. Using tmux ensures the Docker services continue running after you disconnect and will persist when you log back in.

2. **Restart Docker Containers:**

   - After running on tmux, you can restart the docker services with Docker Compose on the project folder:
     ```bash
     docker-compose up -d
     ```

3. **Initialize Database:**

   - Run the `db_init.sql` file on the PostgreSQL database to initialize it.

     ```bash
     docker exec -i culturaeduca_db psql -U culturaeduca -d culturaeduca < db_init.sql
     ```

4. **Run API Migrations:**

   - Execute migrations on both API containers.

     ```bash
     docker exec -i culturaeduca_data_pipeline_api npx prisma migrate deploy

     docker exec -i culturaeduca_plataforma_api npx prisma migrate deploy
     ```

5. **Run Default SQL Views:**

   - Execute `db_default_views_rede.sql` on the PostgreSQL database.

     ```bash
     docker exec -i culturaeduca_db psql -U culturaeduca -d culturaeduca < db_default_views_rede.sql
     ```

6. **Configure NGINX:**

   - Copy the NGINX configuration files to the `/etc/nginx/sites-available/` folder.
   - Rename the "-nginx.conf" part of the file to the correct domain name (example: `plataforma-nginx.conf`-> `plataforma.culturaeduca.cc`)
   - Create a symbolic link to each new configuration file in the `/etc/nginx/sites-enabled/` folder. Example:

     ```bash
     sudo ln -s /etc/nginx/sites-available/plataforma.culturaeduca.cc /etc/nginx/sites-enabled/
     ```

7. **Generate SSL Certificates:**
   - Use Certbot to generate SSL certificates for your domain:
     ```bash
     sudo certbot --nginx
     ```

## SSL Certificate for DB

(TODO: Improve these instructions)

While the SSL certificate is not generated, set secure as false or ssl as disabled for DB connections

1. **Generate SSL Certificate for PostgreSQL:**

   - Run Certbot to generate an SSL certificate for the database.

2. **Configure DB SSL:**

   - Create a `config/db` folder in your project and place the SSL certificate and configuration files inside.

3. **Set Correct Permissions:**

   - Set appropriate permissions for the certificate directory:
     ```bash
     sudo chown -R <USER-NUMBER>:ubuntu config/db
     ```

4. **Update Docker Compose for DB:**
   - Configure the `db` service in the `docker-compose.yml` to use the SSL certificates and mount the volumes (just uncomment the lines).

## Minio

1. **Access Minio:**

   - Open the Minio web interface to create the necessary bucket and access keys.

2. **Update .env File:**
   - Add the Minio access key and secret to the `data_pipeline_api.env` file.

## Geoserver

1. **Set up Geoserver:**

   - Access the Geoserver admin interface and create a new workspace.

2. **Create Database Stores:**

   - For each PostgreSQL schema, create a corresponding data store in Geoserver.

3. **Configure styles:**

   - Create necessary styles from SLD files.

4. **Publish layers:**
   - Publish layers from datasets, questions and plataforma "rede views"

## Updates

1. **Versioning:**

   - Update the version number in `package.json` files as needed. For example:
     ```json
     "version": "0.4.2"
     ```
     Update to:
     ```json
     "version": "0.5.0"
     ```
     This version will be used to differentiate the images built.

2. **Rebuild Backend Images:**

   - Rebuild and push new backend Docker images (use `build-docker.sh`).

3. **Update Containers:**

   - Pull the latest Docker images and restart the services.

     ```bash
     docker pull bcdcode/data_pipeline_api:latest

     docker pull bcdcode/plataforma_api:latest

     docker compose up -d
     ```

4. **Run Migrations (if applicable):**

   - If database migrations are needed, run them after restarting the API services.

     ```bash
     docker exec -i culturaeduca_data_pipeline_api npx prisma migrate deploy

     docker exec -i culturaeduca_plataforma_api npx prisma migrate deploy
     ```

5. **Rebuild Frontend:**
   - Rebuild the frontend distributions and sync them to the server via rsync (use `build-rsync.sh`).

## Future Improvements

- **Automate Deployment:**

  - Consider using CI/CD tools to automate the build and deploy processes.

- **Backup Strategy:**

  - Regularly backup the databases and configuration files to prevent data loss.

- **Monitoring and Logging:**

  - Implement monitoring tools and centralized logging to keep track of application health and performance.

- **Security Best Practices:**
  - Regularly update your server and dependencies.
  - Use strong, unique passwords and rotate them periodically.
  - Limit SSH access using security groups and consider using tools like Fail2Ban.
