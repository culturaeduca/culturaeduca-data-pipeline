import {
  IsBoolean,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { CategoriaOpcao } from '@prisma/client';

export class CreateOpcaoDto {
  @IsEnum(CategoriaOpcao)
  categoria: CategoriaOpcao;

  @IsNotEmpty()
  @IsString()
  nome: string;

  @IsOptional()
  @IsString()
  descricao: string | null;

  @IsOptional()
  @IsInt()
  @Min(0)
  ordem: number | null;

  @IsOptional()
  @IsBoolean()
  outro: boolean | null;
}
