import { IsEnum, IsOptional } from 'class-validator';
import { CategoriaOpcao } from '@prisma/client';
import { PaginationQueryDto } from '../../pagination-query.dto';

export class FindAllQueryDto extends PaginationQueryDto {
  @IsOptional()
  @IsEnum(CategoriaOpcao)
  categoria?: CategoriaOpcao | null;
}
