import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOpcaoDto, FindAllQueryDto, UpdateOpcaoDto } from './dto';
import { PrismaService } from '../prisma.service';

@Injectable()
export class OpcoesService {
  constructor(private readonly prisma: PrismaService) {}

  async bulkCreate(dto: CreateOpcaoDto[]) {
    const dataOpcoes = dto.map((el) => ({
      categoria: el.categoria,
      nome: el.nome?.trim(),
      descricao: el.descricao?.trim(),
      ...(!Number.isNaN(Number(el.ordem)) ? { ordem: el.ordem } : {}),
      outro: el.outro === true,
    }));
    return this.prisma.opcao.createMany({
      data: dataOpcoes,
    });
  }

  async create(dto: CreateOpcaoDto) {
    const dataOpcao = {
      categoria: dto.categoria,
      nome: dto.nome?.trim(),
      descricao: dto.descricao?.trim(),
      ...(!Number.isNaN(Number(dto.ordem)) ? { ordem: dto.ordem } : {}),
      outro: dto.outro === true,
    };
    return this.prisma.opcao.create({ data: dataOpcao });
  }

  async findAll(queryDto: FindAllQueryDto) {
    const { page, perPage, search, categoria } = queryDto;
    // take (limit) and skip (offset)
    let skip, take: number;
    if (perPage > 0 && page > 0) {
      take = perPage;
      skip = (page - 1) * perPage;
    }
    // where
    const where = {};
    if (categoria) {
      Object.assign(where, {
        categoria: {
          equals: categoria,
        },
      });
    }
    const cleanSearch = search?.trim();
    if (cleanSearch) {
      Object.assign(where, {
        nome: {
          contains: cleanSearch,
          mode: 'insensitive',
        },
      });
    }
    // perform queries
    const [count, results] = await Promise.all([
      this.prisma.opcao.count({ where }),
      this.prisma.opcao.findMany({
        where,
        orderBy: [{ ordem: 'asc' }, { nome: 'asc' }],
        ...(take ? { take } : {}),
        ...(skip ? { skip } : {}),
      }),
    ]);
    return {
      total: count,
      results,
    };
  }

  async findOne(id: number) {
    const opcao = await this.prisma.opcao.findUnique({ where: { id } });
    if (!opcao) {
      throw new NotFoundException(`Opção ${id} não encontrada`);
    }
    return opcao;
  }

  async update(id: number, dto: UpdateOpcaoDto) {
    const opcao = await this.prisma.opcao.findUnique({ where: { id } });
    if (!opcao) {
      throw new NotFoundException(`Opção ${id} não encontrada`);
    }
    const dataOpcao = {
      ...(dto.nome ? { nome: dto.nome?.trim() } : {}),
      ...(!isNaN(dto.ordem) ? { ordem: dto.ordem } : {}),
    };
    return this.prisma.opcao.update({
      where: { id: opcao.id },
      data: dataOpcao,
    });
  }

  async remove(id: number) {
    const opcao = await this.prisma.opcao.findUnique({ where: { id } });
    if (!opcao) {
      throw new NotFoundException(`Opção ${id} não encontrada`);
    }
    await this.prisma.opcao.delete({ where: { id: opcao.id } });
    return opcao;
  }
}
