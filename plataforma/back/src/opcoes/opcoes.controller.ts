import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { OpcoesService } from './opcoes.service';
import { CreateOpcaoDto } from './dto/create-opcao.dto';
import { UpdateOpcaoDto } from './dto/update-opcao.dto';
import { FindAllQueryDto } from './dto';
import { Public } from '../public.decorator';

@Controller('opcoes')
export class OpcoesController {
  constructor(private readonly opcoesService: OpcoesService) {}

  // POST

  @Post('bulk')
  bulkCreate(@Body() dto: CreateOpcaoDto[]) {
    return this.opcoesService.bulkCreate(dto);
  }

  @Post('')
  create(@Body() dto: CreateOpcaoDto) {
    return this.opcoesService.create(dto);
  }

  // GET

  @Public()
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.opcoesService.findOne(id);
  }

  @Public()
  @Get('')
  findAll(@Query() queryDto: FindAllQueryDto) {
    return this.opcoesService.findAll(queryDto);
  }

  // PATCH

  @Patch(':id')
  update(@Param('id', ParseIntPipe) id: number, @Body() dto: UpdateOpcaoDto) {
    return this.opcoesService.update(id, dto);
  }

  // DELETE

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.opcoesService.remove(id);
  }
}
