import { Module } from '@nestjs/common';
import { OpcoesService } from './opcoes.service';
import { OpcoesController } from './opcoes.controller';
import { PrismaService } from '../prisma.service';

@Module({
  controllers: [OpcoesController],
  providers: [OpcoesService, PrismaService],
})
export class OpcoesModule {}
