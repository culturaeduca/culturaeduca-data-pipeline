export enum CategoriasOpcoes {
  ABRANGENCIA = 'ABRANGENCIA',
  AREA_ATUACAO = 'AREA_ATUACAO',
  AREA_ESTUDO_PESQUISA = 'AREA_ESTUDO_PESQUISA',
  IDENTIDADE_ETNICO_CULTURAL = 'IDENTIDADE_ETNICO_CULTURAL',
  IDENTIDADE_GENERO = 'IDENTIDADE_GENERO',
  NIVEL_ESCOLARIDADE = 'NIVEL_ESCOLARIDADE',
  OCUPACAO = 'OCUPACAO',
  PUBLICO_FOCAL = 'PUBLICO_FOCAL',
  RACA_COR_IBGE = 'RACA_COR_IBGE',
  TIPO_ESPACO = 'TIPO_ESPACO',
}

export const modelCategoriaOpcao = {
  [CategoriasOpcoes.ABRANGENCIA]: 'opcaoAbrangencia',
  [CategoriasOpcoes.AREA_ATUACAO]: 'opcaoAreaAtuacao',
  [CategoriasOpcoes.AREA_ESTUDO_PESQUISA]: 'opcaoAreaEstudoPesquisa',
  [CategoriasOpcoes.IDENTIDADE_ETNICO_CULTURAL]:
    'opcaoIdentidadeEtnicoCultural',
  [CategoriasOpcoes.IDENTIDADE_GENERO]: 'opcaoIdentidadeGenero',
  [CategoriasOpcoes.NIVEL_ESCOLARIDADE]: 'opcaoNivelEscolaridade',
  [CategoriasOpcoes.OCUPACAO]: 'opcaoOcupacao',
  [CategoriasOpcoes.PUBLICO_FOCAL]: 'opcaoPublicoFocal',
  [CategoriasOpcoes.RACA_COR_IBGE]: 'opcaoRacaCorIbge',
  [CategoriasOpcoes.TIPO_ESPACO]: 'opcaoTipoEspaco',
};
