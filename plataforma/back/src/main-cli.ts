import { AppModule } from './app.module';
import { program } from 'commander';
import { NestFactory } from '@nestjs/core';
import { StorageService } from './storage/storage.service';
import { AuthService } from './auth/auth.service';

async function bootstrap() {
  program
    .command('create-admin <nome> <email> <senha>')
    .description('Create user to config base')
    .action(async (nome: string, email: string, senha: string) => {
      const app = await NestFactory.createApplicationContext(AppModule);
      const authService = app.get(AuthService);

      const newUser = await authService.createAdmin({ nome, email, senha });
      console.log(newUser);
      await app.close();
    });

  program
    .command('list-admins')
    .description('List admins')
    .action(async () => {
      const app = await NestFactory.createApplicationContext(AppModule);
      const authService = app.get(AuthService);

      const newUser = await authService.listAdmins();
      console.log(newUser);
      await app.close();
    });

  program
    .command('delete-admin <id>')
    .description('Delete a admin')
    .action(async (id: string) => {
      const app = await NestFactory.createApplicationContext(AppModule);
      const authService = app.get(AuthService);
      // const parsedId = parseInt(id, 10);
      const newUser = await authService.deleteAdmin(id);
      console.log(newUser);
      await app.close();
    });

  program
    .command('create-bucket')
    .description('Create a new bucket')
    .option('-n, --name <string>', 'Name')
    .action(async (args) => {
      const { name } = args;
      if (!name) {
        console.log('No bucket name was provide');
        process.exit(1);
      }

      const app = await NestFactory.createApplicationContext(AppModule, {
        logger: false,
      });

      const listBuckets = await app
        .get(StorageService)
        .createBucketIfNotExists(name);
      console.log(listBuckets);
      await app.close();
      process.exit(0);
    });

  program
    .command('validate-user')
    .description('Validate a user')
    .option('-e, --email <string>', 'Email')
    .action(async (args) => {
      const { email } = args;
      if (!email) {
        console.log('No email was provide');
        process.exit(1);
      }
      console.log(email);

      const app = await NestFactory.createApplicationContext(AppModule, {
        logger: false,
      });

      const tokenValidacaoEmail = await app
        .get(AuthService)
        .getUserTokenValidacaoByEmail({ email: email });

      console.log('TOKEN');
      console.log(tokenValidacaoEmail);

      if (!tokenValidacaoEmail) {
        console.log('User not found');
        process.exit(1);
      }
      const validate = await app
        .get(AuthService)
        .verifyToken(tokenValidacaoEmail.tokenValidacaoEmail);

      console.log(validate);

      await app.close();
      process.exit(0);
    });

  // parse
  try {
    program.parse(process.argv);
  } catch (err) {
    process.exit(1);
  }
}

try {
  bootstrap();
} catch (err) {
  console.error(err);
  process.exit(1);
}
