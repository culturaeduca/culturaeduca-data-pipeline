import { Module } from '@nestjs/common';
import { AgentesColetivosService } from './agentes-coletivos.service';
import { AgentesColetivosController } from './agentes-coletivos.controller';
import { PrismaService } from '../prisma.service';
import { AtividadesModule } from '../atividades/atividades.module';
import { ProjetosModule } from '../projetos/projetos.module';

@Module({
  imports: [AtividadesModule, ProjetosModule],
  controllers: [AgentesColetivosController],
  providers: [AgentesColetivosService, PrismaService],
})
export class AgentesColetivosModule {}
