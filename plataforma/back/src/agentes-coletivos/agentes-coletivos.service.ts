import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAgenteColetivoDto } from './dto';
import { PrismaService } from '../prisma.service';
import { TipoAgente, TipoAgenteColetivoMembro } from '@prisma/client';
import { AtividadesService } from '../atividades/atividades.service';
import { ProjetosService } from '../projetos/projetos.service';
import { CreateAtividadeDto } from '../atividades/dto';
import { CreateProjetoDto } from '../projetos/dto';

@Injectable()
export class AgentesColetivosService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly atividadesService: AtividadesService,
    private readonly projetosService: ProjetosService,
  ) {}

  create(dto: CreateAgenteColetivoDto, usuarioId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // endereco
      const dataEndereco = {
        ...dto.agente.endereco,
      };
      const endereco = await trxPrisma.endereco.create({
        data: dataEndereco,
      });
      // agente
      const dataAgente = {
        enderecoId: endereco.id,
        tipo: TipoAgente.COLETIVO,
        perfilPublico: true,
        nome: dto.agente.nome?.trim() || null,
        email: dto.agente.email?.trim()?.toLowerCase() || null,
        website: dto.agente.website?.trim() || null,
        telefone1: dto.agente.telefone1?.trim() || null,
        telefone2: dto.agente.telefone2?.trim() || null,
        abrangenciaId: dto.agente.abrangenciaId || null,
        objetivo: dto.agente.objetivo?.trim() || null,
        atuacao: dto.agente.atuacao?.trim() || null,
        atuaDesde: dto.agente.atuaDesde,
        areasAtuacaoOutros: dto.agente.areasAtuacaoOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
        areasEstudoPesquisaOutros: dto.agente.areasEstudoPesquisaOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
        publicosFocaisOutros: dto.agente.publicosFocaisOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
        tiposEspacosOutros: dto.agente.tiposEspacosOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
      };
      const agente = await trxPrisma.agente.create({
        data: dataAgente,
      });
      // opcoes
      const dataAgenteOpcoes = [
        ...dto.agente.areasAtuacaoIds,
        ...dto.agente.areasEstudoPesquisaIds,
        ...dto.agente.publicosFocaisIds,
        ...dto.agente.tiposEspacosIds,
      ].map((opcaoId) => ({ agenteId: agente.id, opcaoId }));
      await trxPrisma.agenteOpcao.createMany({
        data: dataAgenteOpcoes,
      });
      // agente coletivo
      const dataAgenteColetivo = {
        agenteId: agente.id,
        tipo: dto.tipo,
        createdBy: usuarioId,
        updatedBy: usuarioId,
      };
      const agenteColetivo = await trxPrisma.agenteColetivo.create({
        data: dataAgenteColetivo,
      });
      // adiciona criador como administrador do agente coletivo
      const dataMembro = {
        agenteColetivoId: agenteColetivo.id,
        usuarioId: usuarioId,
        tipo: TipoAgenteColetivoMembro.ADMINISTRADOR,
      };
      await trxPrisma.agenteColetivoMembro.create({
        data: dataMembro,
      });
      // retorna agente completo
      return trxPrisma.agenteColetivo.findUnique({
        where: { id: agenteColetivo.id },
        include: {
          agente: {
            include: {
              endereco: true,
              abrangencia: true,
              opcoes: { include: { opcao: true } },
            },
          },
        },
      });
    });
  }

  async createAtividade(
    id: number,
    dto: CreateAtividadeDto,
    usuarioId: number,
  ) {
    const agenteColetivo = await this.prisma.agenteColetivo.findUnique({
      where: { id },
    });
    if (!agenteColetivo) {
      throw new NotFoundException('Agente coletivo não encontrado');
    }
    return this.atividadesService.create(
      dto,
      agenteColetivo.agenteId,
      usuarioId,
    );
  }

  async createProjeto(id: number, dto: CreateProjetoDto, usuarioId: number) {
    const agenteColetivo = await this.prisma.agenteColetivo.findUnique({
      where: { id },
    });
    if (!agenteColetivo) {
      throw new NotFoundException('Agente coletivo não encontrado');
    }
    return this.projetosService.create(dto, agenteColetivo.agenteId, usuarioId);
  }

  findAll() {
    return this.prisma.agenteColetivo.findMany();
  }

  async findAtividades(id: number) {
    const agenteColetivo = await this.prisma.agenteColetivo.findUnique({
      where: { id },
    });
    if (!agenteColetivo) {
      throw new NotFoundException('Agente coletivo não encontrado');
    }
    return this.prisma.atividade.findMany({
      where: {
        agenteId: agenteColetivo.agenteId,
      },
      include: {
        endereco: true,
        projeto: true,
        opcoes: { include: { opcao: true } },
      },
    });
  }

  async findEndereco(id: number) {
    const agenteColetivo = await this.prisma.agenteColetivo.findUnique({
      where: { id },
      include: {
        agente: true,
      },
    });
    if (!agenteColetivo) {
      throw new Error('Agente coletivo não encontrado');
    }
    return this.prisma.endereco.findUnique({
      where: { id: agenteColetivo.agente.enderecoId },
    });
  }

  async findOne(id: number) {
    const agenteColetivo = await this.prisma.agenteColetivo.findUnique({
      where: { id },
      include: {
        agente: {
          include: {
            endereco: true,
            abrangencia: true,
            opcoes: { include: { opcao: true } },
          },
        },
      },
    });
    if (!agenteColetivo) {
      throw new Error('Agente coletivo não encontrado');
    }
    return agenteColetivo;
  }

  async findProjetos(id: number) {
    const agenteColetivo = await this.prisma.agenteColetivo.findUnique({
      where: { id },
    });
    if (!agenteColetivo) {
      throw new NotFoundException('Agente coletivo não encontrado');
    }
    return this.prisma.projeto.findMany({
      where: {
        agenteId: agenteColetivo.agenteId,
      },
      include: {
        endereco: true,
        atividades: {
          include: { endereco: true, opcoes: { include: { opcao: true } } },
        },
        opcoes: { include: { opcao: true } },
      },
    });
  }

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const agenteColetivo = await trxPrisma.agenteColetivo.findUnique({
        where: { id },
      });
      if (!agenteColetivo) {
        throw new NotFoundException('Agente coletivo não encontrado');
      }
      await trxPrisma.agenteColetivo.delete({
        where: { id: agenteColetivo.id },
      });
      return agenteColetivo;
    });
  }
}
