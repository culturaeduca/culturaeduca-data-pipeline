import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { AgentesColetivosService } from './agentes-coletivos.service';
import { CreateAgenteColetivoDto } from './dto';
import { User } from '../user.decorator';
import { CreateAtividadeDto } from '../atividades/dto';
import { CreateProjetoDto } from '../projetos/dto';

@Controller('agentes_coletivos')
export class AgentesColetivosController {
  constructor(
    private readonly agentesColetivosService: AgentesColetivosService,
  ) {}

  // POST

  @Post(':id/atividades')
  createAtividade(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateAtividadeDto,
    @User('id') usuarioId: number,
  ) {
    return this.agentesColetivosService.createAtividade(id, dto, usuarioId);
  }

  @Post(':id/projetos')
  createProjeto(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateProjetoDto,
    @User('id') usuarioId: number,
  ) {
    return this.agentesColetivosService.createProjeto(id, dto, usuarioId);
  }

  @Post()
  create(@Body() dto: CreateAgenteColetivoDto, @User('id') usuarioId: number) {
    return this.agentesColetivosService.create(dto, usuarioId);
  }

  // GET

  @Get(':id/atividades')
  findAtividades(@Param('id', ParseIntPipe) id: number) {
    return this.agentesColetivosService.findAtividades(id);
  }

  @Get(':id/endereco')
  findEndereco(@Param('id', ParseIntPipe) id: number) {
    return this.agentesColetivosService.findEndereco(id);
  }

  @Get(':id/projetos')
  findProjetos(@Param('id', ParseIntPipe) id: number) {
    return this.agentesColetivosService.findProjetos(id);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.agentesColetivosService.findOne(id);
  }

  @Get()
  findAll() {
    return this.agentesColetivosService.findAll();
  }

  // DELETE

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.agentesColetivosService.remove(id);
  }
}
