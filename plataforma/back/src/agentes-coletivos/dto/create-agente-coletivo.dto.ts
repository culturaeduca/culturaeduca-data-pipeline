import { Type } from 'class-transformer';
import {
  IsArray,
  IsEmail,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import { TipoAgenteColetivo } from '../agentes-coletivos.constants';

class CreateEnderecoDto {
  @IsNumber()
  @Min(-90)
  @Max(90)
  latitude: number;

  @IsNumber()
  @Min(-180)
  @Max(180)
  longitude: number;

  @IsOptional()
  @IsString()
  logradouro: string | null;

  @IsOptional()
  @IsString()
  numero: string | null;

  @IsOptional()
  @IsString()
  bairro: string | null;

  @IsOptional()
  @IsString()
  complemento: string | null;

  @IsOptional()
  @IsString()
  codigoPostal: string | null;

  @IsOptional()
  @IsString()
  municipioCodigo: string | null;

  @IsOptional()
  @IsString()
  municipioNome: string | null;

  @IsOptional()
  @IsString()
  ufSigla: string | null;

  @IsOptional()
  @IsString()
  ufNome: string | null;
}

class CreateAgenteDto {
  // identificacao

  @IsString()
  @IsNotEmpty()
  nome: string;

  @IsOptional()
  @IsEmail()
  email: string | null;

  @IsOptional()
  @IsString()
  website: string | null;

  @IsOptional()
  @IsString()
  telefone1: string | null;

  @IsOptional()
  @IsString()
  telefone2: string | null;

  // endereco

  @ValidateNested()
  @Type(() => CreateEnderecoDto)
  endereco: CreateEnderecoDto;

  // atuacao

  @IsOptional()
  @IsInt()
  abrangenciaId: number | null;

  @IsOptional()
  @IsString()
  objetivo: string | null;

  @IsOptional()
  @IsString()
  atuacao: string | null;

  @IsInt()
  atuaDesde: number | null;

  @IsArray()
  @IsInt({ each: true })
  areasAtuacaoIds: number[];

  @IsArray()
  @IsString({ each: true })
  areasAtuacaoOutros: string[];

  @IsArray()
  @IsInt({ each: true })
  areasEstudoPesquisaIds: number[];

  @IsArray()
  @IsString({ each: true })
  areasEstudoPesquisaOutros: string[];

  @IsArray()
  @IsInt({ each: true })
  publicosFocaisIds: number[];

  @IsArray()
  @IsString({ each: true })
  publicosFocaisOutros: string[];

  @IsArray()
  @IsInt({ each: true })
  tiposEspacosIds: number[];

  @IsArray()
  @IsString({ each: true })
  tiposEspacosOutros: string[];
}

export class CreateAgenteColetivoDto {
  @IsEnum(TipoAgenteColetivo)
  tipo: TipoAgenteColetivo;

  @ValidateNested()
  @Type(() => CreateAgenteDto)
  agente: CreateAgenteDto;
}
