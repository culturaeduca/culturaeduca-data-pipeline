import { Type } from 'class-transformer';
import {
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';

export class BuscaAvancadaAtividadeQueryDto {
  @IsOptional()
  @IsString()
  q?: string | null;

  @IsOptional()
  @IsString()
  datetimeInicio?: string;

  @IsOptional()
  @IsString()
  datetimeFim?: string;

  @IsOptional()
  @Type(() => Number)
  @IsInt({ each: true })
  opcoesIds?: number[];

  @IsOptional()
  @IsString({ each: true })
  municipioCodigo?: string[];

  @IsOptional()
  @IsString({ each: true })
  ufSigla?: string[];

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(-90)
  @Max(90)
  lat?: number | null;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(-180)
  @Max(180)
  lng?: number | null;

  @Type(() => Number)
  @IsInt()
  @Min(1)
  limit: number;

  @Type(() => Number)
  @IsInt()
  @Min(0)
  offset: number;
}
