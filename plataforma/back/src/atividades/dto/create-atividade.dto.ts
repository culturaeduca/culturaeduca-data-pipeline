import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import { AtividadeEnderecoReferenciaEnum } from '../atividades.constants';

class CreateEnderecoDto {
  @IsNumber()
  @Min(-90)
  @Max(90)
  latitude: number;

  @IsNumber()
  @Min(-180)
  @Max(180)
  longitude: number;

  @IsOptional()
  @IsString()
  logradouro: string | null;

  @IsOptional()
  @IsString()
  numero: string | null;

  @IsOptional()
  @IsString()
  bairro: string | null;

  @IsOptional()
  @IsString()
  complemento: string | null;

  @IsOptional()
  @IsString()
  codigoPostal: string | null;

  @IsOptional()
  @IsString()
  municipioCodigo: string | null;

  @IsOptional()
  @IsString()
  municipioNome: string | null;

  @IsOptional()
  @IsString()
  ufSigla: string | null;

  @IsOptional()
  @IsString()
  ufNome: string | null;
}

export class CreateAtividadeDto {
  @IsOptional()
  @IsInt()
  projetoId: number | null;

  @IsString()
  datetimeInicio: string;

  @IsOptional()
  @IsString()
  datetimeFim: string | null;

  @IsBoolean()
  diaTodo: boolean;

  @IsString()
  @IsNotEmpty()
  titulo: string;

  @IsOptional()
  @IsString()
  descricao: string | null;

  @IsArray()
  @IsInt({ each: true })
  opcoesIds: number[];

  @IsOptional()
  @IsEnum(AtividadeEnderecoReferenciaEnum)
  enderecoReferencia: AtividadeEnderecoReferenciaEnum | null;

  @IsOptional()
  @ValidateNested()
  @Type(() => CreateEnderecoDto)
  endereco?: CreateEnderecoDto | null;
}
