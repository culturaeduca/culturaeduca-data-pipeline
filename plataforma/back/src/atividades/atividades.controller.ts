import {
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { AtividadesService } from './atividades.service';
import { Public } from '../public.decorator';
import { BuscaAvancadaAtividadeQueryDto } from './dto';

@Controller('atividades')
export class AtividadesController {
  constructor(private readonly atividadesService: AtividadesService) {}

  @Public()
  @Get('busca_avancada')
  buscaAvancada(@Query() queryDto: BuscaAvancadaAtividadeQueryDto) {
    return this.atividadesService.buscaAvancada(queryDto);
  }

  @Public()
  @Get()
  findAll() {
    return this.atividadesService.findAll();
  }

  @Public()
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.atividadesService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.atividadesService.remove(id);
  }
}
