import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { BuscaAvancadaAtividadeQueryDto, CreateAtividadeDto } from './dto';
import { AtividadeEnderecoReferencia } from '@prisma/client';

@Injectable()
export class AtividadesService {
  constructor(private readonly prisma: PrismaService) {}

  async buscaAvancada(queryDto: BuscaAvancadaAtividadeQueryDto) {
    const {
      q,
      datetimeFim,
      datetimeInicio,
      opcoesIds,
      municipioCodigo,
      ufSigla,
      // lat, TODO
      // lng, TODO
      limit = 10,
      offset = 0,
    } = queryDto;
    const search = q?.trim()?.toLowerCase();
    return this.prisma.atividade.findMany({
      where: {
        AND: [
          ...(search
            ? [
                {
                  OR: [
                    {
                      titulo: { contains: search, mode: 'insensitive' } as any,
                    },
                    {
                      descricao: {
                        contains: search,
                        mode: 'insensitive',
                      } as any,
                    },
                    {
                      projeto: {
                        titulo: {
                          contains: search,
                          mode: 'insensitive',
                        } as any,
                      },
                    },
                  ],
                },
              ]
            : []),
          {
            OR: [{ projetoId: null }, { projeto: { ativo: true } }],
          },
          {
            ...(datetimeInicio
              ? { datetimeInicio: { gte: new Date(datetimeInicio) } }
              : {}),
            ...(datetimeFim
              ? { datetimeFim: { lte: new Date(datetimeFim) } }
              : {}),
            ativo: true,
            agente: {
              perfilPublico: true,
            },
            ...((municipioCodigo && municipioCodigo.length > 0) ||
            (ufSigla && ufSigla.length > 0)
              ? {
                  endereco: {
                    municipioCodigo: { in: municipioCodigo },
                    ufSigla: { in: ufSigla },
                  },
                }
              : {}),
            ...(opcoesIds && opcoesIds.length > 0
              ? { opcoes: { some: { opcaoId: { in: opcoesIds } } } }
              : {}),
          },
        ],
      },
      include: {
        agente: true,
        projeto: true,
        endereco: true,
        opcoes: {
          include: { opcao: true },
        },
      },
      take: limit,
      skip: offset,
    });
  }

  async create(dto: CreateAtividadeDto, agenteId: number, usuarioId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      let dataEndereco;
      // copia endereco do agente
      if (dto.enderecoReferencia === AtividadeEnderecoReferencia.AGENTE) {
        const agente = await trxPrisma.agente.findUnique({
          where: { id: agenteId },
          include: {
            endereco: true,
          },
        });
        if (!agente.endereco) {
          throw new BadRequestException('Agente não tem endereço cadastrado!');
        }
        dataEndereco = {
          latitude: agente.endereco.latitude,
          longitude: agente.endereco.longitude,
          logradouro: agente.endereco.logradouro,
          numero: agente.endereco.numero,
          bairro: agente.endereco.bairro,
          complemento: agente.endereco.complemento,
          codigoPostal: agente.endereco.codigoPostal,
          municipioCodigo: agente.endereco.municipioCodigo,
          municipioNome: agente.endereco.municipioNome,
          ufSigla: agente.endereco.ufSigla,
          ufNome: agente.endereco.ufNome,
        };
      }
      // copia endereco do projeto
      else if (dto.enderecoReferencia === AtividadeEnderecoReferencia.PROJETO) {
        const projeto = await trxPrisma.projeto.findUnique({
          where: { id: dto.projetoId },
          include: {
            endereco: true,
          },
        });
        if (!projeto) {
          throw new NotFoundException('Projeto não encontrado!');
        }
        if (!projeto.endereco) {
          throw new BadRequestException('Projeto não tem endereço cadastrado!');
        }
        dataEndereco = {
          latitude: projeto.endereco.latitude,
          longitude: projeto.endereco.longitude,
          logradouro: projeto.endereco.logradouro,
          numero: projeto.endereco.numero,
          bairro: projeto.endereco.bairro,
          complemento: projeto.endereco.complemento,
          codigoPostal: projeto.endereco.codigoPostal,
          municipioCodigo: projeto.endereco.municipioCodigo,
          municipioNome: projeto.endereco.municipioNome,
          ufSigla: projeto.endereco.ufSigla,
          ufNome: projeto.endereco.ufNome,
        };
      }
      // cria novo endereco
      else {
        dataEndereco = {
          ...dto.endereco,
        };
      }
      // endereco
      const endereco = await trxPrisma.endereco.create({
        data: dataEndereco,
      });
      // atividade
      const dataAtividade = {
        titulo: dto.titulo?.trim() || null,
        descricao: dto.descricao?.trim() || null,
        datetimeInicio: new Date(dto.datetimeInicio) || null,
        datetimeFim: new Date(dto.datetimeFim) || null,
        diaTodo: dto.diaTodo,
        agente: {
          connect: { id: agenteId },
        },
        endereco: {
          connect: { id: endereco.id },
        },
        ...(dto.projetoId
          ? {
              projeto: {
                connect: { id: dto.projetoId },
              },
            }
          : {}),
        usuarioCreatedBy: {
          connect: { id: usuarioId },
        },
        usuarioUpdatedBy: {
          connect: { id: usuarioId },
        },
      };
      const atividade = await trxPrisma.atividade.create({
        data: dataAtividade as any,
      });
      // opcoes
      const dataAtividadeOpcoes = dto.opcoesIds.map((opcaoId) => ({
        opcaoId,
        atividadeId: atividade.id,
      }));
      await trxPrisma.atividadeOpcao.createMany({
        data: dataAtividadeOpcoes,
      });
      // retorna atividade completa
      return trxPrisma.atividade.findUnique({
        where: { id: atividade.id },
        include: {
          agente: true,
          projeto: true,
          endereco: true,
          opcoes: { include: { opcao: true } },
        },
      });
    });
  }

  async findAll() {
    return this.prisma.atividade.findMany({
      include: {
        agente: true,
        projeto: true,
        endereco: true,
        opcoes: { include: { opcao: true } },
      },
    });
  }

  async findOne(id: number) {
    const atividade = await this.prisma.atividade.findUnique({
      where: { id },
      include: {
        agente: true,
        projeto: true,
        endereco: true,
        opcoes: { include: { opcao: true } },
      },
    });
    if (!atividade) {
      throw new NotFoundException('Atividade não encontrada');
    }
    return atividade;
  }

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const atividade = await trxPrisma.atividade.findUnique({ where: { id } });
      if (!atividade) {
        throw new NotFoundException('Atividade não encontrada');
      }
      await trxPrisma.atividade.delete({ where: { id: atividade.id } });
      return atividade;
    });
  }
}
