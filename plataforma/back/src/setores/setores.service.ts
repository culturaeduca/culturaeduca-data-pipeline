import { Injectable } from '@nestjs/common';
import { KnexService } from '../knex/knex.service';
import { GetSetoresBreadcrumbsQueryDto, GetSetoresRadiusQueryDto } from './dto';

@Injectable()
export class SetoresService {
  constructor(private readonly knexBasesService: KnexService) {}

  async getSetoresBreadcrumbs(queryDto: GetSetoresBreadcrumbsQueryDto) {
    const { latitude, longitude } = queryDto;
    return this.knexBasesService.getSetoresBreadcrumbs({
      latitude,
      longitude,
    });
  }

  async getSetoresRadius(queryDto: GetSetoresRadiusQueryDto) {
    const { latitude, longitude, radiusKm } = queryDto;
    return this.knexBasesService.getSetoresRadius({
      key: 'setor',
      latitude,
      longitude,
      radiusKm,
    });
  }
}
