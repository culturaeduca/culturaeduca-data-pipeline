import { Module } from '@nestjs/common';
import { KnexModule } from '../knex/knex.module';
import { PrismaService } from '../prisma.service';
import { SetoresService } from './setores.service';
import { SetoresController } from './setores.controller';

@Module({
  imports: [KnexModule],
  providers: [SetoresService, PrismaService],
  controllers: [SetoresController],
})
export class SetoresModule {}
