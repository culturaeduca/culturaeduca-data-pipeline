import { Controller, Get, Query } from '@nestjs/common';
import { GetSetoresBreadcrumbsQueryDto, GetSetoresRadiusQueryDto } from './dto';
import { SetoresService } from './setores.service';
import { Public } from '../public.decorator';

@Controller('setores')
export class SetoresController {
  constructor(private readonly setoresService: SetoresService) {}

  @Public()
  @Get('breadcrumbs')
  getBreadcrumbs(@Query() queryDto: GetSetoresBreadcrumbsQueryDto) {
    return this.setoresService.getSetoresBreadcrumbs(queryDto);
  }

  @Public()
  @Get('radius')
  getSetoresRadius(@Query() queryDto: GetSetoresRadiusQueryDto) {
    return this.setoresService.getSetoresRadius(queryDto);
  }
}
