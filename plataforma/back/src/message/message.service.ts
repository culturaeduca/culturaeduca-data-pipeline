import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { createTransport, SendMailOptions, Transporter } from 'nodemailer';
import {
  SendEmailDto,
  EmailValidationTokenDto,
  EmailChangePasswordTokenDto,
} from '../message/dto';
// import { convert, ht } from 'html-to-text';
import { convert } from 'html-to-text';
@Injectable()
export class MessageService {
  private mailTransport: Transporter;
  public frontUrl: string;

  constructor(private configService: ConfigService) {
    const { transport, frontUrl } = this.configService.get('mailer');
    // TODO: Criar opção unsubcribe. Ajuda a nao enviar para o spam
    this.mailTransport = createTransport(transport);
    this.frontUrl = frontUrl;
  }

  getEmailTextFromHtml = (html: string) => {
    const options = {
      wordwrap: 130,
      // ...
    };
    return convert(html, options);
  };
  getContentValidateUser(dto: EmailValidationTokenDto): string {
    return `
      <p>Olá ${dto.nome},</p><p>Você pode confirmar o seu registro no link: <br /><a href="${this.frontUrl}/auth/validar-email/${dto.tokenValidacaoEmail}" style="font-size:16px; font-weight: 700;">
      Validação
      </a></p><p>Grato,<br />CulturaEduca</p>`;
  }

  getContentResetPassword(dto: EmailChangePasswordTokenDto) {
    return `
    <p>Recebemos sua solicitação para alteração de senha no CulturaEduca</p>
    <p>Para redefinir sua senha, por favor, siga os passos abaixo:</p>
    <ol>
      <li>
        Clique no link abaixo para acessar a página de redefinição de senha:
        <a href="${this.frontUrl}/auth/trocar-senha/${dto.tokenTrocaSenha}?email=${dto.email}">link</a>
      </li>
      <li>Siga as instruções na página para criar uma nova senha.</li>
    </ol>
    <p style="font-weight: 700">Dicas para criar uma senha forte:</p>
    <ul>
      <li>Utilize pelo menos 8 caracteres</li>
      <li>Combine letras maiúsculas e minúsculas</li>
      <li>Inclua números e símbolos</li>
    </ul>
    <p>
      Se você não solicitou a alteração de senha, não se preocupe, basta ignorar
      este e-mail e sua senha atual permanecerá inalterada.
    </p>
    <p>Estamos à disposição para qualquer dúvida ou assistência.</p>
    <p>Atenciosamente,</p>
    <p>CulturaEduca</p>`;
  }

  async sendEmail(dto: SendEmailDto): Promise<{ success: boolean } | null> {
    const { sender, dkim } = this.configService.get('mailer');
    const mailOptions: SendMailOptions = {
      ...sender,
      ...dto,
      text: this.getEmailTextFromHtml(dto.html),
      dkim,
    };

    try {
      return await this.mailTransport.sendMail(mailOptions);
    } catch (error) {
      throw new UnprocessableEntityException('Email não foi enviado');
    }
  }
}
