import { IsEmail, IsString } from 'class-validator';

export class EmailValidationTokenDto {
  @IsString()
  nome: string;

  @IsEmail()
  email: string;

  @IsString()
  tokenValidacaoEmail: string;
}
