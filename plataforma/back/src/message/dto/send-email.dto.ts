import { Address } from 'nodemailer/lib/mailer';

export class SendEmailDto {
  // sender?: Address;
  to: Address[];
  subject: string;
  html: string;
  text?: string;
}
