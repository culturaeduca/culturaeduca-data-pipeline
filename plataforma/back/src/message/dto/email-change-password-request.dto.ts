import { IsString, IsEmail } from 'class-validator';

export class EmailChangePasswordTokenDto {
  @IsString()
  tokenTrocaSenha: string;

  @IsEmail()
  email: string;
}
