import { Module } from '@nestjs/common';
import { KnexService } from './knex.service';
import { KnexProvider } from './knex.provider';

@Module({
  providers: [KnexService, KnexProvider],
  exports: [KnexService],
})
export class KnexModule {}
