export enum DB_SCHEMA {
  PLATAFORMA = 'plataforma',
  DATASETS = 'datasets',
  QUESTIONS = 'questions',
}
export const DB_TEXT_SEARCH_CONFIG = 'portuguese_unaccent';
export const VIEW_NAME_GENERAL_SEARCH = 'view_general_search';

export enum ColumnViewSearch {
  CATEGORY_TYPE = 'category_type',
  CATEGORY_URI = 'category_uri',
  BASE_URI = 'base_uri',
  URI = 'uri',
  PK = 'pk',
  NOME = 'nome',
  DESCRICAO = 'descricao',
  TSV_NOME = 'tsv_nome',
  GEOM = 'geom',
  GEOG = 'geog',
  UF_SIGLA = 'uf_sigla',
  MUNICIPIO_CODIGO = 'municipio_codigo',
  MUNICIPIO_NOME = 'municipio_nome',
}
