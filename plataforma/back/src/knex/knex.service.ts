import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { KnexConnection } from './knex.provider';
import { Knex } from 'knex';
import { ConfigService } from '@nestjs/config';
import {
  ColumnViewSearch,
  DB_SCHEMA,
  VIEW_NAME_GENERAL_SEARCH,
} from './knex.constants';
import { Base, BaseCategory } from '@prisma/client';

@Injectable()
export class KnexService {
  constructor(
    @Inject(KnexConnection) private readonly knex: Knex,
    private readonly configService: ConfigService,
  ) {}

  async searchViewBuscas(options: {
    q: string;
    filter: {
      categoryType: string[];
      categoryUri: string[];
      baseUri: string[];
      municipioCodigo: string[];
      ufSigla: string[];
    };
    lat: number | null;
    lng: number | null;
    limit: number | null;
    offset: number | null;
  }) {
    const search = options.q.trim().toLowerCase();
    const { lat, lng, limit, offset } = options;
    const hasLatLng = lat !== null && lng !== null;
    let knexQuery = this.knex
      .withSchema(DB_SCHEMA.PLATAFORMA)
      .select([
        ColumnViewSearch.CATEGORY_TYPE,
        ColumnViewSearch.CATEGORY_URI,
        ColumnViewSearch.BASE_URI,
        ColumnViewSearch.PK,
        ColumnViewSearch.NOME,
        this.knex.raw(
          `ST_AsGeojson("${ColumnViewSearch.GEOM}")::jsonb AS "${ColumnViewSearch.GEOM}"`,
        ),
        ColumnViewSearch.UF_SIGLA,
        ColumnViewSearch.MUNICIPIO_CODIGO,
        ColumnViewSearch.MUNICIPIO_NOME,
        this.knex.raw(
          `similarity(UNACCENT(${ColumnViewSearch.NOME}), UNACCENT(?)) AS "rank"`,
          [search],
        ),
        ...(hasLatLng
          ? [
              this.knex.raw(
                `COALESCE(ST_Distance(${ColumnViewSearch.GEOG}, ST_Point(?, ?, 4326)) / 1000, '+infinity'::float) AS "distance_km"`,
                [lng, lat],
              ),
            ]
          : [this.knex.raw('NULL AS "distance_km"')]),
      ])
      .from(VIEW_NAME_GENERAL_SEARCH);

    // FILTER CATEGORY_TYPE
    if (
      options.filter?.categoryType &&
      options.filter.categoryType?.length > 0
    ) {
      knexQuery = knexQuery.whereIn(
        ColumnViewSearch.CATEGORY_TYPE,
        options.filter.categoryType,
      );
    }
    // FILTER CATEGORY_TYPE
    if (options.filter?.categoryUri && options.filter.categoryUri?.length > 0) {
      knexQuery = knexQuery.whereIn(
        ColumnViewSearch.CATEGORY_URI,
        options.filter.categoryUri,
      );
    }
    // FILTER BASE_URI
    if (options.filter?.baseUri && options.filter.baseUri?.length > 0) {
      knexQuery = knexQuery.whereIn(
        ColumnViewSearch.BASE_URI,
        options.filter.baseUri,
      );
    }
    // FILTER BASE_URI
    if (options.filter?.baseUri && options.filter.baseUri?.length > 0) {
      knexQuery = knexQuery.whereIn(
        ColumnViewSearch.BASE_URI,
        options.filter.baseUri,
      );
    }
    // FILTER MUNICIPIO_CODIGO
    if (
      options.filter?.municipioCodigo &&
      options.filter.municipioCodigo?.length > 0
    ) {
      knexQuery = knexQuery.whereIn(
        ColumnViewSearch.MUNICIPIO_CODIGO,
        options.filter.municipioCodigo,
      );
    }
    // FILTER UF_SIGLA
    if (options.filter?.ufSigla && options.filter.ufSigla?.length > 0) {
      knexQuery = knexQuery.whereIn(
        ColumnViewSearch.UF_SIGLA,
        options.filter.ufSigla,
      );
    }
    // SEARCH TERM
    if (search) {
      knexQuery = knexQuery.whereRaw(
        `UNACCENT("${ColumnViewSearch.NOME}") ILIKE UNACCENT(?)`,
        [`%${search}%`],
      );
    }
    // PERFORM QUERY
    knexQuery = knexQuery
      .orderBy('distance_km', 'ASC', 'LAST')
      .orderBy('rank', 'DESC', 'LAST')
      .limit(limit)
      .offset(offset);
    const results = await knexQuery;
    return results;
  }

  async refreshViewBuscas(
    bases: (Base & { category: BaseCategory })[],
    _trxKnex?: Knex.Transaction,
  ) {
    const trxKnex = _trxKnex || this.knex;
    // drop view if no bases are provided
    if (bases.length === 0) {
      return trxKnex.schema
        .withSchema(DB_SCHEMA.PLATAFORMA)
        .dropViewIfExists(VIEW_NAME_GENERAL_SEARCH);
    }
    // create union of all bases views
    const knexQuery = this.knex.union([
      ...bases.map((base) =>
        this.knex
          .withSchema(base.databaseSchema)
          .select(
            this.knex.raw(`
                '${base.category.type}' AS "${ColumnViewSearch.CATEGORY_TYPE}",
                '${base.category.uri}' AS "${ColumnViewSearch.CATEGORY_URI}",
                '${base.uri}' AS "${ColumnViewSearch.BASE_URI}",
                "${base.columnPk}"::text AS "${ColumnViewSearch.PK}",
                "${base.columnName}" AS "${ColumnViewSearch.NOME}",
                ${base.columnDescription ? `"${base.columnDescription}"` : `'${base.category.name} - ${base.title}'`} AS "${ColumnViewSearch.DESCRICAO}",
                CASE WHEN ST_GeometryType("${base.columnGeom}") = 'ST_Point'
                  THEN "${base.columnGeom}"
                  ELSE ST_Centroid("${base.columnGeom}")
                END AS "${ColumnViewSearch.GEOM}",
                CASE WHEN ST_GeometryType("${base.columnGeom}") = 'ST_Point'
                  THEN ${base.columnGeog ? `"${base.columnGeog}"` : `"${base.columnGeom}"::geography`}
                  ELSE ${base.columnGeog ? `ST_Centroid("${base.columnGeog}")` : `ST_Centroid("${base.columnGeom}"::geography)`}
                END AS "${ColumnViewSearch.GEOG}",
                ${base.columnUf ? `"${base.columnUf}"::text` : 'NULL'} AS "${ColumnViewSearch.UF_SIGLA}",
                ${base.columnMunCd ? `"${base.columnMunCd}"::text` : 'NULL'} AS "${ColumnViewSearch.MUNICIPIO_CODIGO}",
                ${base.columnMunNm ? `"${base.columnMunNm}"::text` : 'NULL'} AS "${ColumnViewSearch.MUNICIPIO_NOME}"
              `),
          )
          .from(base.tableName),
      ),
    ]);
    // create view
    return trxKnex.schema
      .withSchema(DB_SCHEMA.PLATAFORMA)
      .dropViewIfExists(VIEW_NAME_GENERAL_SEARCH)
      .createView(VIEW_NAME_GENERAL_SEARCH, (view) => view.as(knexQuery));
  }

  async findOne(options: {
    databaseSchema: string;
    tableName: string;
    columnPk: string;
    columnGeom?: string;
    pk: string;
  }): Promise<any> {
    const { databaseSchema, tableName, columnPk, columnGeom, pk } = options;
    // TODO: select geometry columns as geojson
    const entity = await this.knex
      .withSchema(databaseSchema)
      .table(tableName)
      .select([
        '*',
        ...(columnGeom
          ? [
              this.knex.raw(
                `ST_AsGeoJSON("${columnGeom}"::geography)::jsonb as "${columnGeom}"`,
              ),
            ]
          : []),
      ])
      .where(columnPk, pk)
      .first();
    if (!entity) {
      throw new NotFoundException(`Entidade ${pk} não encontrada`);
    }
    return entity;
  }

  async getSetoresBreadcrumbs(options: {
    latitude: number;
    longitude: number;
  }) {
    const { latitude, longitude } = options;
    const { boundaries } = this.configService.get<{ boundaries: any[] }>(
      'setores',
    );
    const boundaryUf = boundaries.find((b) => b.key === 'uf');
    const boundaryMunicipio = boundaries.find((b) => b.key === 'municipio');
    const boundarySetor = boundaries.find((b) => b.key === 'setor');
    const select = boundarySetor.columnsSelect.map(
      (column) => `"${boundarySetor.key}"."${column}" AS "${column}"`,
    );
    const from = `
      "${boundarySetor.databaseSchema}"."${boundarySetor.tableName}" AS "${boundarySetor.key}"
      INNER JOIN "${boundaryMunicipio.databaseSchema}"."${boundaryMunicipio.tableName}" AS "${boundaryMunicipio.key}" ON "${boundaryMunicipio.key}"."${boundaryMunicipio.columnPk}" = "${boundarySetor.key}"."${boundarySetor.columnFk}"
      INNER JOIN "${boundaryUf.databaseSchema}"."${boundaryUf.tableName}" AS "${boundaryUf.key}" ON "${boundaryUf.key}"."${boundaryUf.columnPk}" = "${boundaryMunicipio.key}"."${boundaryMunicipio.columnFk}"
    `;
    const where = `
      ST_Contains("${boundaryUf.key}"."${boundaryUf.columnGeom}", ST_Point(${longitude}, ${latitude}, 4326))
      AND ST_Contains("${boundaryMunicipio.key}"."${boundaryMunicipio.columnGeom}", ST_Point(${longitude}, ${latitude}, 4326))
      AND ST_Contains("${boundarySetor.key}"."${boundarySetor.columnGeom}", ST_Point(${longitude}, ${latitude}, 4326))
    `;
    const rawQuery = `
      SELECT ${select.join(',')}
      FROM ${from}
      WHERE ${where}
      LIMIT 1
    `;
    const results = await this.knex.raw(rawQuery);
    const setor = results.rows?.[0];
    if (!setor) {
      throw new NotFoundException('Setor not found!');
    }
    const breadcrumbs = [
      {
        key: 'uf',
        label: 'Unidade Federativa',
        pk: setor?.['sigla_uf'],
        name: setor?.['sigla_uf'],
      },
      {
        key: 'municipio',
        label: 'Município',
        pk: setor?.['cd_mun'],
        name: setor?.['nm_mun'],
      },
      {
        key: 'distrito',
        label: 'Distrito',
        pk: setor?.['cd_dist'],
        name: setor?.['nm_dist'],
      },
      {
        key: 'setor',
        label: 'Setor Censitário',
        pk: setor?.['cd_setor'],
        name: setor?.['cd_setor'],
      },
    ];
    return breadcrumbs;
  }

  async getEntidadesSetoresRadius(options: {
    key: string;
    latitude: number;
    longitude: number;
    radiusKm: number;
  }) {
    const { key, latitude, longitude, radiusKm } = options;
    const { boundaries } = this.configService.get<{ boundaries: any[] }>(
      'setores',
    );
    const filteredBoundaries =
      key === 'setor'
        ? boundaries.filter((boundary) => !boundary.divisaoAdministrativa)
        : boundaries;
    const index = filteredBoundaries.findIndex((b) => b.key === key);
    let select = '';
    let from = '';
    let where = '';
    let boundary = null;
    for (let i = 0; i <= index; i++) {
      const previousBoundary = boundary;
      boundary = filteredBoundaries[i];
      const stDWithin = `ST_DWithin("${boundary.key}"."${boundary.columnGeog}", ST_Point(${longitude}, ${latitude}), ${radiusKm * 1000})`;
      if (!previousBoundary) {
        from = `"${boundary.databaseSchema}"."${boundary.tableName}" AS "${boundary.key}"`;
        where = `${stDWithin}`;
      } else {
        from = `${from}
          INNER JOIN "${boundary.databaseSchema}"."${boundary.tableName}" AS "${boundary.key}" ON "${boundary.key}"."${boundary.columnFk}"::text = "${previousBoundary.key}"."${previousBoundary.columnPk}"::text`;
        where = `${where}
          AND ${stDWithin}`;
      }
    }
    select = `ST_Union("${boundary.key}"."${boundary.columnGeom}")::geography AS "${boundary.key}"`;
    const rawQuery = `
      SELECT 
        "${ColumnViewSearch.CATEGORY_TYPE}",
        "${ColumnViewSearch.CATEGORY_URI}",
        "${ColumnViewSearch.BASE_URI}",
        "${ColumnViewSearch.PK}",
        "${ColumnViewSearch.NOME}",
        "${ColumnViewSearch.DESCRICAO}",
        "${ColumnViewSearch.UF_SIGLA}",
        "${ColumnViewSearch.MUNICIPIO_CODIGO}",
        "${ColumnViewSearch.MUNICIPIO_NOME}",
        ST_AsGeoJSON("${ColumnViewSearch.GEOM}")::jsonb as "${ColumnViewSearch.GEOM}",
        COALESCE(ST_Distance("${ColumnViewSearch.GEOG}", ST_Point(${longitude}, ${latitude}, 4326)) / 1000, '+infinity'::float) AS "distance_km"
      FROM "${DB_SCHEMA.PLATAFORMA}"."${VIEW_NAME_GENERAL_SEARCH}"
      WHERE
        ${ColumnViewSearch.CATEGORY_TYPE} = 'EQUIPAMENTO'
        AND ST_DWithin(
        (
          SELECT ${select}
          FROM ${from}
          WHERE ${where}
        ), "geog", 0)
       ;
    `;
    const results = await this.knex.raw(rawQuery);
    return results.rows;
  }

  async getSetoresRadius(options: {
    key: string;
    latitude: number;
    longitude: number;
    radiusKm: number;
  }) {
    const { key, latitude, longitude, radiusKm } = options;
    const { boundaries } = this.configService.get<{ boundaries: any[] }>(
      'setores',
    );
    const index = boundaries.findIndex((b) => b.key === key);
    let select = '';
    let from = '';
    let where = '';
    let boundary = null;
    for (let i = 0; i <= index; i++) {
      const previousBoundary = boundary;
      boundary = boundaries[i];
      const stDWithin = `ST_DWithin("${boundary.key}"."${boundary.columnGeog}", ST_Point(${longitude}, ${latitude}), ${radiusKm * 1000})`;
      if (!previousBoundary) {
        from = `"${boundary.databaseSchema}"."${boundary.tableName}" AS "${boundary.key}"`;
        where = `${stDWithin}`;
      } else {
        from = `${from}
          INNER JOIN "${boundary.databaseSchema}"."${boundary.tableName}" AS "${boundary.key}" ON "${boundary.key}"."${boundary.columnFk}" = "${previousBoundary.key}"."${previousBoundary.columnPk}"`;
        where = `${where}
          AND ${stDWithin}`;
      }
    }
    let geojson = `
      'Feature' as "type",
      jsonb_build_object(
        'type', "${boundary.columnGeog}"->'type',`;
    let properties = `jsonb_build_object(`;
    boundary.columnsSelect.forEach((column) => {
      select = `${select}
        "${boundary.key}"."${column}" AS "${column}",`;
      properties = `${properties}
        '${column}', "${column}",`;
    });
    select = `${select}
      ST_AsGeoJSON(${boundary.key}.${boundary.columnGeog})::jsonb as "${boundary.columnGeog}"`;
    properties = `${properties.slice(0, -1)})`;
    geojson = `${geojson}
      'coordinates', "${boundary.columnGeog}"->'coordinates'
    ) as "geometry",
    ${properties} as "properties"`;
    const rawQuery = `
      SELECT ${geojson}
      FROM (
        SELECT ${select}
        FROM ${from}
        WHERE ${where}
      ) AS geojson;
    `;
    const results = await this.knex.raw(rawQuery);
    return { type: 'FeatureCollection', features: results.rows };
  }

  // async getSetoresRadiusOld(latitude, longitude, radiusKm) {
  //   const { uf, municipio, setor } = this.configService.get<{
  //     uf: {
  //       tableName: string;
  //       columnPk: string;
  //       columnGeom: string;
  //     };
  //     municipio: {
  //       tableName: string;
  //       columnPk: string;
  //       columnUf: string;
  //       columnGeom: string;
  //     };
  //     setor: {
  //       tableName: string;
  //       columnPk: string;
  //       columnMunicipio: string;
  //       columnGeom: string;
  //     };
  //   }>('setores');
  //   const setores = await this.knex
  //     .table(uf.tableName)
  //     .leftJoin(
  //       municipio.tableName,
  //       `${municipio.tableName}.${municipio.columnUf}`,
  //       `${uf.tableName}.${uf.columnPk}`,
  //     )
  //     .leftJoin(
  //       setor.tableName,
  //       `${setor.tableName}.${setor.columnMunicipio}`,
  //       `${municipio.tableName}.${municipio.columnPk}`,
  //     )
  //     .whereRaw(
  //       `
  //       ST_DWithin(
  //         ${uf.tableName}.${uf.columnGeom}::geography,
  //         ST_Point(${longitude}, ${latitude}),
  //         ${radiusKm * 1000}
  //       )
  //       AND ST_DWithin(
  //         ${municipio.tableName}.${municipio.columnGeom}::geography,
  //         ST_Point(${longitude}, ${latitude}),
  //         ${radiusKm * 1000}
  //       )
  //       AND ST_DWithin(
  //         ${setor.tableName}.${setor.columnGeom}::geography,
  //         ST_Point(${longitude}, ${latitude}),
  //         ${radiusKm * 1000}
  //       );
  //     `,
  //     )
  //     .select([
  //       `${setor.tableName}.attribute_26`,
  //       `${setor.tableName}.attribute_19`,
  //       `${setor.tableName}.attribute_23`,
  //       `${setor.tableName}.attribute_27`,
  //       `${setor.tableName}.attribute_20`,
  //       `${setor.tableName}.attribute_18`,
  //       `${setor.tableName}.attribute_29`,
  //       `${setor.tableName}.attribute_21`,
  //       `${setor.tableName}.attribute_30`,
  //       `${setor.tableName}.attribute_22`,
  //       `${setor.tableName}.attribute_24`,
  //       `${setor.tableName}.attribute_25`,
  //       this.knex.raw(
  //         `ST_AsGeoJSON(${setor.tableName}.${setor.columnGeom}::geography)::jsonb as "${setor.tableName}.${setor.columnGeom}"`,
  //       ),
  //     ]);
  //   return setores;
  // }
}
