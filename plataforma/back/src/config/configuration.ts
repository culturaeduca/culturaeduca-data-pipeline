import 'dotenv/config';
import { Knex } from 'knex';

export default () => ({
  nodeEnv: process.env.NODE_ENV || 'development',
  appPort: Number(process.env.APP_PORT) || 3000,
  enableCorsOrigin: process.env.ENABLE_CORS_ORIGIN.split(',')
    .map((url) => url.trim)
    .filter((url) => !!url) || ['http://localhost:8080'],
  knex: {
    client: 'pg',
    connection: {
      host: process.env.DB_CONNECTION_HOST,
      port: process.env.DB_CONNECTION_PORT,
      user: process.env.DB_CONNECTION_USER,
      password: process.env.DB_CONNECTION_PASSWORD,
      database: process.env.DB_CONNECTION_DATABASE,
      ssl: process.env.DB_CONNECTION_SSL === 'true',
    } as Knex.Config,
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: Number(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD,
  },
  session: {
    name: process.env.SESS_NAME,
    resave: true,
    saveUninitialized: true,
    secret: [process.env.SESS_SECRET_NEW, process.env.SESS_SECRET_OLD],
    cookie: {
      httpOnly: true,
      maxAge: Number(process.env.SESS_LIFETIME),
      sameSite: true,
      secure: process.env.NODE_ENV === 'production',
    },
  },
  setores: {
    boundaries: [
      {
        key: 'uf',
        title: 'UF',
        description: 'Unidade Federativa',
        databaseSchema: 'datasets',
        tableName: 'ufs_2022',
        columnPk: 'sigla_uf',
        columnFk: null,
        columnName: 'nm_uf',
        columnGeom: '_geom',
        columnGeog: '_geog',
        columnsSelect: ['cd_uf', 'nm_uf', 'sigla_uf', 'nm_regiao', 'area_km2'],
        divisaoAdministrativa: false,
      },
      {
        key: 'municipio',
        title: 'Município',
        description: 'Município',
        databaseSchema: 'datasets',
        tableName: 'municipios_2022',
        columnPk: 'cd_mun',
        columnFk: 'sigla_uf',
        columnName: 'nm_mun',
        columnGeom: '_geom',
        columnGeog: '_geog',
        columnsSelect: ['cd_mun', 'nm_mun', 'sigla_uf', 'area_km2'],
        divisaoAdministrativa: false,
      },
      {
        key: 'distrito',
        title: 'Distrito',
        description: 'Distrito',
        databaseSchema: 'datasets',
        tableName: 'distritos_2022',
        columnPk: 'cd_dist',
        columnFk: 'cd_mun',
        columnName: 'nm_dist',
        columnGeom: '_geom',
        columnGeog: '_geog',
        columnsSelect: ['cd_dist', 'nm_dist', 'cd_mun', 'cd_uf'],
        divisaoAdministrativa: true,
      },
      {
        key: 'subdistrito',
        title: 'Subdistrito',
        description: 'Subdistrito',
        databaseSchema: 'datasets',
        tableName: 'subdistritos_2022',
        columnPk: 'cd_subdist',
        columnFk: 'cd_mun',
        columnName: 'nm_subdist',
        columnGeom: '_geom',
        columnGeog: '_geog',
        columnsSelect: ['cd_subdist', 'nm_subdist', 'cd_mun', 'cd_uf'],
        divisaoAdministrativa: true,
      },
      {
        key: 'bairro',
        title: 'Bairro',
        description: 'Bairro',
        databaseSchema: 'datasets',
        tableName: 'bairros_2022',
        columnPk: 'cd_bairro',
        columnFk: 'cd_mun',
        columnName: 'nm_bairro',
        columnGeom: '_geom',
        columnGeog: '_geog',
        columnsSelect: ['cd_bairro', 'nm_bairro', 'cd_mun', 'cd_uf'],
        divisaoAdministrativa: true,
      },
      {
        key: 'setor',
        title: 'Setor',
        description: 'Setor Censitário',
        databaseSchema: 'datasets',
        tableName: 'setores_censitarios_2021',
        columnPk: 'cd_setor',
        columnFk: 'cd_mun',
        columnName: 'cd_setor',
        columnGeom: '_geom',
        columnGeog: '_geog',
        columnsSelect: [
          'cd_setor',
          'cd_sit',
          'nm_sit',
          'cd_uf',
          'nm_uf',
          'sigla_uf',
          'cd_mun',
          'nm_mun',
          'cd_dist',
          'nm_dist',
          'cd_subdist',
          'nm_subdist',
        ],
        divisaoAdministrativa: false,
      },
    ],
  },
  mailer: {
    transport: {
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure:
        process.env.NODE_ENV === 'production' ||
        process.env.MAIL_HOST === 'mail.gandi.net',
      auth: {
        user: `${process.env.MAIL_USER}@${process.env.MAIL_DOMAIN}`,
        pass: process.env.MAIL_PASSWORD,
      },
      logger: process.env.NODE_ENV === 'development', // Enable logger
      debug: process.env.NODE_ENV === 'development', // Enable debug mode
    },
    sender: {
      from: `${process.env.MAIL_SENDER_NAME_DEFAULT} ${process.env.MAIL_SENDER_DEFAULT}@${process.env.MAIL_DOMAIN}`,
      name: process.env.MAIL_SENDER_NAME_DEFAULT,
      address: `${process.env.MAIL_SENDER_DEFAULT}@${process.env.MAIL_DOMAIN}`,
    },
    dkim: {
      domainName: process.env.MAIL_DOMAIN, // Your domain name
      keySelector: 'mail', // DKIM selector (e.g., 'default')
      privateKey: process.env.DKIM_PRIVATE_KEY, // Load private key from environment variable
      cacheDir: false,
    },
    frontUrl: process.env.FRONT_URL,
  },
  multer: {
    dest: process.env.MULTER_DEST || './static/tmp/',
  },
  minio: {
    client: {
      endPoint: process.env.MINIO_END_POINT || 'localhost',
      port: Number(process.env.MINIO_PORT) || 9000,
      useSSL: process.env.MINIO_USE_SSL === 'true' || false,
      accessKey: process.env.MINIO_ACCESS_KEY,
      secretKey: process.env.MINIO_SECRET_KEY,
    },
  },
});
