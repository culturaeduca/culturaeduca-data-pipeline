import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { AgentesService } from './agentes.service';

@Controller('agentes')
export class AgentesController {
  constructor(private readonly agentesService: AgentesService) {}

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.agentesService.findOne(id);
  }
}
