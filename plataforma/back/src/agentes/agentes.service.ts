import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma.service';

@Injectable()
export class AgentesService {
  constructor(private readonly prisma: PrismaService) {}

  async findOne(id: number) {
    const agente = await this.prisma.agente.findUnique({
      where: { id },
      include: {
        agenteColetivo: true,
        agenteIndividual: {
          include: {
            identidadeEtnicoCultural: true,
            identidadeGenero: true,
            nivelEscolaridade: true,
            ocupacao: true,
            racaCorIbge: true,
          },
        },
        endereco: true,
        abrangencia: true,
        opcoes: { include: { opcao: true } },
      },
    });
    if (!agente || !agente.perfilPublico) {
      throw new NotFoundException('Agente não encontrado');
    }
    return agente;
  }
}
