import { Module } from '@nestjs/common';
import { AgentesService } from './agentes.service';
import { AgentesController } from './agentes.controller';
import { PrismaService } from '../prisma.service';

@Module({
  controllers: [AgentesController],
  providers: [AgentesService, PrismaService],
})
export class AgentesModule {}
