import { Module } from '@nestjs/common';
import BasesService, { BasesCategoriesServices } from './services/';
import BasesController, { CategoryController } from './controllers';
import { KnexModule } from '../knex/knex.module';
import { PrismaService } from '../prisma.service';

@Module({
  imports: [KnexModule],
  providers: [BasesService, BasesCategoriesServices, PrismaService],
  controllers: [BasesController, CategoryController],
})
export class BasesModule {}
