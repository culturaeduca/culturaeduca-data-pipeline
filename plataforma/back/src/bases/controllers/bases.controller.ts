import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import BasesService, { BasesCategoriesServices } from '../services';
import {
  CreateBaseDto,
  SearchViewBuscasEntornoQueryDto,
  SearchViewBuscasDivisaoQueryDto,
  SearchViewBuscasQueryDto,
} from '../dto';
import { Public } from '../../public.decorator';

@Controller('bases')
export class BasesController {
  constructor(
    private readonly basesService: BasesService,
    private readonly basesCaregoriesService: BasesCategoriesServices,
  ) {}

  // GET

  @Public()
  @Get('search/entorno')
  searchViewBuscasEntorno(@Query() queryDto: SearchViewBuscasEntornoQueryDto) {
    return this.basesService.searchViewBuscasEntorno(queryDto);
  }

  @Public()
  @Get('search/divisao')
  searchViewBuscasDivisao(@Query() queryDto: SearchViewBuscasDivisaoQueryDto) {
    return this.basesService.searchViewBuscasDivisao(queryDto);
  }

  @Public()
  @Get('search')
  searchViewBuscas(@Query() queryDto: SearchViewBuscasQueryDto) {
    return this.basesService.searchViewBuscas(queryDto);
  }

  @Public()
  @Get(':uri/sections')
  findSections(@Param('uri') uri: string) {
    return this.basesService.findSections(uri);
  }

  @Public()
  @Get(':uri/:entity_pk')
  findEntityByPk(
    @Param('uri') uri: string,
    @Param('entity_pk') entityPk: string,
  ) {
    return this.basesService.findEntityByPk(uri, entityPk);
  }

  @Public()
  @Get(':uri')
  findOne(@Param('uri') uri: string) {
    return this.basesService.findOne(uri);
  }

  @Public()
  @Get('')
  findAll() {
    return this.basesService.findAll();
  }

  // POST

  @HttpCode(HttpStatus.OK)
  @Post('search/refresh_view')
  refreshViewBases() {
    return this.basesService.refreshViewBuscas();
  }

  @Post()
  create(@Body() dto: CreateBaseDto) {
    return this.basesService.create(dto);
  }

  // DELETE

  @Delete(':uri')
  remove(@Param('uri') uri: string) {
    return this.basesService.remove(uri);
  }
}
