import { BasesController } from './bases.controller';
import { CategoryController } from './category.controller';

export default BasesController;
export { CategoryController };
