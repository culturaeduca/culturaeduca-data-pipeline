import {
  Body,
  Controller,
  Put,
  Delete,
  Get,
  Param,
  Post,
} from '@nestjs/common';
import { BasesCategoriesServices } from '../services';
import { CreateBaseCategoryDto } from '../dto';
import { User } from '../../user.decorator';
import { Roles } from '../../roles.decorator';

@Controller('category')
@Roles(['canDefineBase'])
export class CategoryController {
  constructor(
    private readonly basesCaregoriesService: BasesCategoriesServices,
  ) {}

  @Get('')
  @Roles(['canDefineBase'])
  listCategories() {
    return this.basesCaregoriesService.listCategories();
  }

  // POST

  @Post('')
  @Roles(['canDefineBase'])
  createCategory(@Body() dto: CreateBaseCategoryDto) {
    return this.basesCaregoriesService.createCategory(dto);
  }

  // DELETE

  @Delete(':category_uri')
  @Roles(['canDefineBase'])
  removeCategory(@Param('category_uri') categoryUri: string) {
    return this.basesCaregoriesService.removeCategory(categoryUri);
  }

  @Put(':category_uri')
  @Roles(['canDefineBase'])
  updateCategory(
    @Param('category_uri') categoryUri: string,
    @Body() dto: CreateBaseCategoryDto,
  ) {
    return this.basesCaregoriesService.updateCategory(categoryUri, dto);
  }
}
