import { IsEnum, IsInt, IsNotEmpty, IsString, Min } from 'class-validator';
import { BaseCategoryTypeEnum } from '../bases.constants';

export class CreateBaseCategoryDto {
  @IsEnum(BaseCategoryTypeEnum)
  type: BaseCategoryTypeEnum;

  @IsNotEmpty()
  @IsString()
  uri: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  icon: string;

  @IsNotEmpty()
  @IsString()
  color: string;

  @IsInt()
  @Min(0)
  order: number;
}
