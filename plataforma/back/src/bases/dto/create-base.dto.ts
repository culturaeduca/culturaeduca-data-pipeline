import { Type } from 'class-transformer';
import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Min,
  ValidateNested,
} from 'class-validator';

class CreateBaseSectionItemDto {
  @IsInt()
  @Min(0)
  order: number;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsString()
  column: string;
}

class CreateBaseSectionDto {
  @IsInt()
  @Min(0)
  order: number;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateBaseSectionItemDto)
  items: CreateBaseSectionItemDto[];
}

export class CreateBaseDto {
  @IsNotEmpty()
  @IsString()
  baseCategoryUri: string;

  @IsNotEmpty()
  @IsString()
  uri: string;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  titleSingular: string | null;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsOptional()
  @IsString()
  icon: string | null;

  @IsOptional()
  @IsString()
  color: string | null;

  @IsInt()
  @Min(0)
  order: number;

  @IsNotEmpty()
  @IsString()
  geoserverUri: string;

  @IsNotEmpty()
  @IsString()
  databaseSchema: string;

  @IsNotEmpty()
  @IsString()
  tableName: string;

  @IsNotEmpty()
  @IsString()
  columnPk: string;

  @IsNotEmpty()
  @IsString()
  columnName: string;

  @IsOptional()
  @IsString()
  columnDescription: string | null;

  @IsNotEmpty()
  @IsString()
  columnGeom: string;

  @IsOptional()
  @IsString()
  columnGeog: string | null;

  @IsOptional()
  @IsString()
  columnUf: string | null;

  @IsOptional()
  @IsString()
  columnMunCd: string | null;

  @IsOptional()
  @IsString()
  columnMunNm: string | null;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateBaseSectionDto)
  sections: CreateBaseSectionDto[];
}
