import { Injectable, NotFoundException } from '@nestjs/common';
import { KnexService } from '../../knex/knex.service';
import { PrismaService } from '../../prisma.service';
import {
  CreateBaseDto,
  SearchViewBuscasEntornoQueryDto,
  SearchViewBuscasQueryDto,
} from '../dto';
import { Base, BaseSection, BaseSectionItem } from '@prisma/client';
import { SearchViewBuscasDivisaoQueryDto } from '../dto/search-view-buscas-divisao-query.dto';

@Injectable()
export class BasesService {
  constructor(
    private readonly knexService: KnexService,
    private readonly prisma: PrismaService,
  ) {}

  async refreshViewBuscas() {
    const bases = await this.prisma.base.findMany({
      include: { category: true },
    });
    await this.knexService.refreshViewBuscas(bases);
  }

  async searchViewBuscas(queryDto: SearchViewBuscasQueryDto) {
    const {
      q = '',
      categoryType = [],
      categoryUri = [],
      baseUri = [],
      municipioCodigo = [],
      ufSigla = [],
      lat = null,
      lng = null,
      limit = 10,
      offset = 0,
    } = queryDto;
    return this.knexService.searchViewBuscas({
      q,
      filter: {
        categoryType,
        categoryUri,
        baseUri,
        municipioCodigo,
        ufSigla,
      },
      lat,
      lng,
      limit,
      offset,
    });
  }

  async searchViewBuscasEntorno(queryDto: SearchViewBuscasEntornoQueryDto) {
    const { latitude, longitude, radiusKm } = queryDto;
    return this.knexService.getEntidadesSetoresRadius({
      key: 'setor',
      latitude,
      longitude,
      radiusKm,
    });
  }

  async searchViewBuscasDivisao(queryDto: SearchViewBuscasDivisaoQueryDto) {
    const { latitude, longitude, key } = queryDto;
    return this.knexService.getEntidadesSetoresRadius({
      key,
      latitude,
      longitude,
      radiusKm: 0,
    });
  }

  async create(dto: CreateBaseDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // check to see if category exists
      const category = await trxPrisma.baseCategory.findUnique({
        where: { uri: dto.baseCategoryUri },
      });
      if (!category) {
        throw new NotFoundException(
          `Base category ${dto.baseCategoryUri} not found!`,
        );
      }
      // base
      const dataBase = {
        uri: dto.uri?.trim()?.toLowerCase() || null,
        baseCategoryUri: dto.baseCategoryUri,
        title: dto.title?.trim() || null,
        titleSingular: dto.titleSingular?.trim() || null,
        description: dto.description?.trim() || null,
        icon: dto.icon?.trim() || null,
        color: dto.color?.trim() || null,
        order: dto.order,
        geoserverUri: dto.geoserverUri?.trim() || null,
        databaseSchema: dto.databaseSchema?.trim() || null,
        tableName: dto.tableName?.trim() || null,
        columnPk: dto.columnPk?.trim() || null,
        columnName: dto.columnName?.trim() || null,
        columnDescription: dto.columnDescription?.trim() || null,
        columnGeom: dto.columnGeom?.trim() || null,
        columnGeog: dto.columnGeog?.trim() || null,
        columnUf: dto.columnUf?.trim() || null,
        columnMunCd: dto.columnMunCd?.trim() || null,
        columnMunNm: dto.columnMunNm?.trim() || null,
      } as Base;
      // update order of all other bases in the same category
      await trxPrisma.base.updateMany({
        where: {
          baseCategoryUri: dto.baseCategoryUri,
          order: { gte: dto.order },
        },
        data: { order: { increment: 1 } },
      });
      const base = await trxPrisma.base.create({ data: dataBase });
      // base section
      for await (const dtoSection of dto.sections) {
        const dataSection = {
          baseUri: base.uri,
          order: dtoSection.order,
          title: dtoSection.title?.trim() || null,
        } as BaseSection;
        const baseSection = await trxPrisma.baseSection.create({
          data: dataSection,
        });
        // items
        const dataItems = dtoSection.items.map(
          (dtoItem) =>
            ({
              baseSectionId: baseSection.id,
              title: dtoItem.title?.trim() || null,
              description: dtoItem.description?.trim() || null,
              type: dtoItem.type?.trim() || null,
              column: dtoItem.column?.trim() || null,
              order: dtoItem.order,
            }) as BaseSectionItem,
        );
        await trxPrisma.baseSectionItem.createMany({ data: dataItems });
      }
      return trxPrisma.base.findUnique({
        where: { uri: base.uri },
        include: {
          category: true,
          sections: {
            include: {
              items: { orderBy: { order: 'asc' } },
            },
            orderBy: { order: 'asc' },
          },
        },
      });
    });
  }

  async findOne(uri: string) {
    const base = await this.prisma.base.findUnique({
      where: { uri },
      include: {
        category: true,
        sections: {
          include: {
            items: { orderBy: { order: 'asc' } },
          },
          orderBy: { order: 'asc' },
        },
      },
    });
    if (!base) {
      throw new NotFoundException(`Base ${uri} not found!`);
    }
    return base;
  }

  async findEntityByPk(uri: string, entityPk: string) {
    const base = await this.prisma.base.findUnique({
      where: { uri },
      include: {
        category: true,
        sections: {
          include: {
            items: { orderBy: { order: 'asc' } },
          },
          orderBy: { order: 'asc' },
        },
      },
    });
    if (!base) {
      throw new NotFoundException(`Base ${uri} not found!`);
    }
    const entity = await this.knexService.findOne({
      databaseSchema: base.databaseSchema,
      tableName: base.tableName,
      columnPk: base.columnPk,
      columnGeom: base.columnGeom,
      pk: entityPk,
    });
    if (!entity) {
      throw new NotFoundException(`Entity ${entityPk} not found!`);
    }
    return { base, entity };
  }

  async findAll() {
    return this.prisma.base.findMany({
      include: {
        category: true,
      },
      orderBy: [{ category: { order: 'asc' } }, { order: 'asc' }],
    });
  }

  async findSections(uri: string) {
    const base = await this.prisma.base.findUnique({
      where: { uri },
      include: {
        sections: {
          include: {
            items: { orderBy: { order: 'asc' } },
          },
          orderBy: { order: 'asc' },
        },
      },
    });
    if (!base) {
      throw new NotFoundException(`Base ${uri} not found!`);
    }
    return base.sections;
  }

  async remove(uri: string) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const base = await trxPrisma.base.findUnique({ where: { uri } });
      if (!base) {
        throw new NotFoundException(`Base ${uri} not found!`);
      }
      await trxPrisma.base.delete({ where: { uri: base.uri } });
      const bases = await trxPrisma.base.findMany({
        include: { category: true },
      });
      await this.knexService.refreshViewBuscas(bases);
      return base;
    });
  }
}
