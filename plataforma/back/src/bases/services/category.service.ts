import { Injectable, NotFoundException } from '@nestjs/common';
import { KnexService } from '../../knex/knex.service';
import { PrismaService } from '../../prisma.service';
import { CreateBaseCategoryDto, UpdataBaseCategoryDto } from '../dto';
import { Base, BaseSection, BaseSectionItem } from '@prisma/client';

@Injectable()
export class BasesCategoriesServices {
  constructor(private readonly prisma: PrismaService) {}

  async createCategory(dto: UpdataBaseCategoryDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const categoryLastOrder = await trxPrisma.baseCategory.findFirst({
        orderBy: { order: 'desc' },
      });
      const dataBaseCategory = {
        type: dto.type,
        uri: dto.uri?.trim(),
        name: dto.name?.trim(),
        icon: dto.icon?.trim() || null,
        color: dto.color?.trim() || null,
        order: dto.order || (categoryLastOrder?.order || -1) + 1,
      };
      await trxPrisma.baseCategory.updateMany({
        data: {
          order: {
            increment: 1,
          },
        },
        where: {
          order: { gte: dataBaseCategory.order },
        },
      });
      const baseCategory = await trxPrisma.baseCategory.create({
        data: dataBaseCategory,
      });
      return baseCategory;
    });
  }

  async removeCategory(categoryUri: string) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const baseCategory = await trxPrisma.baseCategory.findUnique({
        where: { uri: categoryUri },
      });
      if (!baseCategory) {
        throw new NotFoundException(`Category ${categoryUri} not found!`);
      }
      return trxPrisma.baseCategory.delete({
        where: { uri: baseCategory.uri },
      });
    });
  }

  async listCategories() {
    return this.prisma.baseCategory.findMany();
  }

  async updateCategory(categoryUri: string, dto: CreateBaseCategoryDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const baseCategory = await trxPrisma.baseCategory.findUnique({
        where: { uri: categoryUri },
      });
      if (!baseCategory) {
        throw new NotFoundException(`Category ${dto.uri} not found!`);
      }
      return trxPrisma.baseCategory.update({
        where: { uri: categoryUri },
        data: dto,
      });
    });
  }
}
