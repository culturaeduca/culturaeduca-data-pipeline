import { Type } from 'class-transformer';
import { IsOptional, IsString, Min } from 'class-validator';

export class PaginationQueryDto {
  @IsOptional()
  @Type(() => Number)
  @Min(1)
  page?: number | null;

  @IsOptional()
  @Type(() => Number)
  @Min(1)
  perPage?: number | null;

  @IsOptional()
  @IsString()
  search?: string | null;
}
