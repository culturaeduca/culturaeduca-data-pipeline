import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import { ProjetoEnderecoReferenciaEnum } from '../projetos.constants';

class CreateEnderecoDto {
  @IsNumber()
  @Min(-90)
  @Max(90)
  latitude: number;

  @IsNumber()
  @Min(-180)
  @Max(180)
  longitude: number;

  @IsOptional()
  @IsString()
  logradouro: string | null;

  @IsOptional()
  @IsString()
  numero: string | null;

  @IsOptional()
  @IsString()
  bairro: string | null;

  @IsOptional()
  @IsString()
  complemento: string | null;

  @IsOptional()
  @IsString()
  codigoPostal: string | null;

  @IsOptional()
  @IsString()
  municipioCodigo: string | null;

  @IsOptional()
  @IsString()
  municipioNome: string | null;

  @IsOptional()
  @IsString()
  ufSigla: string | null;

  @IsOptional()
  @IsString()
  ufNome: string | null;
}

export class CreateProjetoDto {
  @IsOptional()
  @IsInt()
  projetoId: number | null;

  @IsString()
  @IsNotEmpty()
  titulo: string;

  @IsOptional()
  @IsString()
  descricao: string | null;

  @IsOptional()
  @IsString()
  vinculos: string | null;

  @IsOptional()
  @IsString()
  objetivo: string | null;

  @IsArray()
  @IsInt({ each: true })
  opcoesIds: number[];

  @IsOptional()
  @IsEnum(ProjetoEnderecoReferenciaEnum)
  enderecoReferencia: ProjetoEnderecoReferenciaEnum | null;

  @IsOptional()
  @ValidateNested()
  @Type(() => CreateEnderecoDto)
  endereco?: CreateEnderecoDto | null;
}
