import { Controller, Delete, Get, Param, ParseIntPipe } from '@nestjs/common';
import { ProjetosService } from './projetos.service';
import { Public } from '../public.decorator';

@Controller('projetos')
export class ProjetosController {
  constructor(private readonly projetosService: ProjetosService) {}

  @Public()
  @Get()
  findAll() {
    return this.projetosService.findAll();
  }

  @Public()
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.projetosService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.projetosService.remove(id);
  }
}
