import { Module } from '@nestjs/common';
import { ProjetosService } from './projetos.service';
import { ProjetosController } from './projetos.controller';
import { PrismaService } from '../prisma.service';

@Module({
  controllers: [ProjetosController],
  providers: [ProjetosService, PrismaService],
  exports: [ProjetosService],
})
export class ProjetosModule {}
