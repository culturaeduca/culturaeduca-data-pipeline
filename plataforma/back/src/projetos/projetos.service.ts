import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { CreateProjetoDto } from './dto';
import { ProjetoEnderecoReferencia } from '@prisma/client';

@Injectable()
export class ProjetosService {
  constructor(private readonly prisma: PrismaService) {}

  async create(dto: CreateProjetoDto, agenteId: number, usuarioId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      let dataEndereco;
      // copia endereco do agente
      if (dto.enderecoReferencia === ProjetoEnderecoReferencia.AGENTE) {
        const agente = await trxPrisma.agente.findUnique({
          where: { id: agenteId },
          include: {
            endereco: true,
          },
        });
        if (!agente.endereco) {
          throw new BadRequestException('Agente não tem endereço cadastrado!');
        }
        dataEndereco = {
          latitude: agente.endereco.latitude,
          longitude: agente.endereco.longitude,
          logradouro: agente.endereco.logradouro,
          numero: agente.endereco.numero,
          bairro: agente.endereco.bairro,
          complemento: agente.endereco.complemento,
          codigoPostal: agente.endereco.codigoPostal,
          municipioCodigo: agente.endereco.municipioCodigo,
          municipioNome: agente.endereco.municipioNome,
          ufSigla: agente.endereco.ufSigla,
          ufNome: agente.endereco.ufNome,
        };
      }
      // cria novo endereco
      else {
        dataEndereco = {
          ...dto.endereco,
        };
      }
      // endereco
      const endereco = await trxPrisma.endereco.create({
        data: dataEndereco,
      });
      // projeto
      const dataProjeto = {
        agenteId,
        enderecoId: endereco.id,
        titulo: dto.titulo?.trim() || null,
        descricao: dto.descricao?.trim() || null,
        vinculos: dto.vinculos?.trim() || null,
        objetivo: dto.objetivo?.trim() || null,
        createdBy: usuarioId,
        updatedBy: usuarioId,
      };
      const projeto = await trxPrisma.projeto.create({
        data: dataProjeto,
      });
      // opcoes
      const dataProjetoOpcoes = dto.opcoesIds.map((opcaoId) => ({
        opcaoId,
        projetoId: projeto.id,
      }));
      await trxPrisma.projetoOpcao.createMany({
        data: dataProjetoOpcoes,
      });
      // retorna projeto completo
      return trxPrisma.projeto.findUnique({
        where: { id: projeto.id },
        include: {
          endereco: true,
          opcoes: { include: { opcao: true } },
        },
      });
    });
  }

  findAll() {
    return this.prisma.projeto.findMany();
  }

  findOne(id: number) {
    const projeto = this.prisma.projeto.findUnique({ where: { id } });
    if (!projeto) {
      throw new NotFoundException('Projeto não encontrado');
    }
    return projeto;
  }

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const projeto = await trxPrisma.projeto.findUnique({ where: { id } });
      if (!projeto) {
        throw new NotFoundException('Atividade não encontrada');
      }
      await trxPrisma.projeto.delete({ where: { id: projeto.id } });
      return projeto;
    });
  }
}
