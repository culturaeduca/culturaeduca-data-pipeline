import { APP_GUARD } from '@nestjs/core';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Inject, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { RedisClient } from 'redis';
import * as RedisStore from 'connect-redis';
import * as passport from 'passport';
import * as session from 'express-session';
import { BasesModule } from './bases/bases.module';
import { KnexModule } from './knex/knex.module';
import { LoggedInGuard } from './auth/logged-in.guard';
import { OpcoesModule } from './opcoes/opcoes.module';
import { REDIS } from './redis/redis.constants';
import { SetoresModule } from './setores/setores.module';
import configuration from './config/configuration';
import { AuthModule } from './auth/auth.module';
import { RedisModule } from './redis/redis.module';
import { UsuariosModule } from './usuarios/usuarios.module';
import { AgentesColetivosModule } from './agentes-coletivos/agentes-coletivos.module';
import { AgentesModule } from './agentes/agentes.module';
import { ProjetosModule } from './projetos/projetos.module';
import { AtividadesModule } from './atividades/atividades.module';
import { MessageModule } from './message/message.module';
import { StorageModule } from './storage/storage.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      load: [configuration],
    }),
    KnexModule,
    RedisModule,
    AuthModule,
    BasesModule,
    SetoresModule,
    OpcoesModule,
    UsuariosModule,
    AgentesColetivosModule,
    AgentesModule,
    ProjetosModule,
    AtividadesModule,
    MessageModule,
    StorageModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: LoggedInGuard,
    },
  ],
})
export class AppModule implements NestModule {
  constructor(
    @Inject(REDIS) private readonly redis: RedisClient,
    private configService: ConfigService,
  ) {}
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        session({
          ...this.configService.get<session.SessionOptions>('session'),
          store: new (RedisStore(session))({
            client: this.redis,
            logErrors: true,
          }),
        }),
        passport.initialize(),
        passport.session(),
      )
      .forRoutes('*');
  }
}
