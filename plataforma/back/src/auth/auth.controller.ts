import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';

import { AuthService } from './auth.service';

import { LocalGuard } from './local.guard';
import { Public } from '../public.decorator';
import {
  SignUpAuthDto,
  RecoverPasswordDto,
  ResetPasswordDto,
  ResendValidateTokenDto,
} from './dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalGuard)
  @Public()
  @Post('sign_in')
  @HttpCode(HttpStatus.OK)
  signIn(@Request() req) {
    return req.user;
  }

  @Public()
  @Post('sign_out')
  @HttpCode(HttpStatus.NO_CONTENT)
  signOut(@Request() req) {
    return req.logout(() => {});
  }

  @Public()
  @Post('sign_up')
  signUp(@Body() dto: SignUpAuthDto) {
    return this.authService.signUp(dto);
  }

  @Get('check_session')
  checkSession(@Request() req) {
    return req.user;
  }

  @Public()
  @Post('resend_verify_token')
  resendVerifyToken(@Body() dto: ResendValidateTokenDto) {
    return this.authService.resendValidationEmail(dto);
  }
  @Public()
  @Post('verify_token/:token')
  verifyToken(@Param('token') token: string) {
    return this.authService.verifyToken(token);
  }

  @Public()
  @Post('recover_password')
  send_reset_possword_token(@Body() dto: RecoverPasswordDto) {
    return this.authService.sendResetPassword(dto);
  }
  @Public()
  @Post('reset_password/:token')
  reset_password(@Param('token') token: string, @Body() dto: ResetPasswordDto) {
    return this.authService.setResetPassword(token, dto);
  }
}
