import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SignInAuthDto {
  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  senha: string;
}

export const USUARIO_NAO_CADASTRADO_MESSAGE =
  'Não existe um usuário registrado com esse e-mail';

export const USUARIO_PASSWORD_SENHA_INCORRETO_MESSAGE =
  'Email e/ou senha incorretos';

export const USUARIO_NAO_VALIDO_MESSAGE =
  'Email não foi validado ainda. Verifique a sua caixa de entrada';
