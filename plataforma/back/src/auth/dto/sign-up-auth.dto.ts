import { IsEmail, IsString, MinLength } from 'class-validator';

export class SignUpAuthDto {
  @IsString()
  nome: string;

  @IsEmail()
  email: string;

  @IsString()
  @MinLength(6)
  senha: string;
}
