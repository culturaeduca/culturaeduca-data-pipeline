import { IsNotEmpty, IsEmail } from 'class-validator';

export class ResendValidateTokenDto {
  @IsNotEmpty()
  @IsEmail()
  email: string;
}
