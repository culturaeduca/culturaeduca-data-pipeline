import { IsNotEmpty, IsString } from 'class-validator';

export class VerifyTokenDto {
  @IsString()
  @IsNotEmpty()
  token: string;
}

export const USUARIO_JA_VALIDOU = 'Usuário já confirmou o email';
