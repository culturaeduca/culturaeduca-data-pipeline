import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { AuthController } from './auth.controller';
import { AuthSerializer } from './serialization.provider';
import { PrismaService } from '../prisma.service';
import { MessageService } from '../message/message.service';

@Module({
  imports: [PassportModule.register({ session: true })],
  providers: [
    AuthSerializer,
    AuthService,
    LocalStrategy,
    PrismaService,
    MessageService,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
