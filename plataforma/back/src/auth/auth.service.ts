import {
  Injectable,
  UnauthorizedException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { MessageService } from '../message/message.service';
import {
  SignInAuthDto,
  SignUpAuthDto,
  RecoverPasswordDto,
  ResetPasswordDto,
  ResendValidateTokenDto,
  USUARIO_NAO_VALIDO_MESSAGE,
  USUARIO_PASSWORD_SENHA_INCORRETO_MESSAGE,
  USUARIO_JA_VALIDOU,
} from './dto';
import { hash, verify } from 'argon2';
import { TipoAgente } from '@prisma/client';
import { randomBytes } from 'crypto';

@Injectable()
export class AuthService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly messageService: MessageService,
  ) {}

  async sendResetPassword(dto: RecoverPasswordDto) {
    const usuario = await this.prisma.usuario.findFirst({
      where: {
        email: { equals: dto.email?.trim(), mode: 'insensitive' },
      },
    });
    if (!usuario) throw new NotFoundException('Usuário não encontrado');

    const tokenTrocaSenha = randomBytes(16).toString('hex');
    const { email } = usuario;

    await this.messageService.sendEmail({
      subject: 'Instruções para Alteração de Senha',
      to: [{ name: usuario.nome ?? 'Usuário', address: email }],
      html: this.messageService.getContentResetPassword({
        tokenTrocaSenha,
        email,
      }),
      text: '',
    });

    const updatedUser = await this.prisma.usuario.update({
      where: { id: usuario.id },
      data: {
        tokenTrocaSenha,
      },
    });
    delete updatedUser.senha;
    return updatedUser;
  }

  async setResetPassword(token: string, dto: ResetPasswordDto) {
    const usuario = await this.prisma.usuario.findFirst({
      where: {
        tokenTrocaSenha: token,
      },
    });

    if (!usuario || usuario.email !== dto.email)
      throw new NotFoundException('Usuário não encontrado');

    const updateUser = await this.prisma.usuario.update({
      where: { id: usuario.id },
      data: {
        trocaSenha: new Date(Date.now()),
        senha: await hash(dto.senha),
        // tokenTrocaSenha: null,
      },
    });
    delete updateUser.senha;
    return updateUser;
  }

  async validateUsuario(dto: SignInAuthDto): Promise<any> {
    const email = dto.email?.trim();
    const usuario = await this.prisma.usuario.findFirst({
      where: {
        email: { equals: email, mode: 'insensitive' },
      },
    });
    // usuario não encontrado
    if (!usuario) {
      throw new UnauthorizedException(USUARIO_PASSWORD_SENHA_INCORRETO_MESSAGE);
    }
    // senha incorreta
    if (!(await verify(usuario.senha, dto.senha))) {
      throw new UnauthorizedException(USUARIO_PASSWORD_SENHA_INCORRETO_MESSAGE);
    }
    // email não validado (TODO: implementar)
    if (!usuario.validacaoEmail) {
      throw new UnauthorizedException(USUARIO_NAO_VALIDO_MESSAGE);
    }
    // aceite de termos (TODO: implementar)
    // if (!usuario.validacaoEmail) {
    //   throw new UnauthorizedException(
    //     'É necessário aceitar os termos primeiro',
    //   );
    // }
    delete usuario.senha;
    return usuario;
  }

  async createAdmin(dto: SignUpAuthDto) {
    const nome = dto.nome?.trim();
    const email = dto.email?.trim()?.toLowerCase();
    const senha = await hash(dto.senha);
    const tokenValidacaoEmail = randomBytes(16).toString('hex');
    try {
      return this.prisma.$transaction(async (trxPrisma) => {
        const usuario = await trxPrisma.usuario.create({
          data: {
            nome,
            email,
            senha,
            tokenValidacaoEmail,
            canDefineBase: true,
            validacaoEmail: new Date(Date.now()),
          },
        });
        return usuario;
      });
    } catch (e) {
      console.log(e);
      process.exit(0);
    }
  }

  async listAdmins() {
    return this.prisma.usuario.findMany({
      select: {
        id: true,
        nome: true,
        email: true,
        createdAt: true,
        updatedAt: true,
      },
      where: {
        canDefineBase: true,
      },
    });
  }

  async deleteAdmin(id: string) {
    const base = 10;
    const parsedId = parseInt(id, base);

    const usuario = await this.prisma.usuario.findUnique({
      where: {
        id: parsedId,
      },
    });
    return usuario;
  }

  async signUp(dto: SignUpAuthDto) {
    const nome = dto.nome?.trim();
    const email = dto.email?.trim()?.toLowerCase();
    const senha = await hash(dto.senha);
    const tokenValidacaoEmail = randomBytes(16).toString('hex');

    return this.prisma.$transaction(async (trxPrisma) => {
      const usuario = await trxPrisma.usuario.create({
        data: {
          nome,
          email,
          senha,
          tokenValidacaoEmail,
        },
      });

      const agente = await trxPrisma.agente.create({
        data: {
          tipo: TipoAgente.INDIVIDUAL,
          perfilPublico: false,
          nome,
          email,
        },
      });
      await trxPrisma.agenteIndividual.create({
        data: {
          agenteId: agente.id,
          usuarioId: usuario.id,
          camposPublicos: [
            'email',
            'website',
            'telefone1',
            'telefone2',
            'dataNascimento',
            'identidadeGenero',
            'identidadeEtnicoCultural',
            'racaCorIbge',
            'nivelEscolaridade',
            'ocupacao',
          ],
        },
      });
      delete usuario.senha;
      await this.messageService.sendEmail({
        subject: 'Confirmação de email plataforma CulturaEduca',
        to: [{ name: usuario.nome ?? 'Usuário', address: usuario.email }],
        html: this.messageService.getContentValidateUser(usuario),
        text: '',
      });

      return usuario;
    });
  }

  async resendValidationEmail(dto: ResendValidateTokenDto) {
    const usuario = await this.prisma.usuario.findFirst({
      where: {
        email: { equals: dto.email?.trim(), mode: 'insensitive' },
      },
    });
    if (!usuario) throw new NotFoundException('Usuário não encontrado');

    try {
      await this.messageService.sendEmail({
        subject: 'Confirmação de email plataforma CulturaEduca',
        to: [{ name: usuario.nome ?? 'Usuário', address: usuario.email }],
        html: this.messageService.getContentValidateUser(usuario),
        text: '',
      });
      delete usuario.senha;
      return usuario;
    } catch (err) {
      throw new UnprocessableEntityException(
        'Não foi possível enviar o e-mail com o token de validação',
      );
    }
  }

  async getUserTokenValidacaoByEmail(dto: RecoverPasswordDto) {
    const usuario = await this.prisma.usuario.findUnique({
      where: {
        email: dto.email,
      },
    });
    return usuario?.tokenValidacaoEmail ? usuario : '';
  }

  async verifyToken(token: string) {
    const usuario = await this.prisma.usuario.findFirst({
      where: {
        tokenValidacaoEmail: token,
      },
    });

    if (!usuario) throw new NotFoundException('Usuário não encontrado');

    if (usuario.validacaoEmail)
      throw new UnprocessableEntityException(USUARIO_JA_VALIDOU);

    return await this.prisma.usuario.update({
      where: { id: usuario.id },
      data: {
        validacaoEmail: new Date(Date.now()),
      },
    });
  }
}
