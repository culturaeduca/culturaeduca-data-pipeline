import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
// import { Usuario } from '@prisma/client';
import { IS_PUBLIC_KEY } from '../public.decorator';
import { Roles } from '../roles.decorator';

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }
    const req = context.switchToHttp().getRequest();
    // const usuario = req.user as Usuario;
    // (TODO: implementar)
    // if (!!usuario.validacaoEmail && !!usuario.aceiteTermos) {
    //   return false;
    // }

    const roles = this.reflector.get(Roles, context.getHandler());
    if (!req.isAuthenticated()) return false;
    if (!roles) return true;
    return req.user?.canDefineBase;
    // TODO: Implement more roles, and change this verification
  }
}
