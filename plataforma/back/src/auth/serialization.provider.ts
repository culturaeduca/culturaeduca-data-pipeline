import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';
import { Usuario } from '@prisma/client';
import { PrismaService } from '../prisma.service';

@Injectable()
export class AuthSerializer extends PassportSerializer {
  constructor(private readonly prisma: PrismaService) {
    super();
  }

  serializeUser(
    user: Usuario,
    done: (err: Error, user: { id: number }) => void,
  ) {
    done(null, { id: user.id });
  }

  async deserializeUser(
    payload: { id: number },
    done: (err: Error, user: Omit<Usuario, 'senha'>) => void,
  ) {
    const user = await this.prisma.usuario.findUnique({
      where: { id: payload.id },
    });
    delete user?.senha;
    done(null, user);
  }
}
