import { Inject, Module, OnApplicationShutdown } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as Redis from 'redis';

import { REDIS } from './redis.constants';

@Module({
  providers: [
    {
      provide: REDIS,
      useFactory: (configService: ConfigService) =>
        Redis.createClient(configService.get('redis')),
      inject: [ConfigService],
    },
  ],
  exports: [REDIS],
})
export class RedisModule implements OnApplicationShutdown {
  constructor(@Inject(REDIS) private redisClient: Redis.RedisClient) {}

  onApplicationShutdown() {
    this.redisClient.quit();
  }
}
