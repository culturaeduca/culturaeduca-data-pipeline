import { Module } from '@nestjs/common';
import { StorageService } from './storage.service';
import { StorageController } from './storage.controller';

import { MulterModule } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MulterModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        ...configService.get<MulterOptions>('multer'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [StorageController],
  providers: [StorageService],
})
export class StorageModule {}
