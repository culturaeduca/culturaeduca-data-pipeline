const bucketNames = ['usuariosprofile', 'anotherbucket'] as const;
export type BucketNames = (typeof bucketNames)[number];

const folders = ['picture', 'anotherfolde'] as const;
export type FoldersNames = (typeof folders)[number];
