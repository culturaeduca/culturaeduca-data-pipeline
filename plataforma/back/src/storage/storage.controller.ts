import { StorageService } from './storage.service';
import {
  Controller,
  Get,
  Post,
  UseInterceptors,
  UploadedFile,
  Delete,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { User } from '../user.decorator';

@Controller('storage')
export class StorageController {
  constructor(private readonly storageService: StorageService) {}

  @Post('profile')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @User('id') userId: number,
  ) {
    return this.storageService.putObject(
      file,
      userId,
      'usuariosprofile',
      'picture',
    );
  }

  @Get('profile')
  getFile(@User('id') userId: string) {
    return this.storageService.getObject(
      `${userId}`,
      'usuariosprofile',
      'picture',
    );
  }

  @Delete('profile')
  delFile(@User('id') userId: string) {
    return this.storageService.delObject(userId, 'usuariosprofile', 'picture');
  }
}
