import { Injectable, NotFoundException } from '@nestjs/common';
import * as Minio from 'minio';
import * as fs from 'fs/promises';
import { ConfigService } from '@nestjs/config';
import { BucketNames, FoldersNames } from './dto/index.dto';
@Injectable()
export class StorageService {
  private minioClient: Minio.Client;

  constructor(private configService: ConfigService) {
    const { client } = this.configService.get('minio');
    this.minioClient = new Minio.Client(client);
  }

  async createBucketIfNotExists(bucketName: BucketNames) {
    const exists = await this.minioClient.bucketExists(bucketName);
    if (exists) {
      return await this.minioClient.listBuckets();
    }
    await this.minioClient.makeBucket(bucketName);
    return await this.minioClient.listBuckets();
  }

  async deleteBucket(bucketName: BucketNames) {
    const exists = await this.minioClient.bucketExists(bucketName);
    if (exists) {
      return await this.minioClient.listBuckets();
    }
    await this.minioClient.removeBucket(bucketName);
    return await this.minioClient.listBuckets();
  }

  async putObject(
    file: Express.Multer.File,
    userId: number,
    bucketName: BucketNames,
    folderPath: FoldersNames,
  ) {
    const metadata = {
      originalName: userId,
    };
    const object = await this.minioClient.fPutObject(
      bucketName,
      `${folderPath}/${userId}`,
      file.path,
      metadata,
    );
    fs.unlink(file.path);
    return object;
  }

  async getObject(
    objectName: string,
    bucketName: BucketNames,
    folderPath: FoldersNames,
  ) {
    const filePath = `${folderPath}/${objectName}`;

    try {
      await this.minioClient.statObject(bucketName, filePath);
      return await this.minioClient.presignedGetObject(
        bucketName,
        filePath,
        24 * 60 * 60,
      );
    } catch (e) {
      throw new NotFoundException(e);
    }
  }

  async delObject(
    objectName: string,
    bucketName: BucketNames,
    folderPath: FoldersNames,
  ) {
    const filePath = `${folderPath}/${objectName}`;
    return await this.minioClient.removeObject(bucketName, filePath);
  }
}
