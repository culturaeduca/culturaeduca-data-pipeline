import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import * as fs from 'fs';
import helmet from 'helmet';

import { AppModule } from './app.module';

const httpsOptions =
  process.env.NODE_ENV === 'production'
    ? {
        key: fs.readFileSync(process.env.SSL_KEY),
        cert: fs.readFileSync(process.env.SSL_CERT),
      }
    : {};

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    ...(process.env.NODE_ENV === 'production' ? { httpsOptions } : {}),
  });

  const configService = app.get(ConfigService);

  app.use(helmet());
  app.enableCors({
    credentials: true,
    origin: configService.get<string[]>('enableCorsOrigin'),
  });

  // Global Validation Pipe
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

  await app.listen(configService.get<number>('appPort'));
}

bootstrap();
