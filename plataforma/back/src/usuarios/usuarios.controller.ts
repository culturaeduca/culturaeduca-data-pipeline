import {
  Controller,
  Body,
  Patch,
  Get,
  Post,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import {
  CreateFavoritoDto,
  UpdateAtuacaoUsuarioDto,
  UpdateIdentificacaoUsuarioDto,
  UpdateLocalizacaoUsuarioDto,
  UpdatePreferenciasUsuarioDto,
} from './dto';
import { User } from '../user.decorator';
import { CreateAtividadeDto } from '../atividades/dto';
import { CreateProjetoDto } from '../projetos/dto';

@Controller('usuarios')
export class UsuariosController {
  constructor(private readonly usuariosService: UsuariosService) {}

  // POST

  @Post('meu_perfil/favoritos')
  createFavorito(
    @Body() dto: CreateFavoritoDto,
    @User('id') usuarioId: number,
  ) {
    return this.usuariosService.createFavorito(usuarioId, dto);
  }

  @Post('meu_perfil/atividades')
  createAtividade(
    @Body() dto: CreateAtividadeDto,
    @User('id') usuarioId: number,
  ) {
    return this.usuariosService.createAtividade(usuarioId, dto);
  }

  @Post('meu_perfil/projetos')
  createProjeto(@Body() dto: CreateProjetoDto, @User('id') usuarioId: number) {
    return this.usuariosService.createProjeto(usuarioId, dto);
  }

  // GET

  @Get('meu_perfil/favoritos/:base_uri/:pk')
  findFavorito(
    @Param('base_uri') baseUri: string,
    @Param('pk') pk: string,
    @User('id') usuarioId: number,
  ) {
    return this.usuariosService.findFavorito(baseUri, pk, usuarioId);
  }

  @Get('meu_perfil/favoritos')
  findFavoritos(@User('id') usuarioId: number) {
    return this.usuariosService.findFavoritos(usuarioId);
  }

  @Get('meu_perfil/atividades')
  findAtividades(@User('id') usuarioId: number) {
    return this.usuariosService.findAtividades(usuarioId);
  }

  @Get('meu_perfil/endereco')
  findEndereco(@User('id') usuarioId: number) {
    return this.usuariosService.findEndereco(usuarioId);
  }

  @Get('meu_perfil/projetos')
  findProjetos(@User('id') usuarioId: number) {
    return this.usuariosService.findProjetos(usuarioId);
  }

  @Get('meu_perfil')
  findOne(@User('id') usuarioId: number) {
    return this.usuariosService.findOne(usuarioId);
  }

  // PATCH

  @Patch('meu_perfil/atuacao')
  updateAtuacao(
    @User('id') usuarioId: number,
    @Body() dto: UpdateAtuacaoUsuarioDto,
  ) {
    return this.usuariosService.updateAtuacao(usuarioId, dto);
  }

  @Patch('meu_perfil/identificacao')
  updateIdentificacao(
    @User('id') usuarioId: number,
    @Body() dto: UpdateIdentificacaoUsuarioDto,
  ) {
    return this.usuariosService.updateIdentificacao(usuarioId, dto);
  }

  @Patch('meu_perfil/localizacao')
  updateLocalizacao(
    @User('id') usuarioId: number,
    @Body() dto: UpdateLocalizacaoUsuarioDto,
  ) {
    return this.usuariosService.updateLocalizacao(usuarioId, dto);
  }

  @Patch('meu_perfil/preferencias')
  updatePreferencias(
    @User('id') usuarioId: number,
    @Body() dto: UpdatePreferenciasUsuarioDto,
  ) {
    return this.usuariosService.updatePreferencias(usuarioId, dto);
  }

  // DELETE

  @Delete('meu_perfil/favoritos/:favorito_id')
  removeFavorito(
    @Param('favorito_id', ParseIntPipe) favoritoId: number,
    @User('id') usuarioId: number,
  ) {
    return this.usuariosService.removeFavorito(favoritoId, usuarioId);
  }
}
