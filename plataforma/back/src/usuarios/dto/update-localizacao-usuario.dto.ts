import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';

class CreateEnderecoDto {
  @IsNumber()
  @Min(-90)
  @Max(90)
  latitude: number;

  @IsNumber()
  @Min(-180)
  @Max(180)
  longitude: number;

  @IsOptional()
  @IsString()
  logradouro: string | null;

  @IsOptional()
  @IsString()
  numero: string | null;

  @IsOptional()
  @IsString()
  bairro: string | null;

  @IsOptional()
  @IsString()
  complemento: string | null;

  @IsOptional()
  @IsString()
  codigoPostal: string | null;

  @IsOptional()
  @IsString()
  municipioCodigo: string | null;

  @IsOptional()
  @IsString()
  municipioNome: string | null;

  @IsOptional()
  @IsString()
  ufSigla: string | null;

  @IsOptional()
  @IsString()
  ufNome: string | null;
}

export class UpdateLocalizacaoUsuarioDto {
  @IsBoolean()
  enderecoPublico: boolean;

  @ValidateNested()
  @Type(() => CreateEnderecoDto)
  endereco: CreateEnderecoDto;
}
