import { IsEmail, IsString, MinLength, IsDate } from 'class-validator';

export class UpdateTokenDto {
  @IsString()
  id: number;

  @IsString()
  nome: string;

  @IsEmail()
  email: string;

  @IsString()
  @MinLength(6)
  senha: string;

  @IsDate()
  validacaoEmail: Date;
}
