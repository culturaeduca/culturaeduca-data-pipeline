import { IsArray, IsInt, IsOptional, IsString } from 'class-validator';

export class UpdateAtuacaoUsuarioDto {
  @IsOptional()
  @IsInt()
  abrangenciaId: number | null;

  @IsOptional()
  @IsString()
  objetivo: string | null;

  @IsOptional()
  @IsString()
  atuacao: string | null;

  @IsInt()
  atuaDesde: number | null;

  @IsArray()
  @IsInt({ each: true })
  areasAtuacaoIds: number[];

  @IsArray()
  @IsString({ each: true })
  areasAtuacaoOutros: string[];

  @IsArray()
  @IsInt({ each: true })
  areasEstudoPesquisaIds: number[];

  @IsArray()
  @IsString({ each: true })
  areasEstudoPesquisaOutros: string[];

  @IsArray()
  @IsInt({ each: true })
  publicosFocaisIds: number[];

  @IsArray()
  @IsString({ each: true })
  publicosFocaisOutros: string[];

  @IsArray()
  @IsInt({ each: true })
  tiposEspacosIds: number[];

  @IsArray()
  @IsString({ each: true })
  tiposEspacosOutros: string[];
}
