import { IsBoolean } from 'class-validator';

export class UpdatePreferenciasUsuarioDto {
  @IsBoolean()
  perfilPublico: boolean;
}
