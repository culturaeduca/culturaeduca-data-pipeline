import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SendTokenEmailDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  nome: string;

  @IsString()
  @IsNotEmpty()
  tokenValidacaoEmail: string;
}
