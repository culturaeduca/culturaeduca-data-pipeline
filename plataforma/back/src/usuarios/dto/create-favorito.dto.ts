import { IsOptional, IsString } from 'class-validator';

export class CreateFavoritoDto {
  @IsString()
  baseUri: string;

  @IsString()
  pk: string;

  @IsString()
  nome: string;

  @IsOptional()
  @IsString()
  descricao: string | null;
}
