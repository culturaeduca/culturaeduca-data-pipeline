import {
  IsArray,
  IsEmail,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateIdentificacaoUsuarioDto {
  @IsArray()
  @IsString({ each: true })
  camposPublicos: string[];

  @IsString()
  @IsNotEmpty()
  nome: string;

  @IsOptional()
  @IsEmail()
  email: string | null;

  @IsOptional()
  @IsString()
  website: string | null;

  @IsOptional()
  @IsString()
  telefone1: string | null;

  @IsOptional()
  @IsString()
  telefone2: string | null;

  @IsOptional()
  @IsString()
  dataNascimento: string | null;

  @IsOptional()
  @IsInt()
  identidadeEtnicoCulturalId: number | null;

  @IsOptional()
  @IsString()
  identidadeEtnicoCulturalOutro: string | null;

  @IsOptional()
  @IsInt()
  identidadeGeneroId: number | null;

  @IsOptional()
  @IsInt()
  nivelEscolaridadeId: number | null;

  @IsOptional()
  @IsInt()
  ocupacaoId: number | null;

  @IsOptional()
  @IsString()
  ocupacaoOutro: string | null;

  @IsOptional()
  @IsInt()
  racaCorIbgeId: number | null;
}
