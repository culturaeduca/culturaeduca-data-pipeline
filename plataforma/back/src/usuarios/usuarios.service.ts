import {
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import {
  CreateFavoritoDto,
  UpdateAtuacaoUsuarioDto,
  UpdateIdentificacaoUsuarioDto,
  UpdateLocalizacaoUsuarioDto,
  UpdatePreferenciasUsuarioDto,
} from './dto';
import { PrismaService, PrismaTransaction } from '../prisma.service';
import { CreateProjetoDto } from '../projetos/dto';
import { CreateAtividadeDto } from '../atividades/dto';
import { ProjetosService } from '../projetos/projetos.service';
import { AtividadesService } from '../atividades/atividades.service';
import { Favorito } from '@prisma/client';

@Injectable()
export class UsuariosService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly atividadesService: AtividadesService,
    private readonly projetosService: ProjetosService,
  ) {}

  async createAtividade(id: number, dto: CreateAtividadeDto) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
      include: {
        agenteIndividual: true,
      },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    const agenteId = usuario.agenteIndividual?.agenteId || null;
    return this.atividadesService.create(dto, agenteId, usuario.id);
  }

  async createFavorito(id: number, dto: CreateFavoritoDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const usuario = await trxPrisma.usuario.findUnique({
        where: { id },
      });
      if (!usuario) {
        throw new NotFoundException('Usuário não encontrado');
      }
      const dataFavorito = {
        usuarioId: usuario.id,
        baseUri: dto?.baseUri?.trim() || null,
        pk: dto?.pk?.trim() || null,
        nome: dto?.nome?.trim() || null,
        descricao: dto?.descricao?.trim() || null,
      } as Favorito;
      const existingFavorito = await trxPrisma.favorito.findFirst({
        where: {
          usuarioId: usuario.id,
          baseUri: dataFavorito.baseUri,
          pk: dataFavorito.pk,
        },
      });
      const favorito = await trxPrisma.favorito.upsert({
        where: { id: existingFavorito?.id || 0 },
        create: dataFavorito,
        update: dataFavorito,
      });
      return trxPrisma.favorito.findUnique({
        where: { id: favorito.id },
        include: { base: { include: { category: true } } },
      });
    });
  }

  async createProjeto(id: number, dto: CreateProjetoDto) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
      include: {
        agenteIndividual: true,
      },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    const agenteId = usuario.agenteIndividual?.agenteId || null;
    return this.projetosService.create(dto, agenteId, usuario.id);
  }

  async findFavorito(baseUri: string, pk: string, id: number) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    const favorito = await this.prisma.favorito.findFirst({
      where: {
        usuarioId: usuario.id,
        baseUri,
        pk,
      },
    });
    if (!favorito) {
      throw new NotFoundException('Favorito não encontrado');
    }
    return favorito;
  }

  async findFavoritos(id: number) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
      include: {
        favoritos: {
          include: {
            base: {
              include: {
                category: true,
              },
            },
          },
          orderBy: { createdAt: 'desc' },
        },
      },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    return usuario.favoritos;
  }

  async findAtividades(id: number) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
      include: {
        agenteIndividual: true,
      },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    const agenteId = usuario.agenteIndividual?.agenteId || null;
    return this.prisma.atividade.findMany({
      where: {
        agenteId,
      },
      include: {
        endereco: true,
        projeto: true,
        opcoes: { include: { opcao: true } },
      },
    });
  }

  async findEndereco(id: number) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
      include: {
        agenteIndividual: {
          include: {
            agente: true,
          },
        },
      },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    const enderecoId = usuario.agenteIndividual?.agente?.enderecoId || null;
    const endereco = await this.prisma.endereco.findUnique({
      where: {
        id: enderecoId,
      },
    });
    if (!endereco) {
      throw new NotFoundException('Endereço não encontrado');
    }
    return endereco;
  }

  async findProjetos(id: number) {
    const usuario = await this.prisma.usuario.findUnique({
      where: { id },
      include: {
        agenteIndividual: true,
      },
    });
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    const agenteId = usuario.agenteIndividual?.agenteId || null;
    return this.prisma.projeto.findMany({
      where: {
        agenteId,
      },
      include: {
        endereco: true,
        atividades: {
          include: { endereco: true, opcoes: { include: { opcao: true } } },
        },
        opcoes: { include: { opcao: true } },
      },
    });
  }

  async updateAtuacao(id: number, dto: UpdateAtuacaoUsuarioDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // busca usuario
      const usuario = await this._getUsuarioCompleto(id, trxPrisma);
      if (!usuario) {
        throw new NotFoundException('Usuário não encontrado');
      }
      // atualiza agenteOpcoes
      // TODO verificar se opções são da categoria correta
      const agenteId = usuario.agenteIndividual.agente.id;
      await trxPrisma.agenteOpcao.deleteMany({
        where: { agenteId },
      });
      const dataAgenteOpcoes = [
        ...dto.areasAtuacaoIds,
        ...dto.areasEstudoPesquisaIds,
        ...dto.publicosFocaisIds,
        ...dto.tiposEspacosIds,
      ].map((opcaoId) => ({ agenteId, opcaoId }));
      await trxPrisma.agenteOpcao.createMany({
        data: dataAgenteOpcoes,
      });
      // atualiza agente
      const dataAgente = {
        abrangenciaId: dto.abrangenciaId,
        objetivo: dto.objetivo?.trim() || null,
        atuacao: dto.atuacao?.trim() || null,
        atuaDesde: dto.atuaDesde,
        areasAtuacaoOutros: dto.areasAtuacaoOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
        areasEstudoPesquisaOutros: dto.areasEstudoPesquisaOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
        publicosFocaisOutros: dto.publicosFocaisOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
        tiposEspacosOutros: dto.tiposEspacosOutros
          .map((el) => el?.trim())
          .filter((el) => !!el),
      };
      await trxPrisma.agente.update({
        where: { id: usuario.agenteIndividual.agente.id },
        data: dataAgente,
      });
      // retorna usuario completo
      return this._getUsuarioCompleto(usuario.id, trxPrisma);
    });
  }

  async updateIdentificacao(id: number, dto: UpdateIdentificacaoUsuarioDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // busca usuario
      const usuario = await this._getUsuarioCompleto(id, trxPrisma);
      if (!usuario) {
        throw new NotFoundException('Usuário não encontrado');
      }
      // atualiza usuario
      const dataUsuario = {
        nome: dto.nome?.trim(),
      };
      await trxPrisma.usuario.update({
        where: { id },
        data: dataUsuario,
      });
      // atualiza agente individual
      // TODO verificar se opções são da categoria correta
      const dataAgenteIndividual = {
        camposPublicos: dto.camposPublicos,
        dataNascimento: dto.dataNascimento,
        identidadeEtnicoCulturalId: dto.identidadeEtnicoCulturalId,
        identidadeEtnicoCulturalOutro:
          dto.identidadeEtnicoCulturalOutro?.trim(),
        identidadeGeneroId: dto.identidadeGeneroId,
        nivelEscolaridadeId: dto.nivelEscolaridadeId,
        ocupacaoId: dto.ocupacaoId,
        ocupacaoOutro: dto.ocupacaoOutro?.trim(),
        racaCorIbgeId: dto.racaCorIbgeId,
      };
      const agenteIndividualId = usuario?.agenteIndividual?.id;
      await trxPrisma.agenteIndividual.update({
        where: { id: agenteIndividualId },
        data: dataAgenteIndividual,
      });
      // atualiza agente
      const dataAgente = {
        nome: dto.nome?.trim(),
        email: dto.email?.trim()?.toLowerCase(),
        website: dto.website?.trim(),
        telefone1: dto.telefone1?.trim(),
        telefone2: dto.telefone2?.trim(),
      };
      const agenteId = usuario?.agenteIndividual?.agente?.id;
      await trxPrisma.agente.update({
        where: { id: agenteId },
        data: dataAgente,
      });
      // retorna usuario completo
      return this._getUsuarioCompleto(usuario.id, trxPrisma);
    });
  }

  async updateLocalizacao(id: number, dto: UpdateLocalizacaoUsuarioDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // busca usuario
      const usuario = await this._getUsuarioCompleto(id, trxPrisma);
      if (!usuario) {
        throw new NotFoundException('Usuário não encontrado');
      }
      // atualiza localizacao
      const dataAgenteIndividual = {
        enderecoPublico: dto.enderecoPublico,
      };
      const agenteIndividualId = usuario?.agenteIndividual?.id;
      await trxPrisma.agenteIndividual.update({
        where: { id: agenteIndividualId },
        data: dataAgenteIndividual,
      });
      // upsert endereco
      const dataEndereco = {
        ...dto.endereco,
      };
      const enderecoId = usuario?.agenteIndividual?.agente?.enderecoId || 0;
      const endereco = await trxPrisma.endereco.upsert({
        where: { id: enderecoId },
        update: dataEndereco,
        create: dataEndereco,
      });
      // atualiza agente
      const dataAgente = {
        enderecoId: endereco.id,
      };
      const agenteId = usuario?.agenteIndividual?.agente?.id;
      await trxPrisma.agente.update({
        where: { id: agenteId },
        data: dataAgente,
      });
      // retorna usuario completo
      return this._getUsuarioCompleto(usuario.id, trxPrisma);
    });
  }

  async updatePreferencias(id: number, dto: UpdatePreferenciasUsuarioDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // busca usuario
      const usuario = await this._getUsuarioCompleto(id, trxPrisma);
      if (!usuario) {
        throw new NotFoundException('Usuário não encontrado');
      }
      // atualiza preferencias
      const dataAgente = {
        perfilPublico: dto.perfilPublico,
      };
      await trxPrisma.agente.update({
        where: { id: usuario.agenteIndividual.agenteId },
        data: dataAgente,
      });
      return this._getUsuarioCompleto(usuario.id, trxPrisma);
    });
  }

  async findOne(id: number) {
    const usuario = await this._getUsuarioCompleto(id);
    if (!usuario) {
      throw new NotFoundException('Usuário não encontrado');
    }
    return usuario;
  }

  async _getUsuarioCompleto(id: number, trxPrisma?: PrismaTransaction) {
    const usuario = await (trxPrisma || this.prisma).usuario.findUnique({
      where: { id },
      include: {
        agenteIndividual: {
          include: {
            agente: {
              include: {
                endereco: true,
                abrangencia: true,
                opcoes: { include: { opcao: true } },
              },
            },
          },
        },
        agentesColetivosMembro: {
          include: {
            agenteColetivo: {
              include: {
                agente: true,
              },
            },
          },
        },
      },
    });
    delete usuario.senha;
    return usuario;
  }

  async removeFavorito(favoritoId: number, id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const usuario = await trxPrisma.usuario.findUnique({
        where: { id },
      });
      if (!usuario) {
        throw new NotFoundException('Usuário não encontrado');
      }
      const favorito = await trxPrisma.favorito.findUnique({
        where: { id: favoritoId, usuarioId: usuario.id },
        include: {
          base: {
            include: {
              category: true,
            },
          },
        },
      });
      if (!favorito) {
        throw new NotFoundException('Favorito não encontrado');
      }
      await trxPrisma.favorito.delete({ where: { id: favorito.id } });
      return favorito;
    });
  }
}
