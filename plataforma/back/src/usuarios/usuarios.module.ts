import { Module } from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import { UsuariosController } from './usuarios.controller';
import { PrismaService } from '../prisma.service';
import { ProjetosModule } from '../projetos/projetos.module';
import { AtividadesModule } from '../atividades/atividades.module';

@Module({
  imports: [AtividadesModule, ProjetosModule],
  controllers: [UsuariosController],
  providers: [UsuariosService, PrismaService],
})
export class UsuariosModule {}
