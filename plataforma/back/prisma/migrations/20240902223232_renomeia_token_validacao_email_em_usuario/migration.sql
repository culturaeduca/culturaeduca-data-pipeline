/*
  Warnings:

  - You are about to drop the column `token_validacao_senha` on the `usuario` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "usuario" DROP COLUMN "token_validacao_senha",
ADD COLUMN     "token_validacao_email" TEXT NOT NULL DEFAULT '';
