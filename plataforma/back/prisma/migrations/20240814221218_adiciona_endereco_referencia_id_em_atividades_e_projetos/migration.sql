-- DropForeignKey
ALTER TABLE "atividade" DROP CONSTRAINT "atividade_endereco_id_fkey";

-- DropForeignKey
ALTER TABLE "projeto" DROP CONSTRAINT "projeto_endereco_id_fkey";

-- AlterTable
ALTER TABLE "atividade" ADD COLUMN     "endereco_referencia_id" INTEGER,
ALTER COLUMN "endereco_id" DROP NOT NULL;

-- AlterTable
ALTER TABLE "projeto" ADD COLUMN     "endereco_referencia_id" INTEGER,
ALTER COLUMN "endereco_id" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "projeto" ADD CONSTRAINT "projeto_endereco_id_fkey" FOREIGN KEY ("endereco_id") REFERENCES "endereco"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto" ADD CONSTRAINT "projeto_endereco_referencia_id_fkey" FOREIGN KEY ("endereco_referencia_id") REFERENCES "endereco"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_endereco_id_fkey" FOREIGN KEY ("endereco_id") REFERENCES "endereco"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_endereco_referencia_id_fkey" FOREIGN KEY ("endereco_referencia_id") REFERENCES "endereco"("id") ON DELETE SET NULL ON UPDATE CASCADE;
