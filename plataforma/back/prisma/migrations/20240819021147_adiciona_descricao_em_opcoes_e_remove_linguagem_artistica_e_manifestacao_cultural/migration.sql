/*
  Warnings:

  - The values [LINGUAGEM_ARTISTICA,MANIFESTACAO_CULTURAL] on the enum `CategoriaOpcao` will be removed. If these variants are still used in the database, this will fail.
  - You are about to drop the column `linguagensArtisticasOutros` on the `agente` table. All the data in the column will be lost.
  - You are about to drop the column `manifestacoesCulturaisOutros` on the `agente` table. All the data in the column will be lost.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "CategoriaOpcao_new" AS ENUM ('ABRANGENCIA', 'AREA_ATUACAO', 'AREA_ESTUDO_PESQUISA', 'IDENTIDADE_ETNICO_CULTURAL', 'IDENTIDADE_GENERO', 'NIVEL_ESCOLARIDADE', 'OCUPACAO', 'PUBLICO_FOCAL', 'RACA_COR_IBGE', 'TIPO_ESPACO');
ALTER TABLE "opcao" ALTER COLUMN "categoria" TYPE "CategoriaOpcao_new" USING ("categoria"::text::"CategoriaOpcao_new");
ALTER TYPE "CategoriaOpcao" RENAME TO "CategoriaOpcao_old";
ALTER TYPE "CategoriaOpcao_new" RENAME TO "CategoriaOpcao";
DROP TYPE "CategoriaOpcao_old";
COMMIT;

-- AlterTable
ALTER TABLE "agente" DROP COLUMN "linguagensArtisticasOutros",
DROP COLUMN "manifestacoesCulturaisOutros";

-- AlterTable
ALTER TABLE "opcao" ADD COLUMN     "descricao" TEXT;

-- CreateTable
CREATE TABLE "vinculo" (
    "id" SERIAL NOT NULL,
    "agente_origem_id" INTEGER NOT NULL,
    "agente_destino_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "vinculo_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "vinculo_agente_origem_id_agente_destino_id_key" ON "vinculo"("agente_origem_id", "agente_destino_id");

-- AddForeignKey
ALTER TABLE "vinculo" ADD CONSTRAINT "vinculo_agente_origem_id_fkey" FOREIGN KEY ("agente_origem_id") REFERENCES "agente"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "vinculo" ADD CONSTRAINT "vinculo_agente_destino_id_fkey" FOREIGN KEY ("agente_destino_id") REFERENCES "agente"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "vinculo" ADD CONSTRAINT "vinculo_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "vinculo" ADD CONSTRAINT "vinculo_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
