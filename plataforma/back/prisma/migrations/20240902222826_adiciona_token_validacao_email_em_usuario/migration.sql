/*
  Warnings:

  - You are about to drop the column `aceiteTermos` on the `usuario` table. All the data in the column will be lost.
  - You are about to drop the column `validacaoEmail` on the `usuario` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "usuario" DROP COLUMN "aceiteTermos",
DROP COLUMN "validacaoEmail",
ADD COLUMN     "aceite_termos" TIMESTAMPTZ,
ADD COLUMN     "token_validacao_senha" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "validacao_email" TIMESTAMPTZ;
