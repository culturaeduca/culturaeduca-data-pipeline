/*
  Warnings:

  - You are about to drop the column `materialized_view` on the `base` table. All the data in the column will be lost.
  - You are about to drop the column `view_name` on the `base` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "base" DROP COLUMN "materialized_view",
DROP COLUMN "view_name";
