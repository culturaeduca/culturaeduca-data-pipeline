-- CreateEnum
CREATE TYPE "BaseCategoryType" AS ENUM ('EQUIPAMENTO', 'LIMITE_TERRITORIAL', 'REDE');

-- CreateEnum
CREATE TYPE "TipoAgente" AS ENUM ('COLETIVO', 'INDIVIDUAL');

-- CreateEnum
CREATE TYPE "TipoAgenteColetivo" AS ENUM ('COLETIVO', 'CONSELHO', 'INSTITUICAO', 'MOVIMENTO_SOCIAL');

-- CreateEnum
CREATE TYPE "TipoAgenteColetivoMembro" AS ENUM ('ADMINISTRADOR', 'COMUM');

-- CreateEnum
CREATE TYPE "CategoriaOpcao" AS ENUM ('ABRANGENCIA', 'AREA_ATUACAO', 'AREA_ESTUDO_PESQUISA', 'IDENTIDADE_ETNICO_CULTURAL', 'IDENTIDADE_GENERO', 'LINGUAGEM_ARTISTICA', 'MANIFESTACAO_CULTURAL', 'NIVEL_ESCOLARIDADE', 'OCUPACAO', 'PUBLICO_FOCAL', 'RACA_COR_IBGE', 'TIPO_ESPACO');

-- CreateTable
CREATE TABLE "base_category" (
    "type" "BaseCategoryType" NOT NULL,
    "uri" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "icon" TEXT NOT NULL,
    "color" TEXT NOT NULL,
    "order" INTEGER NOT NULL,

    CONSTRAINT "base_category_pkey" PRIMARY KEY ("uri")
);

-- CreateTable
CREATE TABLE "base" (
    "uri" TEXT NOT NULL,
    "category_uri" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "title_singular" TEXT,
    "description" TEXT,
    "icon" TEXT,
    "color" TEXT,
    "order" INTEGER NOT NULL,
    "geoserver_uri" TEXT NOT NULL,
    "database_schema" TEXT NOT NULL,
    "table_name" TEXT NOT NULL,
    "materialized_view" BOOLEAN NOT NULL DEFAULT true,
    "view_name" TEXT,
    "column_pk" TEXT NOT NULL,
    "column_name" TEXT NOT NULL,
    "column_geom" TEXT NOT NULL,
    "column_uf" TEXT,
    "column_mun_cd" TEXT,
    "column_mun_nm" TEXT,

    CONSTRAINT "base_pkey" PRIMARY KEY ("uri")
);

-- CreateTable
CREATE TABLE "base_section" (
    "id" SERIAL NOT NULL,
    "base_uri" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "order" INTEGER NOT NULL,

    CONSTRAINT "base_section_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "base_section_item" (
    "id" SERIAL NOT NULL,
    "base_section_id" INTEGER NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "type" TEXT NOT NULL,
    "column" TEXT NOT NULL,
    "order" INTEGER NOT NULL,

    CONSTRAINT "base_section_item_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "endereco" (
    "id" SERIAL NOT NULL,
    "latitude" DOUBLE PRECISION NOT NULL,
    "longitude" DOUBLE PRECISION NOT NULL,
    "logradouro" TEXT,
    "numero" TEXT,
    "bairro" TEXT,
    "complemento" TEXT,
    "codigo_postal" TEXT,
    "municipio_codigo" TEXT,
    "municipio_nome" TEXT NOT NULL,
    "uf_sigla" TEXT,
    "uf_nome" TEXT NOT NULL,
    "pais_iso2" TEXT NOT NULL DEFAULT 'BR',
    "pais_nome" TEXT NOT NULL DEFAULT 'Brasil',
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "endereco_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "usuario" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "senha" TEXT NOT NULL,
    "validacaoEmail" TIMESTAMP(3),
    "aceiteTermos" TIMESTAMP(3),
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "usuario_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "agente" (
    "id" SERIAL NOT NULL,
    "endereco_id" INTEGER,
    "tipo" "TipoAgente" NOT NULL,
    "perfil_publico" BOOLEAN NOT NULL DEFAULT false,
    "nome" TEXT NOT NULL,
    "email" TEXT,
    "telefone1" TEXT,
    "telefone2" TEXT,
    "website" TEXT,
    "abrangencia_id" INTEGER,
    "objetivo" TEXT,
    "atuacao" TEXT,
    "atua_desde" INTEGER,
    "areasAtuacaoOutros" TEXT[],
    "areasEstudoPesquisaOutros" TEXT[],
    "linguagensArtisticasOutros" TEXT[],
    "manifestacoesCulturaisOutros" TEXT[],
    "publicosFocaisOutros" TEXT[],
    "tiposEspacosOutros" TEXT[],
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "agente_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "agente_individual" (
    "id" SERIAL NOT NULL,
    "agente_id" INTEGER NOT NULL,
    "usuario_id" INTEGER NOT NULL,
    "endereco_publico" BOOLEAN NOT NULL DEFAULT false,
    "campos_publicos" TEXT[] DEFAULT ARRAY[]::TEXT[],
    "dataNascimento" DATE,
    "identidade_etnico_cultural_id" INTEGER,
    "identidade_etnico_cultural_outro" TEXT,
    "identidade_genero_id" INTEGER,
    "nivel_escolaridade_id" INTEGER,
    "ocupacao_id" INTEGER,
    "ocupacao_outro" TEXT,
    "raca_cor_ibge_id" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "agente_individual_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "agente_coletivo" (
    "id" SERIAL NOT NULL,
    "agente_id" INTEGER NOT NULL,
    "tipo" "TipoAgenteColetivo" NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "agente_coletivo_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "agente_coletivo_membro" (
    "id" SERIAL NOT NULL,
    "agente_coletivo_id" INTEGER NOT NULL,
    "usuario_id" INTEGER NOT NULL,
    "tipo" "TipoAgenteColetivoMembro" NOT NULL,

    CONSTRAINT "agente_coletivo_membro_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "projeto" (
    "id" SERIAL NOT NULL,
    "agente_id" INTEGER NOT NULL,
    "endereco_id" INTEGER NOT NULL,
    "titulo" TEXT NOT NULL,
    "descricao" TEXT,
    "vinculos" TEXT,
    "objetivo" TEXT,
    "ativo" BOOLEAN NOT NULL DEFAULT true,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "projeto_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "atividade" (
    "id" SERIAL NOT NULL,
    "agente_id" INTEGER NOT NULL,
    "endereco_id" INTEGER NOT NULL,
    "projeto_id" INTEGER,
    "datetime_inicio" TIMESTAMP(3) NOT NULL,
    "datetime_fim" TIMESTAMP(3),
    "titulo" TEXT NOT NULL,
    "descricao" TEXT,
    "ativo" BOOLEAN NOT NULL DEFAULT true,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "atividade_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "agente_opcao" (
    "agente_id" INTEGER NOT NULL,
    "opcao_id" INTEGER NOT NULL,

    CONSTRAINT "agente_opcao_pkey" PRIMARY KEY ("agente_id","opcao_id")
);

-- CreateTable
CREATE TABLE "atividade_opcao" (
    "atividade_id" INTEGER NOT NULL,
    "opcao_id" INTEGER NOT NULL,

    CONSTRAINT "atividade_opcao_pkey" PRIMARY KEY ("atividade_id","opcao_id")
);

-- CreateTable
CREATE TABLE "projeto_opcao" (
    "projeto_id" INTEGER NOT NULL,
    "opcao_id" INTEGER NOT NULL,

    CONSTRAINT "projeto_opcao_pkey" PRIMARY KEY ("projeto_id","opcao_id")
);

-- CreateTable
CREATE TABLE "opcao" (
    "id" SERIAL NOT NULL,
    "categoria" "CategoriaOpcao" NOT NULL,
    "nome" TEXT NOT NULL,
    "ordem" INTEGER,
    "outro" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "opcao_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "base_category_name_key" ON "base_category"("name");

-- CreateIndex
CREATE UNIQUE INDEX "base_geoserver_uri_key" ON "base"("geoserver_uri");

-- CreateIndex
CREATE UNIQUE INDEX "base_database_schema_table_name_key" ON "base"("database_schema", "table_name");

-- CreateIndex
CREATE UNIQUE INDEX "usuario_email_key" ON "usuario"("email");

-- CreateIndex
CREATE UNIQUE INDEX "agente_endereco_id_key" ON "agente"("endereco_id");

-- CreateIndex
CREATE UNIQUE INDEX "agente_individual_agente_id_key" ON "agente_individual"("agente_id");

-- CreateIndex
CREATE UNIQUE INDEX "agente_individual_usuario_id_key" ON "agente_individual"("usuario_id");

-- CreateIndex
CREATE UNIQUE INDEX "agente_coletivo_agente_id_key" ON "agente_coletivo"("agente_id");

-- CreateIndex
CREATE UNIQUE INDEX "agente_coletivo_membro_agente_coletivo_id_usuario_id_key" ON "agente_coletivo_membro"("agente_coletivo_id", "usuario_id");

-- CreateIndex
CREATE UNIQUE INDEX "projeto_endereco_id_key" ON "projeto"("endereco_id");

-- CreateIndex
CREATE UNIQUE INDEX "atividade_endereco_id_key" ON "atividade"("endereco_id");

-- CreateIndex
CREATE UNIQUE INDEX "opcao_categoria_nome_key" ON "opcao"("categoria", "nome");

-- AddForeignKey
ALTER TABLE "base" ADD CONSTRAINT "base_category_uri_fkey" FOREIGN KEY ("category_uri") REFERENCES "base_category"("uri") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "base_section" ADD CONSTRAINT "base_section_base_uri_fkey" FOREIGN KEY ("base_uri") REFERENCES "base"("uri") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "base_section_item" ADD CONSTRAINT "base_section_item_base_section_id_fkey" FOREIGN KEY ("base_section_id") REFERENCES "base_section"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente" ADD CONSTRAINT "agente_endereco_id_fkey" FOREIGN KEY ("endereco_id") REFERENCES "endereco"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente" ADD CONSTRAINT "agente_abrangencia_id_fkey" FOREIGN KEY ("abrangencia_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_agente_id_fkey" FOREIGN KEY ("agente_id") REFERENCES "agente"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_usuario_id_fkey" FOREIGN KEY ("usuario_id") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_identidade_etnico_cultural_id_fkey" FOREIGN KEY ("identidade_etnico_cultural_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_identidade_genero_id_fkey" FOREIGN KEY ("identidade_genero_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_nivel_escolaridade_id_fkey" FOREIGN KEY ("nivel_escolaridade_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_ocupacao_id_fkey" FOREIGN KEY ("ocupacao_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_individual" ADD CONSTRAINT "agente_individual_raca_cor_ibge_id_fkey" FOREIGN KEY ("raca_cor_ibge_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_coletivo" ADD CONSTRAINT "agente_coletivo_agente_id_fkey" FOREIGN KEY ("agente_id") REFERENCES "agente"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_coletivo" ADD CONSTRAINT "agente_coletivo_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_coletivo" ADD CONSTRAINT "agente_coletivo_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_coletivo_membro" ADD CONSTRAINT "agente_coletivo_membro_agente_coletivo_id_fkey" FOREIGN KEY ("agente_coletivo_id") REFERENCES "agente_coletivo"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_coletivo_membro" ADD CONSTRAINT "agente_coletivo_membro_usuario_id_fkey" FOREIGN KEY ("usuario_id") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto" ADD CONSTRAINT "projeto_endereco_id_fkey" FOREIGN KEY ("endereco_id") REFERENCES "endereco"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto" ADD CONSTRAINT "projeto_agente_id_fkey" FOREIGN KEY ("agente_id") REFERENCES "agente"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto" ADD CONSTRAINT "projeto_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto" ADD CONSTRAINT "projeto_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_agente_id_fkey" FOREIGN KEY ("agente_id") REFERENCES "agente"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_endereco_id_fkey" FOREIGN KEY ("endereco_id") REFERENCES "endereco"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_projeto_id_fkey" FOREIGN KEY ("projeto_id") REFERENCES "projeto"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade" ADD CONSTRAINT "atividade_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "usuario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_opcao" ADD CONSTRAINT "agente_opcao_agente_id_fkey" FOREIGN KEY ("agente_id") REFERENCES "agente"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "agente_opcao" ADD CONSTRAINT "agente_opcao_opcao_id_fkey" FOREIGN KEY ("opcao_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade_opcao" ADD CONSTRAINT "atividade_opcao_atividade_id_fkey" FOREIGN KEY ("atividade_id") REFERENCES "atividade"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "atividade_opcao" ADD CONSTRAINT "atividade_opcao_opcao_id_fkey" FOREIGN KEY ("opcao_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto_opcao" ADD CONSTRAINT "projeto_opcao_projeto_id_fkey" FOREIGN KEY ("projeto_id") REFERENCES "projeto"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "projeto_opcao" ADD CONSTRAINT "projeto_opcao_opcao_id_fkey" FOREIGN KEY ("opcao_id") REFERENCES "opcao"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
