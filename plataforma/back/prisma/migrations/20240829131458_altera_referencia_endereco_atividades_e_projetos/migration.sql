/*
  Warnings:

  - You are about to drop the column `endereco_referencia_id` on the `atividade` table. All the data in the column will be lost.
  - You are about to drop the column `endereco_referencia_id` on the `projeto` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "ProjetoEnderecoReferencia" AS ENUM ('AGENTE');

-- CreateEnum
CREATE TYPE "AtividadeEnderecoReferencia" AS ENUM ('AGENTE', 'PROJETO');

-- DropForeignKey
ALTER TABLE "atividade" DROP CONSTRAINT "atividade_endereco_referencia_id_fkey";

-- DropForeignKey
ALTER TABLE "projeto" DROP CONSTRAINT "projeto_endereco_referencia_id_fkey";

-- AlterTable
ALTER TABLE "atividade" DROP COLUMN "endereco_referencia_id",
ADD COLUMN     "endereco_referencia" "AtividadeEnderecoReferencia";

-- AlterTable
ALTER TABLE "projeto" DROP COLUMN "endereco_referencia_id",
ADD COLUMN     "endereco_referencia" "ProjetoEnderecoReferencia";
