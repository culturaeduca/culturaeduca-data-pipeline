// Utilities
import { defineStore } from "pinia";
import { TileLayer, Point } from "leaflet";
import requester from "@/requester";
import {
  geoserverWmsUrl,
  getFeatureInfoUrl,
  wmsTileLayerDefaultOptions,
} from "@/utils/geoserver-helper.utils";
import { Base, BaseCategory } from "@backPrisma/client";
import { BaseCategoryTypeEnum } from "@backSrc/bases/bases.constants";
import { ColumnViewSearch } from "@backSrc/knex/knex.constants";

type BaseCamada = Base & {
  category: BaseCategory;
  checked: boolean;
  wmsTileLayer: TileLayer.WMS;
  polygonTileLayer: TileLayer.WMS; //Layer that was filtered by a polygon
};

interface CategoriesGrouped {
  [type: string]: {
    categories: (BaseCategory & { bases: BaseCamada[]; opened: boolean })[];
  };
}

const categoryTypesNames = {
  [BaseCategoryTypeEnum.EQUIPAMENTO]: "Equipamentos",
  [BaseCategoryTypeEnum.LIMITE_TERRITORIAL]: "Limites territoriais",
  [BaseCategoryTypeEnum.REDE]: "Rede",
};

export const useBasesStore = defineStore("bases", {
  state: () => ({
    bases: [] as BaseCamada[],
    categoriesGrouped: {} as CategoriesGrouped,
  }),
  getters: {
    checkedBases: (state) => state.bases.filter((base) => base.checked),
    uncheckedBases: (state) => state.bases.filter((base) => !base.checked),
  },
  actions: {
    getBase(baseUri: string) {
      return this.bases.find((b) => b.uri === baseUri) || null;
    },
    getBaseProperty(baseUri: string, property: string) {
      const base = this.getBase(baseUri);
      return base?.[property] ?? base?.category?.[property] ?? null;
    },
    checkBase(baseUri: string) {
      const base = this.getBase(baseUri);
      if (!base) return null;
      base.checked = !base.checked;
      return base;
    },
    async checkEntitiesFromLatLng(options: {
      point: Point;
      size: Point;
      bbox: string;
    }) {
      const { point, size, bbox } = options;
      let found = false;
      const promises = this.checkedBases.map(async (base) => {
        if (found) return false; // Skip if we've already found a true result
        const response = await fetch(
          getFeatureInfoUrl({
            wmsTileLayer: base.wmsTileLayer,
            point,
            size,
            bbox,
            propertyNames: ["uri"],
            featureCount: 1,
          })
        );
        const data = await response.json();
        const result = (data?.features || []).length > 0;
        if (result) {
          found = true; // Set the flag to true if a true result is found
        }
        return result;
      });
      const results = await Promise.all(promises);
      return results.includes(true);
    },
    async getEntitiesFromLatLng(options: {
      point: Point;
      size: Point;
      bbox: string;
    }) {
      const { point, size, bbox } = options;
      return Promise.all(
        this.checkedBases.map(async (base) => {
          const response = await fetch(
            getFeatureInfoUrl({
              wmsTileLayer: base.wmsTileLayer,
              point,
              size,
              bbox,
            })
          );
          const data = await response.json();
          return (data?.features || []).map((feature) => ({
            [ColumnViewSearch.CATEGORY_TYPE]: base.category.type,
            [ColumnViewSearch.CATEGORY_URI]: base.category.uri,
            [ColumnViewSearch.BASE_URI]: base.uri,
            [ColumnViewSearch.PK]:
              feature?.properties?.[base.columnPk].toString(),
            [ColumnViewSearch.NOME]: feature?.properties?.[base.columnName],
            [ColumnViewSearch.DESCRICAO]:
              feature?.properties?.[base.columnDescription] || null,
            [ColumnViewSearch.GEOM]: feature?.geometry,
            [ColumnViewSearch.UF_SIGLA]: feature?.properties?.[base.columnUf],
            [ColumnViewSearch.MUNICIPIO_CODIGO]:
              feature?.properties?.[base.columnMunCd],
            [ColumnViewSearch.MUNICIPIO_NOME]:
              feature?.properties?.[base.columnMunNm],
          }));
        })
      );
    },
    applyAttributeFilterToBases(column: String, value: String) {
      const cqlFilter = `${column} = '${value}'`;
      for (const base of this.bases) {
        const params = {
          layers: base.geoserverUri,
          CQL_FILTER: cqlFilter,
          ...wmsTileLayerDefaultOptions,
        };

        if (base.polygonTileLayer) {
          base.polygonTileLayer.remove();
        }

        base.polygonTileLayer = new TileLayer.WMS(geoserverWmsUrl, params);
      }
    },
    applyDatasetFilterToBases(dataset: String, column: String, value: String) {
      const cqlFilter = `intersects(_geom, querySingle('${dataset}', '_geom','${column}=${value}'))`;
      for (const base of this.bases) {
        const params = {
          layers: base.geoserverUri,
          CQL_FILTER: cqlFilter,
          ...wmsTileLayerDefaultOptions,
        };

        if (base.polygonTileLayer) {
          base.polygonTileLayer.remove();
        }

        base.polygonTileLayer = new TileLayer.WMS(geoserverWmsUrl, params);
      }
    },
    async initBases() {
      this.bases = (await requester.bases.findAll()).map((base) => ({
        ...base,
        checked: false,
        wmsTileLayer: new TileLayer.WMS(geoserverWmsUrl, {
          layers: base.geoserverUri,
          ...wmsTileLayerDefaultOptions,
        }),
      }));
      this.categoriesGrouped = {};
      this.bases.forEach((base: BaseCamada) => {
        const categoryType = base.category.type;
        const categoryUri = base.category.uri;
        if (!this.categoriesGrouped[categoryType]) {
          this.categoriesGrouped[categoryType] = {
            name: categoryTypesNames?.[categoryType] || categoryType,
            categories: [],
          };
        }
        const index = this.categoriesGrouped[categoryType].categories.findIndex(
          (c) => c.uri === categoryUri
        );
        if (index < 0) {
          this.categoriesGrouped[categoryType].categories.push({
            ...base.category,
            bases: [base],
            opened: false,
          });
        } else {
          this.categoriesGrouped[categoryType].categories[index].bases.push(
            base
          );
        }
      });
    },
  },
});
