// Utilities
import requester from "@/requester";
import { defineStore } from "pinia";

export const useBoundariesStore = defineStore("boundaries", {
  state: () => ({
    boundaryHierarchyUri: import.meta.env.VITE_DEFAULT_BOUNDARY_HIERARCHY,
    boundariesHierarchies: [],
  }),
  getters: {
    boundaryHierarchy: (state) => {
      return state.boundariesHierarchies.find(
        (bh) => bh.uri === state.boundaryHierarchyUri
      );
    },
    boundaryHierarchyLeaf: (state) => {
      const hierarchy = state.boundariesHierarchies.find(
        (bh) => bh.uri === state.boundaryHierarchyUri
      );
      return hierarchy?.boundaries?.[hierarchy.boundaries.length - 1] || null;
    },
    boundaryById: (state) => (id: number) => {
      const hierarchy = state.boundariesHierarchies.find(
        (bh) => bh.uri === state.boundaryHierarchyUri
      );
      return hierarchy?.boundaries.find((b) => b.id === id) || null;
    },
    boundaryByName: (state) => (name: string) => {
      const hierarchy = state.boundariesHierarchies.find(
        (bh) => bh.uri === state.boundaryHierarchyUri
      );
      return hierarchy?.boundaries.find((b) => b.name === name) || null;
    }
  },
  actions: {
    setBoundaryHierarchyUri(uri: string) {
      this.boundaryHierarchyUri = uri;
    },
    async initBoundariesHierarchies() {
      this.boundariesHierarchies =
        await requester.dataPipeline.boundariesHierarchies.findAll();
    },
  },
});
