// Utilities
import requester from "@/requester";
import { defineStore } from "pinia";
import {
  Usuario,
  AgenteIndividual,
  Agente,
  Endereco,
} from "@backPrisma/client";

type UsuarioAgenteInidividualAgenteEndereco = Usuario & {
  agenteIndividual?: AgenteIndividual & {
    agente: Agente & { endereco?: Endereco };
  };
};

export const useAuthStore = defineStore("auth", {
  state: () => ({
    usuario: null as UsuarioAgenteInidividualAgenteEndereco | null,
  }),
  actions: {
    async checkSession() {
      this.usuario = await requester.auth.checkSession();
    },
    async signIn(dto: { email: string; senha: string }) {
      this.usuario = await requester.auth.signIn(dto);
    },
    async signOut() {
      await requester.auth.signOut();
      this.usuario = null;
    },
  },
});
