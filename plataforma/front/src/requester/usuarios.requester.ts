import axios from "axios";
import {
  Agente,
  AgenteColetivo,
  AgenteColetivoMembro,
  AgenteIndividual,
  AgenteOpcao,
  Base,
  BaseCategory,
  Endereco,
  Favorito,
  Opcao,
  Usuario,
} from "@backPrisma/client";
import {
  CreateFavoritoDto,
  UpdateAtuacaoUsuarioDto,
  UpdateIdentificacaoUsuarioDto,
  UpdateLocalizacaoUsuarioDto,
  UpdatePreferenciasUsuarioDto,
} from "@backSrc/usuarios/dto";
import { CreateAtividadeDto } from "@backSrc/atividades/dto";
import { CreateProjetoDto } from "@backSrc/projetos/dto";
import { AtividadeCompleta } from "./atividades.requester";
import { ProjetoCompleto } from "./projetos.requester";

export type FavoritoComBase = Favorito & {
  base: Base & { category: BaseCategory };
};

export type UsuarioCompleto = Usuario & {
  agenteIndividual: AgenteIndividual & {
    agente: Agente & {
      endereco: Endereco;
      abrangencia: Opcao;
      opcoes: (AgenteOpcao & { opcao: Opcao })[];
    };
  };
  agentesColetivosMembro: (AgenteColetivoMembro & {
    agenteColetivo: AgenteColetivo & {
      agente: Agente;
    };
  })[];
};

const createAtividade = async (dto: CreateAtividadeDto) => {
  const res = await axios.post<AtividadeCompleta>(
    `usuarios/meu_perfil/atividades`,
    dto
  );
  return res.data;
};

const createFavorito = async (dto: CreateFavoritoDto) => {
  const res = await axios.post<FavoritoComBase>(
    `usuarios/meu_perfil/favoritos`,
    dto
  );
  return res.data;
};

const createProjeto = async (dto: CreateProjetoDto) => {
  const res = await axios.post<ProjetoCompleto>(
    `usuarios/meu_perfil/projetos`,
    dto
  );
  return res.data;
};

const meuEndereco = async () => {
  const res = await axios.get<Endereco>(`usuarios/meu_perfil/endereco`);
  return res.data;
};

const meuFavorito = async (baseUri: string, pk: string) => {
  const res = await axios.get<FavoritoComBase>(
    `usuarios/meu_perfil/favoritos/${baseUri}/${pk}`
  );
  return res.data;
};

const meuPerfil = async () => {
  const res = await axios.get<UsuarioCompleto>(`usuarios/meu_perfil`);
  return res.data;
};

const meusFavoritos = async () => {
  const res = await axios.get<FavoritoComBase[]>(
    `usuarios/meu_perfil/favoritos`
  );
  return res.data;
};

const meusProjetos = async () => {
  const res = await axios.get<ProjetoCompleto[]>(
    `usuarios/meu_perfil/projetos`
  );
  return res.data;
};

const minhasAtividades = async () => {
  const res = await axios.get<AtividadeCompleta[]>(
    `usuarios/meu_perfil/atividades`
  );
  return res.data;
};

const removeFavorito = async (favoritoId: number) => {
  const res = await axios.delete<FavoritoComBase>(
    `usuarios/meu_perfil/favoritos/${favoritoId}`
  );
  return res.data;
};

const updateAtuacao = async (dto: UpdateAtuacaoUsuarioDto) => {
  const res = await axios.patch<UsuarioCompleto>(
    `usuarios/meu_perfil/atuacao`,
    dto
  );
  return res.data;
};

const updateIdentificacao = async (dto: UpdateIdentificacaoUsuarioDto) => {
  const res = await axios.patch<UsuarioCompleto>(
    `usuarios/meu_perfil/identificacao`,
    dto
  );
  return res.data;
};

const updateIdentificacaoPhoto = async (data: FormData) => {
  const res = await axios.post(`storage/profile`, data, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
  return res.data;
};

const getIdentificacaoPhoto = async () => {
  const res = await axios.get("storage/profile");
  return res.data;
};

const deleteIdentificaçaoPhoto = async () => {
  const res = await axios.delete("storage/profile");
  return res.data;
};

const updateLocalizacao = async (dto: UpdateLocalizacaoUsuarioDto) => {
  const res = await axios.patch<UsuarioCompleto>(
    `usuarios/meu_perfil/localizacao`,
    dto
  );
  return res.data;
};

const updatePreferencias = async (dto: UpdatePreferenciasUsuarioDto) => {
  const res = await axios.patch<UsuarioCompleto>(
    `usuarios/meu_perfil/preferencias`,
    dto
  );
  return res.data;
};

export default {
  deleteIdentificaçaoPhoto,
  getIdentificacaoPhoto,
  updateIdentificacaoPhoto,
  createAtividade,
  createFavorito,
  createProjeto,
  meuEndereco,
  meuFavorito,
  meuPerfil,
  meusFavoritos,
  meusProjetos,
  minhasAtividades,
  removeFavorito,
  updateAtuacao,
  updateIdentificacao,
  updateLocalizacao,
  updatePreferencias,
};
