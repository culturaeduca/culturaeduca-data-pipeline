import axios from "axios";
import {
  SignInAuthDto,
  SignUpAuthDto,
  RecoverPasswordDto,
  ResendValidateTokenDto,
  ResetPasswordDto,
} from "@backSrc/auth/dto";
import { Usuario } from "@backPrisma/client";

const checkSession = async () => {
  const res = await axios.get<Usuario>(`auth/check_session`);
  return res.data;
};

const signIn = async (dto: SignInAuthDto) => {
  const res = await axios.post<Usuario>(`auth/sign_in`, dto);
  return res.data;
};

const signOut = async () => {
  await axios.post<void>(`auth/sign_out`);
};

const signUp = async (dto: SignUpAuthDto) => {
  const res = await axios.post<Usuario>(`auth/sign_up`, dto);
  return res.data;
};

const validateUser = async (token: string) => {
  const res = await axios.post(`auth/verify_token/${token}`);
  return res.data;
};

const resendValidateUserToken = async (dto: ResendValidateTokenDto) => {
  const res = await axios.post(`auth/resend_verify_token`, dto);
  return res.data;
};

const sendTokenRecover = async (dto: RecoverPasswordDto) => {
  const res = await axios.post(`auth/recover_password`, dto);
  return res.data;
};

const recoverPassword = async (token: string, dto: ResetPasswordDto) => {
  const res = await axios.post(`auth/reset_password/${token}`, dto);
  return res.data;
};

export default {
  checkSession,
  signIn,
  signOut,
  signUp,
  validateUser,
  sendTokenRecover,
  resendValidateUserToken,
  recoverPassword,
};
