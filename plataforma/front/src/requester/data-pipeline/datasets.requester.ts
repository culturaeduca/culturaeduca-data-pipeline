import axios from "axios";

const baseURL =
  import.meta.env.VITE_DATA_PIPELINE_API_URL || "http://localhost:3001";

const findSectionData = async (
  id: number,
  page: number = 1,
  perPage: number = 99999
) => {
  const response = await axios.get<any>(`sections/${id}/data_table`, {
    baseURL,
    params: {
      page,
      perPage,
    },
  });
  return response.data;
};

const getDataGeojson = async (sectionId: number, dataId: number) => {
  const res = await axios.get<GeoJSON.Feature>(
    `sections/${sectionId}/data_geojson/${dataId}`,
    {
      baseURL,
    }
  );
  return res.data;
};

export default {
  findSectionData,
  getDataGeojson,
};
