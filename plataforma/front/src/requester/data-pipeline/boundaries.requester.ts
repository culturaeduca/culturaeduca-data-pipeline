import axios from "axios";

const baseURL =
  import.meta.env.VITE_DATA_PIPELINE_API_URL || "http://localhost:3001";

const getGeojsonRadius = async (
  id: number,
  queryDto: { lat: number; lng: number; radiusKm: number }
) => {
  const response = await axios.get<GeoJSON.FeatureCollection>(
    `/boundaries/${id}/geojson_radius`,
    {
      baseURL,
      params: queryDto,
    }
  );
  return response.data;
};

export default {
  getGeojsonRadius,
};
