import axios from "axios";

const baseURL =
  import.meta.env.VITE_DATA_PIPELINE_API_URL || "http://localhost:3001";

const findAll = async () => {
  const response = await axios.get<any[]>("boundaries_hierarchies", {
    baseURL,
  });
  return response.data;
};

const findOne = async (uri: string) => {
  const response = await axios.get<any>(`boundaries_hierarchies/${uri}`, {
    baseURL,
  });
  return response.data;
};

const getBreadcrumbs = async (
  uri: string,
  queryDto: { lat: number; lng: number }
) => {
  const response = await axios.get<any[]>(
    `boundaries_hierarchies/${uri}/breadcrumbs`,
    {
      baseURL,
      params: queryDto,
    }
  );
  return response.data;
};

export default {
  findAll,
  findOne,
  getBreadcrumbs,
};
