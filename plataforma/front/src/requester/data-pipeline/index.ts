import boundaries from "./boundaries.requester";
import boundariesHierarchies from "./boundaries-hierarchies.requester";
import datasets from "./datasets.requester";

export default {
  boundaries,
  boundariesHierarchies,
  datasets,
};
