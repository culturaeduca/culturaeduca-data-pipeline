import axios from "axios";

import {
  Agente,
  AgenteColetivo,
  AgenteOpcao,
  Endereco,
  Opcao,
} from "@backPrisma/client";
import { CreateAgenteColetivoDto } from "@backSrc/agentes-coletivos/dto";
import { CreateAtividadeDto } from "@backSrc/atividades/dto";
import { CreateProjetoDto } from "@backSrc/projetos/dto";
import { AtividadeCompleta } from "./atividades.requester";
import { ProjetoCompleto } from "./projetos.requester";

export type AgenteColetivoCompleto = AgenteColetivo & {
  agente: Agente & {
    endereco: Endereco;
    abrangencia: Opcao;
    opcoes: (AgenteOpcao & { opcao: Opcao })[];
  };
};

const create = async (dto: CreateAgenteColetivoDto) => {
  const res = await axios.post<AgenteColetivoCompleto>(
    `agentes_coletivos`,
    dto
  );
  return res.data;
};

const createAtividade = async (
  agenteColetivoId: number,
  dto: CreateAtividadeDto
) => {
  const res = await axios.post<AtividadeCompleta>(
    `agentes_coletivos/${agenteColetivoId}/atividades`,

    dto
  );
  return res.data;
};

const createProjeto = async (
  agenteColetivoId: number,
  dto: CreateProjetoDto
) => {
  const res = await axios.post<ProjetoCompleto>(
    `agentes_coletivos/${agenteColetivoId}/projetos`,

    dto
  );
  return res.data;
};

const findAtividades = async (agenteColetivoId: number) => {
  const res = await axios.get<AtividadeCompleta[]>(
    `agentes_coletivos/${agenteColetivoId}/atividades`
  );
  return res.data;
};

const findEndereco = async (agenteColetivoId: number) => {
  const res = await axios.get<Endereco>(
    `agentes_coletivos/${agenteColetivoId}/endereco`
  );
  return res.data;
};

const findOne = async (agenteColetivoId: number) => {
  const res = await axios.get<AgenteColetivoCompleto>(
    `agentes_coletivos/${agenteColetivoId}`
  );
  return res.data;
};

const findProjetos = async (agenteColetivoId: number) => {
  const res = await axios.get<ProjetoCompleto[]>(
    `agentes_coletivos/${agenteColetivoId}/projetos`
  );
  return res.data;
};

export default {
  create,
  createAtividade,
  createProjeto,
  findAtividades,
  findEndereco,
  findOne,
  findProjetos,
};
