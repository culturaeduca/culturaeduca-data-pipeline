import axios from "axios";

import {
  Agente,
  Atividade,
  AtividadeOpcao,
  Endereco,
  Opcao,
  Projeto,
} from "@backPrisma/client";
import { BuscaAvancadaAtividadeQueryDto } from "@backSrc/atividades/dto";

export type AtividadeCompleta = Atividade & {
  agente: Agente;
  endereco: Endereco;
  projeto: Projeto | null;
  opcoes: (AtividadeOpcao & { opcao: Opcao })[];
};

const buscaAvancada = async (queryDto: BuscaAvancadaAtividadeQueryDto) => {
  const res = await axios.get<AtividadeCompleta[]>(
    `atividades/busca_avancada`,
    { params: queryDto }
  );
  return res.data;
};

const findAll = async () => {
  const res = await axios.get<AtividadeCompleta[]>(`atividades`);
  return res.data;
};

const findOne = async (atividadeId: number) => {
  const res = await axios.get<AtividadeCompleta>(`atividades/${atividadeId}`);
  return res.data;
};

const remove = async (atividadeId: number) => {
  const res = await axios.delete<Atividade>(`atividades/${atividadeId}`);
  return res.data;
};

export default {
  buscaAvancada,
  findAll,
  findOne,
  remove,
};
