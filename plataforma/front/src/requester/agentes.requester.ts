import axios from "axios";

import {
  Agente,
  AgenteColetivo,
  AgenteIndividual,
  AgenteOpcao,
  Endereco,
  Opcao,
} from "@backPrisma/client";

export type AgenteCompleto = Agente & {
  agenteColetivo?: AgenteColetivo;
  agenteIndividual?: AgenteIndividual & {
    identidadeEtnicoCultural: Opcao;
    identidadeGenero: Opcao;
    nivelEscolaridade: Opcao;
    ocupacao: Opcao;
    racaCorIbge: Opcao;
  };
  endereco: Endereco;
  abrangencia: Opcao;
  opcoes: (AgenteOpcao & { opcao: Opcao })[];
};

const findOne = async (agenteId: number) => {
  const res = await axios.get<AgenteCompleto>(`agentes/${agenteId}`);
  return res.data;
};

export default {
  findOne,
};
