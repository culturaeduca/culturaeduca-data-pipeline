import axios from "axios";

import { Opcao } from "@backPrisma/client";
import { FindAllQueryDto } from "@backSrc/opcoes/dto";

const findAll = async (queryDto: FindAllQueryDto) => {
  const res = await axios.get<{ total: number; results: Opcao[] }>(`opcoes`, {
    params: queryDto,
  });
  return res.data;
};

const findOne = async (opcaoId: number) => {
  const res = await axios.get<Opcao>(`opcoes/${opcaoId}`);
  return res.data;
};

export default {
  findAll,
  findOne,
};
