import axios from "axios";

import {
  GetSetoresBreadcrumbsQueryDto,
  GetSetoresRadiusQueryDto,
} from "@backSrc/setores/dto";

const breadcrumbs = async (queryDto: GetSetoresBreadcrumbsQueryDto) => {
  const res = await axios.get<any>(`setores/breadcrumbs`, {
    params: queryDto,
  });
  return res.data;
};

const radius = async (queryDto: GetSetoresRadiusQueryDto) => {
  const res = await axios.get<GeoJSON.FeatureCollection>(`setores/radius`, {
    params: queryDto,
  });
  return res.data;
};

export default {
  breadcrumbs,
  radius,
};
