import axios from "axios";

import {
  Atividade,
  AtividadeOpcao,
  Endereco,
  Opcao,
  Projeto,
  ProjetoOpcao,
} from "@backPrisma/client";

export type ProjetoCompleto = Projeto & {
  endereco: Endereco;
  atividades: (Atividade & {
    endereco: Endereco;
    opcoes: (AtividadeOpcao & { opcao: Opcao })[];
  })[];
  opcoes: (ProjetoOpcao & { opcao: Opcao })[];
};

const findAll = async () => {
  const res = await axios.get<ProjetoCompleto[]>(`projetos`);
  return res.data;
};

const findOne = async (projetoId: number) => {
  const res = await axios.get<ProjetoCompleto>(`projetos/${projetoId}`);
  return res.data;
};

const remove = async (projetoId: number) => {
  const res = await axios.delete<Projeto>(`projetos/${projetoId}`);
  return res.data;
};

export default {
  findAll,
  findOne,
  remove,
};
