import axios from "axios";

import {
  Base,
  BaseCategory,
  BaseSection,
  BaseSectionItem,
} from "@backPrisma/client";
import {
  SearchViewBuscasQueryDto,
  SearchViewBuscasEntornoQueryDto,
  SearchViewBuscasDivisaoQueryDto,
} from "@backSrc/bases/dto";
import { ColumnViewSearch } from "@backSrc/knex/knex.constants";

export interface ItemSearchResponse {
  [ColumnViewSearch.CATEGORY_TYPE]: string;
  [ColumnViewSearch.CATEGORY_URI]: string;
  [ColumnViewSearch.BASE_URI]: string;
  [ColumnViewSearch.PK]: string;
  [ColumnViewSearch.NOME]: string;
  [ColumnViewSearch.DESCRICAO]: string;
  [ColumnViewSearch.GEOM]: GeoJSON.Point;
  [ColumnViewSearch.UF_SIGLA]: string;
  [ColumnViewSearch.MUNICIPIO_CODIGO]: string;
  [ColumnViewSearch.MUNICIPIO_NOME]: string;
  distance_km: number;
  rank: number;
}

export type BaseCompleto = Base & {
  category: BaseCategory;
  sections: (BaseSection & { items: BaseSectionItem[] })[];
};

const findAll = async () => {
  const res = await axios.get<(Base & { category: BaseCategory })[]>(`bases`);
  return res.data;
};

const findOne = async (uri: string) => {
  const res = await axios.get<BaseCompleto>(`bases/${uri}`);
  return res.data;
};

const findEntityByPk = async (uri: string, entityPk: string) => {
  const res = await axios.get<{ base: BaseCompleto; entity: any }>(
    `bases/${uri}/${entityPk}`
  );
  return res.data;
};

const findSections = async (uri: string) => {
  const res = await axios.get<(BaseSection & { items: BaseSectionItem[] })[]>(
    `bases/${uri}/sections`
  );
  return res.data;
};

const search = async (queryDto: SearchViewBuscasQueryDto) => {
  const res = await axios.get<ItemSearchResponse[]>(`bases/search`, {
    params: queryDto,
  });
  return res.data;
};

const searchEntorno = async (queryDto: SearchViewBuscasEntornoQueryDto) => {
  const res = await axios.get<ItemSearchResponse[]>(`bases/search/entorno`, {
    params: queryDto,
  });
  return res.data;
};

const searchDivisao = async (queryDto: SearchViewBuscasDivisaoQueryDto) => {
  const res = await axios.get<ItemSearchResponse[]>(`bases/search/divisao`, {
    params: queryDto,
  });
  return res.data;
};

const setoresRadius = async (queryDto: {
  latitude: number;
  longitude: number;
  radiusKm: number;
}) => {
  const res = await axios.get<GeoJSON.FeatureCollection>(`setores/radius`, {
    params: queryDto,
  });
  return res.data;
};

const createBaseCategory = async (createCategoryDto) => {
  const res = await axios.post("category", createCategoryDto);
  return res.data;
};

const updateBaseCategory = async (uri, createCategoryDto) => {
  const res = await axios.put(`category/${uri}`, createCategoryDto);
  return res.data;
};

const listCategories = async () => {
  const res = await axios.get("category");
  return res.data;
};

const deleteCategories = async (categoryUri: string) => {
  const res = await axios.delete(`category/${categoryUri}`);
  return res.data;
};

export default {
  findAll,
  findEntityByPk,
  findOne,
  findSections,
  search,
  searchEntorno,
  searchDivisao,
  setoresRadius,
  createBaseCategory,
  listCategories,
  deleteCategories,
  updateBaseCategory,
};
