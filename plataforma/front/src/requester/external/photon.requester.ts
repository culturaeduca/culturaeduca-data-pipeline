import axios from "axios";

async function search(options: {
  q: string;
  lat?: number;
  lng?: number;
  bbox?: string;
  limit?: number;
}) {
  const params = {
    q: options.q.trim().toLowerCase(),
    lang: "en",
    ...(options.lat ? { lat: options.lat } : {}),
    ...(options.lng ? { lon: options.lng } : {}),
    ...(options.bbox ? { lon: options.bbox } : {}),
    limit: options.limit > 0 ? options.limit : 10,
  };
  const response = await axios.get("https://photon.komoot.io/api/", {
    withCredentials: false,
    params,
  });
  return response.data;
}

export default {
  search,
};
