import axios from "axios";

type AwesomeapiAddress = {
  address: string;
  address_name: string;
  address_type: string;
  cep: string;
  city: string;
  city_ibge: string;
  ddd: string;
  district: string;
  lat: string;
  lng: string;
  state: string;
};

// awesomeapi cep [https://docs.awesomeapi.com.br/api-cep]
async function cep(cep: string) {
  const cleanCep = cep.replace(/\D/g, "");
  const response = await axios.get<AwesomeapiAddress>(
    `https://cep.awesomeapi.com.br/json/${cleanCep}`,
    {
      withCredentials: false,
    }
  );
  return response.data;
}

export default {
  cep,
};
