import axios from "axios";

async function reverse(options: { lat: number; lng: number }) {
  const params = { lat: options.lat, lon: options.lng, format: "json" };
  const response = await axios.get(
    "https://nominatim.openstreetmap.org/reverse",
    {
      withCredentials: false,
      params,
    }
  );
  return response.data;
}

async function search(options: {
  q: string;
  lat?: number;
  lng?: number;
  bbox?: string;
  limit?: number;
}) {
  const params = {
    q: options.q.trim().toLowerCase(),
    ...(options.lat ? { lat: options.lat } : {}),
    ...(options.lng ? { lon: options.lng } : {}),
    ...(options.bbox ? { bbox: options.bbox } : {}),
    limit: options.limit > 0 ? options.limit : 10,
    countrycodes: "br",
    layer: "address",
    addressdetails: 1,
    format: "json",
    "accept-language": "pt-br",
  };

  const response = await axios.get(
    "https://nominatim.openstreetmap.org/search",
    {
      withCredentials: false,
      params,
    }
  );
  return response.data;
}

export default {
  reverse,
  search,
};
