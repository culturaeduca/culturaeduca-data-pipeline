import awesomeapi from "./awesomeapi.requester";
import localidadesIbge from "./localidadesIbge.requester";
import nominatim from "./nominatim.requester";
import photon from "./photon.requester";

export default {
  awesomeapi,
  localidadesIbge,
  nominatim,
  photon,
};
