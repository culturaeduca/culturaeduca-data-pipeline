import axios from "axios";

const baseURL =
  import.meta.env.VITE_GEOSERVER_URL || "http://localhost:8088/geoserver";

const address = baseURL + "/rest/layers";

const username = import.meta.env.VITE_GEOSERVER_USERNAME || "culturaeduca";
const password = import.meta.env.VITE_GEOSERVER_PASSWORD || "password";

const getLayers = async () => {
  const response = await axios.get(address, {
    withCredentials: false,
    auth: {
      username,
      password,
    },
    headers: {
      Accept: "application/json",
    },
  });
  return response.data;
};

export default {
  getLayers,
};
