import axios from "axios";

import agentes from "./agentes.requester";
import agentesColetivos from "./agentes-coletivos.requester";
import atividades from "./atividades.requester";
import auth from "./auth.requester";
import bases from "./bases.requester";
import external from "./external";
import opcoes from "./opcoes.requester";
import projetos from "./projetos.requester";
import setores from "./setores.requester";
import usuarios from "./usuarios.requester";
import dataPipeline from "./data-pipeline";
import geoServer from "./geoserver";

axios.defaults.baseURL =
  import.meta.env.VITE_API_URL || "http://localhost:3000";
axios.defaults.withCredentials = true;

export default {
  agentes,
  agentesColetivos,
  atividades,
  auth,
  bases,
  dataPipeline,
  external,
  opcoes,
  projetos,
  setores,
  usuarios,
  geoServer,
};
