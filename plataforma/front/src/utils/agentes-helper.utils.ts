// importar direto do Prisma dá errado
export enum TipoAgente {
  COLETIVO = "COLETIVO",
  INDIVIDUAL = "INDIVIDUAL",
}

export enum TipoAgenteColetivo {
  COLETIVO = "COLETIVO",
  CONSELHO = "CONSELHO",
  INSTITUICAO = "INSTITUICAO",
  MOVIMENTO_SOCIAL = "MOVIMENTO_SOCIAL",
}

export const tipoAgenteColetivoOptions = [
  {
    value: TipoAgenteColetivo.COLETIVO,
    title: "Grupo / Coletivo",
    description:
      "São grupos organizados, formalmente ou não, por um interesse comum. Por exemplo: clubes de leitura, grupos de moradores, e coletivos artísticos.",
    icon: "mdi-account-group",
  },
  {
    value: TipoAgenteColetivo.CONSELHO,
    title: "Fórum / Conselho / Comitê",
    description:
      "São instâncias de participação e debate que ajudam instituições a tomar decisões. Nas políticas públicas, são órgãos do controle social para aproximar a sociedade do Estado. Por exemplo: conselhos de escola, fóruns temáticos, e comitês gestores.",
    icon: "mdi-account-group",
  },
  {
    value: TipoAgenteColetivo.INSTITUICAO,
    title: "Instituição",
    description:
      "São órgãos formais, públicos ou privados, que visam desempenhar determinadas funções na sociedade. Por exemplo: serviços públicos, institutos, organizações não governamentais (ONGs).",
    icon: "mdi-account-group",
  },
  {
    value: TipoAgenteColetivo.MOVIMENTO_SOCIAL,
    title: "Movimento Social",
    description:
      "São grupos reunidos para defender, demandar ou lutar politicamente por determinadas mudanças sociais. Por exemplo: movimentos de moradia, feministas, e ambientalistas.",
    icon: "mdi-account-group",
  },
];

export function getTipoAgenteColetivoProperty(
  property: "title" | "description" | "icon",
  tipo: TipoAgenteColetivo
) {
  return tipoAgenteColetivoOptions?.[tipo]?.[property];
}
