import { Endereco } from "@backPrisma/client";

export function formatEndereco(endereco: Endereco) {
  if (!endereco) return "";
  return `${endereco.logradouro}${
    endereco.numero ? `, ${endereco.numero}` : ""
  }${endereco.complemento ? `- ${endereco.complemento}` : ""}${
    endereco.bairro ? `- ${endereco.bairro}` : ""
  }, ${endereco.municipioNome}, ${endereco.ufSigla} - CEP ${
    endereco.codigoPostal
  }`;
}
