import requester from "@/requester";

import {
  Base,
  BaseCategory,
  BaseSection,
  BaseSectionItem,
} from "@backPrisma/client";
import {
  CRS,
  LatLng,
  Map as LeafletMap,
  TileLayer,
  Util,
  WMSOptions,
} from "leaflet";

const geoserverWmsUrl = `${import.meta.env.VITE_GEOSERVER_URL}/wms`;

const wmsTileLayerOptions: WMSOptions = {
  format: "image/png",
  transparent: true,
  attribution: "CulturaEduca",
  crs: CRS.EPSG4326,
};

export class BaseLayer {
  base: Base & {
    category: BaseCategory;
    sections?: (BaseSection & { items: BaseSectionItem[] })[];
  };
  wmsTileLayer: TileLayer.WMS;
  show: boolean;
  entities: Map<string | number, BaseLayerEntity>;

  constructor(base: Base & { category: BaseCategory }) {
    this.base = base;
    this.wmsTileLayer = new TileLayer.WMS(geoserverWmsUrl, {
      layers: this.base.geoserverUri,
      ...wmsTileLayerOptions,
    });
    this.show = false;
    this.entities = new Map<string | number, BaseLayerEntity>();
  }

  get title() {
    return this.base.title;
  }

  get icon() {
    return this.base.icon || this.base.category.icon || "mdi-view-list";
  }

  get color() {
    return this.base.color || this.base.category.color || "primary";
  }

  public async getSections() {
    if (this.base.sections) {
      return this.base.sections;
    }
    return await requester.bases.findSections(this.base.uri);
  }

  public insertEntity(feature: GeoJSON.Feature) {
    const pk = feature?.properties?.[this.base.columnPk];
    if (this.entities.has(pk)) {
      return this.entities.get(pk);
    }
    const options = {
      baseUri: this.base.uri,
      pk,
      name: feature?.properties?.[this.base.columnName],
      feature,
    };
    this.entities.set(pk, new BaseLayerEntity(options));
    return this.entities.get(pk);
  }

  public removeEntity(pk: string | number) {
    this.entities.delete(pk);
  }

  public async getEntitiesFromLatLng(options: {
    map: LeafletMap;
    latLng: LatLng;
    buffer?: number;
    featureCount?: number;
  }) {
    if (!this.show) return [];
    const response = await fetch(this.getFeatureInfoUrl(options));
    const data = await response.json();
    return (data?.features || []).map((feature) => this.insertEntity(feature));
  }

  private getFeatureInfoUrl(options: {
    map: LeafletMap;
    latLng: LatLng;
    buffer?: number;
    featureCount?: number;
  }) {
    const point = options.map.latLngToContainerPoint(options.latLng);
    const size = options.map.getSize();
    const bbox = options.map.getBounds().toBBoxString();
    const buffer = options.buffer > 0 ? options.buffer : 10;
    const featureCount = options.featureCount > 0 ? options.featureCount : 10;

    const params = {
      request: "GetFeatureInfo",
      service: "WMS",
      srs: "EPSG:4326",
      styles: "generic",
      transparent: this.wmsTileLayer.wmsParams.transparent,
      version: this.wmsTileLayer.wmsParams.version,
      format: this.wmsTileLayer.wmsParams.format,
      bbox: bbox,
      height: size.y,
      width: size.x,
      layers: this.wmsTileLayer.wmsParams.layers,
      query_layers: this.wmsTileLayer.wmsParams.layers,
      info_format: "application/json",
      x: Math.round(point.x),
      y: Math.round(point.y),
      buffer: buffer,
      feature_count: featureCount,
    };

    return geoserverWmsUrl + Util.getParamString(params, geoserverWmsUrl, true);
  }
}

export class BaseLayerEntity {
  public baseUri: string;
  public pk: string | number;
  public name: string;
  public feature: any;
  public setores: Map<number, GeoJSON.FeatureCollection>;

  constructor(options: {
    baseUri: string;
    pk: number | string;
    name: string;
    feature: any;
  }) {
    this.baseUri = options.baseUri;
    this.pk = options.pk;
    this.name = options.name;
    this.feature = options.feature;
    this.setores = new Map<number, GeoJSON.FeatureCollection>();
  }

  get lat() {
    return this.feature?.geometry?.coordinates[1];
  }

  get lng() {
    return this.feature?.geometry?.coordinates[0];
  }

  public getLatLng() {
    return { lat: this.lat, lng: this.lng };
  }

  public hasProperty(property: string) {
    return (
      Object.keys(this.feature?.properties || []).findIndex(
        (key) => key === property
      ) >= 0
    );
  }

  public getProperty(property: string) {
    return this.feature?.properties?.[property];
  }

  private async setSetoresByRadiusKm(radiusKm: number) {
    const geojson = await requester.bases.setoresRadius({
      latitude: this.lat,
      longitude: this.lng,
      radiusKm,
    });
    this.setores.set(radiusKm, geojson);
  }

  public async getSetoresByRadiusKm(radiusKm: number) {
    if (radiusKm <= 0) return;
    if (!this.setores.has(radiusKm)) {
      await this.setSetoresByRadiusKm(radiusKm);
    }
    return this.setores.get(radiusKm);
  }
}
