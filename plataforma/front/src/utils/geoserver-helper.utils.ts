import { CRS, LatLng, Map, Point, TileLayer, Util, WMSOptions } from "leaflet";

export const geoserverWmsUrl = `${import.meta.env.VITE_GEOSERVER_URL}/wms`;
export const wmsTileLayerDefaultOptions: WMSOptions = {
  format: "image/png",
  transparent: true,
  attribution: "CulturaEduca",
  crs: CRS.EPSG4326,
};

export function getFeatureInfoUrl(options: {
  wmsTileLayer: TileLayer.WMS;
  point: Point;
  size: Point;
  bbox: String;
  buffer?: number;
  featureCount?: number;
  propertyNames?: string[];
}) {
  const { wmsTileLayer, point, size, bbox } = options;
  const buffer = options.buffer > 0 ? options.buffer : 10;
  const featureCount = options.featureCount > 0 ? options.featureCount : 25;
  const propertyNames =
    options?.propertyNames && options.propertyNames.length > 0
      ? options.propertyNames.join(",")
      : "";

  const params = {
    request: "GetFeatureInfo",
    service: "WMS",
    srs: "EPSG:4326",
    styles: "generic",
    transparent: wmsTileLayer.wmsParams.transparent,
    version: wmsTileLayer.wmsParams.version,
    format: wmsTileLayer.wmsParams.format,
    bbox: bbox,
    height: size.y,
    width: size.x,
    layers: wmsTileLayer.wmsParams.layers,
    query_layers: wmsTileLayer.wmsParams.layers,
    info_format: "application/json",
    x: Math.round(point.x),
    y: Math.round(point.y),
    buffer: buffer,
    feature_count: featureCount,
    ...(propertyNames ? { propertyNames } : {}),
  };

  return geoserverWmsUrl + Util.getParamString(params, geoserverWmsUrl, true);
}
