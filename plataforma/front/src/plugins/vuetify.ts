/**
 * plugins/vuetify.ts
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

// Composables
import { createVuetify } from "vuetify";

// Translations provided by Vuetify
import { pt, en } from "vuetify/locale";

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  theme: {
    themes: {
      light: {
        colors: {
          // primary
          "primary-lighten-5": "#E8EAF6",
          "primary-lighten-4": "#C5CAE9",
          "primary-lighten-3": "#9FA8DA",
          "primary-lighten-2": "#7985CB",
          "primary-lighten-1": "#5C6AC0",
          primary: "#404FB4",
          "primary-darken-1": "#3A47AA",
          "primary-darken-2": "#313E9E",
          "primary-darken-3": "#293492",
          "primary-darken-4": "#1B227E",
          // secondary
          "secondary-lighten-5": "#E3F2FD",
          "secondary-lighten-4": "#BBDEFB",
          "secondary-lighten-3": "#90CAF9",
          "secondary-lighten-2": "#64B5F6",
          "secondary-lighten-1": "#42A5F5",
          secondary: "#2196F3",
          "secondary-darken-1": "#1E88E5",
          "secondary-darken-2": "#1976D2",
          "secondary-darken-3": "#1565C0",
          "secondary-darken-4": "#0D47A1",
          // neutral
          "neutral-lighten-5": "#F8F9FB",
          "neutral-lighten-4": "#EFF0F6",
          "neutral-lighten-3": "#E0E2EB",
          "neutral-lighten-2": "#C2C6D6",
          "neutral-lighten-1": "#A5ABC0",
          neutral: "#8B91A7",
          "neutral-darken-1": "#71788E",
          "neutral-darken-2": "#5B6071",
          "neutral-darken-3": "#444855",
          "neutral-darken-4": "#292B33",
        },
      },
    },
  },
  locale: {
    locale: "pt",
    fallback: "en",
    messages: { pt, en },
  },
});
