import { LatLng } from "leaflet";
import mitt from "mitt";

export const EventBus = mitt();

export const SNACKBAR_ALERT_EVENT_KEY = "snackbar-alert";
export const CONFIRMATION_DIALOG_OPEN_EVENT_KEY = "confirmation-dialog-open";
export const CONFIRMATION_DIALOG_RESPONSE_EVENT_KEY =
  "confirmation-dialog-response";
export const MAIN_MARKER_DRAG_END_EVENT_KEY = "main-marker-drag-end";
export const MAP_POPUP_SELECT_ENTITY_EVENT_KEY =
  "map-popup-select-entity-event-key";

// SNACKBAR
export interface SnackbarAlertEventOptions {
  message?: string;
  color?: string;
  timeout?: number;
  error?: any;
}

export function emitAlert(options: SnackbarAlertEventOptions) {
  EventBus.emit(SNACKBAR_ALERT_EVENT_KEY, options);
}

export function emitAlertError(error: any) {
  emitAlert({ color: "error", timeout: 5000, error });
}

export function emitAlertSuccess(message: string) {
  emitAlert({ message, color: "success", timeout: 3000 });
}

// CONFIRMATION

export function emitConfirmation(
  title,
  options?: {
    description?: string;
    okText?: string;
    okColor?: string;
    okIcon?: string;
    cancelText?: string;
    cancelColor?: string;
    cancelIcon?: string;
    width?: string | number;
  }
) {
  return new Promise((resolve, reject) => {
    const eventId = Date.now();

    // LISTEN TO RESPONSE
    const responseHandler = (response: {
      id: number;
      data: boolean;
      err: any;
    }) => {
      if (response.id === eventId) {
        EventBus.off(CONFIRMATION_DIALOG_RESPONSE_EVENT_KEY, responseHandler);
        if (response.err) {
          reject(response.err);
        } else {
          resolve(response.data);
        }
      }
    };
    EventBus.on(CONFIRMATION_DIALOG_RESPONSE_EVENT_KEY, responseHandler);

    // EMIT OPEN
    EventBus.emit(CONFIRMATION_DIALOG_OPEN_EVENT_KEY, {
      id: eventId,
      data: { title, ...options },
    });
  });
}

// MAIN MARKER

export function emitMainMarkerDragEnd(latLng: LatLng) {
  EventBus.emit(MAIN_MARKER_DRAG_END_EVENT_KEY, latLng);
}

export function emitMapPopupSelectEntityEventKey(entity: {
  uri: string;
  pk: string;
  nome: string;
  geom: GeoJSON.Point;
  uf_sigla: string;
  municipio_codigo: string;
  municipio_nome: string;
}) {
  EventBus.emit(MAP_POPUP_SELECT_ENTITY_EVENT_KEY, entity);
}
