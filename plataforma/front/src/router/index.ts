// Composables
import { emitAlertError } from "@/plugins/eventBus";
import { useAuthStore } from "@/store/auth.store";
import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: RouteRecordRaw[] = [
  // AUTH
  {
    path: "/auth",
    component: () => import("@/layouts/LayoutAuth.vue"),
    children: [
      {
        path: "criar-conta",
        name: "AuthCriarConta",
        component: () =>
          import(
            /* webpackChunkName: "auth" */ "@/pages/auth/AuthCriarConta.vue"
          ),
        meta: {
          public: true,
        },
      },
      {
        path: "entrar",
        name: "AuthEntrar",
        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/auth/AuthEntrar.vue"),
        meta: {
          public: true,
        },
      },
      {
        path: "reenviar-email-validacao/",
        name: "AuthResendValidate",
        component: () =>
          import(
            /* webpackChunkName: "auth" */ "@/pages/auth/AuthResendValidate.vue"
          ),
        meta: {
          public: true,
        },
      },
      {
        path: "validar-email/:token",
        name: "AuthValidarEmail",
        component: () =>
          import(
            /* webpackChunkName: "auth" */ "@/pages/auth/AuthValidarEmail.vue"
          ),
        meta: {
          public: true,
        },
      },
      {
        path: "recuperar-senha",
        name: "AuthEnviarTokenSenha",
        component: () =>
          import(
            /* webpackChunkName: "auth" */ "@/pages/auth/AuthEnviarTokenSenha.vue"
          ),
        meta: {
          public: true,
        },
      },
      {
        path: "trocar-senha/:token",
        name: "AuthTrocarSenha",
        component: () =>
          import(
            /* webpackChunkName: "auth" */ "@/pages/auth/AuthTrocarSenha.vue"
          ),
        meta: {
          public: true,
        },
      },
    ],
  },
  // MAPA
  {
    path: "/mapa",
    component: () => import("@/layouts/LayoutMapa.vue"),
    children: [
      {
        path: "",
        name: "Mapa",
        component: () =>
          import(/* webpackChunkName: "mapa" */ "@/pages/Mapa.vue"),
        meta: {
          public: true,
        },
      },
    ],
  },
  // ENTIDADES
  {
    path: "/detalhes/:baseUri/:pk",
    component: () => import("@/layouts/LayoutEntidade.vue"),
    children: [
      {
        path: "equipamentos",
        name: "EntidadesEntornoBases",
        component: () =>
          import(
            /* webpackChunkName: "entidades" */ "@/pages/entidades/EntidadesEntornoBases.vue"
          ),
        meta: {
          public: true,
          title: "Entorno - Equipamentos",
        },
      },
      {
        path: "entorno",
        name: "EntidadesEntornoEstatisticas",
        component: () =>
          import(
            /* webpackChunkName: "entidades" */ "@/pages/entidades/EntidadesEntornoEstatisticas.vue"
          ),
        meta: {
          public: true,
          title: "Entorno - Dados Públicos",
        },
      },
      {
        path: "",
        name: "EntidadesInicio",
        component: () =>
          import(
            /* webpackChunkName: "entidades" */ "@/pages/entidades/EntidadesInicio.vue"
          ),
        meta: {
          public: true,
          title: "Ficha",
        },
      },
    ],
  },
  // AGENTES
  {
    path: "/agentes/:id",
    component: () => import("@/layouts/LayoutAgente.vue"),
    children: [
      {
        path: "rede",
        name: "AgentesRede",
        component: () =>
          import(
            /* webpackChunkName: "agentes" */ "@/pages/agentes/AgentesRede.vue"
          ),
        meta: {
          public: true,
          title: "Rede",
        },
      },
      {
        path: "entorno",
        name: "AgentesEntorno",
        component: () =>
          import(
            /* webpackChunkName: "agentes" */ "@/pages/agentes/AgentesEntorno.vue"
          ),
        meta: {
          public: true,
          title: "Entorno",
        },
      },
      {
        path: "",
        name: "AgentesInicio",
        component: () =>
          import(
            /* webpackChunkName: "agentes" */ "@/pages/agentes/AgentesInicio.vue"
          ),
        meta: {
          public: true,
          title: "Ficha",
        },
      },
    ],
  },
  // AGENTES COLETIVOS
  {
    path: "/agentes_coletivos/:id",
    component: () => import("@/layouts/LayoutAgenteColetivo.vue"),
    children: [
      {
        path: "atividades",
        name: "AgentesColetivosAtividades",
        component: () =>
          import(
            /* webpackChunkName: "agentes-coletivos" */ "@/pages/agentes-coletivos/AgentesColetivosAtividades.vue"
          ),
        meta: {
          title: "Ações | Atividades",
        },
      },
      {
        path: "projetos",
        name: "AgentesColetivosProjetos",
        component: () =>
          import(
            /* webpackChunkName: "agentes-coletivos" */ "@/pages/agentes-coletivos/AgentesColetivosProjetos.vue"
          ),
        meta: {
          title: "Ações | Projetos",
        },
      },
      {
        path: "",
        name: "AgentesColetivosInicio",
        component: () =>
          import(
            /* webpackChunkName: "agentes-coletivos" */ "@/pages/agentes-coletivos/AgentesColetivosInicio.vue"
          ),
      },
    ],
  },
  // BASES
  {
    path: "/bases",
    component: () => import("@/layouts/LayoutBases.vue"),
    children: [
      {
        path: "",
        name: "BasesList",
        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/bases/BasesList.vue"),
        meta: {
          public: false,
        },
      },
      {
        path: "detail/:id",
        name: "BaseDetail",
        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/bases/BaseDetail.vue"),
        meta: {
          public: false,
        },
      },
    ],
  },
  // BASES
  {
    path: "/bases-config",
    component: () => import("@/layouts/LayoutBasesConfig.vue"),
    redirect: "/bases-config/step-1",
    children: [
      {
        path: "step-1",
        name: "BasesStep1",

        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/bases/BasesStep1.vue"),
        meta: {
          public: false,
        },
      },
      {
        path: "step-2",
        name: "BasesStep2",
        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/bases/BasesStep2.vue"),
        meta: {
          public: false,
        },
      },
      {
        path: "step-3",
        name: "BasesStep3",
        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/bases/BasesStep3.vue"),
        meta: {
          public: false,
        },
      },
      {
        path: "step-4",
        name: "BasesStep4",
        component: () =>
          import(/* webpackChunkName: "auth" */ "@/pages/bases/BasesStep4.vue"),
        meta: {
          public: false,
        },
      },
    ],
  },
  // USUARIO
  {
    path: "/meu_perfil",
    component: () => import("@/layouts/LayoutUsuario.vue"),
    children: [
      {
        path: "cadastro_agente_coletivo",
        name: "AgentesColetivosCadastro",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/agentes-coletivos/AgentesColetivosCadastro.vue"
          ),
      },
      {
        path: "cadastro/atuacao",
        name: "UsuariosCadastroAtuacao",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosCadastroAtuacao.vue"
          ),
        meta: {
          title: "Meu cadastro | Atuação",
        },
      },
      {
        path: "cadastro/identificacao",
        name: "UsuariosCadastroIdentificacao",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosCadastroIdentificacao.vue"
          ),
        meta: {
          title: "Meu cadastro | Identificação",
        },
      },
      {
        path: "cadastro/localizacao",
        name: "UsuariosCadastroLocalizacao",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosCadastroLocalizacao.vue"
          ),
        meta: {
          title: "Meu cadastro | Localização",
        },
      },
      {
        path: "cadastro",
        name: "UsuariosCadastro",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosCadastro.vue"
          ),
        meta: {
          title: "Meu cadastro | Preferências",
        },
      },
      {
        path: "projetos",
        name: "UsuariosProjetos",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosProjetos.vue"
          ),
        meta: {
          title: "Ações | Projetos",
        },
      },
      {
        path: "atividades",
        name: "UsuariosAtividades",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosAtividades.vue"
          ),
        meta: {
          title: "Ações | Atividades",
        },
      },
      {
        path: "",
        name: "UsuariosInicio",
        component: () =>
          import(
            /* webpackChunkName: "meu-perfil" */ "@/pages/usuarios/UsuariosInicio.vue"
          ),
      },
    ],
  },
  // EXTERNAL
  {
    path: "/",
    component: () => import("@/layouts/LayoutExternal.vue"),
    children: [
      {
        path: "rede",
        name: "Rede",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Rede.vue"),
        meta: {
          public: true,
        },
      },
      {
        path: "sobre",
        name: "Sobre",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Sobre.vue"),
        meta: {
          public: true,
        },
      },
      {
        path: "",
        name: "Home",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Home.vue"),
        meta: {
          public: true,
        },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach(async (to, from, next) => {
  const restrictedPattern = /\/bases/;
  const userBaseRequired = restrictedPattern.test(to.path);
  if (to.name === from.name) {
    next();
    return;
  }
  try {
    const auth = await useAuthStore();
    await auth.checkSession();
    if (userBaseRequired) {
      if (!auth.usuario.canDefineBase) {
        emitAlertError("É necessário ter autorização para acessar a base");
        next({ name: "AuthEntrar" });
      }
    }
  } catch (error) {
    if (![401, 403].includes(error.response?.status)) {
      emitAlertError(error);
    } else {
      if (!to.meta.public) {
        emitAlertError(error);
        next({ name: "AuthEntrar" });
        return;
      }
    }
  }
  next();
});

export default router;
