export const mockUsuarios = [
  {
    id: 1,
    email: "fulano@email.com",
    agenteIndividual: {
      agente: {
        nome: "Fulano de Tal",
        endereco: {
          latitude: -23.5558,
          longitude: -46.6396,
        },
      },
    },
  },
];
