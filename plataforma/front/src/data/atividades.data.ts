export const mockAtividades = [
  {
    id: 1,
    projeto: {
      id: 1,
      titulo: "Projeto Teste",
    },
    endereco: "Rua Teste, 123, São Paulo - SP, 12345-678",
    latlng: { lat: -23.173188837559046, lng: -46.891450881958015 },
    imgSrc: null,
    titulo: "Atividade Teste",
    descricao:
      "Laboris velit et deserunt exercitation deserunt eu dolor. Incididunt quis ut ipsum et dolor anim ipsum fugiat. Ut id qui ipsum amet excepteur do duis ullamco tempor esse Lorem labore ipsum ex. Occaecat veniam ut est ea aliquip do sint ad deserunt exercitation. Labore commodo et labore ullamco velit ipsum adipisicing cillum sint commodo Lorem sint aliquip nostrud.",
    dataInicio: new Date(),
    dataFim: new Date(),
    temas: ["Arquitetura e urbanismo", "Arte Contemporânea"],
  },
  {
    id: 2,
    projeto: null,
    endereco: "Avenida Lorem Ipsum, 44, São Paulo - SP, 01423-200",
    latlng: { lat: -23.184156298879476, lng: -46.88836097717286 },
    imgSrc: null,
    titulo: "Lorem Ipsum",
    descricao:
      "Et deserunt laboris dolore dolore consequat id nulla enim. Magna incididunt eiusmod do ea minim cupidatat adipisicing. Enim reprehenderit incididunt adipisicing amet eiusmod aliquip. Fugiat nulla commodo laborum sit. Ullamco ea officia anim ullamco aliquip labore cupidatat sit elit. Aute minim sint nulla aliquip aliquip sit deserunt.",
    dataInicio: new Date(),
    dataFim: null,
    temas: ["Música"],
  },
  {
    id: 3,
    projeto: null,
    endereco: "Rua Teste, 123, São Paulo - SP, 12345-678",
    latlng: { lat: -23.180684681739546, lng: -46.90132141113282 },
    imgSrc: null,
    titulo: "Evento X",
    descricao:
      "Et deserunt laboris dolore dolore consequat id nulla enim. Magna incididunt eiusmod do ea minim cupidatat adipisicing. Enim reprehenderit incididunt adipisicing amet eiusmod aliquip. Fugiat nulla commodo laborum sit. Ullamco ea officia anim ullamco aliquip labore cupidatat sit elit. Aute minim sint nulla aliquip aliquip sit deserunt.",
    dataInicio: new Date(),
    dataFim: null,
    temas: ["Políticas Pública", "Território"],
  },
];
