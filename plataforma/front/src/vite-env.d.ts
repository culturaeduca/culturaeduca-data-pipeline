/// <reference types="vite/client" />

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

interface ImportMetaEnv {
  readonly VITE_APP_TITLE: string;
  readonly VITE_API_URL: string;
  readonly VITE_DATA_PIPELINE_API_URL: string;
  readonly VITE_GEOSERVER_URL: string;
  readonly VITE_MAP_INITIAL_LAT: string;
  readonly VITE_MAP_INITIAL_LNG: string;
  readonly VITE_MAP_INITIAL_ZOOM: string;
  readonly VITE_DEFAULT_BOUNDARY_HIERARCHY: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
