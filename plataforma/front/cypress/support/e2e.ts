import "./commands";

// import { ROUTES } from "../e2e/constant";

declare global {
  namespace Cypress {
    // noinspection JSUnusedGlobalSymbols
    interface Chainable {
      login(): void;
      adminLogin(username?: string, password?: string): Chainable<void>;
    }
  }
}
