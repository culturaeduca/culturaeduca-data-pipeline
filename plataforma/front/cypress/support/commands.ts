/// <reference types="cypress" />
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

import "@testing-library/cypress/add-commands";

Cypress.Commands.add(
  "adminLogin",
  (username = "admin", password = "admin-password") => {
    cy.fixture("user_admin.json").then((authResponse) => {
      authResponse.nome = username;
      cy.intercept("POST", /auth\/sign_in$/, {
        statusCode: 200,
        body: authResponse,
      });
    });

    cy.intercept("GET", /boundaries_hierarchies/, {
      fixture: "boundaries_hierarchies.json",
    }).as("getBoundries");

    // auth/check_session
    cy.intercept("GET", /auth\/check_session$/, {
      fixture: "check_session_admin.json",
    }).as("getSession");

    cy.fixture("meu_perfil_admin.json").then((perfilResponse) => {
      cy.intercept("GET", /usuarios\/meu_perfil/, {
        statusCode: 200,
        body: perfilResponse,
      }).as("getPerfil");
    });

    cy.intercept("GET", /usuarios\/meu_perfil\/favoritos/, {
      fixture: "favoritos.json",
    }).as("getFavoritos");

    cy.intercept("GET", /storage\/profile/, { fixture: "profile.json" }).as(
      "getPhoto"
    );

    cy.visit("/auth/entrar");

    cy.get('input[type="email"]').type(username);
    cy.get('input[type="password"]').type(password);
    cy.get('[type="button"]').contains("Entrar").click();

    cy.url().should("match", /\/meu_perfil/);
    cy.get('[data-test="user-menu-button"]').click();
    cy.get(".v-list-item-title").should("contain", "Admin Bases");
  }
);

Cypress.Commands.add("login", (username = "plumrx") => {
  cy.fixture("user.json").then((authResponse) => {
    authResponse.nome = username;
    cy.intercept("POST", /auth\/sign_in$/, {
      statusCode: 200,
      body: authResponse,
    });
  });

  cy.intercept("GET", /boundaries_hierarchies/, {
    fixture: "boundaries_hierarchies.json",
  }).as("getBoundries");

  // auth/check_session
  cy.intercept("GET", /auth\/check_session$/, {
    fixture: "check_session.json",
  }).as("getSession");

  // click sign in button in home page
  cy.visit("/auth/entrar");

  cy.get('[type="email"]').type("foo@example.com");
  cy.get('[type="password"]').type("12345678");
  cy.get('[type="button"]').contains("Entrar").click();

  cy.url().should("match", /\/meu_perfil/);
});
