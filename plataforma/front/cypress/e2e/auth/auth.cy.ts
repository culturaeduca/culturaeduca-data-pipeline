describe("auth", () => {
  beforeEach(() => {
    cy.intercept("GET", /meu_perfil/, { fixture: "meu_perfil.json" }).as(
      "getPerfil"
    );
    cy.intercept("GET", /storage\/profile/, { fixture: "profile.json" }).as(
      "getPhoto"
    );
    cy.intercept("GET", /usuarios\/meu_perfil\/favoritos/, {
      fixture: "favoritos.json",
    }).as("getFavoritos");
    cy.login();
  });

  describe("login and logout", () => {
    it("should login success when submit a valid login form", () => {
      cy.fixture("meu_perfil.json").then((perfilResponse) => {
        cy.intercept("GET", /usuarios\/meu_perfil/, {
          statusCode: 200,
          body: perfilResponse,
        }).as("getPerfil");
      });

      cy.url().should("match", /\/meu_perfil/);
      cy.get("button").contains("test-user").click();
      cy.get(".v-list-item-title").should("contain", "Sair");
    });
  });
});
