describe("perfil", () => {
  beforeEach(() => {
    cy.intercept("GET", /meu_perfil/, { fixture: "meu_perfil.json" }).as(
      "getPerfil"
    );
    cy.intercept("GET", /usuarios\/meu_perfil\/favoritos/, {
      fixture: "favoritos.json",
    }).as("getFavoritos");

    cy.intercept("GET", /storage\/profile/, { fixture: "profile.json" }).as(
      "getPhoto"
    );
  });

  describe("file upload", () => {
    it("should change the default img to the selected blob", () => {
      cy.login();
      cy.intercept("GET", /auth\/check_session$/, {
        fixture: "check_session.json",
      }).as("getSession");

      cy.fixture("meu_perfil.json").then((perfilResponse) => {
        cy.intercept("GET", /usuarios\/meu_perfil/, {
          statusCode: 200,
          body: perfilResponse,
        }).as("getPerfil");
      });

      cy.intercept("GET", /usuarios\/meu_perfil\/favoritos/, {
        fixture: "favoritos.json",
      }).as("getFavoritos");

      cy.get("button").contains("PERFIL").click();
      cy.get('[href="/meu_perfil/cadastro"]').last().click();
      cy.get('[href="/meu_perfil/cadastro/identificacao"]').click();

      // cy.get(".v-img__img.v-img__img--cover").should("not.exist");
      cy.get(".v-img__img.v-img__img--cover").should("be.visible");
      cy.get("input[type=file]").selectFile("./public/img/rede.jpeg");
      cy.get(".v-img__img.v-img__img--cover")
        .last()
        .should("have.attr", "src")
        .and(
          "match",
          /^blob:http:\/\/localhost:\d{1,5}\/[a-zA-Z0-9\-]{8}-[a-zA-Z0-9\-]{4}-[a-zA-Z0-9\-]{4}-[a-zA-Z0-9\-]{4}-[a-zA-Z0-9\-]{12}$/
        );
    });

    it("should display user image on banner", () => {
      cy.login();
      cy.intercept("GET", /auth\/check_session$/, {
        fixture: "check_session.json",
      }).as("getSession");

      cy.fixture("meu_perfil.json").then((perfilResponse) => {
        cy.intercept("GET", /usuarios\/meu_perfil/, {
          statusCode: 200,
          body: perfilResponse,
        }).as("getPerfil");
      });

      cy.intercept("GET", /usuarios\/meu_perfil\/favoritos/, {
        fixture: "favoritos.json",
      }).as("getFavoritos");

      cy.get(".v-img__img.v-img__img--cover").should("be.visible");
      cy.get(".v-img__img.v-img__img--cover").should(
        "have.attr",
        "src",
        "./public/img/logo.png"
      );
    });
  });
});
