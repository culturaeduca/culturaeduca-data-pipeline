#!/bin/bash

cd /plataforma/front/
yarn install
if [[ $ENV != 'local' ]]; then
	yarn build
else
	yarn dev --host
fi
