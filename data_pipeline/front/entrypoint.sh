#!/bin/bash

cd /data_pipeline/front/
yarn install
if [[ $ENV != 'local' ]]; then
	yarn build
else
	yarn dev --host
fi
