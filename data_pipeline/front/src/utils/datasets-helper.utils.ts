import type { DatasetFormat } from "@backPrisma/client";
import { DatasetFormatEnum } from "@backSrc/datasets/datasets.constants";

const datasetFormatHelper: {
  readonly [k in DatasetFormat]: {
    readonly text: string;
    readonly icon: string;
  };
} = {
  [DatasetFormatEnum.GEOSPATIAL_POINT]: {
    text: "Geospatial Point",
    icon: "mdi-map-marker",
  },
  [DatasetFormatEnum.GEOSPATIAL_LINE]: {
    text: "Geospatial Line",
    icon: "mdi-vector-line",
  },
  [DatasetFormatEnum.GEOSPATIAL_POLYGON]: {
    text: "Geospatial Polygon",
    icon: "mdi-vector-polygon",
  },
  [DatasetFormatEnum.NON_GEOSPATIAL]: {
    text: "Non-Geospatial",
    icon: "mdi-table-large",
  },
} as const;

export function getDatasetFormatIcon(datasetFormat: string) {
  return datasetFormatHelper?.[datasetFormat]?.icon;
}

export function getDatasetFormatText(datasetFormat: string) {
  return datasetFormatHelper?.[datasetFormat]?.text;
}
