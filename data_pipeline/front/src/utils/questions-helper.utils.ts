import {
  Attribute,
  AttributeSeries,
  Dataset,
  Question,
  QuestionColumn,
  QuestionTable,
  QuestionTableJoin,
  Section,
  Version,
} from "@backPrisma/client";

export type QuestionWithTablesAndColumns = Question & {
  questionTables: Array<
    QuestionTable & {
      dataset: Dataset;
      version: Version;
      section: Section;
      questionTableJoinsLeft: QuestionTableJoin;
      questionTableJoinsRight: QuestionTableJoin;
    }
  >;
  questionColumns: Array<
    QuestionColumn & {
      attributeSeries: AttributeSeries & { attributes: Attribute[] };
    }
  >;
};
