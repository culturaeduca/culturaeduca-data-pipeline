import {
  Attribute,
  AttributeSeries,
  AttributeSeriesRelationship,
  Dataset,
  QuestionColumn,
  Section,
  Version,
} from "@backPrisma/client";

// NEW

type AttributeSeriesAndRelations = AttributeSeries & {
  attributes: (Attribute & { section: Section })[];
  relationshipsForeignKey: AttributeSeriesRelationship[];
  relationshipsReference: AttributeSeriesRelationship[];
};

export enum QuestionQueryTableJoinTypeEnum {
  LEFT = "LEFT",
  RIGHT = "RIGHT",
  INNER = "INNER",
  FULL = "FULL",
}

export enum QuestionQueryTableJoinOnOperatorEnum {
  EQUAL = "EQUAL",
  NOT_EQUAL = "NOT_EQUAL",
  GREATER_THAN = "GREATER_THAN",
  GREATER_THAN_OR_EQUAL = "GREATER_THAN_OR_EQUAL",
  LESS_THAN = "LESS_THAN",
  LESS_THAN_OR_EQUAL = "LESS_THAN_OR_EQUAL",
}

export interface QuestionQueryTableColumnAs {
  name?: string;
  type?: string;
  indexable?: boolean;
  sensitiveData?: boolean;
  defaultValue?: string | null;
  decodeValue?: any;
}

export interface QuestionQueryTableColumn {
  _selected: boolean;
  _as: QuestionQueryTableColumnAs | null;
  attributeSeries?: AttributeSeriesAndRelations;
  questionColumn?: QuestionColumn;
}

export interface QuestionQueryTableJoinOnColumn {
  tableKey: number;
  attributeSeriesId: number;
}

export interface QuestionQueryTableJoinOn {
  key: number;
  left: QuestionQueryTableJoinOnColumn;
  right: QuestionQueryTableJoinOnColumn;
  operator: QuestionQueryTableJoinOnOperatorEnum;
}

export interface QuestionQueryTableJoin {
  type: QuestionQueryTableJoinTypeEnum;
  on: QuestionQueryTableJoinOn[];
}

export interface QuestionQueryTable {
  key: number;

  datasetId: number | null;
  versionId: number | null;
  sectionId: number | null;

  dataset?: Dataset | null;
  version?: Version | null;
  section?: Section | null;

  columns: QuestionQueryTableColumn[];

  join: QuestionQueryTableJoin | null;
}

const questionQueryTableJoinTypeFormatHelper = {
  [QuestionQueryTableJoinTypeEnum.LEFT]: {
    icon: "mdi-set-left-center",
    text: "Left join",
  },
  [QuestionQueryTableJoinTypeEnum.RIGHT]: {
    icon: "mdi-set-center-right",
    text: "Right join",
  },
  [QuestionQueryTableJoinTypeEnum.INNER]: {
    icon: "mdi-set-center",
    text: "Inner join",
  },
  [QuestionQueryTableJoinTypeEnum.FULL]: {
    icon: "mdi-set-all",
    text: "Full outer join",
  },
};

export function getQuestionQueryTableJoinType(
  key: "icon" | "text",
  type: QuestionQueryTableJoinTypeEnum
) {
  return questionQueryTableJoinTypeFormatHelper?.[type]?.[key];
}

const questionQueryTableJoinOnOperatorFormatHelper = {
  [QuestionQueryTableJoinOnOperatorEnum.EQUAL]: {
    text: "Equal",
    icon: "mdi-equal",
  },
  [QuestionQueryTableJoinOnOperatorEnum.NOT_EQUAL]: {
    text: "Not equal",
    icon: "mdi-not-equal-variant",
  },
  [QuestionQueryTableJoinOnOperatorEnum.GREATER_THAN]: {
    text: "Greater than",
    icon: "mdi-greater-than",
  },
  [QuestionQueryTableJoinOnOperatorEnum.GREATER_THAN_OR_EQUAL]: {
    text: "Greater than or equal",
    icon: "mdi-greater-than-or-equal",
  },
  [QuestionQueryTableJoinOnOperatorEnum.LESS_THAN]: {
    text: "Less than",
    icon: "mdi-less-than",
  },
  [QuestionQueryTableJoinOnOperatorEnum.LESS_THAN_OR_EQUAL]: {
    text: "Less than or equal",
    icon: "mdi-less-than-or-equal",
  },
};

export function getQuestionQueryTableJoinOnOperator(
  key: "icon" | "text",
  type: QuestionQueryTableJoinOnOperatorEnum
) {
  return questionQueryTableJoinOnOperatorFormatHelper?.[type]?.[key];
}

// OLD

type AttributeAndRelations = Attribute & {
  _selected: boolean;
  series: AttributeSeries & {
    relationshipsForeignKey: AttributeSeriesRelationship[];
    relationshipsReference: AttributeSeriesRelationship[];
  };
};

type SectionAndRelations = Section & {
  attributes: AttributeAndRelations[];
  version: Version & { dataset: Dataset };
};

export interface JoinTableOn {
  key: number;
  operation: JoinTableOnOperationEnum;
  leftAttribute: AttributeAndRelations;
  rightAttribute: AttributeAndRelations;
}

export interface InitialTable {
  key: number;
  section: SectionAndRelations;
}

export interface JoinTable extends InitialTable {
  type: JoinTableTypeEnum;
  on: JoinTableOn[];
}

export enum JoinTableTypeEnum {
  LEFT = "LEFT",
  RIGHT = "RIGHT",
  INNER = "INNER",
  FULL = "FULL",
}

const joinTableTypeFormatHelper = {
  [JoinTableTypeEnum.LEFT]: {
    text: "Left outer join",
    icon: "mdi-set-left-center",
  },
  [JoinTableTypeEnum.RIGHT]: {
    text: "Right outer join",
    icon: "mdi-set-center-right",
  },
  [JoinTableTypeEnum.INNER]: {
    text: "Inner join",
    icon: "mdi-set-center",
  },
  [JoinTableTypeEnum.FULL]: {
    text: "Full outer join",
    icon: "mdi-set-all",
  },
};

export function getJoinTableTypeIcon(type: JoinTableTypeEnum) {
  return joinTableTypeFormatHelper[type]?.icon;
}

export function getJoinTableTypeText(type: JoinTableTypeEnum) {
  return joinTableTypeFormatHelper[type]?.text;
}

export enum JoinTableOnOperationEnum {
  EQUAL = "EQUAL",
  NOT_EQUAL = "NOT_EQUAL",
  GREATER_THAN = "GREATER_THAN",
  GREATER_THAN_OR_EQUAL = "GREATER_THAN_OR_EQUAL",
  LESS_THAN = "LESS_THAN",
  LESS_THAN_OR_EQUAL = "LESS_THAN_OR_EQUAL",
}

const joinTableOnOperationFormatHelper = {
  [JoinTableOnOperationEnum.EQUAL]: {
    text: "Equal",
    icon: "mdi-equal",
  },
  [JoinTableOnOperationEnum.NOT_EQUAL]: {
    text: "Not equal",
    icon: "mdi-not-equal-variant",
  },
  [JoinTableOnOperationEnum.GREATER_THAN]: {
    text: "Greater than",
    icon: "mdi-greater-than",
  },
  [JoinTableOnOperationEnum.GREATER_THAN_OR_EQUAL]: {
    text: "Greater than or equal",
    icon: "mdi-greater-than-or-equal",
  },
  [JoinTableOnOperationEnum.LESS_THAN]: {
    text: "Less than",
    icon: "mdi-less-than",
  },
  [JoinTableOnOperationEnum.LESS_THAN_OR_EQUAL]: {
    text: "Less than or equal",
    icon: "mdi-less-than-or-equal",
  },
};

export function getJoinTableOnOperationIcon(
  onOperation: JoinTableOnOperationEnum
) {
  return joinTableOnOperationFormatHelper[onOperation]?.icon;
}

export function getJoinTableOnOperationText(
  onOperation: JoinTableOnOperationEnum
) {
  return joinTableOnOperationFormatHelper[onOperation]?.text;
}
