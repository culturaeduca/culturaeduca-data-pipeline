import { Version } from "@backPrisma/client";

export function getVersionDate(version: Version): string {
  let name = `${version.startDateYear}`;
  if (version.startDateMonth) {
    name = `${name}/${version.startDateMonth.toString().padStart(2, "0")}`;
    if (version.startDateDay) {
      name = `${name}/${version.startDateDay.toString().padStart(2, "0")}`;
    }
  }
  if (version.endDateYear) {
    name = `${name}-`;
    if (version.endDateYear !== version.startDateYear) {
      name = `${name}${version.endDateYear}`;
    }
    if (version.endDateMonth) {
      name = `${name}${
        version.endDateYear !== version.startDateYear ? "/" : ""
      }${version.endDateMonth.toString().padStart(2, "0")}`;
      if (version.endDateDay) {
        name = `${name}/${version.endDateDay.toString().padStart(2, "0")}`;
      }
    }
  }
  return name;
}

export function getVersionStartDate(version: Version): string {
  const startDate = new Date(
    version.startDateYear,
    (version.startDateMonth || 1) - 1,
    version.startDateDay || 1
  );
  return startDate.toLocaleDateString("pt-BR");
}

export function getVersionEndDate(version: Version): string {
  const endDate = new Date();
  if (!version.endDateYear) {
    endDate.setFullYear(version.startDateYear + 1);
    endDate.setMonth((version.startDateMonth || 1) - 1);
    endDate.setDate(version.startDateDay || 1);
  } else if (!version.endDateMonth) {
    endDate.setFullYear(version.endDateYear);
    endDate.setMonth(version.startDateMonth || 1);
    endDate.setDate(version.startDateDay || 1);
  } else if (!version.endDateDay) {
    endDate.setFullYear(version.endDateYear);
    endDate.setMonth((version.startDateMonth || 1) - 1);
    endDate.setDate((version.endDateDay || 1) + 1);
  }
  endDate.setDate(endDate.getDate() - 1);
  return endDate.toLocaleDateString("pt-BR");
}
