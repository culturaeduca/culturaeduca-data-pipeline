import {
  AttributeTypeEnum,
  AttributeIndexTypeEnum,
} from "@backSrc/attributes/attributes.constants";

export const attributeTypesDescriptions = {
  [AttributeTypeEnum.BOOLEAN]: "a binary value",
  [AttributeTypeEnum.INT]: "32-bit signed integer",
  [AttributeTypeEnum.LONG]: "64-bit signed integer",
  [AttributeTypeEnum.FLOAT]:
    "single precision (32-bit) IEEE 754 floating-point number",
  [AttributeTypeEnum.DOUBLE]:
    "double precision (64-bit) IEEE 754 floating-point number",
  [AttributeTypeEnum.BYTES]: "sequence of 8-bit unsigned bytes",
  [AttributeTypeEnum.STRING]: "unicode character sequence",
  [AttributeTypeEnum.GEOMETRY]: "geometric geospatial data",
  [AttributeTypeEnum.GEOGRAPHY]: "geographic geospatial data",
};

export const attributeIndexTypesTitles = {
  [AttributeIndexTypeEnum.BTREE]: "B-tree",
  [AttributeIndexTypeEnum.HASH]: "Hash",
  [AttributeIndexTypeEnum.GIST]: "GiST",
  [AttributeIndexTypeEnum.SPGIST]: "SP-GiST",
  [AttributeIndexTypeEnum.GIN]: "GIN",
  [AttributeIndexTypeEnum.BRIN]: "BRIN",
};

export const attributeIndexTypesDescriptions = {
  [AttributeIndexTypeEnum.BTREE]:
    "The default and most common index type, suitable for most general-purpose indexing tasks.",
  [AttributeIndexTypeEnum.HASH]: "Hash: Used for equality comparisons.",
  [AttributeIndexTypeEnum.GIST]:
    "(Generalized Search Tree) Supports various types of searches, such as spatial data with PostGIS.",
  [AttributeIndexTypeEnum.SPGIST]:
    "(Space-Partitioned Generalized Search Tree) Optimized for certain non-balanced data structures, such as quadtrees and k-d trees.",
  [AttributeIndexTypeEnum.GIN]:
    "(Generalized Inverted Index) Efficient for indexing composite values, like arrays, and full-text search.",
  [AttributeIndexTypeEnum.BRIN]:
    "(Block Range INdex) Efficient for large tables where the column values are correlated with their physical location on disk.",
};
