export function filterFormatDate(date: string | number | Date) {
  if (!date) return "";
  return new Intl.DateTimeFormat("pt-BR", {
    dateStyle: "short",
    timeStyle: "medium",
  }).format(new Date(date));
}

export function filterFormatBytes(bytes: number): string {
  return new Intl.NumberFormat(navigator.language, {
    notation: "compact",
    style: "unit",
    unit: "byte",
    unitDisplay: "narrow",
    maximumFractionDigits: 2,
    minimumFractionDigits: 0,
  }).format(bytes);
}
