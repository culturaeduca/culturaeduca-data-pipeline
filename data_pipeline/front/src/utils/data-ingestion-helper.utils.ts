import type { DataIngestionFileType } from "@backPrisma/client";
import { DataIngestionFileTypeEnum } from "@backSrc/data-ingestions/data-ingestions.contants";

const dataIngestionFileTypeHelper: {
  readonly [k in DataIngestionFileType]: {
    readonly text: string;
    readonly icon: string;
  };
} = {
  [DataIngestionFileTypeEnum.CSV]: {
    text: "CSV",
    icon: "mdi-file-delimited",
  },
  [DataIngestionFileTypeEnum.GEOJSON]: {
    text: "GeoJSON",
    icon: "mdi-file-marker",
  },
  [DataIngestionFileTypeEnum.JSON]: {
    text: "JSON",
    icon: "mdi-file-code",
  },
  [DataIngestionFileTypeEnum.SHAPEFILE]: {
    text: "Shapefile",
    icon: "mdi-folder-zip",
  },
} as const;

export function getDataIngestionFileTypeIcon(dataIngestionFileType: string) {
  return dataIngestionFileTypeHelper?.[dataIngestionFileType]?.icon;
}

export function getDataIngestionFileTypeText(dataIngestionFileType: string) {
  return dataIngestionFileTypeHelper?.[dataIngestionFileType]?.text;
}
