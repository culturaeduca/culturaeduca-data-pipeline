import axios from "axios";
import { DataIngestion, DataIngestionLog, Section } from "@backPrisma/client";
import {
  CreatePresignedUploadDataIngestionDto,
  GetCollectionDataIngestionQueryDto,
  LoadDataDataIngestionDto,
  ParseFileDataIngestionDto,
} from "@backSrc/data-ingestions/dto";

const checkUploadedFile = async (dataIngestionId: number) => {
  const res = await axios.post<string>(
    `data_ingestions/${dataIngestionId}/check_uploaded_file`,
    null
  );
  return res.data;
};

const createPresignedUploadUrl = async (
  dataIngestionId: number,
  dto: CreatePresignedUploadDataIngestionDto
) => {
  const res = await axios.post<string>(
    `data_ingestions/${dataIngestionId}/presigned_upload_url`,
    dto
  );
  return res.data;
};

const findCurrentLog = async (dataIngestionId: number) => {
  const res = await axios.get<DataIngestionLog>(
    `data_ingestions/${dataIngestionId}/logs/current`
  );
  return res.data;
};

const findLogs = async (dataIngestionId: number) => {
  const res = await axios.get<DataIngestionLog[]>(
    `data_ingestions/${dataIngestionId}/logs`
  );
  return res.data;
};

const findOne = async (dataIngestionId: number) => {
  const res = await axios.get<DataIngestion & { section: Section }>(
    `data_ingestions/${dataIngestionId}`
  );
  return res.data;
};

const getCollection = async (
  dataIngestionId: number,
  queryObject: GetCollectionDataIngestionQueryDto
) => {
  const res = await axios.get<{
    total: number;
    results: any[];
  }>(`data_ingestions/${dataIngestionId}/collection`, {
    params: queryObject,
  });
  return res.data;
};

const getFileInfo = async (dataIngestionId: number) => {
  const res = await axios.get<any>(
    `data_ingestions/${dataIngestionId}/file_info`
  );
  return res.data;
};

const loadData = async (
  dataIngestionId: number,
  dto: LoadDataDataIngestionDto
) => {
  await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/load_data`,
    dto
  );
};

const mapAttributes = async (dataIngestionId: number, dto: any) => {
  await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/map_attributes`,
    dto
  );
};

const parseFile = async (
  dataIngestionId: number,
  dto: ParseFileDataIngestionDto
) => {
  await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/parse_file`,
    dto
  );
};

const remove = async (dataIngestionId: number) => {
  const res = await axios.delete<DataIngestion>(
    `data_ingestions/${dataIngestionId}`
  );
  return res.data;
};

const removeCurrentLog = async (dataIngestionId: number) => {
  const res = await axios.delete<DataIngestionLog>(
    `data_ingestions/${dataIngestionId}/logs/current`
  );
  return res.data;
};

const uploadFile = async (dataIngestionId: number, formData: FormData) => {
  const res = await axios.post<DataIngestion>(
    `data_ingestions/${dataIngestionId}/upload_file`,
    formData
  );
  return res.data;
};

export default {
  checkUploadedFile,
  createPresignedUploadUrl,
  findCurrentLog,
  findLogs,
  findOne,
  getCollection,
  getFileInfo,
  loadData,
  mapAttributes,
  parseFile,
  remove,
  removeCurrentLog,
  uploadFile,
};
