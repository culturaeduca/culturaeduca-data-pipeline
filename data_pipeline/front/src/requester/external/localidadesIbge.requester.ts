import axios from "axios";

type IbgeMunicipio = {
  id: number;
  nome: string;
  microrregiao: {
    id: number;
    nome: string;
    mesorregiao: {
      id: number;
      nome: string;
      UF: {
        id: number;
        nome: string;
        sigla: string;
        regiao: {
          id: number;
          nome: string;
          sigla: string;
        };
      };
    };
  };
};

type IbgeUF = {
  id: number;
  nome: string;
  sigla: string;
  regiao: {
    id: number;
    nome: string;
    sigla: string;
  };
};

// localidades ibge

async function municipios() {
  const response = await axios.get<IbgeMunicipio[]>(
    `https://servicodados.ibge.gov.br/api/v1/localidades/municipios`,
    {
      withCredentials: false,
      params: {
        orderBy: "nome",
      },
    }
  );
  return response.data;
}

async function municipiosUf(ufSigla: string) {
  const response = await axios.get<IbgeMunicipio[]>(
    `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${ufSigla}/municipios`,
    {
      withCredentials: false,
      params: {
        orderBy: "nome",
      },
    }
  );
  return response.data;
}

async function ufs() {
  const response = await axios.get<IbgeUF[]>(
    "https://servicodados.ibge.gov.br/api/v1/localidades/estados",
    {
      withCredentials: false,
      params: {
        orderBy: "nome",
      },
    }
  );
  return response.data;
}

export default {
  municipios,
  municipiosUf,
  ufs,
};
