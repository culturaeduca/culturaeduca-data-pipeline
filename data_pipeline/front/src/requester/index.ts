import axios from "axios";

import attributes from "./attributes.requester";
import attributesSeries from "./attributes-series.requester";
import attributesSeriesRelationships from "./attributes-series-relationships.requester";
import auth from "./auth.requester";
import boundariesHierarchies from "./boundaries-hierarchies.requester";
import dataIngestions from "./data-ingestions.requester";
import datasets from "./datasets.requester";
import external from "./external";
import questions from "./questions.requester";
import sections from "./sections.requester";
import versions from "./versions.requester";

axios.defaults.baseURL =
  import.meta.env.VITE_API_URL || "http://localhost:3000";
axios.defaults.withCredentials = true;
// interceptor to add the access token to the request
axios.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem(
    "accessToken"
  )}`;
  return config;
});

export default {
  attributes,
  attributesSeries,
  attributesSeriesRelationships,
  auth,
  boundariesHierarchies,
  dataIngestions,
  datasets,
  external,
  questions,
  sections,
  versions,
};
