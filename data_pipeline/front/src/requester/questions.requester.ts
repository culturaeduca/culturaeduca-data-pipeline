import axios from "axios";
import { GeoJSON } from "leaflet";
import {
  CreateQuestionDto,
  PreviewQueryGeojsonQuestionQueryDto,
  PreviewQueryQuestionQueryDto,
} from "@backSrc/questions/dto";
import { Question } from "@backPrisma/client";
import { QuestionWithTablesAndColumns } from "@/utils/questions-helper.utils";

const create = async (dto: CreateQuestionDto) => {
  const res = await axios.post<Question>(`questions`, dto);
  return res.data;
};

const findAll = async () => {
  const res = await axios.get<Question[]>("questions");
  return res.data;
};

const findOne = async (questionId: number) => {
  const res = await axios.get<QuestionWithTablesAndColumns>(
    `questions/${questionId}`
  );
  return res.data;
};

const getPreviewQueryGeojson = async (
  queryDto: PreviewQueryGeojsonQuestionQueryDto
) => {
  const res = await axios.get<GeoJSON.Feature>(
    `questions/preview_query/geojson`,
    {
      params: queryDto,
    }
  );
  return res.data;
};

const previewQuery = async (
  dto: CreateQuestionDto["query"],
  queryDto: PreviewQueryQuestionQueryDto
) => {
  const res = await axios.post<{
    query: any;
    total: number;
    results: any[];
  }>(`questions/preview_query`, dto, {
    params: queryDto,
  });
  return res.data;
};

const remove = async (questionId: number) => {
  const res = await axios.delete<Question>(`questions/${questionId}`);
  return res.data;
};

export default {
  create,
  findAll,
  findOne,
  getPreviewQueryGeojson,
  previewQuery,
  remove,
};
