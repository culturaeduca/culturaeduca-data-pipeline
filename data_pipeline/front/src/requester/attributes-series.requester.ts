import axios from "axios";
import {
  Attribute,
  AttributeSeries,
  AttributeSeriesRelationship,
  Dataset,
  Section,
} from "@backPrisma/client";
import { CreateRelationshipForeignKeyAttributeSeriesDto } from "@backSrc/attributes-series/dto";

const createRelationshipForeignKey = async (
  id: number,
  dto: CreateRelationshipForeignKeyAttributeSeriesDto
) => {
  const res = await axios.post<AttributeSeriesRelationship>(
    `attributes_series/${id}/relationships_foreign_key/`,
    dto
  );
  return res.data;
};

const findOne = async (id: number) => {
  const res = await axios.get<
    AttributeSeries & {
      attributes: (Attribute & { section: Section })[];
      dataset: Dataset;
    }
  >(`attributes_series/${id}`);
  return res.data;
};

export default { createRelationshipForeignKey, findOne };
