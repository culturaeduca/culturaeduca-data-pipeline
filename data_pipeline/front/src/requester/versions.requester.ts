import axios from "axios";
import { Attribute, DataIngestion, Version, Section } from "@backPrisma/client";
import { CreateSectionDto } from "@backSrc/sections/dto";
import { UpdateVersionDto } from "@backSrc/versions/dto";

const createSection = async (versionId: number, dto: CreateSectionDto) => {
  const res = await axios.post<Section>(`versions/${versionId}/sections`, dto);
  return res.data;
};

const findAttributes = async (versionId: number) => {
  const res = await axios.get<(Attribute & { section: Section })[]>(
    `versions/${versionId}/attributes`
  );
  return res.data;
};

const findDataIngestions = async (versionId: number) => {
  const res = await axios.get<(DataIngestion & { section: Section })[]>(
    `versions/${versionId}/data_ingestions`
  );
  return res.data;
};

const findOne = async (versionId: number) => {
  const res = await axios.get<Version>(`versions/${versionId}`);
  return res.data;
};

const findSections = async (versionId: number) => {
  const res = await axios.get<Section[]>(`versions/${versionId}/sections`);
  return res.data;
};

const publish = async (versionId: number) => {
  const res = await axios.patch<Version>(`versions/${versionId}/publish`);
  return res.data;
};

const remove = async (versionId: number) => {
  const res = await axios.delete<Version>(`versions/${versionId}`);
  return res.data;
};

const setDefault = async (versionId: number) => {
  const res = await axios.patch<Version>(`versions/${versionId}/set_default`);
  return res.data;
};

const unpublish = async (versionId: number) => {
  const res = await axios.patch<Version>(`versions/${versionId}/unpublish`);
  return res.data;
};

const update = async (versionId: number, dto: UpdateVersionDto) => {
  const res = await axios.patch<Version>(`versions/${versionId}`, dto);
  return res.data;
};

export default {
  createSection,
  findAttributes,
  findDataIngestions,
  findOne,
  findSections,
  publish,
  remove,
  setDefault,
  unpublish,
  update,
};
