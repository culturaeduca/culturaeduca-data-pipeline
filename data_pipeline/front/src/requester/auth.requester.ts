import axios from "axios";

const profile = async () => {
  const res = await axios.get<{ id: number; name: string; email: string }>(
    `auth/profile`
  );
  return res.data;
};

const signIn = async (dto: { username: string; password: string }) => {
  const res = await axios.post<{ accessToken: string }>(`auth/sign_in`, dto);
  return res.data;
};

export default {
  profile,
  signIn,
};
