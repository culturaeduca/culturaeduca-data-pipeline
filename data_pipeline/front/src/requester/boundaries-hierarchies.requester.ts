import axios from "axios";

import {
  BoundaryHierarchy,
  Boundary,
  Question,
  Section,
} from "@backPrisma/client";
import {
  CreateBoundaryHierarchyDto,
  UpdateBoundaryHierarchyDto,
} from "@backSrc/boundaries-hierarchies/dto";
import { CreateBoundaryDto, UpdateBoundaryDto } from "@backSrc/boundaries/dto";
export type BoundaryHierarchyWithBoundaries = BoundaryHierarchy & {
  boundaries: BoundaryWithSectionAndQuestion[];
};
export type BoundaryWithSectionAndQuestion = Boundary & {
  question: Question;
  section: Section;
};

const create = async (dto: CreateBoundaryHierarchyDto) => {
  const res = await axios.post<BoundaryHierarchy>(
    "boundaries_hierarchies",
    dto
  );
  return res.data;
};

const createBoundary = async (uri: string, dto: CreateBoundaryDto) => {
  const res = await axios.post<BoundaryWithSectionAndQuestion>(
    `boundaries_hierarchies/${uri}/boundaries`,
    dto
  );
  return res.data;
};

const findAll = async () => {
  const res = await axios.get<BoundaryHierarchy[]>("boundaries_hierarchies");
  return res.data;
};

const findOne = async (boundaryHierarchyUri: string) => {
  const res = await axios.get<BoundaryHierarchyWithBoundaries>(
    `boundaries_hierarchies/${boundaryHierarchyUri}`
  );
  return res.data;
};

const remove = async (boundaryHierarchyUri: string) => {
  const res = await axios.delete<BoundaryHierarchy>(
    `boundaries_hierarchies/${boundaryHierarchyUri}`
  );
  return res.data;
};

const removeBoundary = async (boundaryId: number) => {
  const res = await axios.delete<Boundary>(`boundaries/${boundaryId}`);
  return res.data;
};

const update = async (
  boundaryHierarchyUri: string,
  dto: UpdateBoundaryHierarchyDto
) => {
  const res = await axios.patch<BoundaryHierarchy>(
    `boundaries_hierarchies/${boundaryHierarchyUri}`,
    dto
  );
  return res.data;
};

const updateBoundary = async (boundaryId: number, dto: UpdateBoundaryDto) => {
  const res = await axios.patch<BoundaryWithSectionAndQuestion>(
    `boundaries/${boundaryId}`,
    dto
  );
  return res.data;
};

export default {
  create,
  createBoundary,
  findAll,
  findOne,
  remove,
  removeBoundary,
  update,
  updateBoundary,
};
