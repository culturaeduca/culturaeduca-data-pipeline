import axios from "axios";
import {
  Attribute,
  AttributeSeries,
  AttributeSeriesRelationship,
  Dataset,
  Section,
  Version,
} from "@backPrisma/client";
import { CreateDatasetDto, UpdateDatasetDto } from "@backSrc/datasets/dto";
import { CreateVersionDto } from "@backSrc/versions/dto";

const create = async (dto: CreateDatasetDto) => {
  const res = await axios.post<Dataset>("datasets", dto);
  return res.data;
};

const createVersion = async (datasetId: number, dto: CreateVersionDto) => {
  const res = await axios.post<Version>(`datasets/${datasetId}/versions`, dto);
  return res.data;
};

const findAll = async () => {
  const res = await axios.get<Dataset[]>("datasets");
  return res.data;
};

const findAttributesSeries = async (datasetId: number) => {
  const res = await axios.get<
    Array<
      AttributeSeries & {
        attributes: (Attribute & { section: Section })[];
        relationshipsForeignKey: AttributeSeriesRelationship[];
        relationshipsReference: AttributeSeriesRelationship[];
      }
    >
  >(`datasets/${datasetId}/attributes_series`);
  return res.data;
};

const findOne = async (datasetId: number) => {
  const res = await axios.get<Dataset>(`datasets/${datasetId}`);
  return res.data;
};

const findVersions = async (datasetId: number) => {
  const res = await axios.get<Version[]>(`datasets/${datasetId}/versions`);
  return res.data;
};

const remove = async (versionId: number) => {
  const res = await axios.delete<Dataset>(`datasets/${versionId}`);
  return res.data;
};

const update = async (datasetId: number, dto: UpdateDatasetDto) => {
  const res = await axios.patch<Dataset>(`datasets/${datasetId}`, dto);
  return res.data;
};

export default {
  create,
  createVersion,
  findAll,
  findAttributesSeries,
  findOne,
  findVersions,
  remove,
  update,
};
