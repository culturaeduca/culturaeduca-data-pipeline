import axios from "axios";

import { AttributeSeriesRelationship } from "@backPrisma/client";

const remove = async (id: number) => {
  const res = await axios.delete<AttributeSeriesRelationship>(
    `attributes_series_relationships/${id}`
  );
  return res.data;
};

export default { remove };
