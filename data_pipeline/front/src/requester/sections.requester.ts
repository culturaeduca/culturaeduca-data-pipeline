import axios from "axios";

import {
  Attribute,
  AttributeSeries,
  AttributeSeriesRelationship,
  DataIngestion,
  Section,
} from "@backPrisma/client";
import { CreateAttributeDto } from "@backSrc/attributes/dto";
import { CreateDataIngestionDto } from "@backSrc/data-ingestions/dto";
import {
  GetDataTableSectionQueryDto,
  UpdateSectionDto,
} from "@backSrc/sections/dto";

const createAttribute = async (sectionId: number, dto: CreateAttributeDto) => {
  const res = await axios.post<Attribute>(
    `sections/${sectionId}/attributes`,
    dto
  );
  return res.data;
};

const createAttributeBulk = async (
  sectionId: number,
  dto: CreateAttributeDto[]
) => {
  const res = await axios.post<Attribute>(
    `sections/${sectionId}/attributes_bulk`,
    dto
  );
  return res.data;
};

const createDataIngestion = async (
  sectionId: number,
  dto: CreateDataIngestionDto
) => {
  const res = await axios.post<DataIngestion>(
    `sections/${sectionId}/data_ingestions`,
    dto
  );
  return res.data;
};

const findAttributes = async (sectionId: number) => {
  const res = await axios.get<
    (Attribute & {
      series: AttributeSeries & {
        relationshipsForeignKey: AttributeSeriesRelationship[];
        relationshipsReference: AttributeSeriesRelationship[];
      };
    })[]
  >(`sections/${sectionId}/attributes`);
  return res.data;
};

const findDataIngestions = async (sectionId: number) => {
  const res = await axios.get<DataIngestion[]>(
    `sections/${sectionId}/data_ingestions`
  );
  return res.data;
};

const findOne = async (sectionId: number) => {
  const res = await axios.get<Section>(`sections/${sectionId}`);
  return res.data;
};

const getDataTable = async (
  sectionId: number,
  queryObject: GetDataTableSectionQueryDto
) => {
  const res = await axios.get<{
    total: number;
    results: any[];
  }>(`sections/${sectionId}/data_table`, {
    params: queryObject,
  });
  return res.data;
};

const getDataGeojson = async (sectionId: number, dataId: number) => {
  const res = await axios.get<GeoJSON.Feature>(
    `sections/${sectionId}/data_geojson/${dataId}`
  );
  return res.data;
};

const publish = async (sectionId: number) => {
  const res = await axios.patch<Section>(`sections/${sectionId}/publish`);
  return res.data;
};

const remove = async (sectionId: number) => {
  const res = await axios.delete<Section>(`sections/${sectionId}`);
  return res.data;
};

const setDefault = async (sectionId: number) => {
  const res = await axios.patch<Section>(`sections/${sectionId}/set_default`);
  return res.data;
};

const unpublish = async (sectionId: number) => {
  const res = await axios.patch<Section>(`sections/${sectionId}/unpublish`);
  return res.data;
};

const update = async (sectionId: number, dto: UpdateSectionDto) => {
  const res = await axios.patch<Section>(`sections/${sectionId}`, dto);
  return res.data;
};

export default {
  createAttribute,
  createAttributeBulk,
  createDataIngestion,
  findAttributes,
  findDataIngestions,
  findOne,
  getDataGeojson,
  getDataTable,
  publish,
  remove,
  setDefault,
  unpublish,
  update,
};
