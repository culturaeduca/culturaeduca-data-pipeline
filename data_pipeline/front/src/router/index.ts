// Composables
import { emitAlertError } from "@/plugins/eventBus";
import { useAuthStore } from "@/store/auth.store";
import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";

const routes: RouteRecordRaw[] = [
  /**
   * DEFAULT ROUTES
   */
  {
    path: "/",
    component: () =>
      import(/* webpackChunkName: "home" */ "@/layouts/LayoutDefault.vue"),
    /**
     * LAYOUT DEFAULT
     */
    children: [
      {
        path: "",
        name: "Home",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/Home.vue"),
      },
      {
        path: "auth/signin",
        name: "AuthSignIn",
        component: () =>
          import(/* webpackChunkName: "home" */ "@/pages/auth/AuthSignIn.vue"),
        meta: {
          public: true,
        },
      },
      {
        path: "boundaries_hierarchies",
        name: "BoundariesHierarchiesIndex",
        component: () =>
          import(
            /* webpackChunkName: "boundaries-hierarchies" */ "@/pages/boundaries-hierarchies/BoundariesHierarchiesIndex.vue"
          ),
      },
      {
        path: "boundaries_hierarchies/:boundaryHierarchyUri",
        component: () =>
          import(
            /* webpackChunkName: "boundaries-hierarchies" */ "@/layouts/LayoutBoundaryHierarchy.vue"
          ),
        /**
         * LAYOUT CATALOG
         */
        children: [
          {
            path: "",
            name: "BoundariesHierarchiesView",
            component: () =>
              import(
                /* webpackChunkName: "boundaries-hierarchies" */ "@/pages/boundaries-hierarchies/BoundariesHierarchiesView.vue"
              ),
          },
        ],
      },
      {
        path: "query_data",
        name: "QueryData",
        component: () =>
          import(
            /* webpackChunkName: "query-data" */ "@/pages/query-data/QueryData.vue"
          ),
      },
      {
        path: "query_data",
        name: "QueryData",
        component: () =>
          import(
            /* webpackChunkName: "query-data" */ "@/pages/query-data/QueryData.vue"
          ),
      },
      {
        path: "questions",
        name: "QuestionsIndex",
        component: () =>
          import(
            /* webpackChunkName: "questions" */ "@/pages/questions/QuestionsIndex.vue"
          ),
      },
      {
        path: "questions/:questionId",
        component: () =>
          import(
            /* webpackChunkName: "questions" */ "@/layouts/LayoutQuestion.vue"
          ),
        children: [
          {
            path: "",
            component: () =>
              import(
                /* webpackChunkName: "questions" */ "@/layouts/LayoutQuestionView.vue"
              ),
            children: [
              {
                path: "",
                name: "QuestionsView",
                component: () =>
                  import(
                    /* webpackChunkName: "questions" */ "@/pages/questions/QuestionsView.vue"
                  ),
              },
              {
                path: "",
                name: "QuestionsViewColumns",
                component: () =>
                  import(
                    /* webpackChunkName: "questions" */ "@/pages/questions/QuestionsViewColumns.vue"
                  ),
              },
              {
                path: "",
                name: "QuestionsViewData",
                component: () =>
                  import(
                    /* webpackChunkName: "questions" */ "@/pages/questions/QuestionsViewData.vue"
                  ),
              },
            ],
          },
        ],
      },
      {
        path: "datasets",
        name: "DatasetsIndex",
        component: () =>
          import(
            /* webpackChunkName: "datasets" */ "@/pages/datasets/DatasetsIndex.vue"
          ),
      },
      {
        path: "datasets/:datasetId",
        component: () =>
          import(
            /* webpackChunkName: "datasets" */ "@/layouts/LayoutDataset.vue"
          ),
        /**
         * LAYOUT CATALOG
         */
        children: [
          {
            path: "",
            component: () =>
              import(
                /* webpackChunkName: "datasets" */ "@/layouts/LayoutDatasetView.vue"
              ),
            /**
             * LAYOUT CATALOG VIEW
             */
            children: [
              {
                path: "",
                name: "DatasetsView",
                component: () =>
                  import(
                    /* webpackChunkName: "datasets" */ "@/pages/datasets/DatasetsView.vue"
                  ),
              },
              {
                path: "attributes_series",
                name: "DatasetsViewAttributesSeries",
                component: () =>
                  import(
                    /* webpackChunkName: "datasets" */ "@/pages/datasets/DatasetsViewAttributesSeries.vue"
                  ),
              },
              {
                path: "versions",
                name: "DatasetsViewVersions",
                component: () =>
                  import(
                    /* webpackChunkName: "datasets" */ "@/pages/datasets/DatasetsViewVersions.vue"
                  ),
              },
            ],
          },
          {
            path: "versions/:versionId",
            component: () =>
              import(
                /* webpackChunkName: "versions" */ "@/layouts/LayoutVersionView.vue"
              ),
            /**
             * LAYOUT VERSION VIEW
             */
            children: [
              {
                path: "",
                name: "VersionsView",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsView.vue"
                  ),
              },
              {
                path: "sections",
                name: "VersionsViewSections",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewSections.vue"
                  ),
              },
              {
                path: "attributes",
                name: "VersionsViewAttributes",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewAttributes.vue"
                  ),
              },
              {
                path: "data_ingestions",
                name: "VersionsViewDataIngestions",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewDataIngestions.vue"
                  ),
              },
              {
                path: "data",
                name: "VersionsViewData",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/versions/VersionsViewData.vue"
                  ),
              },
            ],
          },
          {
            path: "versions/:versionId/import_attributes",
            name: "VersionsImportAttributes",
            component: () =>
              import(
                /* webpackChunkName: "versions" */ "@/pages/versions/VersionsImportAttributes.vue"
              ),
          },
          {
            path: "versions/:versionId/data_ingestions/:dataIngestionId",
            component: () =>
              import(
                /* webpackChunkName: "versions" */ "@/layouts/LayoutDataIngestionView.vue"
              ),
            /**
             * LAYOUT DATA INGESTION VIEW
             */
            children: [
              {
                path: "",
                name: "DataIngestionsView",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsView.vue"
                  ),
              },
              {
                path: "file_upload",
                name: "DataIngestionsViewFileUpload",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewFileUpload.vue"
                  ),
              },
              {
                path: "file_parsing",
                name: "DataIngestionsViewFileParsing",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewFileParsing.vue"
                  ),
              },
              {
                path: "attributes_mapping",
                name: "DataIngestionsViewAttributesMapping",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewAttributesMapping.vue"
                  ),
              },
              {
                path: "data_loading",
                name: "DataIngestionsViewDataLoading",
                component: () =>
                  import(
                    /* webpackChunkName: "versions" */ "@/pages/data-ingestions/DataIngestionsViewDataLoading.vue"
                  ),
              },
            ],
          },
        ],
      },
    ],
  },
  /**
   * MAP PREVIEWS
   */
  {
    path: "/map",
    component: () => import("@/layouts/LayoutMap.vue"),
    children: [
      {
        path: "sections/:sectionId/preview_geojson/:dataId",
        name: "SectionsPreviewGeojson",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/sections/SectionsPreviewGeojson.vue"
          ),
      },
      {
        path: "query_data/preview_geojson",
        name: "QueryDataPreviewGeoJson",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/query-data/QueryDataPreviewGeoJson.vue"
          ),
      },
      {
        path: "sections/:sectionId/preview_geoserver_layer",
        name: "SectionsPreviewGeoserverLayer",
        component: () =>
          import(
            /* webpackChunkName: "map" */ "@/pages/sections/SectionsPreviewGeoserverLayer.vue"
          ),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach(async (to, from, next) => {
  try {
    await useAuthStore().getProfile();
  } catch (error) {
    if (error.response?.status !== 401) {
      emitAlertError(error);
    } else {
      if (!to.meta.public) {
        emitAlertError(error);
        next({ name: "AuthSignIn" });
        return;
      }
    }
  }
  next();
});

export default router;
