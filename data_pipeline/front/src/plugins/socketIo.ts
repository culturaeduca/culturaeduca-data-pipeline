import { io } from "socket.io-client";
import { reactive } from "vue";

export const socketIoState = reactive({
  connected: false,
  dataIngestionEvents: [],
});

const URL = import.meta.env.VITE_API_URL;

export const socket = io(URL);

socket.on("connect", () => {
  socketIoState.connected = true;
});

socket.on("disconnect", () => {
  socketIoState.connected = false;
});

socket.on("data-ingestion", (...args) => {
  socketIoState.dataIngestionEvents.push(args);
});
