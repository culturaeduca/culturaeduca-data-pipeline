// Utilities
import requester from "@/requester";
import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    user: null,
  }),
  actions: {
    async getProfile() {
      this.user = await requester.auth.profile();
    },
    async signIn(username, password) {
      const responseData = await requester.auth.signIn({ username, password });
      localStorage.setItem("accessToken", responseData.accessToken);
    },
    async signOut() {
      this.user = null;
      localStorage.setItem("accessToken", null);
    },
  },
});
