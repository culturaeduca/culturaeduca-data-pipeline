#!/bin/sh

skip_build=false
skip_rsync=false

# Loop through the arguments
for arg in "$@"
do
  # Check if the argument is "--skip-build"
  if [ "$arg" = "--skip-build" ]; then
    skip_build=true
    break
  fi
  # Check if the argument is "--skip-rsync"
  if [ "$arg" = "--skip-rsync" ]; then
    skip_rsync=true
    break
  fi
done

# Check the value of skip_build
if [ "$skip_build" = false ]; then
  # lembrar de ter um arquivo .env.production
  yarn build --mode production
  if [ $? -gt 0 ]
    then
      exit 1
  fi
fi

# Check the value of skip_rsync
if [ "$skip_rsync" = false ]; then
  # atualiza dist na produção
  rsync -Iav dist/ <name_in_ssh_config>:<front_path> --delete
  if [ $? -gt 0 ]
    then
      echo 'Rsync falhou, abortando build front'
      exit 1
  fi
fi

exit 0