import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { AttributeSeries, Dataset, Version } from '@prisma/client';
import { CreateVersionDto } from '@/versions/dto';
import { PrismaService } from '@/prisma.service';
import { CreateDatasetDto, UpdateDatasetDto } from './dto';
import { validateStartEndDates } from '@/utils/versions-helper.utils';
import { DB_SCHEMA } from '@/knex/knex.constants';
import { generateSectionTableName } from '@/sections/sections.helper';
import { KnexService } from '@/knex/knex.service';
import { DefaultSectionTableColumnEnum } from '@/sections/sections.contants';

@Injectable()
export class DatasetsService {
  constructor(
    private readonly knexService: KnexService,
    private readonly prisma: PrismaService,
  ) {}

  async create(dto: CreateDatasetDto, userId: number): Promise<Dataset> {
    return this.prisma.dataset.create({
      data: {
        uri: dto?.uri?.trim()?.toLowerCase() || null,
        title: dto?.title?.trim() || null,
        description: dto?.description?.trim() || null,
        format: dto?.format || null,
        owner: dto?.owner?.trim() || null,
        userCreatedBy: { connect: { id: userId } },
        userUpdatedBy: { connect: { id: userId } },
      },
    });
  }

  async createVersion(
    id: number,
    dto: CreateVersionDto,
    userId: number,
  ): Promise<Version> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find dataset
      const dataset = await trxPrisma.dataset.findUnique({
        where: { id },
        include: { versions: true },
      });
      if (!dataset) {
        throw new NotFoundException('Dataset not found!');
      }
      // validate start/end dates
      const errorMessageValidateStartEndDates = validateStartEndDates(dto);
      if (errorMessageValidateStartEndDates) {
        throw new BadRequestException(errorMessageValidateStartEndDates);
      }
      // create version
      const version = await trxPrisma.version.create({
        data: {
          datasetId: dataset.id,
          uri: dto?.uri?.trim()?.toLowerCase() || null,
          title: dto?.title?.trim() || null,
          description: dto?.description?.trim() || null,
          startDateYear: dto?.startDateYear,
          startDateMonth: dto?.startDateMonth,
          startDateDay: dto?.startDateDay,
          endDateYear: dto?.endDateYear,
          endDateMonth: dto?.endDateMonth,
          endDateDay: dto?.endDateDay,
          default: dataset.versions.length === 0,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      return version;
    });
  }

  async findAll(): Promise<Dataset[]> {
    return this.prisma.dataset.findMany({
      orderBy: { uri: 'asc' },
    });
  }

  async findOne(id: number): Promise<Dataset> {
    const dataset = await this.prisma.dataset.findUnique({
      where: { id },
    });
    if (!dataset) {
      throw new NotFoundException('Dataset not found!');
    }
    return dataset;
  }

  async findVersions(id: number): Promise<Version[]> {
    const dataset = await this.prisma.dataset.findUnique({
      where: { id },
      include: {
        versions: {
          orderBy: [
            { startDateYear: 'asc' },
            { startDateMonth: 'asc' },
            { startDateDay: 'asc' },
          ],
        },
      },
    });
    if (!dataset) {
      throw new NotFoundException('Dataset not found!');
    }
    return dataset.versions;
  }

  async findAttributesSeries(id: number): Promise<AttributeSeries[]> {
    const dataset = await this.prisma.dataset.findUnique({
      where: { id },
      include: {
        attributesSeries: {
          include: {
            attributes: {
              include: { section: true },
            },
            relationshipsForeignKey: true,
            relationshipsReference: true,
          },
          orderBy: { index: 'asc' },
        },
      },
    });
    if (!dataset) {
      throw new NotFoundException('Dataset not found!');
    }
    return dataset.attributesSeries;
  }

  async update(
    id: number,
    dto: UpdateDatasetDto,
    userId: number,
  ): Promise<Dataset> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find dataset
      const dataset = await trxPrisma.dataset.findUnique({
        where: { id },
        include: {
          versions: {
            include: { sections: true },
          },
        },
      });
      if (!dataset) {
        throw new NotFoundException('Dataset not found!');
      }
      // update dataset
      const newUri = dto.uri?.trim()?.toLowerCase();
      await trxPrisma.dataset.update({
        where: { id },
        data: {
          ...(dto?.uri !== undefined ? { uri: newUri || null } : {}),
          ...(dto?.title !== undefined
            ? { title: dto.title?.trim() || null }
            : {}),
          ...(dto?.description !== undefined
            ? { description: dto.description?.trim() || null }
            : {}),
          ...(dto?.format !== undefined ? { format: dto.format || null } : {}),
          ...(dto?.owner !== undefined
            ? { owner: dto.owner?.trim() || null }
            : {}),
          updatedBy: userId,
        },
      });
      // update sections table names
      if (!!newUri && newUri !== dataset.uri) {
        const itemsToUpdate: {
          databaseSchema: DB_SCHEMA;
          oldTableName: string;
          newTableName: string;
        }[] = [];
        for await (const version of dataset.versions) {
          for await (const section of version.sections) {
            const newTableName = generateSectionTableName(
              newUri,
              version.uri,
              section.uri,
            );
            itemsToUpdate.push({
              databaseSchema: DB_SCHEMA.DATASETS,
              oldTableName: section.tableName,
              newTableName: newTableName,
            });
            await trxPrisma.section.update({
              data: { tableName: newTableName },
              where: { id: section.id },
            });
          }
        }
        await this.knexService.renameTables(itemsToUpdate);
      }
      return trxPrisma.dataset.findUnique({ where: { id: dataset.id } });
    });
  }

  async remove(id: number): Promise<Dataset> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find dataset
      const dataset = await trxPrisma.dataset.findUnique({
        where: { id },
      });
      if (!dataset) {
        throw new NotFoundException('Dataset not found!');
      }
      // delete dataset
      return trxPrisma.dataset.delete({
        where: { id },
      });
    });
  }
}
