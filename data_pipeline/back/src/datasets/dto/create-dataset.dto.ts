import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { DatasetFormat } from '@prisma/client';

export class CreateDatasetDto {
  @IsString()
  @IsNotEmpty()
  uri: string;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsEnum(DatasetFormat)
  format: DatasetFormat;

  @IsOptional()
  @IsString()
  owner: string | null;
}
