import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { DatasetsService } from './datasets.service';
import { CreateDatasetDto, UpdateDatasetDto } from './dto';
import { CreateVersionDto } from '@/versions/dto';
import { User } from '@/user.decorator';

@Controller('datasets')
export class DatasetsController {
  constructor(private readonly datasetsService: DatasetsService) {}

  // Get

  @Get(':id/attributes_series')
  findAttributesSeries(@Param('id', ParseIntPipe) id: number) {
    return this.datasetsService.findAttributesSeries(id);
  }

  @Get(':id/versions')
  findVersions(@Param('id', ParseIntPipe) id: number) {
    return this.datasetsService.findVersions(id);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.datasetsService.findOne(id);
  }

  @Get()
  findAll() {
    return this.datasetsService.findAll();
  }

  // Post

  @Post(':id/versions')
  createVersion(
    @Param('id', ParseIntPipe) id: number,
    @Body() createVersionDto: CreateVersionDto,
    @User('id') userId: number,
  ) {
    return this.datasetsService.createVersion(id, createVersionDto, userId);
  }

  @Post()
  create(
    @Body() createDatasetDto: CreateDatasetDto,
    @User('id') userId: number,
  ) {
    return this.datasetsService.create(createDatasetDto, userId);
  }

  // Patch

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDatasetDto: UpdateDatasetDto,
    @User('id') userId: number,
  ) {
    return this.datasetsService.update(id, updateDatasetDto, userId);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.datasetsService.remove(id);
  }
}
