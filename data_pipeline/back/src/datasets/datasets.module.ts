import { Module } from '@nestjs/common';

import { DatasetsController } from './datasets.controller';
import { DatasetsService } from './datasets.service';
import { KnexModule } from '@/knex/knex.module';
import { PrismaService } from '@/prisma.service';

@Module({
  imports: [KnexModule],
  controllers: [DatasetsController],
  providers: [DatasetsService, PrismaService],
})
export class DatasetsModule {}
