import 'dotenv/config';
import type { Knex } from 'knex';
import type { MinioModuleConfig } from '@/minio/config.interface';
import { MongooseModuleFactoryOptions } from '@nestjs/mongoose';
import { JwtModuleOptions } from '@nestjs/jwt';

export default () => ({
  nodeEnv: process.env.NODE_ENV || 'development',
  appPort: Number(process.env.APP_PORT) || 3000,
  enableCorsOrigin: process.env.ENABLE_CORS_ORIGIN.split(',')
    .map((url) => url.trim())
    .filter((url) => !!url) || ['http://localhost:8080'],
  multer: {
    dest: process.env.MULTER_DEST || './static/tmp/',
  },
  minio: {
    bucketName: process.env.MINIO_BUCKET_NAME,
    client: {
      endPoint: process.env.MINIO_END_POINT || 'localhost',
      port: Number(process.env.MINIO_PORT) || 9000,
      useSSL: process.env.MINIO_USE_SSL === 'true' || false,
      accessKey: process.env.MINIO_ACCESS_KEY,
      secretKey: process.env.MINIO_SECRET_KEY,
    } as MinioModuleConfig,
  },
  mongoose: {
    uri: process.env.MONGODB_URL,
  } as MongooseModuleFactoryOptions,
  knex: {
    client: 'pg',
    connection: {
      host: process.env.DB_CONNECTION_HOST,
      port: process.env.DB_CONNECTION_PORT,
      user: process.env.DB_CONNECTION_USER,
      password: process.env.DB_CONNECTION_PASSWORD,
      database: process.env.DB_CONNECTION_DATABASE,
      ssl: process.env.DB_CONNECTION_SSL === 'true',
    } as Knex.Config,
  },
  jwt: {
    secret: process.env.JWT_SECRET || 'jwt-secret',
    signOptions: {
      expiresIn: process.env.JWT_EXPIRES_IN || '60s',
    },
  } as JwtModuleOptions,
});
