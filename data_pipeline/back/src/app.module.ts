import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { AttributesModule } from './attributes/attributes.module';
import { AttributesSeriesModule } from './attributes-series/attributes-series.module';
import { AttributesSeriesRelationshipsModule } from './attributes-series-relationships/attributes-series-relationships.module';
import { AuthModule } from './auth/auth.module';
import { DataIngestionsModule } from './data-ingestions/data-ingestions.module';
import { DatasetsModule } from './datasets/datasets.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { KnexModule } from './knex/knex.module';
import { MongoCollectionsModule } from './mongo-collections/mongo-collections.module';
import { QuestionsModule } from './questions/questions.module';
import { SectionsModule } from './sections/sections.module';
import { UsersModule } from './users/users.module';
import { VersionsModule } from './versions/versions.module';
import { BoundariesHierarchiesModule } from './boundaries-hierarchies/boundaries-hierarchies.module';
import { BoundariesModule } from './boundaries/boundaries.module';
import configuration from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      load: [configuration],
    }),
    KnexModule,
    MongoCollectionsModule,
    AuthModule,
    // models
    AttributesModule,
    AttributesSeriesModule,
    AttributesSeriesRelationshipsModule,
    DataIngestionsModule,
    DatasetsModule,
    QuestionsModule,
    SectionsModule,
    UsersModule,
    VersionsModule,
    BoundariesHierarchiesModule,
    BoundariesModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
  exports: [],
})
export class AppModule {}
