import { IsNotEmpty, IsString } from 'class-validator';

export class CreateBoundaryHierarchyDto {
  @IsString()
  @IsNotEmpty()
  uri: string;

  @IsString()
  @IsNotEmpty()
  name: string;
}
