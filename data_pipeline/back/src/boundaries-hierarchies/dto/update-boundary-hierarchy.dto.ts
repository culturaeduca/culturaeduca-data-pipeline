import { PartialType } from '@nestjs/mapped-types';
import { CreateBoundaryHierarchyDto } from './create-boundary-hierarchy.dto';

export class UpdateBoundaryHierarchyDto extends PartialType(
  CreateBoundaryHierarchyDto,
) {}
