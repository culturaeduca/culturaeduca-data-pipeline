import { Module } from '@nestjs/common';
import { BoundariesHierarchiesService } from './boundaries-hierarchies.service';
import { BoundariesHierarchiesController } from './boundaries-hierarchies.controller';
import { PrismaService } from '@/prisma.service';
import { KnexModule } from '@/knex/knex.module';

@Module({
  imports: [KnexModule],
  controllers: [BoundariesHierarchiesController],
  providers: [BoundariesHierarchiesService, PrismaService],
})
export class BoundariesHierarchiesModule {}
