import { Injectable } from '@nestjs/common';
import {
  BadRequestException,
  NotFoundException,
} from '@nestjs/common/exceptions';
import {
  CreateBoundaryHierarchyDto,
  GetBreadcrumbsQueryDto,
  UpdateBoundaryHierarchyDto,
} from './dto';
import { PrismaService } from '@/prisma.service';
import { CreateBoundaryDto } from '@/boundaries/dto';
import { Boundary } from '@prisma/client';
import { DB_SCHEMA } from '@/knex/knex.constants';
import { KnexService } from '@/knex/knex.service';
import { DefaultSectionTableColumnEnum } from '@/sections/sections.contants';
import { AttributeTypeEnum } from '@/attributes/attributes.constants';

@Injectable()
export class BoundariesHierarchiesService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly knexService: KnexService,
  ) {}

  create(dto: CreateBoundaryHierarchyDto, userId: number) {
    return this.prisma.boundaryHierarchy.create({
      data: {
        uri: dto.uri,
        name: dto.name,
        createdBy: userId,
        updatedBy: userId,
      },
    });
  }

  async createBoundary(uri: string, dto: CreateBoundaryDto, userId: number) {
    const boundaryHierarchy = await this.prisma.boundaryHierarchy.findUnique({
      where: {
        uri,
      },
    });
    if (!boundaryHierarchy) {
      throw new NotFoundException('Boundary hierarchy not found');
    }

    const sectionId = dto.sectionId || null;
    const questionId = dto.questionId || null;
    if (!sectionId && !questionId) {
      throw new BadRequestException('Section or question id is required');
    }
    if (sectionId && questionId) {
      throw new BadRequestException(
        "Can't send both section and question id at the same time",
      );
    }

    if (sectionId) {
      const section = await this.prisma.section.findUnique({
        where: {
          id: sectionId,
        },
      });
      if (!section) {
        throw new NotFoundException('Section not found');
      }
    }
    if (questionId) {
      const question = await this.prisma.section.findUnique({
        where: {
          id: questionId,
        },
      });
      if (!question) {
        throw new NotFoundException('Question not found');
      }
    }

    return this.prisma.boundary.create({
      data: {
        boundaryHierarchyUri: uri,
        name: dto.name,
        singularName: dto.singularName,
        level: dto.level,
        required: dto.required,
        sectionId: sectionId,
        questionId: questionId,
        boundaryNameAttribute: dto.boundaryNameAttribute,
        divisaoAdministrativaMunId: dto.divisaoAdministrativaMunId,
        createdBy: userId,
        updatedBy: userId,
      },
      include: {
        section: true,
        question: true,
      },
    });
  }

  findAll() {
    return this.prisma.boundaryHierarchy.findMany({
      include: {
        boundaries: {
          orderBy: { level: 'asc' },
        },
      },
      orderBy: {
        uri: 'asc',
      },
    });
  }

  async findBoundaries(uri: string): Promise<Boundary[]> {
    const boundaryHierarchy = await this.prisma.boundaryHierarchy.findUnique({
      where: { uri },
      include: {
        boundaries: {
          orderBy: [{ level: 'asc' }],
        },
      },
    });
    if (!boundaryHierarchy) {
      throw new NotFoundException('Boundary Hierarchy not found!');
    }
    return boundaryHierarchy.boundaries;
  }

  async findOne(uri: string) {
    const boundaryHierarchy = await this.prisma.boundaryHierarchy.findUnique({
      where: {
        uri,
      },
      include: {
        boundaries: {
          include: {
            section: true,
            question: true,
          },
          orderBy: [{ level: 'asc' }],
        },
      },
    });
    if (!boundaryHierarchy) {
      throw new NotFoundException('Boundary hierarchy not found');
    }
    return boundaryHierarchy;
  }

  async getBreadcrumbs(uri: string, dto: GetBreadcrumbsQueryDto) {
    const boundaryHierarchy = await this.prisma.boundaryHierarchy.findUnique({
      where: {
        uri,
      },
      include: {
        boundaries: {
          include: {
            section: {
              include: {
                attributes: true,
              },
            },
            question: {
              include: {
                questionColumns: true,
              },
            },
          },
          orderBy: [{ level: 'asc' }],
        },
      },
    });
    if (!boundaryHierarchy) {
      throw new NotFoundException('Boundary hierarchy not found');
    }
    const { lat, lng } = dto;
    const items = boundaryHierarchy.boundaries.map((boundary) => ({
      id: boundary.id,
      name: boundary.name,
      singularName: boundary.singularName,
      level: boundary.level,
      boundaryNameAttribute: boundary.boundaryNameAttribute,
      divisaoAdministrativaMunId: boundary.divisaoAdministrativaMunId,
      required: boundary.required,
      ...(boundary.section
        ? {
            databaseSchema: DB_SCHEMA.DATASETS,
            tableName: boundary.section.tableName,
            columns: boundary.section.attributes.map((attribute) => ({
              name: attribute.name,
              type: attribute.type,
            })),
            columnGeom: DefaultSectionTableColumnEnum.GEOMETRY,
            columnGeog: DefaultSectionTableColumnEnum.GEOGRAPHY,
          }
        : {
            databaseSchema: DB_SCHEMA.QUESTIONS,
            tableName: boundary.question.viewName,
            columns: boundary.question.questionColumns.map(
              (questionColumn) => ({
                name: questionColumn.name,
                type: questionColumn.type,
              }),
            ),
            columnGeom: boundary.question.questionColumns.find(
              (questionColumn) =>
                questionColumn.type === AttributeTypeEnum.GEOMETRY,
            )?.name, // pega primeira coluna geometrica, TODO: REVER ISSO QUANDO AVANÇARMOS EM QUESTIONS
            columnGeog: boundary.question.questionColumns.find(
              (questionColumn) =>
                questionColumn.type === AttributeTypeEnum.GEOGRAPHY,
            )?.name, // pega primeira coluna geografica, TODO: REVER ISSO QUANDO AVANÇARMOS EM QUESTIONS
          }),
    }));
    return this.knexService.getBreadcrumbs({ lat, lng, items });
  }

  async update(uri: string, dto: UpdateBoundaryHierarchyDto, userId: number) {
    const boundaryHierarchy = await this.prisma.boundaryHierarchy.findUnique({
      where: {
        uri,
      },
    });
    if (!boundaryHierarchy) {
      throw new NotFoundException('Boundary hierarchy not found');
    }
    const dataBoundaryHiearchy = {
      ...(dto?.uri !== undefined
        ? { uri: dto.uri?.trim().toLowerCase() || null }
        : {}),
      ...(dto?.name !== undefined ? { name: dto.name?.trim() || null } : {}),
      updatedBy: userId,
    };
    return this.prisma.boundaryHierarchy.update({
      where: {
        uri,
      },
      data: dataBoundaryHiearchy,
    });
  }

  async remove(uri: string) {
    const boundaryHierarchy = await this.prisma.boundaryHierarchy.findUnique({
      where: {
        uri,
      },
    });
    if (!boundaryHierarchy) {
      throw new NotFoundException('Boundary hierarchy not found');
    }
    return this.prisma.boundaryHierarchy.delete({
      where: {
        uri,
      },
    });
  }
}
