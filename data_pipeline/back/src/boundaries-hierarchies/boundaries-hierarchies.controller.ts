import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { BoundariesHierarchiesService } from './boundaries-hierarchies.service';
import {
  CreateBoundaryHierarchyDto,
  UpdateBoundaryHierarchyDto,
  GetBreadcrumbsQueryDto,
} from './dto';
import { User } from '@/user.decorator';
import { Public } from '@/public.decorator';
import { CreateBoundaryDto } from '@/boundaries/dto';

@Controller('boundaries_hierarchies')
export class BoundariesHierarchiesController {
  constructor(
    private readonly boundariesHierarchiesService: BoundariesHierarchiesService,
  ) {}

  // Get

  @Public()
  @Get(':uri/breadcrumbs')
  getBreadcrumbs(
    @Param('uri') uri: string,
    @Query() queryDto: GetBreadcrumbsQueryDto,
  ) {
    return this.boundariesHierarchiesService.getBreadcrumbs(uri, queryDto);
  }

  @Public()
  @Get(':uri/boundaries')
  findVersions(@Param('uri') uri: string) {
    return this.boundariesHierarchiesService.findBoundaries(uri);
  }

  @Public()
  @Get(':uri')
  findOne(@Param('uri') uri: string) {
    return this.boundariesHierarchiesService.findOne(uri);
  }

  @Public()
  @Get()
  findAll() {
    return this.boundariesHierarchiesService.findAll();
  }

  // Post

  @Post()
  create(@Body() dto: CreateBoundaryHierarchyDto, @User('id') userId: number) {
    return this.boundariesHierarchiesService.create(dto, userId);
  }

  @Post(':uri/boundaries')
  createBoundary(
    @Param('uri') uri: string,
    @Body() dto: CreateBoundaryDto,
    @User('id') userId: number,
  ) {
    return this.boundariesHierarchiesService.createBoundary(uri, dto, userId);
  }

  //Patch

  @Patch(':uri')
  update(
    @Param('uri') uri: string,
    @Body() dto: UpdateBoundaryHierarchyDto,
    @User('id') userId: number,
  ) {
    return this.boundariesHierarchiesService.update(uri, dto, userId);
  }

  // Delete

  @Delete(':uri')
  remove(@Param('uri') uri: string) {
    return this.boundariesHierarchiesService.remove(uri);
  }
}
