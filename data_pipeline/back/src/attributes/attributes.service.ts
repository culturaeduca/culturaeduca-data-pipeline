import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { Attribute, AttributeIndexType } from '@prisma/client';

import { CreateAttributeDto, UpdateAttributeDto } from './dto';
import { KnexService } from '@/knex/knex.service';
import { PrismaService, PrismaTransaction } from '@/prisma.service';
import { AttributeTypeEnum } from './attributes.constants';
import { DefaultSectionTableColumnEnum } from '@/sections/sections.contants';
import { DB_SCHEMA } from '@/knex/knex.constants';

@Injectable()
export class AttributesService {
  constructor(
    private readonly knexService: KnexService,
    private readonly prisma: PrismaService,
  ) {}

  async create(
    datasetId: number,
    sectionId: number,
    dto: CreateAttributeDto,
    userId: number,
    newAttributeSeriesIndex?: number,
    prismaTransaction?: PrismaTransaction,
  ): Promise<Attribute> {
    return this.prisma.$transaction(async (_trxPrisma) => {
      // use incoming transaction or this one
      const trxPrisma = prismaTransaction || _trxPrisma;
      // get data from dto
      const attributeType = dto?.type?.trim()?.toUpperCase() || null;
      const optional = dto?.optional !== undefined ? dto.optional : null;
      const indexable = dto?.indexable !== undefined ? dto.indexable : null;
      const unique = dto?.unique !== undefined ? dto.unique : null;
      const primary = dto?.primary !== undefined ? dto.primary : null;
      const sensitiveData =
        dto?.sensitiveData !== undefined ? dto.sensitiveData : null;
      // validate data if attribute is geometry
      if (attributeType === AttributeTypeEnum.GEOMETRY) {
        if (primary) {
          throw new BadRequestException(
            'Geometry attribute cannot be primary!',
          );
        }
        if (unique) {
          throw new BadRequestException('Geometry attribute cannot be unique!');
        }
      }
      // validate data if attribute is primary
      if (primary) {
        if (optional) {
          throw new BadRequestException(
            'Primary attribute cannot be optional!',
          );
        }
        if (!indexable) {
          throw new BadRequestException(
            'Primary attribute has to be indexable!',
          );
        }
        if (!unique) {
          throw new BadRequestException('Primary attribute has to be unique!');
        }
        if (sensitiveData) {
          throw new BadRequestException(
            'Primary attribute cannot be sensitive!',
          );
        }
        const existingPrimaryAttribute = await trxPrisma.attribute.findFirst({
          where: { sectionId: sectionId, primary: true },
        });
        if (existingPrimaryAttribute) {
          throw new BadRequestException(
            `This section already have a primary attribute (${existingPrimaryAttribute.name})!`,
          );
        }
      }
      let indexType: AttributeIndexType;
      if (indexable) {
        if (
          [
            AttributeTypeEnum.GEOMETRY as string,
            AttributeTypeEnum.GEOGRAPHY as string,
          ].includes(attributeType)
        ) {
          indexType = AttributeIndexType.GIST;
        } else {
          indexType = dto?.indexType || AttributeIndexType.BTREE;
        }
      } else {
        indexType = null;
      }
      // create attribute
      const attribute = await trxPrisma.attribute.create({
        data: {
          name: dto?.name?.trim()?.toLowerCase().slice(0, 59) || null,
          rawFileProperty: dto?.rawFileProperty?.trim() || null,
          description: dto?.description?.trim() || null,
          type: attributeType,
          typeOptions: dto?.typeOptions || null,
          optional,
          indexable,
          indexType,
          unique,
          primary: dto?.primary || false,
          sensitiveData: dto?.sensitiveData || false,
          defaultValue: dto?.defaultValue || null,
          unit: dto?.unit?.trim() || null,
          decodeValue: dto?.decodeValue || null,
          section: {
            connect: {
              id: sectionId,
            },
          },
          series: {
            create: {
              datasetId,
              index: newAttributeSeriesIndex,
            },
          },
          userCreatedBy: { connect: { id: userId } },
          userUpdatedBy: { connect: { id: userId } },
        },
      });
      return attribute;
    });
  }

  async findOne(id: number): Promise<Attribute> {
    // find attribute
    const attribute = await this.prisma.attribute.findUnique({
      where: { id },
      include: { section: true },
    });
    if (!attribute) {
      throw new NotFoundException('Attribute not found!');
    }
    return attribute;
  }

  async getBasesGeojson(id: number, basesRowId: number) {
    // find attribute
    const attribute = await this.prisma.attribute.findUnique({
      where: { id },
      include: { section: true },
    });
    if (!attribute) {
      throw new NotFoundException('Attribute not found!');
    }
    // check if attribute is geospatial
    if (attribute.type !== AttributeTypeEnum.GEOMETRY) {
      throw new NotFoundException('Attribute is not geospatial!');
    }
    // get bases row geojson
    return this.knexService.findGeojsonValue(
      DB_SCHEMA.DATASETS,
      attribute.section.tableName,
      attribute.name,
      DefaultSectionTableColumnEnum.ID,
      basesRowId,
    );
  }

  async remove(id: number): Promise<Attribute> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find attribute
      const attribute = await trxPrisma.attribute.findUnique({
        where: { id },
        include: {
          section: true,
          series: true,
        },
      });
      if (!attribute) {
        throw new NotFoundException('Attribute not found!');
      }
      // delete attribute
      await trxPrisma.attribute.delete({ where: { id: attribute.id } });
      // check if attribute series is empty
      const countAttributesSameSeries = await trxPrisma.attribute.count({
        where: { seriesId: attribute.seriesId },
      });
      if (countAttributesSameSeries === 0) {
        // delete series
        await trxPrisma.attributeSeries.delete({
          where: { id: attribute.seriesId },
        });
        // update index of series above
        await trxPrisma.attributeSeries.updateMany({
          where: {
            datasetId: attribute.series.datasetId,
            index: { gt: attribute.series.index },
          },
          data: {
            index: {
              decrement: 1,
            },
          },
        });
      }
      // drop column
      await this.knexService.dropColumnsDatasetsTable(
        attribute.section.tableName,
        [attribute.name],
      );
      return attribute;
    });
  }

  async update(
    id: number,
    dto: UpdateAttributeDto,
    userId: number,
  ): Promise<Attribute> {
    return this.prisma.$transaction(
      async (trxPrisma) => {
        // find attribute
        const oldAttribute = await trxPrisma.attribute.findUnique({
          where: { id },
          include: { section: true },
        });
        if (!oldAttribute) {
          throw new NotFoundException('Attribute not found!');
        }
        // get data from dto
        const attributeType = dto?.type?.trim()?.toUpperCase() || null;
        const optional = dto?.optional !== undefined ? dto.optional : null;
        const indexable = dto?.indexable !== undefined ? dto.indexable : null;
        const unique = dto?.unique !== undefined ? dto.unique : null;
        const primary = dto?.primary !== undefined ? dto.primary : null;
        const sensitiveData =
          dto?.sensitiveData !== undefined ? dto.sensitiveData : null;
        // validate data if attribute is geometry
        if (attributeType === AttributeTypeEnum.GEOMETRY) {
          if (primary || (primary === null && oldAttribute.primary)) {
            throw new BadRequestException(
              'Geometry attribute cannot be primary!',
            );
          }
          if (unique || (unique === null && oldAttribute.unique)) {
            throw new BadRequestException(
              'Geometry attribute cannot be unique!',
            );
          }
        }
        // validate data if attribute is primary
        if (primary || (primary === null && oldAttribute.primary)) {
          if (optional || (optional === null && oldAttribute.optional)) {
            throw new BadRequestException(
              'Primary attribute cannot be optional!',
            );
          }
          if (
            indexable === false ||
            (indexable === null && !oldAttribute.indexable)
          ) {
            throw new BadRequestException(
              'Primary attribute has to be indexable!',
            );
          }
          if (unique === false || (unique === null && !oldAttribute.unique)) {
            throw new BadRequestException(
              'Primary attribute has to be unique!',
            );
          }
          if (
            sensitiveData ||
            (sensitiveData === null && oldAttribute.sensitiveData)
          ) {
            throw new BadRequestException(
              'Primary attribute cannot be sensitive!',
            );
          }
          const existingPrimaryAttribute = await trxPrisma.attribute.findFirst({
            where: {
              id: { not: oldAttribute.id },
              sectionId: oldAttribute.sectionId,
              primary: true,
            },
          });
          if (existingPrimaryAttribute) {
            throw new BadRequestException(
              `This section already have a primary attribute (${existingPrimaryAttribute.name})!`,
            );
          }
        }
        let indexType: AttributeIndexType;
        if (dto?.indexable !== undefined) {
          if (dto.indexable) {
            if (
              [
                AttributeTypeEnum.GEOMETRY as string,
                AttributeTypeEnum.GEOGRAPHY as string,
              ].includes(dto?.type)
            ) {
              indexType = AttributeIndexType.GIST;
            } else {
              indexType = dto?.indexType || AttributeIndexType.BTREE;
            }
          } else {
            indexType = null;
          }
        }
        // data for update
        const dataNewAttribute = {
          // name
          ...(dto?.name !== undefined
            ? { name: dto.name?.trim()?.toLowerCase().slice(0, 59) || null }
            : {}),
          // rawFileProperty
          ...(dto?.rawFileProperty !== undefined
            ? { rawFileProperty: dto.rawFileProperty?.trim() || null }
            : {}),
          // description
          ...(dto?.description !== undefined
            ? { description: dto.description?.trim() || null }
            : {}),
          // type
          ...(dto?.type !== undefined ? { type: attributeType || null } : {}),
          // typeOptions
          ...(dto?.typeOptions !== undefined
            ? { typeOptions: dto.typeOptions || null }
            : {}),
          // optional
          ...(dto?.optional !== undefined
            ? { optional: oldAttribute.primary ? false : dto.optional }
            : {}),
          // indexable
          ...(dto?.indexable !== undefined
            ? { indexable: oldAttribute.primary ? true : dto.indexable }
            : {}),
          // indexType
          ...(indexType !== undefined ? { indexType: indexType || null } : {}),
          // unique
          ...(dto?.unique !== undefined
            ? { unique: oldAttribute.primary ? true : dto.unique }
            : {}),
          // sensitive
          ...(dto?.sensitiveData !== undefined
            ? {
                sensitiveData: oldAttribute.primary ? false : dto.sensitiveData,
              }
            : {}),
          // TODO check default value
          // unit
          ...(dto?.unit !== undefined
            ? { unit: dto.unit?.trim() || null }
            : {}),
          // decode value
          ...(dto?.decodeValue !== undefined
            ? { decodeValue: dto.decodeValue || null }
            : {}),
          updatedBy: userId,
        };
        // update attribute
        const newAttribute = await trxPrisma.attribute.update({
          where: { id },
          data: dataNewAttribute,
        });
        // compare changes to update column in the section table
        await this.knexService.changeColumnDatasetsTable(
          oldAttribute.section.tableName,
          oldAttribute,
          newAttribute,
        );
        return newAttribute;
      },
      { timeout: 60000 },
    );
  }

  // TODO: check attribute series automatic connection

  // async _findNextVersions(
  //   version: Version,
  //   prismaTransaction?: PrismaTransaction,
  // ) {
  //   return (prismaTransaction || this.prisma).version.findMany({
  //     where: {
  //       id: { not: version.id },
  //       datasetId: version.datasetId,
  //       OR: [
  //         { startDateYear: { gt: version.startDateYear } },
  //         {
  //           startDateYear: { equals: version.startDateYear },
  //           ...(version.startDateMonth
  //             ? {
  //                 OR: [
  //                   { startDateMonth: { gt: version.startDateMonth } },
  //                   {
  //                     startDateMonth: { equals: version.startDateMonth },
  //                     ...(version.startDateDay
  //                       ? { startDateDay: { gt: version.startDateDay } }
  //                       : {}),
  //                   },
  //                 ],
  //               }
  //             : {}),
  //         },
  //       ],
  //     },
  //     orderBy: [
  //       { startDateYear: 'asc' },
  //       { startDateMonth: 'asc' },
  //       { startDateDay: 'asc' },
  //     ],
  //   });
  // }

  // async _findOrCreateAttributeSeries(
  //   version: Version,
  //   dto: CreateAttributeDto,
  //   newIndex?: number,
  //   prismaTransaction?: PrismaTransaction,
  // ): Promise<AttributeSeries> {
  //   const newAttributeName = dto?.name?.trim() || null;
  //   const newAttributeType = dto?.type?.trim() || null;
  //   // check previous versions
  //   const previousVersions = await this._findPreviousVersions(
  //     version,
  //     prismaTransaction,
  //   );
  //   for await (const previousVersion of previousVersions) {
  //     const previousAttribute = previousVersion.attributes.find(
  //       (attribute) =>
  //         attribute.name === newAttributeName &&
  //         attribute.type === newAttributeType,
  //     );
  //     if (previousAttribute) {
  //       return previousAttribute.series;
  //     }
  //   }
  //   // check next versions
  //   const nextVersions = await this._findNextVersions(
  //     version,
  //     prismaTransaction,
  //   );
  //   for await (const nextVersion of nextVersions) {
  //     const nextAttribute = nextVersion.attributes.find(
  //       (attribute) =>
  //         attribute.name === newAttributeName &&
  //         attribute.type === newAttributeType,
  //     );
  //     if (nextAttribute) {
  //       return nextAttribute.series;
  //     }
  //   }
  //   // create new attribute series
  //   let newIndexAux = newIndex;
  //   if (!newIndex) {
  //     const auxMaxIndex = await (
  //       prismaTransaction || this.prisma
  //     ).attributeSeries.groupBy({
  //       by: ['datasetId'],
  //       where: { datasetId: version.datasetId },
  //       _max: { index: true },
  //     });
  //     newIndexAux = (auxMaxIndex?.[0]?._max.index || 0) + 1;
  //   }
  //   return (prismaTransaction || this.prisma).attributeSeries.create({
  //     data: { datasetId: version.datasetId, index: newIndexAux },
  //   });
  // }

  // async _findPreviousVersions(
  //   version: Version,
  //   prismaTransaction?: PrismaTransaction,
  // ) {
  //   return (prismaTransaction || this.prisma).version.findMany({
  //     where: {
  //       id: { not: version.id },
  //       datasetId: version.datasetId,
  //       OR: [
  //         { startDateYear: { lt: version.startDateYear } },
  //         {
  //           startDateYear: { equals: version.startDateYear },
  //           ...(version.startDateMonth
  //             ? {
  //                 OR: [
  //                   { startDateMonth: { lt: version.startDateMonth } },
  //                   {
  //                     startDateMonth: { equals: version.startDateMonth },
  //                     ...(version.startDateDay
  //                       ? { startDateDay: { lt: version.startDateDay } }
  //                       : {}),
  //                   },
  //                 ],
  //               }
  //             : {}),
  //         },
  //       ],
  //     },
  //     orderBy: [
  //       { startDateYear: 'desc' },
  //       { startDateMonth: 'desc' },
  //       { startDateDay: 'desc' },
  //     ],
  //   });
  // }
}
