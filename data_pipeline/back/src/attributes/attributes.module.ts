import { Module } from '@nestjs/common';

import { AttributesController } from './attributes.controller';
import { AttributesService } from './attributes.service';
import { KnexModule } from '@/knex/knex.module';
import { PrismaService } from '@/prisma.service';

@Module({
  imports: [KnexModule],
  controllers: [AttributesController],
  providers: [AttributesService, PrismaService],
  exports: [AttributesService],
})
export class AttributesModule {}
