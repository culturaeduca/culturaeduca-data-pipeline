import {
  IsBoolean,
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';
import { AttributeIndexTypeEnum } from '../attributes.constants';

export class CreateAttributeDto {
  @IsString()
  @MaxLength(59)
  name: string;

  @IsString()
  @IsOptional()
  rawFileProperty: string | null;

  @IsString()
  @IsOptional()
  description: string | null;

  @IsString()
  type: string;

  @IsOptional()
  typeOptions: any;

  @IsBoolean()
  optional: boolean;

  @IsBoolean()
  indexable: boolean;

  @IsOptional()
  @IsEnum(AttributeIndexTypeEnum)
  indexType: AttributeIndexTypeEnum | null;

  @IsBoolean()
  unique: boolean;

  @IsBoolean()
  primary: boolean;

  @IsBoolean()
  sensitiveData: boolean;

  @IsString()
  @IsOptional()
  defaultValue: string | null;

  @IsString()
  @IsOptional()
  unit: string | null;

  @IsOptional()
  decodeValue: any;
}
