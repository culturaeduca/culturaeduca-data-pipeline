import { PrismaService } from '@/prisma.service';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from './dto';
import { User } from '@prisma/client';
import { hash } from 'argon2';
import { KnexService } from '@/knex/knex.service';

@Injectable()
export class UsersService {
  constructor(
    private readonly knexService: KnexService,
    private readonly prisma: PrismaService,
  ) {}

  async create(dto: CreateUserDto) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const hashedPassword = await hash(dto.password);
      const dataUser = {
        name: dto.name?.trim() || null,
        username: dto.username?.trim()?.toLowerCase(),
        email: dto.email?.trim()?.toLowerCase() || null,
        password: hashedPassword,
      } as User;
      // avoid username equal another user's email
      if (
        await trxPrisma.user.findUnique({ where: { username: dataUser.email } })
      ) {
        throw new BadRequestException('Invalid username');
      }
      // avoid email equal another user's username
      if (
        await trxPrisma.user.findUnique({ where: { email: dataUser.username } })
      ) {
        throw new BadRequestException('Invalid email');
      }
      // create user
      const user = await trxPrisma.user.create({
        data: dataUser,
      });
      // create user in psql database
      await this.knexService.createDatabaseUser(user.username, dto.password);
      return user;
    });
  }

  async findAll() {
    return this.prisma.user.findMany({
      select: {
        id: true,
        name: true,
        email: true,
        username: true,
        createdAt: true,
        updatedAt: true,
      },
    });
  }

  async findOne(id: number) {
    const user = await this.prisma.user.findUnique({
      where: { id },
    });
    if (!user) {
      throw new NotFoundException('User not found!');
    }
    delete user.password;
    return user;
  }

  async findForAuth(usernameOrEmail: string) {
    const cleanString = usernameOrEmail.trim().toLowerCase();
    return this.prisma.user.findFirst({
      where: {
        OR: [{ username: cleanString }, { email: cleanString }],
      },
    });
  }

  async update(id: number, dto: UpdateUserDto, permitPasswordChange = false) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find user
      const user = await trxPrisma.user.findUnique({
        where: { id },
      });
      if (!user) {
        throw new NotFoundException('User not found!');
      }
      // update user
      const updatedUser = await trxPrisma.user.update({
        where: { id },
        data: {
          ...(dto?.name !== undefined
            ? { name: dto.name?.trim() || null }
            : {}),
          ...(dto?.email !== undefined
            ? { email: dto.email?.trim()?.toLowerCase() || null }
            : {}),
          ...(dto?.username !== undefined
            ? { username: dto.username?.trim()?.toLowerCase() || null }
            : {}),
          ...(permitPasswordChange && dto?.password !== undefined
            ? { password: await hash(dto.password) }
            : {}),
        },
      });
      // update user in psql database
      if (dto?.username !== undefined) {
        await this.knexService.alterDatabaseUserUsername(
          user.username,
          updatedUser.username,
        );
      }
      if (permitPasswordChange && dto?.password !== undefined) {
        await this.knexService.alterDatabaseUserPassword(
          updatedUser.username,
          dto.password,
        );
      }
      // return user info
      delete updatedUser.password;
      return updatedUser;
    });
  }

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find user
      const user = await trxPrisma.user.findUnique({
        where: { id },
      });
      if (!user) {
        throw new NotFoundException('User not found!');
      }
      // delete user
      await trxPrisma.user.delete({ where: { id } });
      // remove user from psql database
      await this.knexService.removeDatabaseUser(user.username);
      // return user info
      delete user.password;
      return user;
    });
  }
}
