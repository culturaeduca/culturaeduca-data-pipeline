import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { PrismaService } from '@/prisma.service';
import { KnexModule } from '@/knex/knex.module';

@Module({
  imports: [KnexModule],
  providers: [UsersService, PrismaService],
  exports: [UsersService],
})
export class UsersModule {}
