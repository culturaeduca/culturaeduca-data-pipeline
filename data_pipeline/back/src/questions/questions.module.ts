import { Module } from '@nestjs/common';

import { KnexModule } from '@/knex/knex.module';
import { PrismaService } from '@/prisma.service';
import { QuestionsController } from './questions.controller';
import { QuestionsService } from './questions.service';

@Module({
  imports: [KnexModule],
  controllers: [QuestionsController],
  providers: [QuestionsService, PrismaService],
})
export class QuestionsModule {}
