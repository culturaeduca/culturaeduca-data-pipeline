import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';

import {
  CreateQuestionDto,
  PreviewQueryGeojsonQuestionQueryDto,
  PreviewQueryQuestionQueryDto,
  UpdateQuestionDto,
} from './dto';
import { KnexService } from '@/knex/knex.service';
import { PrismaService, PrismaTransaction } from '@/prisma.service';
import { DB_SCHEMA, PerformQuery } from '@/knex/knex.constants';

@Injectable()
export class QuestionsService {
  constructor(
    private readonly knexService: KnexService,
    private readonly prisma: PrismaService,
  ) {}

  async create(dto: CreateQuestionDto, userId: number) {
    // const query = await this._getQuestionQuery(dto.query);
    return this.prisma.$transaction(
      async (trxPrisma) => {
        // create question
        const uri = dto.uri?.trim()?.toLowerCase() || null;
        const question = await trxPrisma.question.create({
          data: {
            uri,
            title: dto.title?.trim() || null,
            description: dto.description?.trim() || null,
            materialized: dto.materialized,
            rawQuery: null,
            viewName: uri,
            userCreatedBy: { connect: { id: userId } },
            userUpdatedBy: { connect: { id: userId } },
          },
        });
        // define question tables aliases
        const sections = await trxPrisma.section.findMany({
          where: {
            id: { in: dto.query.tables.map((table) => table.sectionId) },
          },
        });
        const sectionsMap = new Map();
        const dtoQueryTables = dto.query.tables.map((table) => {
          // define alias and tableName
          const section = sections.find((s) => s.id === table.sectionId);
          let aliasIndex = 0;
          if (sectionsMap.has(section.id)) {
            aliasIndex = sectionsMap.get(section.id) + 1;
          }
          sectionsMap.set(section.id, aliasIndex);
          return {
            aliasIndex,
            ...table,
          };
        });
        // create question tables
        const questionTables = [];
        for await (const queryTable of dtoQueryTables) {
          const questionTable = await trxPrisma.questionTable.create({
            data: {
              queryKey: queryTable.key,
              aliasIndex: queryTable.aliasIndex,
              joinType: queryTable.join?.type || null,
              question: {
                connect: { id: question.id },
              },
              dataset: {
                connect: { id: queryTable.datasetId },
              },
              version: {
                connect: { id: queryTable.versionId },
              },
              section: {
                connect: { id: queryTable.sectionId },
              },
              userCreatedBy: { connect: { id: userId } },
              userUpdatedBy: { connect: { id: userId } },
            },
          });
          questionTables.push(questionTable);
        }
        // create question table joins and question columns
        for await (const queryTable of dto.query.tables) {
          const questionTable = questionTables.find(
            (el) => el.queryKey === queryTable.key,
          );
          // joins
          await trxPrisma.questionTableJoin.createMany({
            data: (queryTable.join?.on || []).map((on, index) => {
              const leftQuestionTable = questionTables.find(
                (el) => el.queryKey === on.left.tableKey,
              );
              const rightQuestionTable = questionTables.find(
                (el) => el.queryKey === on.right.tableKey,
              );
              return {
                leftQuestionTableId: leftQuestionTable.id,
                leftAttributeSeriesId: on.left.attributeSeriesId,
                rightQuestionTableId: rightQuestionTable.id,
                rightAttributeSeriesId: on.right.attributeSeriesId,
                queryKey: index,
                operator: on.operator,
                createdBy: userId,
                updatedBy: userId,
              };
            }),
          });
          // columns
          await trxPrisma.questionColumn.createMany({
            data: queryTable.columns.map((column, index) => ({
              questionId: question.id,
              questionTableId: questionTable.id,
              attributeSeriesId: column.attributeSeriesId,
              queryKey: index,
              expression: null,
              name: column.as?.name?.trim()?.toLowerCase() || null,
              description: column.as?.description?.trim() || null,
              type: column.as?.type?.trim() || null,
              optional: column.as?.optional || null,
              indexable: column.as?.indexable || null,
              unique: column.as?.unique || null,
              primary: column.as?.primary || null,
              sensitiveData: column.as?.sensitiveData || null,
              defaultValue: column.as?.defaultValue?.trim() || null,
              unit: column.as?.unit?.trim() || null,
              decodeValue: column.as?.decodeValue || null,
              createdBy: userId,
              updatedBy: userId,
            })),
          });
        }
        // get query to create view
        const questionViewQuery = await this._getQuestionViewQuery(
          question.id,
          trxPrisma,
        );
        // create view in the questions schema
        await this.knexService.createQuestionView(
          question.viewName,
          question.materialized,
          questionViewQuery,
        );
        return question;
      },
      { timeout: 60000 },
    );
  }

  async findAll() {
    return this.prisma.question.findMany();
  }

  async findOne(id: number) {
    const question = await this.prisma.question.findUnique({
      where: { id },
      include: {
        questionTables: {
          include: {
            dataset: true,
            version: true,
            section: true,
            questionTableJoinsLeft: {
              orderBy: { queryKey: 'asc' },
            },
            questionTableJoinsRight: {
              orderBy: { queryKey: 'asc' },
            },
          },
          orderBy: { queryKey: 'asc' },
        },
        questionColumns: {
          include: {
            attributeSeries: {
              include: {
                attributes: true,
              },
            },
          },
        },
      },
    });
    if (!question) {
      throw new NotFoundException(`Question #${id} not found!`);
    }
    return question;
  }

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const question = await this.prisma.question.findUnique({
        where: { id },
      });
      if (!question) {
        throw new NotFoundException(`Question #${id} not found!`);
      }
      // delete question (questionTables, questionTableJoins and questionColumns should cascade)
      await trxPrisma.question.delete({ where: { id: question.id } });
      // drop question view
      await this.knexService.dropQuestionView(
        question.viewName,
        question.materialized,
      );
      return question;
    });
  }

  async update(id: number, dto: UpdateQuestionDto, userId: number) {
    throw new NotImplementedException();
  }

  async previewQuery(
    dto: CreateQuestionDto['query'],
    queryDto: PreviewQueryQuestionQueryDto,
  ) {
    const previewQuery = await this._getQuestionPreviewQuery(dto);
    return this.knexService.performPreviewQuery(previewQuery, queryDto);
  }

  private async _getQuestionViewQuery(
    questionId: number,
    prismaTransaction?: PrismaTransaction,
  ): Promise<PerformQuery> {
    return this.prisma.$transaction(async (_trxPrisma) => {
      const trxPrisma = prismaTransaction || _trxPrisma;
      const question = await trxPrisma.question.findUnique({
        where: { id: questionId },
        include: {
          questionTables: {
            include: {
              dataset: true,
              version: true,
              section: true,
              questionTableJoinsLeft: {
                orderBy: { queryKey: 'asc' },
              },
              questionTableJoinsRight: {
                orderBy: { queryKey: 'asc' },
              },
            },
            orderBy: { queryKey: 'asc' },
          },
          questionColumns: {
            orderBy: { queryKey: 'asc' },
          },
        },
      });
      const sections = question.questionTables.map((qt) => qt.section);
      const questionTableJoins = question.questionTables.flatMap((qt) => [
        ...qt.questionTableJoinsLeft,
        ...qt.questionTableJoinsRight,
      ]);
      const attributesSeriesIds = question.questionColumns
        .map((qc) => qc.attributeSeriesId)
        .concat(
          questionTableJoins.flatMap((qtj) => [
            qtj.leftAttributeSeriesId,
            qtj.rightAttributeSeriesId,
          ]),
        );
      const attributes = await trxPrisma.attribute.findMany({
        where: {
          sectionId: { in: sections.map((s) => s.id) },
          seriesId: { in: attributesSeriesIds },
        },
      });
      const tables = question.questionTables.map((questionTable) => {
        const tableName = questionTable.section.tableName;
        const alias = `${tableName}_${questionTable.aliasIndex}`;

        const joinOn = questionTable.questionTableJoinsLeft.map(
          (questionTableJoin) => {
            // left
            const leftQuestionTable = question.questionTables.find(
              (qt) => qt.id === questionTableJoin.leftQuestionTableId,
            );
            const leftAttribute = attributes.find(
              (a) =>
                a.seriesId === questionTableJoin.leftAttributeSeriesId &&
                a.sectionId === leftQuestionTable.sectionId,
            );
            // right
            const rightQuestionTable = question.questionTables.find(
              (qt) => (qt.id = questionTableJoin.rightQuestionTableId),
            );
            const rightAttribute = attributes.find(
              (a) =>
                a.seriesId === questionTableJoin.rightAttributeSeriesId &&
                a.sectionId === rightQuestionTable.sectionId,
            );
            return {
              leftAlias: `${leftQuestionTable.section.tableName}_${leftQuestionTable.aliasIndex}`,
              leftColumnName: leftAttribute.name,
              rightAlias: `${rightQuestionTable.section.tableName}_${rightQuestionTable.aliasIndex}`,
              rightColumnName: rightAttribute.name,
              operator: questionTableJoin.operator,
            };
          },
        );

        // select columns
        const selectColumns = question.questionColumns
          .filter((qc) => qc.questionTableId === questionTable.id)
          .map((questionColumn) => {
            const attribute = attributes.find(
              (el) => el.seriesId === questionColumn.attributeSeriesId,
            );
            return {
              // TODO: complete name, casting, indexes, etc
              columnName: attribute.name,
              alias: `${alias}_${questionColumn.name || attribute.name}`,
              type: questionColumn.type || attribute.type,
            };
          });

        // return
        return {
          schema: DB_SCHEMA.DATASETS,
          alias,
          tableName,
          joinType: questionTable.joinType,
          joinOn,
          selectColumns,
        };
      });
      return {
        tables,
      };
    });
  }

  private async _getQuestionPreviewQuery(
    dto: CreateQuestionDto['query'],
  ): Promise<PerformQuery> {
    // get sections and attributes
    const sections = await this.prisma.section.findMany({
      where: { id: { in: dto.tables.map((table) => table.sectionId) } },
    });
    const attributeSeriesIds = dto.tables
      .flatMap((table) =>
        table.columns.map((column) => column.attributeSeriesId),
      )
      .concat(
        dto.tables
          .flatMap((table) => table.join?.on || [])
          .flatMap((on) => [
            on.left.attributeSeriesId,
            on.right.attributeSeriesId,
          ]),
      );
    const attributes = await this.prisma.attribute.findMany({
      where: {
        sectionId: { in: sections.map((section) => section.id) },
        seriesId: { in: attributeSeriesIds },
      },
    });
    // define alias regarding repeating sections
    const sectionsMap = new Map();
    const dtoTables = dto.tables.map((table) => {
      const section = sections.find((s) => s.id === table.sectionId);
      let aliasIndex = 0;
      if (sectionsMap.has(table.sectionId)) {
        aliasIndex = sectionsMap.get(table.sectionId) + 1;
      }
      sectionsMap.set(table.sectionId, aliasIndex);
      return {
        alias: `${section.tableName}_${aliasIndex}`,
        tableName: section.tableName,
        ...table,
      };
    });
    const tables = dtoTables.map((table) => {
      // define joinType and joinOn
      const joinOn = (table.join?.on || []).map((on) => {
        // left
        const leftTable = dtoTables.find((t) => t.key === on.left.tableKey);
        const leftAttribute = attributes.find(
          (a) =>
            a.seriesId === on.left.attributeSeriesId &&
            a.sectionId === leftTable.sectionId,
        );
        // right
        const rightTable = dtoTables.find((t) => t.key === on.right.tableKey);
        const rightAttribute = attributes.find(
          (a) =>
            a.seriesId === on.right.attributeSeriesId &&
            a.sectionId === rightTable.sectionId,
        );
        // return
        return {
          leftAlias: leftTable.alias,
          leftColumnName: leftAttribute.name,
          rightAlias: rightTable.alias,
          rightColumnName: rightAttribute.name,
          operator: on.operator,
        };
      });
      // select columns
      const selectColumns = table.columns.map((column) => {
        const attribute = attributes.find(
          (a) =>
            a.seriesId === column.attributeSeriesId &&
            a.sectionId === table.sectionId,
        );
        // implement rest of column.as
        return {
          columnName: attribute.name,
          alias: `${table.alias}_${column.as?.name || attribute.name}`,
          type: column.as?.type || attribute.type,
        };
      });
      return {
        schema: DB_SCHEMA.DATASETS,
        tableName: table.tableName,
        alias: table.alias,
        joinType: table.join?.type || null,
        joinOn,
        selectColumns,
      };
    });
    return { tables };
  }

  getPreviewQueryGeojson(queryDto: PreviewQueryGeojsonQuestionQueryDto) {
    return this.knexService.findGeojsonValue(
      DB_SCHEMA.DATASETS,
      queryDto.table,
      queryDto.columnGeom,
      queryDto.columnPk,
      queryDto.valuePk,
    );
  }
}
