import { IsString } from 'class-validator';

export class PreviewQueryGeojsonQuestionQueryDto {
  @IsString()
  table: string;

  @IsString()
  columnGeom: string;

  @IsString()
  columnPk: string;

  @IsString()
  valuePk: string;
}
