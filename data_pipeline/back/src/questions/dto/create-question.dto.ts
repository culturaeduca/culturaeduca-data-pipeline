import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import {
  QueryTableJoinOnOperatorEnum,
  QueryTableJoinTypeEnum,
} from '../questions.contants';

class QueryTableColumnAs {
  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  description: string | null;

  @IsString()
  type: string;

  @IsBoolean()
  optional: boolean;

  @IsBoolean()
  indexable: boolean;

  @IsBoolean()
  unique: boolean;

  @IsBoolean()
  primary: boolean;

  @IsBoolean()
  sensitiveData: boolean;

  @IsString()
  @IsOptional()
  defaultValue: string | null;

  @IsString()
  @IsOptional()
  unit: string | null;

  @IsOptional()
  decodeValue: any;
}

class QueryTableColumn {
  @IsInt()
  attributeSeriesId: number;

  @IsOptional()
  @ValidateNested()
  @Type(() => QueryTableColumnAs)
  as: QueryTableColumnAs | null;
}

class QueryTableJoinOnColumn {
  @IsInt()
  tableKey: number;

  @IsInt()
  attributeSeriesId: number;
}

class QueryTableJoinOn {
  @ValidateNested()
  @Type(() => QueryTableJoinOnColumn)
  left: QueryTableJoinOnColumn;

  @ValidateNested()
  @Type(() => QueryTableJoinOnColumn)
  right: QueryTableJoinOnColumn;

  @IsEnum(QueryTableJoinOnOperatorEnum)
  operator: QueryTableJoinOnOperatorEnum;
}

class QueryTableJoin {
  @IsEnum(QueryTableJoinTypeEnum)
  type: QueryTableJoinTypeEnum;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => QueryTableJoinOn)
  on: QueryTableJoinOn[];
}

class QueryTable {
  @IsInt()
  key: number;

  @IsInt()
  datasetId: number;

  @IsInt()
  versionId: number;

  @IsInt()
  sectionId: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => QueryTableColumn)
  columns: QueryTableColumn[];

  @IsOptional()
  @ValidateNested()
  @Type(() => QueryTableJoin)
  join: QueryTableJoin | null;
}

class QuestionQuery {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => QueryTable)
  tables: QueryTable[];
}

export class CreateQuestionDto {
  @IsString()
  @IsNotEmpty()
  uri: string;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsOptional()
  description: string | null;

  @IsBoolean()
  materialized: boolean;

  @ValidateNested()
  @Type(() => QuestionQuery)
  query: QuestionQuery;
}
