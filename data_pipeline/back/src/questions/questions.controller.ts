import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { QuestionsService } from './questions.service';
import {
  CreateQuestionDto,
  PreviewQueryGeojsonQuestionQueryDto,
  PreviewQueryQuestionQueryDto,
  UpdateQuestionDto,
} from './dto';
import { User } from '@/user.decorator';

@Controller('questions')
export class QuestionsController {
  constructor(private readonly questionsService: QuestionsService) {}

  // Get

  @Get('preview_query/geojson')
  getPreviewQueryGeojson(
    @Query() queryDto: PreviewQueryGeojsonQuestionQueryDto,
  ) {
    return this.questionsService.getPreviewQueryGeojson(queryDto);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.questionsService.findOne(id);
  }

  @Get()
  findAll() {
    return this.questionsService.findAll();
  }

  // Post

  @Post('preview_query')
  previewQuery(
    @Body() dto: CreateQuestionDto['query'],
    @Query() queryDto: PreviewQueryQuestionQueryDto,
  ) {
    return this.questionsService.previewQuery(dto, queryDto);
  }

  @Post()
  create(@Body() dto: CreateQuestionDto, @User('id') userId: number) {
    return this.questionsService.create(dto, userId);
  }

  // Patch

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateQuestionDto,
    @User('id') userId: number,
  ) {
    return this.questionsService.update(id, dto, userId);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.questionsService.remove(id);
  }
}
