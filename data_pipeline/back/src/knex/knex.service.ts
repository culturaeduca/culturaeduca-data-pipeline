import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import type { Knex } from 'knex';
import { sortBy } from 'lodash';
import {
  Attribute,
  DataIngestionGeoType,
  DatasetFormat,
  Section,
} from '@prisma/client';
import { DefaultSectionTableColumnEnum } from '@/sections/sections.contants';
import {
  AttributeIndexTypeEnum,
  AttributeTypeEnum,
} from '@/attributes/attributes.constants';
import { MongoCollectionsService } from '@/mongo-collections/mongo-collections.service';
import { KnexConnection } from './knex.provider';
import {
  DB_SCHEMA,
  PerformQuery,
  knexJoinFunctions,
  knexJoinOperators,
  attributesTypesKnexColumnFunction,
  attributesIndexTypesPostgreSql,
} from './knex.constants';
import { asyncIterable } from '@/utils/misc-functions.utils';

@Injectable()
export class KnexService {
  constructor(
    @Inject(KnexConnection) private readonly knex: Knex,
    private readonly mongoCollectionsService: MongoCollectionsService,
  ) {}

  async getGeojsonRadius(options: {
    lat: number;
    lng: number;
    radiusKm: number;
    item: {
      id: number;
      name: string;
      level: number;
      required: boolean;
      databaseSchema: DB_SCHEMA;
      tableName: string;
      columnGeom: string;
      columnGeog: string;
      columns: { name: string; type: string }[];
    };
  }) {
    const { lat, lng, radiusKm, item } = options;

    const from = `"${item.databaseSchema}"."${item.tableName}"`;
    const point = `ST_Point(${lng}, ${lat}, 4326)::geography`;
    const where = `ST_DWithin(${item.columnGeog},${point}, ${radiusKm * 1000})`;
    let select = '';
    let geojson = `
    'Feature' as "type",
    jsonb_build_object(
      'type', "${item.columnGeog}"->'type',`;
    let properties = `jsonb_build_object(`;
    item.columns.forEach((column) => {
      select = `${select}
        "${column.name}",`;
      properties = `${properties}
        '${column.name}', "${column.name}",`;
    });
    select = `${select}
      ST_AsGeoJSON("${item.columnGeog}")::jsonb as "${item.columnGeog}"`;
    properties = `${properties.slice(0, -1)})`;
    geojson = `${geojson}
      'coordinates', "${item.columnGeog}"->'coordinates'
      ) as "geometry",
      ${properties} as "properties"`;
    const rawQuery = `
      SELECT ${geojson}
      FROM (
        SELECT ${select}
        FROM ${from}
        WHERE ${where}
        ) AS geojson;
        `;

    const results = await this.knex.raw(rawQuery);
    return { type: 'FeatureCollection', features: results.rows };
  }

  async getBreadcrumbs(options: {
    lat: number;
    lng: number;
    items: {
      id: number;
      name: string;
      singularName: string;
      level: number;
      required: boolean;
      boundaryNameAttribute: string;
      databaseSchema: DB_SCHEMA;
      tableName: string;
      columnGeom: string;
      columnGeog: string;
      columns: { name: string; type: string }[];
    }[];
  }) {
    const { lat, lng } = options;
    const sortedItems = sortBy(options.items, ['level']);

    // select columns as "boundary_<id>_<columnName>"
    let knexQuery = this.knex.select(
      sortedItems
        .map((item) =>
          item.columns.map((column) =>
            this.knex.raw(
              `"boundary_${item.id}"."${column.name}" AS "boundary_${item.id}_${column.name}"`,
            ),
          ),
        )
        .flat(),
    );

    // from tables as "boundary_<id>"
    const point = `ST_Point(${lng}, ${lat}, 4326)`;
    const fromArray = sortedItems.map((item, index) => {
      const table = `"${item.databaseSchema}"."${item.tableName}" AS "boundary_${item.id}"`;
      if (index === 0) {
        return table;
      }
      if (item.required) {
        return `INNER JOIN ${table} ON ST_Contains("boundary_${item.id}"."${item.columnGeom}", ${point})`;
      }
      return `LEFT JOIN ${table} ON ST_Contains("boundary_${item.id}"."${item.columnGeom}", ${point})`;
    });
    knexQuery = knexQuery.fromRaw(fromArray.join(' '));

    // where contains point from (lng,lat)
    const where = `ST_Contains("boundary_${sortedItems[0].id}"."${sortedItems[0].columnGeom}", ${point})`;
    knexQuery = knexQuery.whereRaw(where);
    const result = await knexQuery.first();
    if (!result) {
      return null;
    }
    return sortedItems
      .map((item) => {
        const prefix = `boundary_${item.id}_`;
        const properties = {};
        Object.keys(result)
          .filter((key) => key.startsWith(prefix))
          .forEach((key) => {
            const newKey = key.replace(prefix, '');
            properties[newKey] = result[key];
          });
        // Check if all properties are null, if so, the point doesn't belong to this boundary
        const allNull = Object.values(properties).reduce((acc, value) => {
          return acc && value === null;
        }, true);
        return {
          id: item.id,
          name: item.name,
          singularName: item.singularName,
          level: item.level,
          required: item.required,
          boundaryNameAttribute: item.boundaryNameAttribute,
          properties: allNull ? null : properties,
        };
      })
      .filter((item) => item.properties !== null);
  }

  async addColumnsDatasetsTable(
    tableName: string,
    attributes: Attribute[],
  ): Promise<void> {
    return this.knex.transaction(async (trxKnex) => {
      for await (const attribute of attributes) {
        const hasColumn = await trxKnex.schema
          .withSchema(DB_SCHEMA.DATASETS)
          .hasColumn(tableName, attribute.name);
        // GEOMETRY
        if (attribute.type === AttributeTypeEnum.GEOMETRY) {
          const geomType = attribute?.typeOptions?.['type'] || 'GEOMETRY';
          const geomSrid = attribute?.typeOptions?.['srid'] || '4326';
          await trxKnex.raw(`
            ALTER TABLE "${DB_SCHEMA.DATASETS}"."${tableName}"
            ADD COLUMN "${attribute.name}" geometry(${geomType}, ${geomSrid})${
            attribute.optional ? '' : ' NOT NULL'
          };
            COMMENT ON COLUMN "${DB_SCHEMA.DATASETS}"."${tableName}"."${
            attribute.name
          }" IS '${attribute.description?.replace('\n', ' ')?.trim() || ''}';
          `);
          if (attribute.indexable) {
            const indexType =
              attributesIndexTypesPostgreSql?.[attribute.indexType] ||
              attributesIndexTypesPostgreSql[AttributeIndexTypeEnum.GIST];
            await trxKnex.raw(`
              CREATE INDEX "${tableName}_${attribute.name}_index"
                ON "${DB_SCHEMA.DATASETS}"."${tableName}"
                USING ${indexType} ("${attribute.name}");
            `);
          }
        }
        // NOT GEOMETRY
        else {
          await trxKnex.schema
            .withSchema(DB_SCHEMA.DATASETS)
            .alterTable(tableName, async (table) => {
              if (hasColumn) {
                table.dropColumn(attribute.name);
              }
              const columnName = attribute.name;
              // create column
              const knexColumnFunction =
                attributesTypesKnexColumnFunction?.[attribute.type];
              const knexNullableFunction = attribute.optional
                ? 'nullable'
                : 'notNullable';
              table[knexColumnFunction](columnName)
                [knexNullableFunction]()
                .comment(
                  attribute.description?.replace('\n', ' ')?.trim() || '',
                );
              // unique
              if (attribute.unique) {
                table.unique([columnName]);
              }
              // index
              if (attribute.indexable) {
                const indexType =
                  attributesIndexTypesPostgreSql?.[attribute.indexType] ||
                  attributesIndexTypesPostgreSql[AttributeIndexTypeEnum.BTREE];
                table.index(columnName, null, indexType);
              }
            });
        }
      }
    });
  }

  async changeColumnDatasetsTable(
    tableName: string,
    oldAttribute: Attribute,
    newAttribute: Attribute,
  ): Promise<void> {
    if (oldAttribute.id !== newAttribute.id) {
      throw new BadRequestException('Attribute id must be the same');
    }
    return this.knex.transaction(async (trxKnex) => {
      await trxKnex.schema
        .withSchema(DB_SCHEMA.DATASETS)
        .alterTable(tableName, async (table) => {
          const columnName = oldAttribute.name;
          if (newAttribute.type === AttributeTypeEnum.GEOMETRY) {
            // GEOMETRY
            // type
            if (oldAttribute.type !== newAttribute.type) {
              const geomType =
                newAttribute?.typeOptions?.['type'] || 'GEOMETRY';
              const geomSrid = newAttribute?.typeOptions?.['srid'] || '4326';
              await trxKnex.raw(`
                ALTER TABLE "${DB_SCHEMA.DATASETS}"."${tableName}"
                ALTER COLUMN "${columnName}" TYPE geometry(${geomType}, ${geomSrid});
              }';
            `);
            }
            // optional
            if (oldAttribute.optional !== newAttribute.optional) {
              await trxKnex.raw(`
                ALTER TABLE "${DB_SCHEMA.DATASETS}"."${tableName}"
                ALTER COLUMN "${columnName}" ${
                newAttribute.optional ? 'DROP' : 'SET'
              } NOT NULL;
            `);
            }
            // description
            if (oldAttribute.description !== newAttribute.description) {
              await trxKnex.raw(`
                COMMENT ON COLUMN "${
                  DB_SCHEMA.DATASETS
                }"."${tableName}"."${columnName}" IS '${
                newAttribute.description?.replace('\n', ' ')?.trim() || ''
              }';
            `);
            }
            // index
            if (oldAttribute.indexable !== newAttribute.indexable) {
              if (newAttribute.indexable === true) {
                const indexType =
                  attributesIndexTypesPostgreSql?.[newAttribute.indexType] ||
                  attributesIndexTypesPostgreSql[AttributeIndexTypeEnum.GIST];
                await trxKnex.raw(`
                  CREATE INDEX "${tableName}_${columnName}_index"
                    ON "${DB_SCHEMA.DATASETS}"."${tableName}"
                    USING ${indexType} ("${columnName}");
                `);
              } else {
                await trxKnex.raw(`
                  DROP INDEX "${DB_SCHEMA.DATASETS}"."${tableName}_${columnName}_index"
                `);
              }
            }
          }
          // NOT GEOMETRY
          else {
            // type and optional
            if (
              oldAttribute.type !== newAttribute.type ||
              oldAttribute.optional !== newAttribute.optional ||
              oldAttribute.description !== newAttribute.description
            ) {
              const knexColumnFunction =
                attributesTypesKnexColumnFunction?.[newAttribute.type];
              const knexNullableFunction = newAttribute.optional
                ? 'nullable'
                : 'notNullable';
              table[knexColumnFunction](columnName)
                [knexNullableFunction]()
                .comment(
                  newAttribute.description?.replace('\n', ' ')?.trim() || '',
                )
                .alter();
            }
            // index
            if (oldAttribute.indexable !== newAttribute.indexable) {
              if (newAttribute.indexable === true) {
                table.index(columnName);
              } else {
                table.dropIndex(columnName);
              }
            }
            // unique
            if (oldAttribute.unique !== newAttribute.unique) {
              if (newAttribute.unique === true) {
                table.unique([columnName]);
              } else {
                table.dropUnique([columnName]);
              }
            }
          }
          // rename
          if (oldAttribute.name !== newAttribute.name) {
            table.renameColumn(oldAttribute.name, newAttribute.name);
          }
        });
      if (oldAttribute.name !== newAttribute.name) {
        await this._renameCascade(
          {
            databaseSchema: DB_SCHEMA.DATASETS,
            tableName,
            oldName: oldAttribute.name,
            newName: newAttribute.name,
          },
          trxKnex,
        );
      }
    });
  }

  async countDatasetsTableRows(tableName: string): Promise<number> {
    const [{ count }] = await this.knex
      .withSchema(DB_SCHEMA.DATASETS)
      .table(tableName)
      .count();
    return Number(count);
  }

  async createDatasetsTable(
    tableName: string,
    description: string | null,
    datasetFormat: DatasetFormat,
  ): Promise<void> {
    return this.knex.transaction(async (trxKnex) => {
      if (
        await trxKnex.schema.withSchema(DB_SCHEMA.DATASETS).hasTable(tableName)
      ) {
        await trxKnex.schema
          .withSchema(DB_SCHEMA.DATASETS)
          .dropTable(tableName);
      }
      await trxKnex.schema
        .withSchema(DB_SCHEMA.DATASETS)
        .createTable(tableName, (table) => {
          // _id
          table.bigIncrements(DefaultSectionTableColumnEnum.ID, {
            primaryKey: true,
          });
          // _data_ingestion_id
          table
            .integer(DefaultSectionTableColumnEnum.DATA_INGESTION_ID)
            .notNullable();
          // _created_at
          table
            .timestamp(DefaultSectionTableColumnEnum.CREATED_AT, {
              useTz: true,
            })
            .notNullable()
            .defaultTo(trxKnex.fn.now());
          // _updated_at
          table
            .timestamp(DefaultSectionTableColumnEnum.UPDATED_AT, {
              useTz: true,
            })
            .notNullable()
            .defaultTo(trxKnex.fn.now());
          // psql table description
          table.comment(description?.replace('\n', ' ')?.trim() || '');
        });
      // add geometry and geography column if format is GEOSPATIAL
      let geometryOptions = '';
      if (datasetFormat === DatasetFormat.GEOSPATIAL_POINT) {
        geometryOptions = '(POINT, 4326)';
      } else if (datasetFormat === DatasetFormat.GEOSPATIAL_LINE) {
        geometryOptions = '(MULTILINE, 4326)';
      } else if (datasetFormat === DatasetFormat.GEOSPATIAL_POLYGON) {
        geometryOptions = '(MULTIPOLYGON, 4326)';
      }
      // _geom, _geog with indexes
      await trxKnex.raw(`
        ALTER TABLE "${DB_SCHEMA.DATASETS}"."${tableName}"
          ADD COLUMN "${DefaultSectionTableColumnEnum.GEOMETRY}" geometry${geometryOptions} NULL,
          ADD COLUMN "${DefaultSectionTableColumnEnum.GEOGRAPHY}" geography GENERATED ALWAYS AS ("${DefaultSectionTableColumnEnum.GEOMETRY}"::geography) STORED NULL;
      `);
      if (geometryOptions) {
        await trxKnex.raw(`
          CREATE INDEX "${tableName}_${DefaultSectionTableColumnEnum.GEOMETRY}_index"
            ON "${DB_SCHEMA.DATASETS}"."${tableName}"
            USING GIST ("${DefaultSectionTableColumnEnum.GEOMETRY}");
          CREATE INDEX "${tableName}_${DefaultSectionTableColumnEnum.GEOGRAPHY}_index"
            ON "${DB_SCHEMA.DATASETS}"."${tableName}"
            USING GIST ("${DefaultSectionTableColumnEnum.GEOGRAPHY}");
        `);
      }
    });
  }

  async renameTables(
    items: {
      databaseSchema: DB_SCHEMA;
      oldTableName: string;
      newTableName: string;
    }[],
  ): Promise<void> {
    return this.knex.transaction(async (trxKnex) => {
      for await (const item of items) {
        const { databaseSchema, oldTableName, newTableName } = item;
        // check if table exists
        if (
          !(await trxKnex.schema
            .withSchema(databaseSchema)
            .hasTable(oldTableName))
        ) {
          continue;
        }
        // rename table
        await trxKnex.schema
          .withSchema(databaseSchema)
          .renameTable(oldTableName, newTableName);
        // rename indexes, constraints, triggers, fks
        await this._renameCascade(
          {
            databaseSchema,
            tableName: newTableName,
            oldName: oldTableName,
            newName: newTableName,
          },
          trxKnex,
        );
      }
    });
  }

  async deleteDatasetsTableRowsFromDataIngestion(
    tableName: string,
    dataIngestionId: number,
  ) {
    return this.knex.transaction(async (trxKnex) => {
      await trxKnex
        .withSchema(DB_SCHEMA.DATASETS)
        .table(tableName)
        .delete()
        .where({
          [DefaultSectionTableColumnEnum.DATA_INGESTION_ID]: dataIngestionId,
        });
    });
  }

  async dropDatasetsTable(tableName: string): Promise<void> {
    return this.knex.transaction(async (trxKnex) => {
      if (
        await trxKnex.schema.withSchema(DB_SCHEMA.DATASETS).hasTable(tableName)
      ) {
        await trxKnex.schema
          .withSchema(DB_SCHEMA.DATASETS)
          .dropTable(tableName);
      }
    });
  }

  async dropColumnsDatasetsTable(
    tableName: string,
    columnsNames: string[],
  ): Promise<void> {
    return this.knex.transaction(async (trxKnex) => {
      for await (const columnName of columnsNames) {
        if (
          await trxKnex.schema
            .withSchema(DB_SCHEMA.DATASETS)
            .hasColumn(tableName, columnName)
        ) {
          await trxKnex.schema
            .withSchema(DB_SCHEMA.DATASETS)
            .alterTable(tableName, (table) => {
              table.dropColumn(columnName);
            });
        }
      }
    });
  }

  async insertDataDatasetsTableFromMongoCollection(
    dataIngestionId: number,
    tableName: string,
    collectionName: string,
    batchSize: number,
    mappers: {
      geoType: DataIngestionGeoType | null;
      geoPropertyNameLat: string | null;
      geoPropertyNameLng: string | null;
      geoPropertyNameWkt: string | null;
      properties: {
        attributeName: string;
        attributeType: AttributeTypeEnum;
        property: string | null;
        expression: string | null;
      }[];
    },
  ): Promise<void> {
    await this.knex.transaction(async (trxKnex) => {
      const total = await this.mongoCollectionsService.countDocuments(
        collectionName,
      );
      const limit = !batchSize || batchSize < 100 ? 100 : batchSize;
      const n = Math.ceil(total / limit);
      let skip = 0;
      for await (const i of asyncIterable(n)) {
        skip = i * limit;
        const results = await this.mongoCollectionsService.findDocuments(
          collectionName,
          { limit, skip },
        );

        const dataInsert = results.map((result) => {
          // data starts with dataIngestionId
          const data = {
            [DefaultSectionTableColumnEnum.DATA_INGESTION_ID]: dataIngestionId,
          };
          // populate properties
          for (let i = 0; i < mappers.properties.length; i++) {
            const property = mappers.properties[i].property;
            const expression = mappers.properties[i].expression;
            let value = property
              ? result['_doc']?.properties?.[property]
              : trxKnex.raw(expression);
            if (
              [AttributeTypeEnum.LONG, AttributeTypeEnum.INT].includes(
                mappers.properties[i].attributeType,
              )
            ) {
              if (Number(value) % 1 !== 0)
                throw new Error(
                  `Integer value has decimal part (${mappers.properties[i].attributeName},${value})`,
                );
              value = !isNaN(Number(value)) ? Math.trunc(value) : null;
            }
            if (typeof value === 'string') {
              value = value.trim() || null;
            }
            data[mappers.properties[i].attributeName] = value;
          }
          // populate geometries
          switch (mappers.geoType) {
            case DataIngestionGeoType.GEOJSON:
              data[DefaultSectionTableColumnEnum.GEOMETRY] = trxKnex.raw(
                `ST_GeomFromGeoJSON(?)`,
                [result['_doc']?.geometry],
              );
              break;
            case DataIngestionGeoType.LAT_LNG:
              const lat =
                result['_doc']?.properties?.[mappers.geoPropertyNameLat];
              const lng =
                result['_doc']?.properties?.[mappers.geoPropertyNameLng];
              data[DefaultSectionTableColumnEnum.GEOMETRY] = trxKnex.raw(
                'ST_Point(?, ?, 4326)',
                [lng, lat],
              );
              break;
            case DataIngestionGeoType.WKT:
              const wkt =
                result['_doc']?.properties?.[mappers.geoPropertyNameWkt];
              data[DefaultSectionTableColumnEnum.GEOMETRY] = trxKnex.raw(
                'ST_GeomFromText(?, 4326)',
                [wkt],
              );
              break;
            default:
              break;
          }
          return data;
        });
        // load data
        await trxKnex
          .withSchema(DB_SCHEMA.DATASETS)
          .insert(dataInsert)
          .into(tableName);
      }
    });
  }

  async findAndCountDatasetsTable(
    tableName: string,
    attributes: Attribute[],
    options: {
      page: number;
      perPage: number;
    },
  ): Promise<{ total: number; results: any[] }> {
    // set limit and offset
    const page = options?.page && options.page > 0 ? options.page : 1;
    const perPage =
      options?.perPage && options.perPage > 1 ? options.perPage : 10;
    const limit = perPage;
    const offset = perPage * (page - 1);
    // attributes query
    const defaultColumns = Object.values(DefaultSectionTableColumnEnum).map(
      (defaultColumn) => {
        if (
          [
            DefaultSectionTableColumnEnum.GEOMETRY,
            DefaultSectionTableColumnEnum.GEOGRAPHY,
          ].includes(defaultColumn)
        ) {
          return this.knex.raw(
            `ST_GeometryType("${defaultColumn}"::geometry) as "${defaultColumn}"`,
          );
        }
        return defaultColumn;
      },
    );
    const columnsQuery = attributes
      .filter(
        (attribute) =>
          !attribute.sensitiveData &&
          attribute.type !== AttributeTypeEnum.GEOMETRY,
      )
      .map((attribute) => attribute.name);
    const columnsGeoRawQuery = attributes
      .filter(
        (attribute) =>
          !attribute.sensitiveData &&
          attribute.type === AttributeTypeEnum.GEOMETRY,
      )
      .map((attribute) =>
        this.knex.raw(
          `ST_GeometryType("${attribute.name}") as "${attribute.name}"`,
        ),
      );
    const [count, results] = await Promise.all([
      // count total
      this.knex.withSchema(DB_SCHEMA.DATASETS).table(tableName).count(),
      // perform query
      this.knex
        .withSchema(DB_SCHEMA.DATASETS)
        .table(tableName)
        .select([...defaultColumns, ...columnsQuery, ...columnsGeoRawQuery])
        .limit(limit)
        .offset(offset),
    ]);
    return { total: count?.[0]?.count, results };
  }

  async findAndCountWithJoinsDatasetsTable(
    tableName: string,
    attributes: Attribute[],
    joins: {
      tableName: string;
      attributes: Attribute[];
      type: string;
      on: {
        operation: string;
        leftAttribute: Attribute & { section: Section };
        rightAttribute: Attribute & { section: Section };
      }[];
    }[],
    options: {
      page: number;
      perPage: number;
    },
  ): Promise<{ total: number; results: any[] }> {
    // set limit and offset
    const page = options?.page && options.page > 0 ? options.page : 1;
    const perPage =
      options?.perPage && options.perPage > 1 ? options.perPage : 10;
    const limit = perPage;
    const offset = perPage * (page - 1);
    // attributes query
    const selectColumns = this._getSelectColumns(tableName, attributes);
    const selectColumnsJoin = joins.flatMap((join) =>
      this._getSelectColumns(join.tableName, join.attributes),
    );
    // queries
    let countQuery = this.knex.withSchema(DB_SCHEMA.DATASETS).table(tableName);
    let knexQuery = this.knex
      .withSchema(DB_SCHEMA.DATASETS)
      .table(tableName)
      .columns([...selectColumns, ...selectColumnsJoin])
      .select();
    joins.forEach((join) => {
      const knexJoinFunction = knexJoinFunctions?.[join.type] || 'innerJoin';
      countQuery = countQuery[knexJoinFunction](join.tableName, function () {
        const operator = knexJoinOperators?.[join.on[0].operation] || '=';
        this.on(
          `${join.on[0].leftAttribute.section.tableName}.${join.on[0].leftAttribute.name}`,
          operator,
          `${join.on[0].rightAttribute.section.tableName}.${join.on[0].rightAttribute.name}`,
        );
        join.on.forEach((el) => {
          const operator = knexJoinOperators?.[el.operation] || '=';
          this.orOn(
            `${el.leftAttribute.section.tableName}.${el.leftAttribute.name}`,
            operator,
            `${el.rightAttribute.section.tableName}.${el.rightAttribute.name}`,
          );
        });
      });
      knexQuery = knexQuery[knexJoinFunction](join.tableName, function () {
        const operator = knexJoinOperators?.[join.on[0].operation] || '=';
        this.on(
          `${join.on[0].leftAttribute.section.tableName}.${join.on[0].leftAttribute.name}`,
          operator,
          `${join.on[0].rightAttribute.section.tableName}.${join.on[0].rightAttribute.name}`,
        );
        join.on.forEach((el) => {
          const operator = knexJoinOperators?.[el.operation] || '=';
          this.orOn(
            `${el.leftAttribute.section.tableName}.${el.leftAttribute.name}`,
            operator,
            `${el.rightAttribute.section.tableName}.${el.rightAttribute.name}`,
          );
        });
      });
    });
    knexQuery.limit(limit).offset(offset);
    countQuery.count();

    const [count, results] = await Promise.all([
      // count total
      countQuery,
      // perform query
      knexQuery,
    ]);
    return { total: count?.[0]?.count, results };
  }

  async findGeojsonValue(
    schema: DB_SCHEMA,
    tableName: string,
    columnGeom: string,
    columnPk: string,
    valuePk: string | number,
  ): Promise<any> {
    // get column as geojson
    const results = await this.knex
      .withSchema(schema)
      .table(tableName)
      .select(
        this.knex.raw(
          `ST_AsGeoJSON("${columnGeom}"::geography)::jsonb as "${columnGeom}"`,
        ),
      )
      .where({ [columnPk]: valuePk });
    return results?.[0]?.[columnGeom];
  }

  private _getSelectColumns(
    tableName: string,
    attributes: {
      id: number;
      name: string;
      type: string;
      sensitiveData?: boolean;
    }[],
  ) {
    const defaultColumns = Object.values(DefaultSectionTableColumnEnum).map(
      (defaultColumn) => {
        if (
          [
            DefaultSectionTableColumnEnum.GEOMETRY,
            DefaultSectionTableColumnEnum.GEOGRAPHY,
          ].includes(defaultColumn)
        ) {
          return this.knex.raw(
            `ST_GeometryType("${tableName}"."${defaultColumn}") as "${defaultColumn}"`,
          );
        }
        return {
          [`${tableName}.${defaultColumn}`]: `${tableName}.${defaultColumn}`,
        };
      },
    );
    const columnsQuery = attributes
      .filter(
        (attribute) =>
          !attribute?.sensitiveData &&
          attribute.type !== AttributeTypeEnum.GEOMETRY,
      )
      .map((attribute) => ({
        [attribute.name]: `"${tableName}"."${attribute.name}"`,
      }));
    const columnsGeoRawQuery = attributes
      .filter(
        (attribute) =>
          !attribute?.sensitiveData &&
          attribute.type === AttributeTypeEnum.GEOMETRY,
      )
      .map((attribute) =>
        this.knex.raw(
          `ST_GeometryType("${tableName}"."${attribute.name}") as "${attribute.name}"`,
        ),
      );
    return [...defaultColumns, ...columnsQuery, ...columnsGeoRawQuery];
  }

  async performPreviewQuery(
    query: PerformQuery,
    options?: { page: number; perPage: number },
  ) {
    const { tables } = query;
    // set limit and offset
    const page = options?.page && options.page > 0 ? options.page : 1;
    const perPage =
      options?.perPage && options.perPage > 1 ? options.perPage : 10;
    const limit = perPage;
    const offset = perPage * (page - 1);
    // inject _id in select columns
    tables.forEach((table) => {
      table.selectColumns = [
        {
          columnName: DefaultSectionTableColumnEnum.ID as string,
          alias: `${table.alias}_${DefaultSectionTableColumnEnum.ID}`,
          type: AttributeTypeEnum.LONG as string,
        },
        {
          columnName: DefaultSectionTableColumnEnum.GEOMETRY as string,
          alias: `${table.alias}_${DefaultSectionTableColumnEnum.GEOMETRY}`,
          type: AttributeTypeEnum.GEOMETRY as string,
        },
        {
          columnName: DefaultSectionTableColumnEnum.GEOGRAPHY as string,
          alias: `${table.alias}_${DefaultSectionTableColumnEnum.GEOGRAPHY}`,
          type: AttributeTypeEnum.GEOGRAPHY as string,
        },
      ].concat(table.selectColumns);
    });
    // get select columns
    const selectColumns = tables
      .map((table) => {
        return table.selectColumns.map((selectColumn) => {
          if (
            [
              AttributeTypeEnum.GEOMETRY as string,
              AttributeTypeEnum.GEOGRAPHY as string,
            ].includes(selectColumn.type)
          ) {
            return this.knex.raw(
              `ST_GeometryType("${table.alias}"."${selectColumn.columnName}") as "${selectColumn.alias}"`,
            );
          }
          return this.knex
            .ref(`${table.alias}.${selectColumn.columnName}`)
            .as(selectColumn.alias);
        });
      })
      .flat();
    // start query
    let knexQuery = this.knex.from({
      [tables[0].alias]: `${DB_SCHEMA.DATASETS}.${tables[0].tableName}`,
    });
    // joins
    tables.forEach((table) => {
      const joinOn = table?.joinOn;
      if (joinOn.length === 0) return;
      const knexJoinFunction =
        knexJoinFunctions?.[table?.joinType] || 'leftJoin';
      knexQuery = knexQuery[knexJoinFunction](
        { [table.alias]: `${DB_SCHEMA.DATASETS}.${table.tableName}` },
        function () {
          joinOn.forEach((join, index) => {
            const left = `${join.leftAlias}.${join.leftColumnName}`;
            const operator = knexJoinOperators?.[join.operator] || '=';
            const right = `${join.rightAlias}.${join.rightColumnName}`;
            const thisOnFunction = index === 0 ? 'on' : 'andOn';
            this[thisOnFunction](left, operator, right);
          });
        },
      );
    });
    const knexQueryCount = knexQuery.clone().count();
    const knexQueryResults = knexQuery
      .columns(selectColumns)
      .select()
      .limit(limit)
      .offset(offset);
    // perform query
    const [count, results] = await Promise.all([
      knexQueryCount,
      knexQueryResults,
    ]);
    return { query, total: count?.[0]?.count, results };
  }

  async createQuestionView(
    viewName: string,
    materialized: boolean,
    query: PerformQuery,
  ): Promise<void> {
    const { tables } = query;
    // get select columns
    const selectColumns = tables
      .map((table) =>
        table.selectColumns.map((selectColumn) =>
          this.knex
            .ref(`${table.alias}.${selectColumn.columnName}`)
            .as(selectColumn.alias),
        ),
      )
      .flat();
    // start query
    let knexQuery = this.knex.select(selectColumns).from({
      [tables[0].alias]: `${DB_SCHEMA.DATASETS}.${tables[0].tableName}`,
    });
    // joins
    tables.forEach((table) => {
      const joinOn = table?.joinOn;
      if (joinOn.length === 0) return;
      const knexJoinFunction =
        knexJoinFunctions?.[table?.joinType] || 'leftJoin';
      knexQuery = knexQuery[knexJoinFunction](
        { [table.alias]: `${DB_SCHEMA.DATASETS}.${table.tableName}` },
        function () {
          joinOn.forEach((join, index) => {
            const left = `${join.leftAlias}.${join.leftColumnName}`;
            const operator = knexJoinOperators?.[join.operator] || '=';
            const right = `${join.rightAlias}.${join.rightColumnName}`;
            const thisOnFunction = index === 0 ? 'on' : 'andOn';
            this[thisOnFunction](left, operator, right);
          });
        },
      );
    });
    // create view
    const dropViewFunction = materialized
      ? 'dropMaterializedViewIfExists'
      : 'dropViewIfExists';
    const createViewFunction = materialized
      ? 'createMaterializedView'
      : 'createView';
    return this.knex.schema
      .withSchema(DB_SCHEMA.QUESTIONS)
      [dropViewFunction](viewName)
      [createViewFunction](viewName, (view) => {
        view.as(knexQuery);
      });
  }

  async dropQuestionView(
    viewName: string,
    materialized: boolean,
  ): Promise<void> {
    if (materialized) {
      return this.knex.schema
        .withSchema(DB_SCHEMA.QUESTIONS)
        .dropMaterializedViewIfExists(viewName);
    }
    return this.knex.schema
      .withSchema(DB_SCHEMA.QUESTIONS)
      .dropViewIfExists(viewName);
  }

  async createDatabaseUser(username: string, password: string) {
    const role = 'readonly_group';
    const escapedPassword = password.replace("'", "''");
    return this.knex.raw(
      `CREATE USER :username: WITH PASSWORD '${escapedPassword}'; GRANT :role: TO :username:;`,
      { username, role },
    );
  }

  async alterDatabaseUserUsername(username: string, newUsername: string) {
    return this.knex.raw(`ALTER USER :username: RENAME TO :newUsername:;`, {
      username,
      newUsername,
    });
  }

  async alterDatabaseUserPassword(username: string, newPassword: string) {
    const escapedNewPassword = newPassword.replace("'", "''");
    return this.knex.raw(
      `ALTER USER :username: WITH PASSWORD '${escapedNewPassword}';`,
      { username },
    );
  }

  async removeDatabaseUser(username: string) {
    return this.knex.raw(`DROP USER :username:;`, { username });
  }

  async _renameCascade(
    options: {
      databaseSchema: DB_SCHEMA;
      tableName: string;
      oldName: string;
      newName: string;
    },
    knexTransacion?: Knex.Transaction,
  ) {
    return this.knex.transaction(async (_trxKnex) => {
      const trxKnex = knexTransacion || _trxKnex;
      const { databaseSchema, tableName, oldName, newName } = options;
      // rename indexes
      const indexes = await trxKnex.raw(
        `SELECT indexname FROM pg_indexes WHERE schemaname = '${databaseSchema}' AND tablename = '${tableName}'`,
      );
      for (const index of indexes.rows) {
        const newIndexName = index.indexname.replace(
          `${oldName}_`,
          `${newName}_`,
        );
        if (index.indexname === newIndexName) continue;
        await trxKnex.raw(
          `ALTER INDEX "${databaseSchema}"."${index.indexname}" RENAME TO "${newIndexName}"`,
        );
      }
      // rename constraints
      const constraints = await trxKnex.raw(
        `SELECT conname FROM pg_constraint WHERE conrelid = '${databaseSchema}.${tableName}'::regclass`,
      );
      for (const constraint of constraints.rows) {
        const newConstraintName = constraint.conname.replace(
          `${oldName}_`,
          `${newName}_`,
        );
        if (constraint.conname === newConstraintName) continue;
        await trxKnex.raw(
          `ALTER TABLE "${databaseSchema}"."${tableName}" RENAME CONSTRAINT "${constraint.conname}" TO "${newConstraintName}"`,
        );
      }
      // rename sequences
      const sequences = await trxKnex.raw(
        `SELECT relname FROM pg_class WHERE relkind = 'S' AND relname LIKE '${oldName}%'`,
      );
      for (const sequence of sequences.rows) {
        const newSequenceName = sequence.relname.replace(
          `${oldName}_`,
          `${newName}_`,
        );
        if (sequence.relname === newSequenceName) continue;
        await trxKnex.raw(
          `ALTER SEQUENCE "${databaseSchema}"."${sequence.relname}" RENAME TO "${newSequenceName}"`,
        );
        await trxKnex.raw(
          `ALTER SEQUENCE "${databaseSchema}"."${newSequenceName}" OWNED BY "${databaseSchema}"."${tableName}"."${sequence.relname
            .replace(`${oldName}_`, '')
            .replace('_seq', '')}"`,
        );
      }
      // rename triggers
      const triggers = await trxKnex.raw(
        `SELECT tgname FROM pg_trigger WHERE tgrelid = '${databaseSchema}.${tableName}'::regclass AND tgname LIKE '${oldName}%'`,
      );
      for (const trigger of triggers.rows) {
        const newTriggerName = trigger.tgname.replace(oldName, newName);
        if (trigger.tgname === newTriggerName) continue;
        await trxKnex.raw(
          `ALTER TRIGGER "${trigger.tgname}" ON "${databaseSchema}"."${tableName}" RENAME TO "${newTriggerName}"`,
        );
      }
      // handle foreign key constraints in other tables
      const foreignKeys = await trxKnex.raw(`
        SELECT conname, confrelid::regclass::text AS referencing_table, nspname AS schema_name
        FROM pg_constraint
          JOIN pg_class ON pg_constraint.conrelid = pg_class.oid
          JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid
        WHERE conrelid != '${databaseSchema}.${tableName}'::regclass AND confrelid = '${databaseSchema}.${tableName}'::regclass
      `);
      for (const foreignKey of foreignKeys.rows) {
        const newForeignKeyName = foreignKey.conname.replace(
          `${oldName}_`,
          `${newName}_`,
        );
        if (foreignKey.conname === newForeignKeyName) continue;
        await trxKnex.raw(
          `ALTER TABLE "${foreignKey.schema_name}"."${foreignKey.referencing_table}" RENAME CONSTRAINT "${foreignKey.conname}" TO "${newForeignKeyName}"`,
        );
      }
    });
  }
}
