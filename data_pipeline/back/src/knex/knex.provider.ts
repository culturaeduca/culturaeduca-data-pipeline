import { ConfigService } from '@nestjs/config';
import { Provider } from '@nestjs/common';
import knex from 'knex';
import type { Knex } from 'knex';

export const KnexConnection = 'KnexConnection';

export const KnexProvider: Provider = {
  provide: KnexConnection,
  useFactory: async (configService: ConfigService): Promise<Knex> =>
    knex(configService.get<Knex.Config>('knex')),
  inject: [ConfigService],
};
