import { Module } from '@nestjs/common';
import { KnexService } from './knex.service';
import { KnexProvider } from './knex.provider';
import { MongoCollectionsModule } from '@/mongo-collections/mongo-collections.module';

@Module({
  imports: [MongoCollectionsModule],
  providers: [KnexService, KnexProvider],
  exports: [KnexService],
})
export class KnexModule {}
