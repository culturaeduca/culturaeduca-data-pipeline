import {
  AttributeIndexTypeEnum,
  AttributeTypeEnum,
} from '@/attributes/attributes.constants';

export const attributesTypesKnexColumnFunction = {
  [AttributeTypeEnum.BOOLEAN]: 'boolean',
  [AttributeTypeEnum.INT]: 'integer',
  [AttributeTypeEnum.LONG]: 'bigInteger',
  [AttributeTypeEnum.FLOAT]: 'float',
  [AttributeTypeEnum.DOUBLE]: 'double',
  [AttributeTypeEnum.BYTES]: 'binary',
  [AttributeTypeEnum.STRING]: 'string',
  [AttributeTypeEnum.GEOMETRY]: 'geometry',
};

export const attributesIndexTypesPostgreSql = {
  [AttributeIndexTypeEnum.BTREE]: 'btree',
  [AttributeIndexTypeEnum.HASH]: 'hash',
  [AttributeIndexTypeEnum.GIST]: 'gist',
  [AttributeIndexTypeEnum.SPGIST]: 'spgist',
  [AttributeIndexTypeEnum.GIN]: 'gin',
  [AttributeIndexTypeEnum.BRIN]: 'brin',
};

export enum DB_SCHEMA {
  DATASETS = 'datasets',
  QUESTIONS = 'questions',
}

export const knexJoinFunctions = {
  LEFT: 'leftJoin',
  RIGHT: 'rightJoin',
  INNER: 'innerJoin',
  OUTER: 'fullOuterJoin',
};

export const knexJoinOperators = {
  EQUAL: '=',
};

export interface PerformQuery {
  tables: PerformQueryTable[];
}

export interface PerformQueryTable {
  schema: DB_SCHEMA;
  tableName: string;
  alias: string;
  joinType: string;
  joinOn: PerformQueryJoinOn[];
  selectColumns: PerformQuerySelectColumn[];
}

export interface PerformQueryJoinOn {
  leftAlias: string;
  leftColumnName: string;
  rightAlias: string;
  rightColumnName: string;
  operator: string;
}

export interface PerformQuerySelectColumn {
  columnName: string;
  alias: string;
  type: string;
}
