import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { BucketItemStat } from 'minio';
import { createReadStream } from 'fs';
import { decode, encodingExists } from 'iconv-lite';
import { join, extname } from 'path';
import { Schema, createSchema, mergeSchemas } from 'genson-js';
import * as csvParser from 'csv-parser';
import * as extract from 'extract-zip';
import * as fs from 'fs/promises';
import * as shapefile from 'shapefile';
import * as chardet from 'chardet';

import {
  Attribute,
  DataIngestion,
  DataIngestionColumn,
  DataIngestionFileType,
  DataIngestionGeoType,
  DataIngestionLog,
  DataIngestionLogStatus,
  Section,
  Version,
} from '@prisma/client';

import { EventsGateway } from '@/events/events.gateway';
import { generateDataIngestionFilePath } from './data-ingestions.helper';
import {
  GetCollectionDataIngestionQueryDto,
  LoadDataDataIngestionDto,
  MapAttributesDataIngestionDto,
  ParseFileDataIngestionDto,
} from './dto';
import { KnexService } from '@/knex/knex.service';
import { MinioService } from '@/minio/minio.service';
import { MongoCollectionsService } from '@/mongo-collections/mongo-collections.service';
import { PrismaService } from '@/prisma.service';
import { AttributeTypeEnum } from '@/attributes/attributes.constants';
import { CreatePresignedUploadDataIngestionDto } from './dto/create-presigned-upload-data-ingestion.dto';
import { expressionParser } from '@/utils/questions-helper.utils';

@Injectable()
export class DataIngestionsService {
  constructor(
    private readonly eventsGateway: EventsGateway,
    private readonly knexService: KnexService,
    private readonly minioService: MinioService,
    private readonly mongoCollectionsService: MongoCollectionsService,
    private readonly prisma: PrismaService,
  ) {}

  async findCurrentLog(id: number): Promise<DataIngestionLog> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: { orderBy: { createdAt: 'desc' }, take: 1 },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // return current log
    return dataIngestion.logs?.[0] || null;
  }

  async findLogs(id: number): Promise<DataIngestionLog[]> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: {
        logs: { orderBy: { createdAt: 'asc' } },
      },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // return logs
    return dataIngestion.logs;
  }

  async findOne(id: number): Promise<DataIngestion> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
      include: { section: true },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    return dataIngestion;
  }

  async getCollection(
    id: number,
    queryDto: GetCollectionDataIngestionQueryDto,
  ): Promise<{ total: number; results: any[] }> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // check if data ingestion has collection
    if (!dataIngestion.collectionName) {
      throw new NotFoundException('Data ingestion does not have a collection!');
    }
    // query collection
    const { page, perPage } = queryDto;
    return this.mongoCollectionsService.findAndCountDocuments(
      dataIngestion.collectionName,
      { page, perPage },
    );
  }

  async getFileInfo(id: number): Promise<BucketItemStat> {
    // find data ingestion
    const dataIngestion = await this.prisma.dataIngestion.findUnique({
      where: { id },
    });
    if (!dataIngestion) {
      throw new NotFoundException('Data ingestion not found!');
    }
    // check if file path exists
    if (!dataIngestion.filePath) {
      return null;
    }
    // return file info (minio object stats)
    return this.minioService.statObject(dataIngestion.filePath);
  }

  async loadData(
    id: number,
    dto: LoadDataDataIngestionDto,
    userId: number,
  ): Promise<void> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // get data ingestion with version and attributes
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
        include: {
          columns: true,
          logs: { orderBy: { createdAt: 'desc' }, take: 1 },
          section: { include: { attributes: true } },
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // check status (log is ordered desc)
      const currentLog = dataIngestion.logs?.[0];
      if (
        currentLog?.status !== DataIngestionLogStatus.FILE_PARSING_SUCCESS &&
        currentLog?.status !== DataIngestionLogStatus.DATA_LOADING_FAILURE
      ) {
        throw new BadRequestException('Invalid data ingestion status!');
      }
      // check if all attributes are mapped
      const attributesMapped = dataIngestion.section.attributes.every(
        (attribute) =>
          !!dataIngestion.columns.find((el) => el.attributeId === attribute.id),
      );
      if (!attributesMapped) {
        throw new BadRequestException('All attributes must be mapped!');
      }
      // create new log
      const newLog = await trxPrisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.DATA_LOADING_STARTED,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      const batchSize = dto.batchSize;
      this._loadDataHelper(dataIngestion, batchSize, userId);
    });
  }

  async mapAttributes(
    id: number,
    dto: MapAttributesDataIngestionDto,
  ): Promise<void> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // update geo info in data ingestion
      await trxPrisma.dataIngestion.update({
        where: { id: dataIngestion.id },
        data: {
          geoType: dto.geoType || null,
          geoPropertyNameLat:
            dto.geoType === DataIngestionGeoType.LAT_LNG
              ? dto.geoPropertyNameLat
              : null,
          geoPropertyNameLng:
            dto.geoType === DataIngestionGeoType.LAT_LNG
              ? dto.geoPropertyNameLng
              : null,
          geoPropertyNameWkt:
            dto.geoType === DataIngestionGeoType.WKT
              ? dto.geoPropertyNameWkt
              : null,
        },
      });
      // reset data ingestion columns
      await trxPrisma.dataIngestionColumn.deleteMany({
        where: { id: dataIngestion.id },
      });
      const dataColumns = (dto.columns || []).map((item) => {
        const propertyName = item?.propertyName || null;
        const expression = !propertyName
          ? expressionParser(item?.expression || '') || null
          : null;
        return {
          dataIngestionId: dataIngestion.id,
          attributeId: item.attributeId,
          propertyName,
          expression,
        };
      });
      await trxPrisma.dataIngestionColumn.createMany({
        data: dataColumns,
      });
    });
  }

  async parseFile(
    id: number,
    dto: ParseFileDataIngestionDto,
    userId: number,
  ): Promise<void> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
        include: {
          logs: { orderBy: { createdAt: 'desc' }, take: 1 },
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // check status (log is ordered desc)
      const currentLog = dataIngestion.logs?.[0];
      if (
        currentLog?.status !== DataIngestionLogStatus.FILE_UPLOAD_SUCCESS &&
        currentLog?.status !== DataIngestionLogStatus.FILE_PARSING_FAILURE
      ) {
        throw new BadRequestException('Invalid data ingestion status!');
      }
      // check file path
      if (!dataIngestion.filePath) {
        throw new NotFoundException('No file uploaded!');
      }
      // create new log
      const newLog = await trxPrisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_PARSING_STARTED,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      // choose correct function
      const batchSize = dto.batchSize;
      switch (dataIngestion.fileType) {
        case DataIngestionFileType.CSV:
          this._parseFileHelperCsv(dataIngestion, batchSize, userId);
          return;
        case DataIngestionFileType.SHAPEFILE:
          this._parseFileHelperShapefile(dataIngestion, batchSize, userId);
          return;
        case DataIngestionFileType.GEOJSON:
        case DataIngestionFileType.JSON:
        default:
          const errorLog = await trxPrisma.dataIngestionLog.create({
            data: {
              dataIngestionId: dataIngestion.id,
              status: DataIngestionLogStatus.FILE_PARSING_FAILURE,
              message: 'Not implemented yet',
              createdBy: userId,
              updatedBy: userId,
            },
          });
          this.eventsGateway.server.emit('dataIngestionLog.created', errorLog);
          throw new NotImplementedException();
      }
    });
  }

  async remove(id: number): Promise<DataIngestion> {
    return this.prisma.$transaction(async (trxPrisma) => {
      const dataIngestion = await trxPrisma.dataIngestion.delete({
        where: { id },
        include: { section: true },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // delete data in section table
      const tableName = dataIngestion.section.tableName;
      await this.knexService.deleteDatasetsTableRowsFromDataIngestion(
        tableName,
        dataIngestion.id,
      );
      // drop mongo collection
      await this.mongoCollectionsService.dropCollection(
        dataIngestion.collectionName,
      );
      return dataIngestion;
    });
  }

  async removeCurrentLog(id: number): Promise<DataIngestionLog> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
        include: {
          logs: { orderBy: { createdAt: 'desc' }, take: 1 },
          section: true,
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // find current log
      const currentLog = dataIngestion.logs?.[0] || null;
      if (!currentLog) {
        throw new NotFoundException('Current data ingestion log not found!');
      }
      // perform extra actions depending on current log status
      switch (currentLog.status) {
        case DataIngestionLogStatus.CREATED:
          // can't remove CREATED logs
          throw new BadRequestException(
            'Current data ingestion log cannot be removed!',
          );
        case DataIngestionLogStatus.FILE_UPLOAD_SUCCESS:
          // remove file from minio if it exists
          await this.minioService.removeObject(dataIngestion.filePath);
          break;
        case DataIngestionLogStatus.FILE_PARSING_SUCCESS:
          // recreate collection
          await this.mongoCollectionsService.createCollection(
            dataIngestion.collectionName,
          );
          break;
        case DataIngestionLogStatus.DATA_LOADING_SUCCESS:
          // delete data in section table
          await this.knexService.deleteDatasetsTableRowsFromDataIngestion(
            dataIngestion.section.tableName,
            dataIngestion.id,
          );
          break;
        default:
        case DataIngestionLogStatus.FILE_UPLOAD_STARTED:
        case DataIngestionLogStatus.FILE_UPLOAD_FAILURE:
        case DataIngestionLogStatus.FILE_PARSING_STARTED:
        case DataIngestionLogStatus.FILE_PARSING_FAILURE:
        case DataIngestionLogStatus.DATA_LOADING_STARTED:
        case DataIngestionLogStatus.DATA_LOADING_FAILURE:
          break;
      }
      // remove log
      const removedLog = await trxPrisma.dataIngestionLog.delete({
        where: { id: currentLog.id },
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.removed', removedLog);

      return removedLog;
    });
  }

  async createPresignedUploadUrl(
    id: number,
    dto: CreatePresignedUploadDataIngestionDto,
    userId: number,
  ) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
        include: {
          logs: { orderBy: { createdAt: 'desc' }, take: 1 },
          section: { include: { version: true } },
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // check status (log is ordered desc)
      const currentLog = dataIngestion.logs?.[0];
      if (
        currentLog?.status !== DataIngestionLogStatus.CREATED &&
        currentLog?.status !== DataIngestionLogStatus.FILE_UPLOAD_FAILURE
      ) {
        throw new BadRequestException('Invalid data ingestion status!');
      }
      // generate object name and get presigned url to upload directly on minio
      const objectName = generateDataIngestionFilePath(
        dataIngestion.section.version.datasetId,
        dataIngestion.section.versionId,
        dataIngestion.sectionId,
        dataIngestion.id,
        dto.fileOriginalName?.trim(),
      );
      const presignedUrl = await this.minioService.getPresignUploadUrl(
        objectName,
      );
      // update data ingestion (we are assuming the frontend will upload the file after this request)
      // there will be a function to check if the file was uploaded and update the log status
      await trxPrisma.dataIngestion.update({
        where: { id },
        data: { filePath: objectName },
      });
      // create log
      const newLog = await trxPrisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_UPLOAD_STARTED,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      return presignedUrl;
    });
  }

  async checkUploadedFile(id: number, userId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
        include: {
          logs: { orderBy: { createdAt: 'desc' }, take: 1 },
          section: { include: { version: true } },
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // check status (log is ordered desc)
      const currentLog = dataIngestion.logs?.[0];
      if (currentLog?.status !== DataIngestionLogStatus.FILE_UPLOAD_STARTED) {
        throw new BadRequestException('Invalid data ingestion status!');
      }
      try {
        const fileInfo = await this.minioService.statObject(
          dataIngestion.filePath,
        );
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { fileInfo: fileInfo as any },
        });
        // create log
        const newLog = await trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_UPLOAD_SUCCESS,
            createdBy: userId,
            updatedBy: userId,
          },
        });
        this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      } catch (err) {
        const newLog = await this.prisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_UPLOAD_FAILURE,
            message: err.message,
            createdBy: userId,
            updatedBy: userId,
          },
        });
        this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      }
    });
  }

  async uploadFile(
    id: number,
    file: Express.Multer.File,
    userId: number,
  ): Promise<void> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.findUnique({
        where: { id },
        include: {
          logs: { orderBy: { createdAt: 'desc' }, take: 1 },
          section: {
            include: { version: true },
          },
        },
      });
      if (!dataIngestion) {
        throw new NotFoundException('Data ingestion not found!');
      }
      // check status (log is ordered desc)
      const currentLog = dataIngestion.logs?.[0];
      if (
        currentLog?.status !== DataIngestionLogStatus.CREATED &&
        currentLog?.status !== DataIngestionLogStatus.FILE_UPLOAD_FAILURE
      ) {
        throw new BadRequestException('Invalid data ingestion status!');
      }
      // create log
      const newLog = await trxPrisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_UPLOAD_STARTED,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
      // upload file (do not await to end the request asap)
      this._uploadFileHelper(dataIngestion, file, userId);
    });
  }

  private async _loadDataHelper(
    dataIngestion: DataIngestion & {
      columns: DataIngestionColumn[];
      section: Section & { attributes: Attribute[] };
    },
    batchSize: number,
    userId: number,
  ): Promise<void> {
    // mappers
    const mappers = {
      geoType: dataIngestion.geoType,
      geoPropertyNameLat: dataIngestion.geoPropertyNameLat,
      geoPropertyNameLng: dataIngestion.geoPropertyNameLng,
      geoPropertyNameWkt: dataIngestion.geoPropertyNameWkt,
      properties: [] as {
        attributeName: string;
        attributeType: AttributeTypeEnum;
        property: string | null;
        expression: string | null;
      }[],
    };
    dataIngestion.section.attributes.forEach((attribute) => {
      // columns
      dataIngestion.columns
        .filter((column) => column.attributeId === attribute.id)
        .forEach((column) => {
          mappers.properties.push({
            attributeName: attribute.name,
            attributeType: attribute.type as AttributeTypeEnum,
            property: column.propertyName,
            expression: column.expression,
          });
        });
    });
    // load data
    const tableName = dataIngestion.section.tableName;
    const collectionName = dataIngestion.collectionName;
    try {
      // insert data from collection
      await this.knexService.insertDataDatasetsTableFromMongoCollection(
        dataIngestion.id,
        tableName,
        collectionName,
        batchSize,
        mappers,
      );
      const count = await this.knexService.countDatasetsTableRows(tableName);
      const newLog = await this.prisma.$transaction(async (trxPrisma) => {
        // update table rows count
        await trxPrisma.section.update({
          where: { id: dataIngestion.sectionId },
          data: { tableRowsCount: count },
        });
        // create new log
        return trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.DATA_LOADING_SUCCESS,
            createdBy: userId,
            updatedBy: userId,
          },
        });
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    } catch (err) {
      // update status
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.DATA_LOADING_FAILURE,
          message: err.message,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }

  private async _parseFileHelperCsv(
    dataIngestion: DataIngestion,
    batchSize: number,
    userId: number,
  ): Promise<void> {
    // download file to tmp
    const fileTmpPath = await this.minioService.fGetObject(
      dataIngestion.filePath,
    );
    const csvFilePath = join(__dirname, '..', '..', fileTmpPath);
    try {
      let collectionSchema: Schema;

      const collectionName = dataIngestion.collectionName;
      const mongoCollectionsService = this.mongoCollectionsService;
      if (!batchSize || batchSize < 1) throw new BadRequestException();
      // read csv and insert in batches
      let count = 0;
      let currentBatch = [];
      await new Promise<void>((resolve, reject) => {
        const stream = createReadStream(csvFilePath).pipe(csvParser());
        stream
          .on('data', async (data) => {
            stream.pause();
            const resultValue = { properties: data };
            // infer schema and merge with current one (stop doing it after 1000 rows)
            if (count < 1000) {
              collectionSchema = mergeSchemas([
                ...(collectionSchema ? [collectionSchema] : []),
                createSchema(resultValue),
              ]);
            }
            currentBatch.push(resultValue);
            if (currentBatch.length % batchSize === 0) {
              await mongoCollectionsService.insertManyDocuments(
                collectionName,
                currentBatch,
              );
              currentBatch = [];
            }
            count++;
            stream.resume();
          })
          .on('end', resolve)
          .on('error', reject);
      });
      await mongoCollectionsService.insertManyDocuments(
        collectionName,
        currentBatch,
      );

      const newLog = await this.prisma.$transaction(async (trxPrisma) => {
        // update collection schema
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { collectionSchema },
        });
        // create log success
        return trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_PARSING_SUCCESS,
            createdBy: userId,
            updatedBy: userId,
          },
        });
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    } catch (err) {
      // create log failure
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_PARSING_FAILURE,
          message: err.message,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }

  private async _parseFileHelperShapefile(
    dataIngestion: DataIngestion,
    batchSize: number,
    userId: number,
  ): Promise<void> {
    // download file to tmp
    const fileTmpPath = await this.minioService.fGetObject(
      dataIngestion.filePath,
    );

    // extract zip and get shp
    const targetDir = join(__dirname, '..', '..', `${fileTmpPath}_extract`);
    await extract(fileTmpPath, { dir: targetDir });
    const filenames = await fs.readdir(targetDir);
    const shpFiles = filenames.filter(
      (filename) => extname(filename) === '.shp',
    );
    if (shpFiles.length === 0) {
      throw new BadRequestException('Shapefile not found!');
    }

    try {
      let collectionSchema: Schema;
      const collectionName = dataIngestion.collectionName;
      const mongoCollectionsService = this.mongoCollectionsService;
      for await (const shpFile of shpFiles) {
        const shpFilePath = join(targetDir, shpFile);

        let encoding = 'latin1';
        const dbfFilePath = shpFilePath.replace('.shp', '.dbf');
        const cpgFilePath = shpFilePath.replace('.shp', '.cpg');
        if (cpgFilePath) {
          const cpgBuffer = await fs.readFile(cpgFilePath);
          encoding = cpgBuffer.toString().trim();
        } else if (dbfFilePath) {
          const dbfBuffer = await fs.readFile(dbfFilePath);
          encoding = chardet.detect(dbfBuffer);
        }

        let index = 0;
        let batch = [];
        let done = false;
        // read shapefile and insert in batches
        await new Promise<void>((resolve, reject) => {
          shapefile
            .open(shpFilePath)
            .then((source) =>
              source.read().then(async function log(result) {
                if (result.done) {
                  done = true;
                }
                if (!!result.value) {
                  // infer schema and merge with current one
                  collectionSchema = mergeSchemas([
                    ...(collectionSchema ? [collectionSchema] : []),
                    createSchema(result.value),
                  ]);
                  // insert value in the collection
                  for (const key in result.value.properties) {
                    if (typeof result.value.properties[key] === 'string') {
                      result.value.properties[key] = decode(
                        Buffer.from(result.value.properties[key], 'binary'),
                        encoding,
                      );
                    }
                  }
                  batch.push(result.value);
                  index++;
                }
                // insert documents
                if (index === batchSize || done) {
                  if (batch.length > 0) {
                    await mongoCollectionsService.insertManyDocuments(
                      collectionName,
                      batch,
                    );
                  }
                  index = 0;
                  batch = [];
                }
                // resolve
                if (done) {
                  resolve();
                  return;
                }
                // continue reading
                return source.read().then(log);
              }),
            )
            .catch(reject);
        });
      }

      // const count = await this.mongoCollectionsService.countDocuments(
      //   collectionName,
      // );

      const newLog = await this.prisma.$transaction(async (trxPrisma) => {
        // update collection schema
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { collectionSchema },
        });
        // create log success
        return trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_PARSING_SUCCESS,
            createdBy: userId,
            updatedBy: userId,
          },
        });
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    } catch (err) {
      // create log failure
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_PARSING_FAILURE,
          message: err.message,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      // emit event
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }

  private async _uploadFileHelper(
    dataIngestion: DataIngestion & { section: Section & { version: Version } },
    file: Express.Multer.File,
    userId: number,
  ): Promise<void> {
    // upload to MinIo and update data ingestion
    try {
      const objectName = generateDataIngestionFilePath(
        dataIngestion.section.version.datasetId,
        dataIngestion.section.versionId,
        dataIngestion.sectionId,
        dataIngestion.id,
        file.originalname,
      );
      await this.minioService.fPutObject(objectName, file);
      const newLog = await this.prisma.$transaction(async (trxPrisma) => {
        await trxPrisma.dataIngestion.update({
          where: { id: dataIngestion.id },
          data: { filePath: objectName },
        });
        return trxPrisma.dataIngestionLog.create({
          data: {
            dataIngestionId: dataIngestion.id,
            status: DataIngestionLogStatus.FILE_UPLOAD_SUCCESS,
            createdBy: userId,
            updatedBy: userId,
          },
        });
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    } catch (err) {
      const newLog = await this.prisma.dataIngestionLog.create({
        data: {
          dataIngestionId: dataIngestion.id,
          status: DataIngestionLogStatus.FILE_UPLOAD_FAILURE,
          message: err.message,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      this.eventsGateway.server.emit('dataIngestionLog.created', newLog);
    }
  }
}
