import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { DataIngestionsService } from './data-ingestions.service';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  GetCollectionDataIngestionQueryDto,
  LoadDataDataIngestionDto,
  MapAttributesDataIngestionDto,
  ParseFileDataIngestionDto,
} from './dto';
import { User } from '@/user.decorator';
import { CreatePresignedUploadDataIngestionDto } from './dto/create-presigned-upload-data-ingestion.dto';

@Controller('data_ingestions')
export class DataIngestionsController {
  constructor(private readonly dataIngestionsService: DataIngestionsService) {}

  // Get

  @Get(':id/logs/current')
  getCurrentLog(@Param('id', ParseIntPipe) id: number) {
    return this.dataIngestionsService.findCurrentLog(id);
  }

  @Get(':id/logs')
  getLog(@Param('id', ParseIntPipe) id: number) {
    return this.dataIngestionsService.findLogs(id);
  }

  @Get(':id/collection')
  getCollection(
    @Param('id', ParseIntPipe) id: number,
    @Query() query: GetCollectionDataIngestionQueryDto,
  ) {
    return this.dataIngestionsService.getCollection(id, query);
  }

  @Get(':id/file_info')
  getFileInfo(@Param('id', ParseIntPipe) id: number) {
    return this.dataIngestionsService.getFileInfo(id);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.dataIngestionsService.findOne(id);
  }

  // Post
  @Post(':id/presigned_upload_url')
  createPresignedUploadUrl(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreatePresignedUploadDataIngestionDto,
    @User('id') userId: number,
  ) {
    return this.dataIngestionsService.createPresignedUploadUrl(id, dto, userId);
  }

  @Post(':id/check_uploaded_file')
  checkUploadedFile(
    @Param('id', ParseIntPipe) id: number,
    @User('id') userId: number,
  ) {
    return this.dataIngestionsService.checkUploadedFile(id, userId);
  }

  @Post(':id/upload_file')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @Param('id', ParseIntPipe) id: number,
    @UploadedFile() file: Express.Multer.File,
    @User('id') userId: number,
  ) {
    return this.dataIngestionsService.uploadFile(id, file, userId);
  }

  @Post(':id/parse_file')
  parseFile(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: ParseFileDataIngestionDto,
    @User('id') userId: number,
  ) {
    return this.dataIngestionsService.parseFile(id, dto, userId);
  }

  @Post(':id/map_attributes')
  mapAttributes(
    @Param('id', ParseIntPipe) id: number,
    @Body() mapAttributesDataIngestionDto: MapAttributesDataIngestionDto,
  ) {
    return this.dataIngestionsService.mapAttributes(
      id,
      mapAttributesDataIngestionDto,
    );
  }

  @Post(':id/load_data')
  loadData(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: LoadDataDataIngestionDto,
    @User('id') userId: number,
  ) {
    return this.dataIngestionsService.loadData(id, dto, userId);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.dataIngestionsService.remove(id);
  }

  @Delete(':id/logs/current')
  removeCurrentLog(@Param('id', ParseIntPipe) id: number) {
    return this.dataIngestionsService.removeCurrentLog(id);
  }
}
