import { ConfigService } from '@nestjs/config';
import { MinioModule } from '@/minio/minio.module';
import type { MinioModuleConfig } from '@/minio/config.interface';
import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';

import { PrismaService } from '@/prisma.service';
import { DataIngestionsService } from './data-ingestions.service';
import { DataIngestionsController } from './data-ingestions.controller';
import { EventsGateway } from '@/events/events.gateway';
import { KnexModule } from '@/knex/knex.module';
import { MongoCollectionsModule } from '@/mongo-collections/mongo-collections.module';

@Module({
  imports: [
    KnexModule,
    MinioModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        ...configService.get<MinioModuleConfig>('minio.client'),
      }),
      inject: [ConfigService],
    }),
    MongoCollectionsModule,
    MulterModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        ...configService.get<MulterOptions>('multer'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [DataIngestionsController],
  providers: [DataIngestionsService, EventsGateway, PrismaService],
})
export class DataIngestionsModule {}
