export function generateDataIngestionCollectionName(
  dataIngestionId: number,
): string {
  return `data_ingestion_${dataIngestionId}`;
}

export function generateDataIngestionFilePath(
  datasetId: number,
  versionId: number,
  sectionId: number,
  dataIngestionId: number,
  originalFilename: string,
): string {
  return `datasets/${datasetId}/versions/${versionId}/sections/${sectionId}/data_ingestions/${dataIngestionId}/raw/${originalFilename}`;
}
