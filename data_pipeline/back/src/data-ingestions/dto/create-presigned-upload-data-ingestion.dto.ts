import { IsNotEmpty, IsString } from 'class-validator';

export class CreatePresignedUploadDataIngestionDto {
  @IsString()
  @IsNotEmpty()
  fileOriginalName: string;
}
