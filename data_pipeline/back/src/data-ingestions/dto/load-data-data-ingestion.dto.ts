import { IsInt, Min } from 'class-validator';

export class LoadDataDataIngestionDto {
  @IsInt()
  @Min(1)
  batchSize: number;
}
