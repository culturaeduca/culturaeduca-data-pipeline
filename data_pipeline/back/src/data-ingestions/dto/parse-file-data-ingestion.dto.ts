import { IsInt, Min } from 'class-validator';

export class ParseFileDataIngestionDto {
  @IsInt()
  @Min(1)
  batchSize: number;
}
