import { IsEnum } from 'class-validator';
import { DataIngestionFileType } from '@prisma/client';

export class CreateDataIngestionDto {
  @IsEnum(DataIngestionFileType)
  fileType: DataIngestionFileType;
}
