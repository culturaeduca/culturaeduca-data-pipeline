import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsInt,
  IsOptional,
  IsString,
  Min,
  ValidateNested,
} from 'class-validator';
import { DataIngestionGeoType } from '@prisma/client';

class CreateDataIngestionColumnDto {
  @IsInt()
  @Min(1)
  attributeId: number;

  @IsOptional()
  @IsString()
  propertyName: string | null;

  @IsOptional()
  @IsString()
  expression: string | null;
}

export class MapAttributesDataIngestionDto {
  @IsOptional()
  @IsEnum(DataIngestionGeoType)
  geoType: DataIngestionGeoType | null;

  @IsOptional()
  @IsString()
  geoPropertyNameLat: string | null;

  @IsOptional()
  @IsString()
  geoPropertyNameLng: string | null;

  @IsOptional()
  @IsString()
  geoPropertyNameWkt: string | null;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateDataIngestionColumnDto)
  columns: CreateDataIngestionColumnDto[];
}
