import { Module } from '@nestjs/common';
import { AttributesSeriesService } from './attributes-series.service';
import { AttributesSeriesController } from './attributes-series.controller';
import { PrismaService } from '@/prisma.service';

@Module({
  controllers: [AttributesSeriesController],
  providers: [AttributesSeriesService, PrismaService],
})
export class AttributesSeriesModule {}
