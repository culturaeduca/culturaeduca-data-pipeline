import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { AttributesSeriesService } from './attributes-series.service';
import { CreateRelationshipForeignKeyAttributeSeriesDto } from './dto';

@Controller('attributes_series')
export class AttributesSeriesController {
  constructor(
    private readonly attributesSeriesService: AttributesSeriesService,
  ) {}

  // Get

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.attributesSeriesService.findOne(id);
  }

  // Post

  @Post(':id/relationships_foreign_key')
  createRelationship(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateRelationshipForeignKeyAttributeSeriesDto,
  ) {
    return this.attributesSeriesService.createRelationshipForeignKey(id, dto);
  }
}
