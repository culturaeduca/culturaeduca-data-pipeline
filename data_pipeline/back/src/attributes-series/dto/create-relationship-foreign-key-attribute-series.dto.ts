import { AttributeSeriesRelationshipType } from '@prisma/client';
import { IsEnum, IsInt, Min } from 'class-validator';

export class CreateRelationshipForeignKeyAttributeSeriesDto {
  @IsInt()
  @Min(1)
  referenceAttributeSeriesId: number;

  @IsEnum(AttributeSeriesRelationshipType)
  type: AttributeSeriesRelationshipType;
}
