import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '@/prisma.service';
import { AttributeSeriesRelationshipType } from '@prisma/client';
import { CreateRelationshipForeignKeyAttributeSeriesDto } from './dto';

@Injectable()
export class AttributesSeriesService {
  constructor(private readonly prisma: PrismaService) {}

  async createRelationshipForeignKey(
    foreignKeyId: number,
    dto: CreateRelationshipForeignKeyAttributeSeriesDto,
  ) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find foreign key
      const foreignKeyAttributeSeries =
        await trxPrisma.attributeSeries.findUnique({
          where: { id: foreignKeyId },
          include: { attributes: true },
        });
      if (!foreignKeyAttributeSeries) {
        return new NotFoundException('Foreign key attribute series not found!');
      }
      // find reference
      const referenceAttributeSeries =
        await trxPrisma.attributeSeries.findUnique({
          where: { id: dto?.referenceAttributeSeriesId },
          include: { attributes: true },
        });
      if (!foreignKeyAttributeSeries) {
        return new NotFoundException('Reference attribute series not found!');
      }
      // if same dataset, throw error
      if (
        foreignKeyAttributeSeries.datasetId ===
        referenceAttributeSeries.datasetId
      ) {
        throw new BadRequestException(
          "Foreign key attribute series can't reference the same dataset!",
        );
      }
      // check to see if all reference attributes are unique
      const everyUniqueReferenceAttribute =
        referenceAttributeSeries.attributes.every(
          (attribute) => attribute.unique,
        );
      if (!everyUniqueReferenceAttribute) {
        throw new BadRequestException(
          'Reference attribute series has to be composed only of unique attributes!',
        );
      }
      const relationshipType = dto?.type;
      // if type is one to one, check to see if all foreign key attributes are unique
      if (relationshipType === AttributeSeriesRelationshipType.ONE_TO_ONE) {
        const everyUniqueForeignKeyAttribute =
          foreignKeyAttributeSeries.attributes.every(
            (attribute) => attribute.unique,
          );
        if (!everyUniqueForeignKeyAttribute) {
          throw new BadRequestException(
            'Reference attribute series has to be composed only of unique attributes!',
          );
        }
      }
      // create relationship
      return trxPrisma.attributeSeriesRelationship.create({
        data: {
          foreignKeyAttributeSeriesId: foreignKeyAttributeSeries.id,
          referenceAttributeSeriesId: referenceAttributeSeries.id,
          type: relationshipType,
        },
      });
    });
  }

  async findOne(id: number) {
    const attributeSeries = await this.prisma.attributeSeries.findUnique({
      where: { id },
      include: {
        attributes: { include: { section: true } },
        dataset: true,
      },
    });
    if (!attributeSeries) {
      return new NotFoundException('Attribute series not found!');
    }
    return attributeSeries;
  }
}
