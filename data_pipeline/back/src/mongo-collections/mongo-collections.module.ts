import { ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';

import { MongoCollectionsService } from './mongo-collections.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        ...configService.get<MongooseModuleFactoryOptions>('mongoose'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [MongoCollectionsService],
  exports: [MongoCollectionsService],
})
export class MongoCollectionsModule {}
