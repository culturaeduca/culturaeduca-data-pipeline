import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Collection, Model } from 'mongoose';
import * as mongoose from 'mongoose';

@Injectable()
export class MongoCollectionsService {
  private _models: { [k: string]: Model<any> }[];

  constructor(
    @InjectConnection() private mongooseConnection: mongoose.Connection,
  ) {
    this._models = [];
  }

  private _getOrCreateModel(collectionName: string): Model<any> {
    const model = this._models?.[collectionName];
    if (!model) {
      this._models[collectionName] = this.mongooseConnection.model(
        collectionName,
        new mongoose.Schema({ any: {} }),
      );
    }
    return this._models[collectionName];
  }

  private _getOrCreateCollection(collectionName: string): Collection<any> {
    return this.mongooseConnection.collection(collectionName);
  }

  async createCollection(collectionName: string): Promise<void> {
    // drop mongo collection if already exists
    await this.dropCollection(collectionName);
    // create mongo collection
    this.mongooseConnection.createCollection(collectionName);
  }

  async countDocuments(collectionName: string): Promise<number> {
    const model = this._getOrCreateModel(collectionName);
    return model.countDocuments();
  }

  async dropCollection(collectionName: string): Promise<void> {
    // check if collection exists
    const collections = await this.mongooseConnection.db
      .listCollections({ name: collectionName })
      .toArray();
    if (collections.length === 0) return;
    // delete model
    const model = this._models?.[collectionName];
    if (model) {
      await this.mongooseConnection.deleteModel(collectionName);
      delete this._models[collectionName];
    }
    // drop collection
    await this.mongooseConnection.dropCollection(collectionName);
  }

  async findAndCountDocuments(
    collectionName: string,
    options: {
      page: number;
      perPage: number;
    },
  ): Promise<{ total: number; results: any[] }> {
    // set limit and skip
    const page = options?.page && options.page > 0 ? options.page : 1;
    const perPage =
      options?.perPage && options.perPage > 1 ? options.perPage : 10;
    const limit = perPage;
    const skip = perPage * (page - 1);
    // get mongoose model
    const model = this._getOrCreateModel(collectionName);
    // query data
    const results = await model
      .find()
      .select(['type', 'properties', 'geometry.type'])
      .limit(limit)
      .skip(skip)
      .exec();
    // count documents
    const total = await model.countDocuments();

    return { total, results };
  }

  async findDocuments(
    collectionName: string,
    options: { limit: number; skip: number },
  ): Promise<any[]> {
    // get mongoose model
    const model = this._getOrCreateModel(collectionName);
    // query data
    return model
      .find()
      .select(['properties', 'geometry'])
      .limit(options.limit)
      .skip(options.skip)
      .exec();
  }

  async insertOneDocument(collectionName: string, document: any) {
    // get mongoose collection
    const collection = this._getOrCreateCollection(collectionName);
    // insert document
    return collection.insertOne(document);
  }

  async insertManyDocuments(collectionName: string, documents: any[]) {
    // get mongoose collection
    const collection = this._getOrCreateCollection(collectionName);
    // insert document
    return collection.insertMany(documents);
  }
}
