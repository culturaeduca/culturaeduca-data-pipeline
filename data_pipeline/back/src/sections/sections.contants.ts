export enum DefaultSectionTableColumnEnum {
  ID = '_id',
  DATA_INGESTION_ID = '_data_ingestion_id',
  CREATED_AT = '_created_at',
  UPDATED_AT = '_updated_at',
  GEOMETRY = '_geom',
  GEOGRAPHY = '_geog',
}
