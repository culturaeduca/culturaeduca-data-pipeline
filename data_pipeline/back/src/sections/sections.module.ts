import { Module } from '@nestjs/common';

import { AttributesModule } from '@/attributes/attributes.module';
import { KnexModule } from '@/knex/knex.module';
import { MongoCollectionsModule } from '@/mongo-collections/mongo-collections.module';
import { PrismaService } from '@/prisma.service';
import { SectionsController } from './sections.controller';
import { SectionsService } from './sections.service';

@Module({
  imports: [AttributesModule, KnexModule, MongoCollectionsModule],
  controllers: [SectionsController],
  providers: [SectionsService, PrismaService],
})
export class SectionsModule {}
