import { Injectable, NotFoundException } from '@nestjs/common';

import {
  Attribute,
  DataIngestion,
  DataIngestionLogStatus,
  Section,
} from '@prisma/client';

import { AttributesService } from '@/attributes/attributes.service';
import { CreateAttributeDto } from '@/attributes/dto';
import { CreateDataIngestionDto } from '@/data-ingestions/dto';
import { GetDataTableSectionQueryDto, UpdateSectionDto } from './dto';
import { KnexService } from '@/knex/knex.service';
import { MongoCollectionsService } from '@/mongo-collections/mongo-collections.service';
import { PrismaService } from '@/prisma.service';
import { generateDataIngestionCollectionName } from '@/data-ingestions/data-ingestions.helper';
import { DB_SCHEMA } from '@/knex/knex.constants';
import { generateSectionTableName } from './sections.helper';
import { DefaultSectionTableColumnEnum } from './sections.contants';

@Injectable()
export class SectionsService {
  constructor(
    private readonly attributesService: AttributesService,
    private readonly knexService: KnexService,
    private readonly mongoCollectionsService: MongoCollectionsService,
    private readonly prisma: PrismaService,
  ) {}

  async bulkCreateAttributes(
    id: number,
    dtos: CreateAttributeDto[],
    userId: number,
  ) {
    return this.prisma.$transaction(
      async (trxPrisma) => {
        // find section
        const section = await trxPrisma.section.findUnique({
          where: { id },
          include: {
            version: {
              include: { dataset: true },
            },
          },
        });
        if (!section) {
          throw new NotFoundException('Version not found!');
        }
        // new attributeSeries index
        const attributeSeriesMaxIndex =
          await trxPrisma.attributeSeries.findFirst({
            where: { datasetId: section.version.datasetId },
            orderBy: { index: 'desc' },
          });
        const newAttributeSeriesIndex =
          (attributeSeriesMaxIndex?.index || 0) + 1;
        // create attributes
        const attributes = await Promise.all(
          dtos.map(async (dto, index) =>
            this.attributesService.create(
              section.version.datasetId,
              section.id,
              dto,
              userId,
              newAttributeSeriesIndex + index,
              trxPrisma,
            ),
          ),
        );
        // add columns to bases table
        await this.knexService.addColumnsDatasetsTable(
          section.tableName,
          attributes,
        );
        return attributes;
      },
      { timeout: 30000 },
    );
  }

  async createDataIngestion(
    id: number,
    dto: CreateDataIngestionDto,
    userId: number,
  ) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find section
      const section = await trxPrisma.section.findUnique({
        where: { id },
      });
      if (!section) {
        throw new NotFoundException('Version not found!');
      }
      // create data ingestion
      const dataIngestion = await trxPrisma.dataIngestion.create({
        data: {
          sectionId: section.id,
          filePath: null,
          fileType: dto?.fileType,
          collectionName: null,
          collectionSchema: null,
          logs: {
            create: {
              status: DataIngestionLogStatus.CREATED,
              userCreatedBy: { connect: { id: userId } },
              userUpdatedBy: { connect: { id: userId } },
            },
          },
        },
      });
      // create mongo collection
      const collectionName = generateDataIngestionCollectionName(
        dataIngestion.id,
      );
      await this.mongoCollectionsService.createCollection(collectionName);
      // update and return data ingestion
      return trxPrisma.dataIngestion.update({
        where: { id: dataIngestion.id },
        data: { collectionName },
      });
    });
  }

  async findAttributes(id: number): Promise<Attribute[]> {
    const section = await this.prisma.section.findUnique({
      where: { id },
      include: {
        attributes: {
          include: {
            series: {
              include: {
                relationshipsForeignKey: true,
                relationshipsReference: true,
              },
            },
          },
          orderBy: { series: { index: 'asc' } },
        },
      },
    });
    if (!section) {
      throw new NotFoundException('Version not found!');
    }
    return section.attributes;
  }

  async findDataIngestions(id: number): Promise<DataIngestion[]> {
    const section = await this.prisma.section.findUnique({
      where: { id },
      include: {
        dataIngestions: {
          orderBy: { id: 'asc' },
        },
      },
    });
    if (!section) {
      throw new NotFoundException('Version not found!');
    }
    return section.dataIngestions;
  }

  async findOne(id: number) {
    const section = await this.prisma.section.findUnique({
      where: { id },
    });
    if (!section) {
      throw new NotFoundException('Section not found!');
    }
    return section;
  }

  async getDataGeojson(id: number, dataId: number) {
    // find section
    const section = await this.prisma.section.findUnique({
      where: { id },
    });
    if (!section) {
      throw new NotFoundException('Section not found!');
    }
    // return geojson
    return this.knexService.findGeojsonValue(
      DB_SCHEMA.DATASETS,
      section.tableName,
      DefaultSectionTableColumnEnum.GEOMETRY,
      DefaultSectionTableColumnEnum.ID,
      dataId,
    );
  }

  async getDataTable(
    id: number,
    queryDto: GetDataTableSectionQueryDto,
  ): Promise<{ total: number; results: any[] }> {
    // find section
    const section = await this.prisma.section.findUnique({
      where: { id },
      include: {
        attributes: {
          orderBy: { series: { index: 'asc' } },
        },
      },
    });
    if (!section) {
      throw new NotFoundException('Section not found!');
    }
    // query data
    const { page, perPage } = queryDto;
    // return results
    return this.knexService.findAndCountDatasetsTable(
      section.tableName,
      section.attributes,
      { page, perPage },
    );
  }

  async setDefault(id: number, userId: number): Promise<Section> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find section
      let section = await trxPrisma.section.findUnique({
        where: { id },
      });
      if (!section) {
        throw new NotFoundException('Section not found!');
      }
      // set default to true
      section = await trxPrisma.section.update({
        where: { id },
        data: { default: true },
      });
      // update the other sections to default false
      await trxPrisma.section.updateMany({
        where: {
          id: { not: section.id },
          versionId: section.versionId,
          default: false,
        },
        data: { default: false, updatedBy: userId },
      });
      return section;
    });
  }

  async update(
    id: number,
    dto: UpdateSectionDto,
    userId: number,
  ): Promise<Section> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find section
      const section = await trxPrisma.section.findUnique({
        where: { id },
        include: {
          version: {
            include: {
              dataset: true,
            },
          },
        },
      });
      if (!section) {
        throw new NotFoundException('Section not found!');
      }
      // update section
      const newUri = dto.uri?.trim()?.toLowerCase();
      await this.prisma.section.update({
        where: { id },
        data: {
          ...(newUri !== undefined ? { uri: newUri || null } : {}),
          ...(dto?.title !== undefined
            ? { title: dto.title?.trim() || null }
            : {}),
          ...(dto?.description !== undefined
            ? { description: dto.description?.trim() || null }
            : {}),
          updatedBy: userId,
        },
      });
      // update table name
      if (!!newUri && newUri !== section.uri) {
        const newTableName = generateSectionTableName(
          section.version.dataset.uri,
          section.version.uri,
          section.uri,
        );
        const itemsToUpdate = [
          {
            databaseSchema: DB_SCHEMA.DATASETS,
            oldTableName: section.tableName,
            newTableName: newTableName,
          },
        ];
        await trxPrisma.section.update({
          data: { tableName: newTableName },
          where: { id: section.id },
        });
        await this.knexService.renameTables(itemsToUpdate);
      }
      // return updated section
      return trxPrisma.section.findUnique({
        where: { id: section.id },
      });
    });
  }

  async remove(id: number): Promise<Section> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find section
      const section = await trxPrisma.section.findUnique({
        where: { id },
        include: { version: true },
      });
      if (!section) {
        throw new NotFoundException('Section not found!');
      }
      // delete section
      await trxPrisma.section.delete({
        where: { id },
      });
      // remove attributes series without attributes
      const attributesSeries = await trxPrisma.attributeSeries.findMany({
        where: {
          datasetId: section.version.datasetId,
        },
        select: {
          id: true,
          _count: { select: { attributes: true } },
        },
      });
      const emptyAttributesSeriesIds = attributesSeries
        .filter((el) => el._count.attributes === 0)
        .map((el) => el.id);
      await trxPrisma.attributeSeries.deleteMany({
        where: { id: { in: emptyAttributesSeriesIds } },
      });
      // drop section table
      await this.knexService.dropDatasetsTable(section.tableName);
      return section;
    });
  }
}
