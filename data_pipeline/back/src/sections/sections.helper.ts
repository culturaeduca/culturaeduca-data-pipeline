export function generateSectionTableName(
  datasetUri: string,
  versionUri: string,
  sectionUri?: string | null,
): string {
  return `${datasetUri}_${versionUri}${sectionUri ? `_${sectionUri}` : ''}`;
}

export function generateSectionTableDescription(
  datasetTitle: string,
  versionTitle: string,
  sectionTitle?: string | null,
): string {
  return `${datasetTitle} - ${versionTitle}${
    sectionTitle ? ` - ${sectionTitle}` : ''
  }`;
}
