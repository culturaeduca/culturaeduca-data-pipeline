import { IsOptional, IsString } from 'class-validator';

export class CreateSectionDto {
  @IsOptional()
  @IsString()
  uri: string | null;

  @IsOptional()
  @IsString()
  title: string | null;

  @IsOptional()
  @IsString()
  description: string | null;
}
