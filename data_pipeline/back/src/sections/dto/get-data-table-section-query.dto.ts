import { Transform } from 'class-transformer';
import { IsInt, Min } from 'class-validator';

export class GetDataTableSectionQueryDto {
  @Transform(({ value }) => Number(value))
  @IsInt()
  @Min(1)
  page: number;

  @Transform(({ value }) => Number(value))
  @IsInt()
  @Min(1)
  perPage: number;
}
