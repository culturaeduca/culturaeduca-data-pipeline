import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { CreateAttributeDto } from '@/attributes/dto';
import { CreateDataIngestionDto } from '@/data-ingestions/dto';
import { GetDataTableSectionQueryDto, UpdateSectionDto } from './dto';
import { SectionsService } from './sections.service';
import { User } from '@/user.decorator';
import { Public } from '@/public.decorator';

@Controller('sections')
export class SectionsController {
  constructor(private readonly sectionsService: SectionsService) {}

  // Get

  @Public()
  @Get(':id/attributes')
  findAttributes(@Param('id', ParseIntPipe) id: number) {
    return this.sectionsService.findAttributes(id);
  }

  @Public()
  @Get(':id/data_ingestions')
  findDataIngestions(@Param('id', ParseIntPipe) id: number) {
    return this.sectionsService.findDataIngestions(id);
  }

  @Public()
  @Get(':id/data_geojson/:dataId')
  getBasesGeojson(
    @Param('id', ParseIntPipe) id: number,
    @Param('dataId', ParseIntPipe) dataId: number,
  ) {
    return this.sectionsService.getDataGeojson(id, dataId);
  }

  @Public()
  @Get(':id/data_table')
  getDataTable(
    @Param('id', ParseIntPipe) id: number,
    @Query() queryDto: GetDataTableSectionQueryDto,
  ) {
    return this.sectionsService.getDataTable(id, queryDto);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.sectionsService.findOne(id);
  }

  // Post

  @Post(':id/attributes_bulk')
  bulkCreateAttribute(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateAttributeDto[],
    @User('id') userId: number,
  ) {
    return this.sectionsService.bulkCreateAttributes(id, dto, userId);
  }

  @Post(':id/attributes')
  async createAttribute(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateAttributeDto,
    @User('id') userId: number,
  ) {
    const attributes = await this.sectionsService.bulkCreateAttributes(
      id,
      [dto],
      userId,
    );
    return attributes[0];
  }

  @Post(':id/data_ingestions')
  createDataIngestion(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateDataIngestionDto,
    @User('id') userId: number,
  ) {
    return this.sectionsService.createDataIngestion(id, dto, userId);
  }

  // Patch

  @Patch(':id/set_default')
  setDefault(
    @Param('id', ParseIntPipe) id: number,
    @User('id') userId: number,
  ) {
    return this.sectionsService.setDefault(id, userId);
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateSectionDto,
    @User('id') userId: number,
  ) {
    return this.sectionsService.update(id, dto, userId);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.sectionsService.remove(id);
  }
}
