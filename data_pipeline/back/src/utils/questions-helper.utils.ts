function columnFunction(...args) {
  if (args.length !== 1) {
    throw new Error('Column function requires exactly one argument');
  }
  return `"column_key_${args[0]}"`;
}

function addFunction(...args) {
  if (args.length < 2) {
    throw new Error('Add function requires at least two arguments');
  }
  return `(${args.join(' + ')})`;
}

function subtractFunction(...args) {
  if (args.length < 2) {
    throw new Error('Subtract function requires at least two arguments');
  }
  return `(${args.join(' - ')})`;
}

function multiplyFunction(...args) {
  if (args.length < 2) {
    throw new Error('Multiply function requires at least two arguments');
  }
  return `(${args.join(' * ')})`;
}

function divideFunction(...args) {
  if (args.length < 2) {
    throw new Error('Divide function requires at least two arguments');
  }
  return `(${args.join(' / ')})`;
}

function concatFunction(...args) {
  if (args.length < 2) {
    throw new Error('Concat function requires at least two arguments');
  }
  return `concat(${args.join(', ')})`;
}

function lowerFunction(...args) {
  if (args.length !== 1) {
    throw new Error('Lower function requires exactly one argument');
  }
  return `lower(${args.join(', ')})`;
}

function upperFunction(...args) {
  if (args.length !== 1) {
    throw new Error('Upper function requires exactly one argument');
  }
  return `upper(${args.join(', ')})`;
}

const availableFunctions = {
  // get column
  column: columnFunction,
  // math
  add: addFunction,
  subtract: subtractFunction,
  multiply: multiplyFunction,
  divide: divideFunction,
  // string
  concat: concatFunction,
  lower: lowerFunction,
  upper: upperFunction,
};

function validateFunctionName(functionName: string) {
  return Object.keys(availableFunctions).includes(functionName);
}

// expression is always a string in the format 'function(arguments)'
// for now ignore everything after the last parenthesis
export function expressionParser(expression: string) {
  const trimmedExp = expression?.trim() || '';
  let insideString = false;
  let firstOpenParenthesis = -1;
  let lastCloseParenthesis = -1;
  let insideParenthesis = 0;
  const commas = [];
  for (let i = 0; i < trimmedExp.length; i++) {
    // inside string
    if (trimmedExp[i] === "'" && (i === 0 || trimmedExp[i - 1] !== '\\')) {
      insideString = !insideString;
    }
    if (insideString) continue;

    // parenthesis
    if (trimmedExp[i] === '(') {
      insideParenthesis++;
      if (firstOpenParenthesis < 0) firstOpenParenthesis = i;
    }
    if (trimmedExp[i] === ')') {
      insideParenthesis--;
      lastCloseParenthesis = i;
    }
    if (insideParenthesis < 0) throw new Error('Invalid expression');
    // arguments' commas
    if (trimmedExp[i] === ',') {
      if (insideParenthesis <= 0) throw new Error('Invalid expression');
      if (insideParenthesis === 1) commas.push(i);
    }
  }
  // check if expression is valid
  if (insideString || insideParenthesis !== 0) {
    throw new Error('Invalid expression');
  }
  // no parenthesis
  if (firstOpenParenthesis < 0 && lastCloseParenthesis < 0) {
    return trimmedExp;
  }

  // get and check function name
  const functionName = trimmedExp
    .substring(0, firstOpenParenthesis)
    .toLowerCase();
  if (!validateFunctionName(functionName)) {
    throw new Error('Invalid function name');
  }

  // get arguments
  const args = [];
  for (let k = 0; k <= commas.length; k++) {
    const i = k === 0 ? firstOpenParenthesis + 1 : commas[k - 1] + 1;
    const j = k === commas.length ? lastCloseParenthesis : commas[k];
    args.push(trimmedExp.substring(i, j));
  }

  return availableFunctions[functionName](
    ...args.map((arg) => expressionParser(arg)),
  );
}
