export async function* asyncIterable(n: number) {
  for (let i = 0; i < n; i++) {
    yield i;
  }
}
