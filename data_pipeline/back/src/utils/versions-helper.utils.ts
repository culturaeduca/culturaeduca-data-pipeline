import { CreateVersionDto, UpdateVersionDto } from '@/versions/dto';

export function validateStartEndDates(
  dto: CreateVersionDto | UpdateVersionDto,
) {
  const startDateYear = Number(dto.startDateYear) || 0;
  const startDateMonth = Number(dto.startDateMonth) || 0;
  const startDateDay = Number(dto.startDateDay) || 0;
  let endDateYear = Number(dto.endDateYear) || 0;
  let endDateMonth = Number(dto.endDateMonth) || 0;
  let endDateDay = Number(dto.endDateDay) || 0;
  let interval: 'year' | 'month' | 'day' = 'year';
  // start year
  if (!startDateYear) {
    return 'Invalid start year!';
  } else if (startDateYear <= 0) {
    return 'Invalid start year!';
  } else {
  }
  // start month
  if (!startDateMonth) {
    if (!!endDateMonth) {
      return 'Start month is required if end month is defined';
    }
  } else if (startDateMonth < 1 || startDateMonth > 12) {
    return 'Invalid start month!';
  } else {
    interval = 'month';
  }
  // start day
  if (!startDateDay) {
    if (!!endDateDay) {
      return 'Start day is required if end day is present';
    }
  } else if (startDateDay < 1 || startDateDay > 31) {
    return 'Invalid start day!';
  } else if (!startDateMonth) {
    return 'Start month is required if start day is defined';
  } else {
    interval = 'day';
  }
  // end year
  if (!endDateYear) {
    endDateYear = startDateYear;
  } else if (endDateYear < 0) {
    return 'Invalid end year!';
  }
  // end month
  else if (!endDateMonth) {
    endDateMonth = startDateMonth;
  } else if (endDateMonth < 1 || endDateMonth > 12) {
    return 'Invalid end month!';
  } else if (!startDateMonth) {
    return 'Start month is required if end month is defined!';
  } else if (!endDateYear) {
    return 'End year is required if end month is defined!';
  }
  // end day
  if (!endDateDay) {
    endDateDay = startDateDay;
  } else if (endDateDay < 1 || endDateDay > 31) {
    return 'Invalid start day!';
  } else if (!startDateDay) {
    return 'Start day is required if end day is defined!';
  } else if (!endDateMonth) {
    return 'End month is required if end day is defined!';
  }
  // create dates
  const startDate = new Date(
    startDateYear,
    startDateMonth ? startDateMonth - 1 : 0,
    startDateDay ? startDateDay - 1 : 1,
  );
  const endDate = new Date(
    endDateYear,
    endDateMonth ? endDateMonth - 1 : 0,
    endDateDay ? endDateDay - 1 : 1,
  );
  switch (interval) {
    case 'year':
      if (endDateYear === startDateYear)
        endDate.setFullYear(startDate.getFullYear() + 1);
      break;
    case 'month':
      if (endDateMonth === startDateMonth)
        endDate.setMonth(startDate.getMonth() + 1);
      break;
    case 'day':
      if (endDateDay === startDateDay) endDate.setDate(startDate.getDate() + 1);
      break;
    default:
      break;
  }
  // validate dates
  if (Number.isNaN(startDate?.getTime)) {
    return 'Invalid start date!';
  } else if (Number.isNaN(endDate?.getTime)) {
    return 'Invalid end date!';
  }
  if (endDate.getTime() < startDate.getTime()) {
    return 'End date has to be after the start date!';
  }
  return '';
}
