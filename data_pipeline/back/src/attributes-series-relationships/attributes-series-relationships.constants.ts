export enum AttributeSeriesRelationshipTypeEnum {
  ONE_TO_ONE = 'ONE_TO_ONE',
  MANY_TO_ONE = 'MANY_TO_ONE',
}
