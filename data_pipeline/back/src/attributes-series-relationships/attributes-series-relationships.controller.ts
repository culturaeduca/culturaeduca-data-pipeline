import { Controller, Delete, Param, ParseIntPipe } from '@nestjs/common';
import { AttributesSeriesRelationshipsService } from './attributes-series-relationships.service';

@Controller('attributes_series_relationships')
export class AttributesSeriesRelationshipsController {
  constructor(
    private readonly attributesSeriesRelationshipsService: AttributesSeriesRelationshipsService,
  ) {}

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.attributesSeriesRelationshipsService.remove(id);
  }
}
