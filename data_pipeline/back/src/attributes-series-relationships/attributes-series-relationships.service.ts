import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '@/prisma.service';

@Injectable()
export class AttributesSeriesRelationshipsService {
  constructor(private readonly prisma: PrismaService) {}

  async remove(id: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const relationship =
        await trxPrisma.attributeSeriesRelationship.findUnique({
          where: { id },
        });
      if (!relationship) {
        throw new NotFoundException('Attribute series relationship not found!');
      }
      await trxPrisma.attributeSeriesRelationship.delete({
        where: { id },
      });
      return relationship;
    });
  }
}
