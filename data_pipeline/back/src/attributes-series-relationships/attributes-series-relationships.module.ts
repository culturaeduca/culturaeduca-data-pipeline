import { Module } from '@nestjs/common';
import { AttributesSeriesRelationshipsService } from './attributes-series-relationships.service';
import { AttributesSeriesRelationshipsController } from './attributes-series-relationships.controller';
import { PrismaService } from '@/prisma.service';

@Module({
  controllers: [AttributesSeriesRelationshipsController],
  providers: [AttributesSeriesRelationshipsService, PrismaService],
})
export class AttributesSeriesRelationshipsModule {}
