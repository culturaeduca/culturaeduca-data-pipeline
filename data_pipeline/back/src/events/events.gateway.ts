import { ConfigService } from '@nestjs/config';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({
  cors: process.env.ENABLE_CORS_ORIGIN.split(',')
    .map((url) => url.trim())
    .filter((url) => !!url) || ['http://localhost:8081'],
})
export class EventsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  constructor(private readonly configService: ConfigService) {}

  @WebSocketServer() server;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  afterInit(server: Server) {
    console.log('WebSocket Server init');
  }

  handleConnection(client: Socket) {
    console.log('Client connected ', client.id);
  }

  handleDisconnect(client: Socket) {
    console.log('Client disconnect ', client.id);
  }
}
