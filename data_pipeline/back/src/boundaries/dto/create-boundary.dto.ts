import {
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateBoundaryDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  singularName: string;

  @IsInt()
  level: number;

  @IsOptional()
  @IsInt()
  sectionId: number | null;

  @IsOptional()
  @IsInt()
  questionId: number | null;

  @IsBoolean()
  required: boolean;

  @IsString()
  boundaryNameAttribute: string;

  @IsOptional()
  @IsInt()
  divisaoAdministrativaMunId: number | null;
}
