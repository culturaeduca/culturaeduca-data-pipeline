import { Injectable, NotFoundException } from '@nestjs/common';

import { UpdateBoundaryDto, GetGeojsonRadiusQueryDto } from './dto';
import { PrismaService } from '@/prisma.service';
import { KnexService } from '@/knex/knex.service';
import { DB_SCHEMA } from '@/knex/knex.constants';
import { DefaultSectionTableColumnEnum } from '@/sections/sections.contants';
import { AttributeTypeEnum } from '@/attributes/attributes.constants';

@Injectable()
export class BoundariesService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly knexService: KnexService,
  ) {}

  async getGeojsonRadius(id: number, queryDto: GetGeojsonRadiusQueryDto) {
    const boundary = await this.prisma.boundary.findUnique({
      where: { id },
      include: {
        section: {
          include: {
            attributes: true,
          },
        },
        question: {
          include: {
            questionColumns: true,
          },
        },
      },
    });
    if (!boundary) {
      throw new NotFoundException('Boundary not found!');
    }
    const { lat, lng, radiusKm } = queryDto;
    const item = {
      id: boundary.id,
      name: boundary.name,
      singularName: boundary.singularName,
      level: boundary.level,
      required: boundary.required,
      boundaryNameAttribute: boundary.boundaryNameAttribute,
      divisaoAdministrativaMunId: boundary.divisaoAdministrativaMunId,
      ...(boundary.section
        ? {
            databaseSchema: DB_SCHEMA.DATASETS,
            tableName: boundary.section.tableName,
            columns: boundary.section.attributes.map((attribute) => ({
              name: attribute.name,
              type: attribute.type,
            })),
            columnGeom: DefaultSectionTableColumnEnum.GEOMETRY,
            columnGeog: DefaultSectionTableColumnEnum.GEOGRAPHY,
          }
        : {
            databaseSchema: DB_SCHEMA.QUESTIONS,
            tableName: boundary.question.viewName,
            columns: boundary.question.questionColumns.map(
              (questionColumn) => ({
                name: questionColumn.name,
                type: questionColumn.type,
              }),
            ),
            columnGeom: boundary.question.questionColumns.find(
              (questionColumn) =>
                questionColumn.type === AttributeTypeEnum.GEOMETRY,
            )?.name, // pega primeira coluna geometrica, TODO: REVER ISSO QUANDO AVANÇARMOS EM QUESTIONS
            columnGeog: boundary.question.questionColumns.find(
              (questionColumn) =>
                questionColumn.type === AttributeTypeEnum.GEOGRAPHY,
            )?.name, // pega primeira coluna geografica, TODO: REVER ISSO QUANDO AVANÇARMOS EM QUESTIONS
          }),
    };
    return this.knexService.getGeojsonRadius({ lat, lng, radiusKm, item });
  }

  async findOne(id: number) {
    const boundary = await this.prisma.boundary.findUnique({
      where: { id },
    });
    if (!boundary) {
      throw new NotFoundException('Boundary not found!');
    }
    return boundary;
  }

  update(id: number, dto: UpdateBoundaryDto, userId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      const boundary = await trxPrisma.boundary.findUnique({
        where: { id },
        include: {
          section: true,
          question: true,
        },
      });
      if (!boundary) {
        throw new NotFoundException('Boundary not found!');
      }

      await trxPrisma.boundary.update({
        where: { id: boundary.id },
        data: {
          ...(dto.name !== undefined ? { name: dto.name?.trim() || null } : {}),
          ...(dto.singularName !== undefined
            ? { singularName: dto.singularName?.trim() || null }
            : {}),
          ...(dto.level !== undefined ? { level: dto.level || null } : {}),
          ...(dto.required !== undefined ? { required: dto.required } : {}),
          ...(dto.boundaryNameAttribute !== undefined
            ? {
                boundaryNameAttribute: dto.boundaryNameAttribute || null,
              }
            : {}),
          ...(dto.divisaoAdministrativaMunId !== undefined
            ? {
                divisaoAdministrativaMunId:
                  dto.divisaoAdministrativaMunId || null,
              }
            : {}),
          ...(dto.sectionId !== undefined
            ? { sectionId: dto.sectionId || null }
            : {}),
          ...(dto.questionId !== undefined
            ? { questionId: dto.questionId || null }
            : {}),
          updatedBy: userId,
        },
      });

      // return updated boundary
      return trxPrisma.boundary.findUnique({ where: { id: boundary.id } });
    });
  }

  async remove(id: number) {
    const boundary = await this.prisma.boundary.findUnique({
      where: {
        id,
      },
    });
    if (!boundary) {
      throw new NotFoundException('Boundary not found');
    }
    return this.prisma.boundary.delete({
      where: {
        id,
      },
    });
  }
}
