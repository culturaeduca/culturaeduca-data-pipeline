import { Module } from '@nestjs/common';
import { BoundariesService } from './boundaries.service';
import { BoundariesController } from './boundaries.controller';
import { PrismaService } from '@/prisma.service';
import { KnexModule } from '@/knex/knex.module';

@Module({
  imports: [KnexModule],
  controllers: [BoundariesController],
  providers: [BoundariesService, PrismaService],
})
export class BoundariesModule {}
