import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { BoundariesService } from './boundaries.service';
import { UpdateBoundaryDto, GetGeojsonRadiusQueryDto } from './dto';
import { User } from '@/user.decorator';
import { Public } from '@/public.decorator';

@Controller('boundaries')
export class BoundariesController {
  constructor(private readonly boundariesService: BoundariesService) {}

  // Get
  @Public()
  @Get(':id/geojson_radius')
  getGeojsonRadius(
    @Param('id', ParseIntPipe) id: number,
    @Query() queryDto: GetGeojsonRadiusQueryDto,
  ) {
    return this.boundariesService.getGeojsonRadius(id, queryDto);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.boundariesService.findOne(id);
  }

  // Patch

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateBoundaryDto,
    @User('id') userId: number,
  ) {
    return this.boundariesService.update(id, dto, userId);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.boundariesService.remove(id);
  }
}
