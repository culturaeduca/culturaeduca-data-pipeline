import { ConfigService } from '@nestjs/config';
import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';

import { PrismaService } from '@/prisma.service';
import { VersionsController } from './versions.controller';
import { VersionsService } from './versions.service';
import { AttributesModule } from '@/attributes/attributes.module';
import { KnexModule } from '@/knex/knex.module';

@Module({
  imports: [
    forwardRef(() => AttributesModule),
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        ...configService.get<MongooseModuleFactoryOptions>('mongoose'),
      }),
      inject: [ConfigService],
    }),
    KnexModule,
  ],
  controllers: [VersionsController],
  providers: [PrismaService, VersionsService],
  exports: [VersionsService],
})
export class VersionsModule {}
