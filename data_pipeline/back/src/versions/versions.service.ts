import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { Attribute, DataIngestion, Section, Version } from '@prisma/client';

import { CreateSectionDto } from '@/sections/dto';
import {
  generateSectionTableDescription,
  generateSectionTableName,
} from '@/sections/sections.helper';
import { UpdateVersionDto } from './dto';
import { PrismaService } from '@/prisma.service';
import { validateStartEndDates } from '@/utils/versions-helper.utils';
import { KnexService } from '@/knex/knex.service';
import { DB_SCHEMA } from '@/knex/knex.constants';

@Injectable()
export class VersionsService {
  constructor(
    private readonly knexService: KnexService,
    private readonly prisma: PrismaService,
  ) {}

  async createSection(
    id: number,
    dto: CreateSectionDto,
    userId: number,
  ): Promise<Section> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find version
      const version = await trxPrisma.version.findUnique({
        where: { id },
        include: { dataset: true, sections: true },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      const sectionUri = dto?.uri?.trim()?.toLowerCase() || null;
      const sectionTitle = dto?.uri?.trim() || null;
      const tableName = generateSectionTableName(
        version.dataset.uri,
        version.uri,
        sectionUri,
      );
      const tableDescription = generateSectionTableDescription(
        version.dataset.title,
        version.title,
        sectionTitle,
      );
      // create section
      const section = await trxPrisma.section.create({
        data: {
          versionId: version.id,
          uri: dto?.uri?.trim()?.toLowerCase() || null,
          title: dto?.title?.trim() || null,
          description: dto?.description?.trim() || null,
          tableName,
          tableRowsCount: 0,
          default: version.sections.length === 0,
          createdBy: userId,
          updatedBy: userId,
        },
      });
      const datasetFormat = version.dataset.format;
      // create section table in the datasets schema
      await this.knexService.createDatasetsTable(
        tableName,
        tableDescription,
        datasetFormat,
      );
      return section;
    });
  }

  async findAttributes(id: number): Promise<Attribute[]> {
    return this.prisma.$transaction(async (trxPrisma) => {
      const version = await trxPrisma.version.findUnique({
        where: { id },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      return trxPrisma.attribute.findMany({
        where: { section: { versionId: version.id } },
        include: { section: true },
        orderBy: { series: { index: 'asc' } },
      });
    });
  }

  async findDataIngestions(id: number): Promise<DataIngestion[]> {
    return this.prisma.$transaction(async (trxPrisma) => {
      const version = await trxPrisma.version.findUnique({
        where: { id },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      return trxPrisma.dataIngestion.findMany({
        where: { section: { versionId: version.id } },
        include: { section: true },
      });
    });
  }

  async findOne(id: number): Promise<Version> {
    // find version
    const version = await this.prisma.version.findUnique({
      where: { id },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return version;
  }

  async findSections(id: number): Promise<Section[]> {
    const version = await this.prisma.version.findUnique({
      where: { id },
      include: {
        sections: {
          orderBy: {
            uri: 'asc',
          },
        },
      },
    });
    if (!version) {
      throw new NotFoundException('Version not found!');
    }
    return version.sections;
  }

  async remove(id: number): Promise<Version> {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find version
      const version = await trxPrisma.version.findUnique({
        where: { id },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      // remove version
      return trxPrisma.version.delete({
        where: { id },
      });
    });
  }

  async setDefault(id: number, userId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find version
      let version = await trxPrisma.version.findUnique({
        where: { id },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      // check if version is already default
      if (version.default) {
        throw new NotFoundException('Version is already default!');
      }
      // update version
      version = await trxPrisma.version.update({
        where: { id: version.id },
        data: { default: true, updatedBy: userId },
      });
      // update other versions to set default false
      await trxPrisma.version.updateMany({
        where: {
          id: { not: version.id },
          datasetId: version.datasetId,
          default: true,
        },
        data: { default: false },
      });
      return version;
    });
  }

  async update(id: number, dto: UpdateVersionDto, userId: number) {
    return this.prisma.$transaction(async (trxPrisma) => {
      // find version
      const version = await trxPrisma.version.findUnique({
        where: { id },
        include: {
          dataset: true,
          sections: true,
        },
      });
      if (!version) {
        throw new NotFoundException('Version not found!');
      }
      // validate start and end dates
      const errorMessageValidateStartEndDates = validateStartEndDates(dto);
      if (errorMessageValidateStartEndDates) {
        throw new BadRequestException(errorMessageValidateStartEndDates);
      }
      // update version
      const newUri = dto.uri?.trim()?.toLowerCase();
      await trxPrisma.version.update({
        where: { id: version.id },
        data: {
          ...(newUri !== undefined ? { uri: newUri || null } : {}),
          ...(dto.title !== undefined
            ? { title: dto.title?.trim() || null }
            : {}),
          ...(dto.description !== undefined
            ? { description: dto.description?.trim() || null }
            : {}),
          ...(dto.startDateYear !== undefined
            ? { startDateYear: dto?.startDateYear }
            : {}),
          ...(dto.startDateMonth !== undefined
            ? { startDateMonth: dto?.startDateMonth }
            : {}),
          ...(dto.startDateDay !== undefined
            ? { startDateDay: dto?.startDateDay }
            : {}),
          ...(dto.endDateYear !== undefined
            ? { endDateYear: dto?.endDateYear }
            : {}),
          ...(dto.endDateMonth !== undefined
            ? { endDateMonth: dto?.endDateMonth }
            : {}),
          ...(dto.endDateDay !== undefined
            ? { endDateDay: dto?.endDateDay }
            : {}),
          updatedBy: userId,
        },
      });
      // update sections table names
      if (!!newUri && newUri !== version.uri) {
        const itemsToUpdate: {
          databaseSchema: DB_SCHEMA;
          oldTableName: string;
          newTableName: string;
        }[] = [];
        for await (const section of version.sections) {
          const newTableName = generateSectionTableName(
            version.dataset.uri,
            newUri,
            section.uri,
          );
          itemsToUpdate.push({
            databaseSchema: DB_SCHEMA.DATASETS,
            oldTableName: section.tableName,
            newTableName: newTableName,
          });
          await trxPrisma.section.update({
            data: { tableName: newTableName },
            where: { id: section.id },
          });
        }
        await this.knexService.renameTables(itemsToUpdate);
      }
      // return updated version
      return trxPrisma.version.findUnique({ where: { id: version.id } });
    });
  }
}
