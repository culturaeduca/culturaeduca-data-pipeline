import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';

import { CreateSectionDto } from '@/sections/dto';
import { UpdateVersionDto } from './dto';
import { VersionsService } from './versions.service';
import { User } from '@/user.decorator';

@Controller('versions')
export class VersionsController {
  constructor(private readonly versionsService: VersionsService) {}

  // Get

  @Get(':id/attributes')
  findAttributes(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findAttributes(id);
  }

  @Get(':id/data_ingestions')
  findDataIngestions(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findDataIngestions(id);
  }

  @Get(':id/sections')
  findSections(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findSections(id);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.findOne(id);
  }

  // Post

  @Post(':id/sections')
  createSection(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateSectionDto,
    @User('id') userId: number,
  ) {
    return this.versionsService.createSection(id, dto, userId);
  }

  // Patch

  @Patch(':id/set_default')
  setDefault(
    @Param('id', ParseIntPipe) id: number,
    @User('id') userId: number,
  ) {
    return this.versionsService.setDefault(id, userId);
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateVersionDto,
    @User('id') userId: number,
  ) {
    return this.versionsService.update(id, dto, userId);
  }

  // Delete

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.versionsService.remove(id);
  }
}
