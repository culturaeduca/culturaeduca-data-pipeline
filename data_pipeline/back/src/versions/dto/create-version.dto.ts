import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateVersionDto {
  @IsString()
  @IsNotEmpty()
  uri: string;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsOptional()
  @IsString()
  description: string | null;

  @IsInt()
  startDateYear: number;

  @IsOptional()
  @IsInt()
  startDateMonth: number | null;

  @IsOptional()
  @IsInt()
  startDateDay: number | null;

  @IsOptional()
  @IsInt()
  endDateYear: number | null;

  @IsOptional()
  @IsInt()
  endDateMonth: number | null;

  @IsOptional()
  @IsInt()
  endDateDay: number | null;
}
