import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { verify } from 'argon2';
import { User } from '@prisma/client';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findForAuth(username);
    if (!user || !(await verify(user.password, password))) {
      throw new UnauthorizedException();
    }
    delete user.password;
    return user;
  }

  async login(user: User) {
    const payload = { sub: user.id };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async getProfile(payload: { sub: number }) {
    const user = await this.usersService.findOne(payload.sub);
    const { id, name, email, username } = user;
    return { id, name, email, username };
  }
}
