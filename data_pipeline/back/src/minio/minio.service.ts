import { Inject, Injectable } from '@nestjs/common';
import * as Minio from 'minio';
import * as fs from 'fs/promises';
import { randomBytes } from 'crypto';

import { ConfigInjectionToken } from './config.interface';
import type { MinioModuleConfig } from './config.interface';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MinioService {
  private minioClient: Minio.Client;
  private bucketName: string;

  constructor(
    @Inject(ConfigInjectionToken) private config: MinioModuleConfig,
    private readonly configService: ConfigService,
  ) {
    this.bucketName = this.configService.get<string>('minio.bucketName');
    this.minioClient = new Minio.Client({
      endPoint: config.endPoint,
      port: config.port,
      useSSL: config.useSSL,
      accessKey: config.accessKey,
      secretKey: config.secretKey,
    });
  }

  async listBuckets() {
    return this.minioClient.listBuckets();
  }

  async fPutObject(objectName: string, file: Express.Multer.File) {
    const metadata = {
      originalname: file.originalname,
    };
    const object = await this.minioClient.fPutObject(
      this.bucketName,
      objectName,
      file.path,
      metadata,
    );
    fs.unlink(file.path);
    return object;
  }

  async fGetObject(objectName: string) {
    const randomHash = randomBytes(16).toString('hex');
    const tmpPath = `static/tmp/${randomHash}`;
    await this.minioClient.fGetObject(this.bucketName, objectName, tmpPath);
    return tmpPath;
  }

  async statObject(objectName: string) {
    return this.minioClient.statObject(this.bucketName, objectName);
  }

  async removeObject(objectName: string) {
    return this.minioClient.removeObject(this.bucketName, objectName);
  }

  async getPresignUploadUrl(objectName: string) {
    return new Promise((resolve, reject) => {
      this.minioClient.presignedPutObject(
        this.bucketName,
        objectName,
        (err, url) => {
          if (err) reject(err);
          resolve(url);
        },
      );
    });
  }
}
