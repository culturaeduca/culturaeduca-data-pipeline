import { ModuleMetadata } from '@nestjs/common';

export const ConfigInjectionToken = 'ConfigInjectionToken';

export type MinioModuleConfig = {
  endPoint: string;
  port: number;
  useSSL: boolean;
  accessKey: string;
  secretKey: string;
};

export interface MinioModuleAsyncConfig
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => Promise<MinioModuleConfig> | MinioModuleConfig;
  inject?: any[];
}
