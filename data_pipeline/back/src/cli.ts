import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { UsersService } from './users/users.service';
import { program } from 'commander';
import { max } from 'lodash';

async function bootstrap() {
  // init
  program
    .name('ce_prot-cli')
    .description('CLI to some utilities')
    .version('0.0.1');

  // create-user
  program
    .command('create-user')
    .description('Create new user')
    .option('-n, --name <string>', 'Name')
    .option('-e, --email <string>', 'Email')
    .option('-u, --username <string>', 'Username')
    .option('-p, --password <string>', 'Password')
    .action(async (args) => {
      const app = await NestFactory.createApplicationContext(AppModule, {
        logger: false,
      });
      const { name, email, username, password } = args;
      const user = await app
        .get(UsersService)
        .create({ name, email, username, password });
      console.log(`User #${user.id} created with success`);
      await app.close();
      process.exit(0);
    });

  // update-user
  program
    .command('update-user')
    .description('Update user')
    .option('-i, --id <string>', 'User ID')
    .option('-n, --name <string>', 'New name')
    .option('-e, --email <string>', 'New email')
    .option('-u, --username <string>', 'New username')
    .option('-p, --password <string>', 'New password')
    .action(async (args) => {
      const app = await NestFactory.createApplicationContext(AppModule, {
        logger: false,
      });
      const { id, name, email, username, password } = args;
      const user = await app.get(UsersService).update(
        +id,
        {
          ...(name ? { name } : {}),
          ...(email ? { email } : {}),
          ...(username ? { username } : {}),
          ...(password ? { password } : {}),
        },
        true,
      );
      console.log(`User #${user.id} updated with success`);
      await app.close();
      process.exit(0);
    });

  // remove-user
  program
    .command('remove-user')
    .description('Remove user')
    .option('-i, --id <string>', 'User ID')
    .action(async (args) => {
      const app = await NestFactory.createApplicationContext(AppModule, {
        logger: false,
      });
      const { id } = args;
      const user = await app.get(UsersService).remove(+id);
      console.log(`User #${user.id} removed with success`);
      await app.close();
      process.exit(0);
    });

  // list-users
  program
    .command('list-users')
    .description('List users')
    .action(async () => {
      const app = await NestFactory.createApplicationContext(AppModule, {
        logger: false,
      });
      const users = await app.get(UsersService).findAll();
      const maxLength = {
        id: max(users.map((user) => user.id.toString().length)),
        name: max(users.map((user) => user.name.length)),
        email: max(users.map((user) => user.email.length)),
        username: max(users.map((user) => user.username.length)),
      };
      console.log();
      console.log(
        ` ${'ID'.padEnd(maxLength.id > 2 ? maxLength.id : 2, ' ')} `,
        ` ${'Name'.padEnd(maxLength.name > 4 ? maxLength.name : 4, ' ')} `,
        ` ${'Email'.padEnd(maxLength.email > 4 ? maxLength.email : 4, ' ')} `,
        ` ${'Username'.padEnd(
          maxLength.username > 8 ? maxLength.username : 8,
          ' ',
        )} `,
        ` ${'Created at'.padEnd(24, ' ')} `,
        ` ${'Updated at'.padEnd(24, ' ')} `,
      );
      console.log(
        ''.padEnd(
          (maxLength.id > 2 ? maxLength.id : 2) +
            maxLength.name +
            maxLength.email +
            maxLength.username +
            62,
          '-',
        ),
      );
      users.forEach((user) => {
        console.log(
          ` ${String(user.id).padEnd(
            maxLength.id > 2 ? maxLength.id : 2,
            ' ',
          )} `,
          ` ${String(user.name).padEnd(maxLength.name, ' ')} `,
          ` ${String(user.email).padEnd(maxLength.email, ' ')} `,
          ` ${String(user.username).padEnd(maxLength.username, ' ')} `,
          ` ${user.createdAt.toISOString()} `,
          ` ${user.updatedAt.toISOString()} `,
        );
      });
      console.log();
      await app.close();
      process.exit(0);
    });

  // parse
  try {
    program.parse(process.argv);
  } catch (err) {
    process.exit(1);
  }
}

try {
  bootstrap();
} catch (err) {
  console.error(err);
  process.exit(1);
}
