#!/bin/sh

# pega versão do package.json

VERSION=$(awk -F: '/"version":/ {print substr($2, 3, length($2)-4)}' package.json)

if [ -z "$VERSION" ]
  then
    echo $VERSION
    echo "Por favor, verifique se a versão em package.json está especificada"
    exit 1
fi

IMAGE="<Image name>"
REPO="<AWS ECR repository URI>"
PROFILE=default

# build
docker build . -t $IMAGE:$VERSION

# atualiza tags
docker tag $IMAGE:$VERSION $IMAGE:latest
docker tag $IMAGE:$VERSION $REPO:$VERSION
docker tag $IMAGE:latest $REPO:latest

# verifica login
aws ecr get-login-password --region sa-east-1 --profile $PROFILE | docker login --username AWS --password-stdin $REPO

# envia nova imagem
docker push $REPO:$VERSION
docker push $REPO:latest
