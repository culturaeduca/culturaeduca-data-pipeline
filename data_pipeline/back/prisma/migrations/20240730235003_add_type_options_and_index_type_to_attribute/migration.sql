-- CreateEnum
CREATE TYPE "AttributeIndexType" AS ENUM ('BTREE', 'HASH', 'GIN', 'GIST', 'SPGIST', 'BRIN');

-- AlterTable
ALTER TABLE "attribute" ADD COLUMN     "index_type" "AttributeIndexType",
ADD COLUMN     "type_options" JSONB;
