-- CreateEnum
CREATE TYPE "DatasetFormat" AS ENUM ('GEOSPATIAL_LINE', 'GEOSPATIAL_POINT', 'GEOSPATIAL_POLYGON', 'NON_GEOSPATIAL');

-- CreateEnum
CREATE TYPE "AttributeSeriesRelationshipType" AS ENUM ('ONE_TO_ONE', 'MANY_TO_ONE');

-- CreateEnum
CREATE TYPE "DataIngestionFileType" AS ENUM ('CSV', 'GEOJSON', 'JSON', 'SHAPEFILE');

-- CreateEnum
CREATE TYPE "DataIngestionLogStatus" AS ENUM ('CREATED', 'FILE_UPLOAD_STARTED', 'FILE_UPLOAD_SUCCESS', 'FILE_UPLOAD_FAILURE', 'FILE_PARSING_STARTED', 'FILE_PARSING_SUCCESS', 'FILE_PARSING_FAILURE', 'DATA_LOADING_STARTED', 'DATA_LOADING_SUCCESS', 'DATA_LOADING_FAILURE');

-- CreateEnum
CREATE TYPE "DataIngestionGeoType" AS ENUM ('LAT_LNG', 'GEOJSON', 'WKT');

-- CreateTable
CREATE TABLE "dataset" (
    "id" SERIAL NOT NULL,
    "uri" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "format" "DatasetFormat" NOT NULL,
    "owner" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "dataset_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "version" (
    "id" SERIAL NOT NULL,
    "dataset_id" INTEGER NOT NULL,
    "uri" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "start_date_year" INTEGER NOT NULL,
    "start_date_month" INTEGER,
    "start_date_day" INTEGER,
    "end_date_year" INTEGER,
    "end_date_month" INTEGER,
    "end_date_day" INTEGER,
    "default" BOOLEAN NOT NULL DEFAULT false,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "version_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "section" (
    "id" SERIAL NOT NULL,
    "version_id" INTEGER NOT NULL,
    "uri" TEXT,
    "title" TEXT,
    "description" TEXT,
    "table_name" TEXT,
    "table_rows_count" INTEGER NOT NULL DEFAULT 0,
    "default" BOOLEAN NOT NULL DEFAULT false,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "section_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "attribute" (
    "id" SERIAL NOT NULL,
    "section_id" INTEGER NOT NULL,
    "series_id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "title" TEXT,
    "description" TEXT,
    "type" TEXT NOT NULL,
    "optional" BOOLEAN NOT NULL DEFAULT false,
    "indexable" BOOLEAN NOT NULL DEFAULT false,
    "unique" BOOLEAN NOT NULL DEFAULT false,
    "primary" BOOLEAN NOT NULL DEFAULT false,
    "sensitive_data" BOOLEAN NOT NULL DEFAULT false,
    "default_value" TEXT,
    "unit" TEXT,
    "decode_value" JSONB,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "attribute_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "attribute_series" (
    "id" SERIAL NOT NULL,
    "dataset_id" INTEGER NOT NULL,
    "index" INTEGER NOT NULL,

    CONSTRAINT "attribute_series_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "attribute_relationship" (
    "id" SERIAL NOT NULL,
    "foreign_key_attribute_series_id" INTEGER NOT NULL,
    "reference_attribute_series_id" INTEGER NOT NULL,
    "type" "AttributeSeriesRelationshipType" NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "attribute_relationship_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "question" (
    "id" SERIAL NOT NULL,
    "uri" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "materialized" BOOLEAN NOT NULL DEFAULT false,
    "view_name" TEXT NOT NULL,
    "raw_query" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "question_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "question_table" (
    "id" SERIAL NOT NULL,
    "question_id" INTEGER NOT NULL,
    "dataset_id" INTEGER NOT NULL,
    "version_id" INTEGER NOT NULL,
    "section_id" INTEGER NOT NULL,
    "query_key" INTEGER NOT NULL,
    "alias_index" INTEGER NOT NULL,
    "join_type" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "question_table_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "question_table_join" (
    "id" SERIAL NOT NULL,
    "left_question_table_id" INTEGER NOT NULL,
    "left_attribute_series_id" INTEGER NOT NULL,
    "right_question_table_id" INTEGER NOT NULL,
    "right_attribute_series_id" INTEGER NOT NULL,
    "query_key" INTEGER NOT NULL,
    "operator" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "question_table_join_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "question_column" (
    "id" SERIAL NOT NULL,
    "question_id" INTEGER NOT NULL,
    "question_table_id" INTEGER,
    "attribute_series_id" INTEGER,
    "query_key" INTEGER NOT NULL,
    "expression" TEXT,
    "name" TEXT,
    "description" TEXT,
    "type" TEXT,
    "optional" BOOLEAN,
    "indexable" BOOLEAN,
    "unique" BOOLEAN,
    "primary" BOOLEAN,
    "sensitive_data" BOOLEAN,
    "default_value" TEXT,
    "unit" TEXT,
    "decode_value" JSONB,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "question_column_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion" (
    "id" SERIAL NOT NULL,
    "section_id" INTEGER NOT NULL,
    "file_type" "DataIngestionFileType" NOT NULL,
    "file_path" TEXT,
    "collection_name" TEXT,
    "collection_schema" JSONB,

    CONSTRAINT "data_ingestion_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion_log" (
    "id" SERIAL NOT NULL,
    "data_ingestion_id" INTEGER NOT NULL,
    "status" "DataIngestionLogStatus" NOT NULL,
    "message" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "data_ingestion_log_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion_column" (
    "id" SERIAL NOT NULL,
    "attribute_id" INTEGER NOT NULL,
    "data_ingestion_id" INTEGER NOT NULL,
    "property_name" TEXT NOT NULL,

    CONSTRAINT "data_ingestion_column_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "data_ingestion_geo" (
    "id" SERIAL NOT NULL,
    "attribute_id" INTEGER NOT NULL,
    "data_ingestion_id" INTEGER NOT NULL,
    "type" "DataIngestionGeoType" NOT NULL,
    "property_name_lat" TEXT,
    "property_name_lng" TEXT,
    "property_name_wkt" TEXT,

    CONSTRAINT "data_ingestion_geo_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "dataset_uri_key" ON "dataset"("uri");

-- CreateIndex
CREATE UNIQUE INDEX "dataset_title_key" ON "dataset"("title");

-- CreateIndex
CREATE UNIQUE INDEX "version_uri_dataset_id_key" ON "version"("uri", "dataset_id");

-- CreateIndex
CREATE UNIQUE INDEX "version_title_dataset_id_key" ON "version"("title", "dataset_id");

-- CreateIndex
CREATE UNIQUE INDEX "section_table_name_key" ON "section"("table_name");

-- CreateIndex
CREATE UNIQUE INDEX "section_uri_version_id_key" ON "section"("uri", "version_id");

-- CreateIndex
CREATE UNIQUE INDEX "section_title_version_id_key" ON "section"("title", "version_id");

-- CreateIndex
CREATE UNIQUE INDEX "attribute_series_id_section_id_key" ON "attribute"("series_id", "section_id");

-- CreateIndex
CREATE UNIQUE INDEX "attribute_name_section_id_key" ON "attribute"("name", "section_id");

-- CreateIndex
CREATE UNIQUE INDEX "attribute_relationship_foreign_key_attribute_series_id_refe_key" ON "attribute_relationship"("foreign_key_attribute_series_id", "reference_attribute_series_id");

-- CreateIndex
CREATE UNIQUE INDEX "question_uri_key" ON "question"("uri");

-- CreateIndex
CREATE UNIQUE INDEX "question_title_key" ON "question"("title");

-- CreateIndex
CREATE UNIQUE INDEX "question_view_name_key" ON "question"("view_name");

-- CreateIndex
CREATE UNIQUE INDEX "user_email_key" ON "user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "user_username_key" ON "user"("username");

-- AddForeignKey
ALTER TABLE "dataset" ADD CONSTRAINT "dataset_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "dataset" ADD CONSTRAINT "dataset_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "version" ADD CONSTRAINT "version_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "version" ADD CONSTRAINT "version_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "version" ADD CONSTRAINT "version_dataset_id_fkey" FOREIGN KEY ("dataset_id") REFERENCES "dataset"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "section" ADD CONSTRAINT "section_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "section" ADD CONSTRAINT "section_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "section" ADD CONSTRAINT "section_version_id_fkey" FOREIGN KEY ("version_id") REFERENCES "version"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute" ADD CONSTRAINT "attribute_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute" ADD CONSTRAINT "attribute_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute" ADD CONSTRAINT "attribute_section_id_fkey" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute" ADD CONSTRAINT "attribute_series_id_fkey" FOREIGN KEY ("series_id") REFERENCES "attribute_series"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute_series" ADD CONSTRAINT "attribute_series_dataset_id_fkey" FOREIGN KEY ("dataset_id") REFERENCES "dataset"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute_relationship" ADD CONSTRAINT "attribute_relationship_foreign_key_attribute_series_id_fkey" FOREIGN KEY ("foreign_key_attribute_series_id") REFERENCES "attribute_series"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "attribute_relationship" ADD CONSTRAINT "attribute_relationship_reference_attribute_series_id_fkey" FOREIGN KEY ("reference_attribute_series_id") REFERENCES "attribute_series"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question" ADD CONSTRAINT "question_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question" ADD CONSTRAINT "question_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table" ADD CONSTRAINT "question_table_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table" ADD CONSTRAINT "question_table_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table" ADD CONSTRAINT "question_table_question_id_fkey" FOREIGN KEY ("question_id") REFERENCES "question"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table" ADD CONSTRAINT "question_table_dataset_id_fkey" FOREIGN KEY ("dataset_id") REFERENCES "dataset"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table" ADD CONSTRAINT "question_table_version_id_fkey" FOREIGN KEY ("version_id") REFERENCES "version"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table" ADD CONSTRAINT "question_table_section_id_fkey" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table_join" ADD CONSTRAINT "question_table_join_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table_join" ADD CONSTRAINT "question_table_join_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table_join" ADD CONSTRAINT "question_table_join_left_question_table_id_fkey" FOREIGN KEY ("left_question_table_id") REFERENCES "question_table"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table_join" ADD CONSTRAINT "question_table_join_left_attribute_series_id_fkey" FOREIGN KEY ("left_attribute_series_id") REFERENCES "attribute_series"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table_join" ADD CONSTRAINT "question_table_join_right_question_table_id_fkey" FOREIGN KEY ("right_question_table_id") REFERENCES "question_table"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_table_join" ADD CONSTRAINT "question_table_join_right_attribute_series_id_fkey" FOREIGN KEY ("right_attribute_series_id") REFERENCES "attribute_series"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_column" ADD CONSTRAINT "question_column_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_column" ADD CONSTRAINT "question_column_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_column" ADD CONSTRAINT "question_column_question_id_fkey" FOREIGN KEY ("question_id") REFERENCES "question"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_column" ADD CONSTRAINT "question_column_question_table_id_fkey" FOREIGN KEY ("question_table_id") REFERENCES "question_table"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "question_column" ADD CONSTRAINT "question_column_attribute_series_id_fkey" FOREIGN KEY ("attribute_series_id") REFERENCES "attribute_series"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion" ADD CONSTRAINT "data_ingestion_section_id_fkey" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_log" ADD CONSTRAINT "data_ingestion_log_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_log" ADD CONSTRAINT "data_ingestion_log_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_log" ADD CONSTRAINT "data_ingestion_log_data_ingestion_id_fkey" FOREIGN KEY ("data_ingestion_id") REFERENCES "data_ingestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_column" ADD CONSTRAINT "data_ingestion_column_attribute_id_fkey" FOREIGN KEY ("attribute_id") REFERENCES "attribute"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_column" ADD CONSTRAINT "data_ingestion_column_data_ingestion_id_fkey" FOREIGN KEY ("data_ingestion_id") REFERENCES "data_ingestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_geo" ADD CONSTRAINT "data_ingestion_geo_attribute_id_fkey" FOREIGN KEY ("attribute_id") REFERENCES "attribute"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "data_ingestion_geo" ADD CONSTRAINT "data_ingestion_geo_data_ingestion_id_fkey" FOREIGN KEY ("data_ingestion_id") REFERENCES "data_ingestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;
