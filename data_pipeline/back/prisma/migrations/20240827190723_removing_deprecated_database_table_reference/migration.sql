/*
  Warnings:

  - You are about to drop the column `database_table_reference_id` on the `boundary` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "boundary" DROP COLUMN "database_table_reference_id";
