/*
  Warnings:

  - Added the required column `database_table_reference_id` to the `boundary` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "boundary" ADD COLUMN     "database_table_reference_id" INTEGER NOT NULL,
ADD COLUMN     "question_id" INTEGER,
ADD COLUMN     "section_id" INTEGER;

-- CreateTable
CREATE TABLE "boundary_link" (
    "id" SERIAL NOT NULL,
    "boundaryParentId" INTEGER NOT NULL,
    "attributeParentId" INTEGER,
    "questionColumnParentId" INTEGER,
    "boundaryChildId" INTEGER NOT NULL,
    "attributeChildId" INTEGER,
    "questionColumnChildId" INTEGER,

    CONSTRAINT "boundary_link_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "boundary" ADD CONSTRAINT "boundary_section_id_fkey" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary" ADD CONSTRAINT "boundary_question_id_fkey" FOREIGN KEY ("question_id") REFERENCES "question"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_link" ADD CONSTRAINT "boundary_link_boundaryParentId_fkey" FOREIGN KEY ("boundaryParentId") REFERENCES "boundary"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_link" ADD CONSTRAINT "boundary_link_boundaryChildId_fkey" FOREIGN KEY ("boundaryChildId") REFERENCES "boundary"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_link" ADD CONSTRAINT "boundary_link_attributeParentId_fkey" FOREIGN KEY ("attributeParentId") REFERENCES "attribute"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_link" ADD CONSTRAINT "boundary_link_attributeChildId_fkey" FOREIGN KEY ("attributeChildId") REFERENCES "attribute"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_link" ADD CONSTRAINT "boundary_link_questionColumnParentId_fkey" FOREIGN KEY ("questionColumnParentId") REFERENCES "question_column"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_link" ADD CONSTRAINT "boundary_link_questionColumnChildId_fkey" FOREIGN KEY ("questionColumnChildId") REFERENCES "question_column"("id") ON DELETE CASCADE ON UPDATE CASCADE;
