-- CreateTable
CREATE TABLE "boundary_hierarchy" (
    "uri" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "boundary_hierarchy_pkey" PRIMARY KEY ("uri")
);

-- CreateTable
CREATE TABLE "boundary" (
    "id" SERIAL NOT NULL,
    "boundary_hierarchy_uri" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "level" INTEGER NOT NULL,
    "required" BOOLEAN NOT NULL DEFAULT true,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by" INTEGER NOT NULL,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "updated_by" INTEGER NOT NULL,

    CONSTRAINT "boundary_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "boundary_hierarchy" ADD CONSTRAINT "boundary_hierarchy_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary_hierarchy" ADD CONSTRAINT "boundary_hierarchy_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary" ADD CONSTRAINT "boundary_created_by_fkey" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary" ADD CONSTRAINT "boundary_updated_by_fkey" FOREIGN KEY ("updated_by") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "boundary" ADD CONSTRAINT "boundary_boundary_hierarchy_uri_fkey" FOREIGN KEY ("boundary_hierarchy_uri") REFERENCES "boundary_hierarchy"("uri") ON DELETE CASCADE ON UPDATE CASCADE;
