/*
  Warnings:

  - You are about to drop the `data_ingestion_geo` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "data_ingestion_geo" DROP CONSTRAINT "data_ingestion_geo_attribute_id_fkey";

-- DropForeignKey
ALTER TABLE "data_ingestion_geo" DROP CONSTRAINT "data_ingestion_geo_data_ingestion_id_fkey";

-- AlterTable
ALTER TABLE "data_ingestion" ADD COLUMN     "geo_property_name_lat" TEXT,
ADD COLUMN     "geo_property_name_lng" TEXT,
ADD COLUMN     "geo_property_name_wkt" TEXT,
ADD COLUMN     "geo_type" "DataIngestionGeoType";

-- DropTable
DROP TABLE "data_ingestion_geo";
