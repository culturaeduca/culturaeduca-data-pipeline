#!/bin/bash

cd /data_pipeline/back/
yarn install
npx prisma migrate dev
yarn cli:dev create-user -n "admin" -e admin@mail.com -p 123admin -u admin
NODE_TLS_REJECT_UNAUTHORIZED=0 yarn start:dev --host
